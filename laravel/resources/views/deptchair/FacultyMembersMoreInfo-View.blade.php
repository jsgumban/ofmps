{{-- CATEGORY1 MODAL --}}
<div class="modal fade modal-success" id="ModalMInfo-modal-1" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">


        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="displayAction">COMPLETE INFORMATION</h4>
            </div>

            <div class="modal-body">

                <form action="" method="POST" class="form-horizontal" id="submitUpdatedProfileButton">
                    {!! csrf_field() !!}
                    <div class="form-group" id="positionSelectDiv">
                        <label class="col-sm-3 control-label">User Type: </label>

                        <div class="col-sm-9 ">
                            {{-- <input type="text" class="form-control input-sm"  id="gender"> --}}
                            <select class="form-control input-sm" name="position" id="positionSelect" required
                                    onchange="hideShowInputs()">
                                <option value=0> Faculty User</option>
                                <option value=5> System Administrator User</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">First Name* : </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" name="first_name" id="first_name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Middle Name* : </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" name="middle_name" id="middle_name"
                                   required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Last Name* : </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" name="last_name" id="last_name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Gender* : </label>

                        <div class="col-sm-9">
                            {{-- <input type="text" class="form-control input-sm"  id="gender"> --}}
                            <select id="gender" class="form-control input-sm" name="gender" required>
                                <option value="Male"> Male</option>
                                <option value="Female"> Female</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Employee Code* : </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" name="employee_code" id="employee_code"
                                   title='Must be all numbers' pattern='\d*' maxlength=9 minlength=9 id="employee_code"
                                   required>
                        </div>
                    </div>

                    <div class="form-group" id="departmentDiv">
                        <label class="col-sm-3 control-label">Department: </label>

                        <div class="col-sm-9">
                            <select class="form-control input-sm"
                                    name="department_belongs" id="department">
                                @foreach($departments as $department)
                                    <option value="{{ $department->department_name }}">  {!! $department->department_name !!}</option>
                                @endforeach
                            </select>
                            {{-- <input type="text" class="form-control input-sm"  id="department"> --}}
                        </div>
                    </div>

                    <div class="form-group" id="positionDiv">
                        <label class="col-sm-3 control-label">Position: </label>

                        <div class="col-sm-9">

                            <input type="text" class="form-control input-sm" id="position" disabled="">
                        </div>
                    </div>

                    <div class="form-group" id="rankDiv">
                        <label class="col-sm-3 control-label">Rank:</label>

                        <div class="col-sm-9">
                            <select id="rank" class="form-control input-sm" name="rank"
                                    onchange="activateStep()">
                                <option value="None"> None</option>
                                <option value="Instructor"> Instructor</option>
                                <option value="Assistant Professor"> Assistant Professor</option>
                                <option value="Associate Professor"> Associate Professor</option>
                                <option value="Professor">Professor</option>
                            </select>
                            {{-- <input type="text" class="form-control input-sm"  id="rank"> --}}
                        </div>
                    </div>

                    <div class="form-group" id="stepDiv">
                        <label class="col-sm-3 control-label">Step:</label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="step" name="step" required disabled="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Birthday* : </label>

                        <div class="col-sm-9">
                            <input type="date" class="form-control input-sm" id="birthday" name="birthday" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Address: </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="address" name="address">
                        </div>
                    </div>

                    @if(Auth::user()->position!=5)
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Last Promotion </label>

                            <div class="col-sm-9">
                                <li class="list-group-item" id="lastPromotion"></li>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Submitted Forms: </label>

                            <div class="col-sm-9">
                                <li class="list-group-item" id="submittedForms"></li>
                            </div>
                        </div>

                @endif


            </div>
            <div class="modal-footer" id="submitButton">
                <button type="submit" class="btn btn-success">Update</button>
            </div>

            <div class="modal-footer" id="addNewButton">
                <button type="submit" class="btn btn-info">Add New User</button>
            </div>
            </form>

        </div>
    </div>
</div>

<script>
    function activateStep() {
        var x = document.getElementById("rank").value;
        if (x != "None") {
            // document.getElementById("demo").innerHTML = "You selected: " + x;
            document.getElementById("step").disabled = false;
        } else if (x == "None") {
            document.getElementById("step").disabled = true;
            document.getElementById("step").value = "";
        }
    }

    function hideShowInputs() {
        var x = document.getElementById("positionSelect").value;
        if (x == 5) {
            document.getElementById("rankDiv").style.display = "none";
            document.getElementById("stepDiv").style.display = "none";
            document.getElementById("departmentDiv").style.display = "none";
        } else {
            document.getElementById("rankDiv").style.display = "block";
            document.getElementById("stepDiv").style.display = "block";
            document.getElementById("departmentDiv").style.display = "block";
        }
    }
</script>