<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MpromotionForm extends Model
{
    protected $table = 'mpromotion_forms';
    public $timestamps = false;

}
