@extends('app')
@section('title', 'OFMPS | UPDATE MY PASSWORD')
@section('content')

    @if(Auth::user())
        @if((Auth::user()->position)==5)
            <?php $preLink = FOLDERNAME . '/admin/'; ?>
            <?php $currentUser = 'admin'; ?>
        @elseif((Auth::user()->position)==4)
            <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
            <?php $currentUser = 'chancellor'; ?>
        @elseif((Auth::user()->position)==3)
            <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
            <?php $currentUser = 'vchancellor'; ?>
        @elseif((Auth::user()->position)==2)
            <?php $preLink = FOLDERNAME . '/dean/'; ?>
            <?php $currentUser = 'dean'; ?>
        @elseif((Auth::user()->position)==1)
            <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
            <?php $currentUser = 'deptchair'; ?>
        @elseif((Auth::user()->position)==0)
            <?php $preLink = FOLDERNAME . '/faculty/'; ?>
            <?php $currentUser = 'faculty'; ?>
        @endif
    @endif



    <div class="page animsition"><br>

        <div class="page-content col-md-6 col-md-offset-3">
            <div class="panel panel-bordered">


                {{-- HEADER --}}
                <header class="panel-heading">
                    <h3 class="panel-title">
                        <b> UPDATE MY PASSWORD </b>
                    </h3>
                </header>
                <div class="panel-body">


                    {{-- NOTIFICATION --}}
                    @if(!empty(session('notification')))
                        <?php $notif = session('notification');?>
                        @if(strpos($notif,'SUCCESS')===false)
                            <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @else
                            <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @endif
                    @endif



                    {{-- FORM --}}
                    <form action="{!! $preLink.'profile/SaveUpdateMyPassword' !!}"
                          method="POST">
                        {!! csrf_field() !!}
                        <div class="row row-lg">


                            {{-- INPUT CURRENT PASSWORD --}}
                            <div class="col-sm-12 form-horizontal">
                                <div class="form-group">
                                    <label class="col-lg-12 col-sm-3 control-label" style="text-align: left;">Current
                                        Password
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-lg-12 col-sm-9">
                                        <div class="input-group">
                              <span class="input-group-addon">
                                 <i class="icon wb-lock" aria-hidden="true"></i>
                              </span>
                                            <input type="password" class="form-control" name="currentpass" required=""
                                                   data-fv-field="password">
                                        </div>


                                        <small class="help-block" data-fv-validator="notEmpty" data-fv-for="password"
                                               data-fv-result="VALID" style="display: none;">The password is required
                                        </small>
                                        <small class="help-block" data-fv-validator="stringLength"
                                               data-fv-for="password" data-fv-result="VALID" style="display: none;">
                                            Please enter more than 6 characters
                                        </small>
                                    </div>
                                </div>
                            </div>


                            {{-- INPUT NEW PASSWORD --}}
                            <div class="col-lg-6 form-horizontal">
                                <div class="form-group">
                                    <label class="col-lg-12 col-sm-3 control-label" style="text-align: left;">New
                                        Password
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-lg-12 col-sm-9">
                                        <div class="input-group">
                              <span class="input-group-addon">
                                 <i class="icon wb-lock" aria-hidden="true"></i>
                              </span>
                                            <input type="password" class="form-control" name="newpass" required=""
                                                   data-fv-field="password" data-fv-notempty="true"
                                                   data-fv-identical="true" data-fv-identical-field="confirm"
                                                   data-fv-identical-message="'Confirm Password' and 'New Password' do not match">
                                        </div>

                                        <small class="help-block" data-fv-validator="stringLength"
                                               data-fv-for="password" data-fv-result="VALID" style="display: none;">
                                            Please enter more than 6 characters
                                        </small>
                                    </div>
                                </div>
                            </div>


                            {{-- INPUT CONFIRM NEW PASSWORD --}}
                            <div class="col-lg-6 form-horizontal">
                                <div class="form-group">
                                    <label class="col-lg-12 col-sm-3 control-label" style="text-align: left;">Confirm
                                        New Password
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-lg-12 col-sm-9">
                                        <div class="input-group">
                              <span class="input-group-addon">
                                 <i class="icon wb-lock" aria-hidden="true"></i>
                              </span>
                                            <input type="password" class="form-control" name="confirm" required=""
                                                   data-fv-field="password" data-fv-notempty="true"
                                                   data-fv-identical="true" data-fv-identical-field="newpass"
                                                   data-fv-identical-message="'Confirm Password' and 'New Password' do not match">
                                        </div>
                                        <small class="help-block" data-fv-validator="stringLength"
                                               data-fv-for="password" data-fv-result="VALID" style="display: none;">
                                            Please enter more than 6 characters
                                        </small>
                                    </div>
                                </div>
                            </div>


                            {{-- SAVE CHANGES BUTTON --}}
                            <div class="form-group col-lg-12 text-right padding-top-m"><br>
                                <button type="submit" class="btn btn-success" id="validateButton1"
                                        data-style="expand-right" data-title="Changes have been saved!"
                                        data-text="Click the OK button to proceed!" data-type="success">
                                    <i class="icon wb-check margin-right-10" aria-hidden="true"></i>Save Changes
                                </button>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


