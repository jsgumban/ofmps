<div class=" tab-pane active" id="category-1" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion2"
         aria-multiselectable="true" role="tablist">
        <div class="panel">
            <dl class="dl-horizontal">
                <h4 id="ASDASD">
                    <div class="btn-group pull-right">
                        @if(!empty($candidateForCVCs))
                            <span data-toggle="tooltip" data-target="#id"
                                  title="Chancellor/Vice Chancellor">
                                <button type="button" data-target="#updateUniversity"
                                        data-toggle="modal"
                                        class="btn btn-animate social-facebook dropdown-toggle"
                                        id="exampleHoverDropdown3" data-toggle="dropdown">
                                    <span><i class="icon fa-refresh"
                                             aria-hidden="true"></i> Update
                                    </span>
                                </button>
                            </span>
                        @else
                            <a href="javascript: void(0)" data-toggle="tooltip"
                               title="Requires Faculty" role="button"
                               class="btn btn-animate social-facebook dropdown-toggle"
                               disabled>
                                <span><i class="icon fa-refresh" aria-hidden="true"></i> Update</span>
                            </a>
                        @endif
                    </div>
                    <center><b>UNIVERSITY OF THE PHILIPPINES MINDANAO</b> &nbsp;
                        <a href="javascript:void(0)" data-plugin="webuiPopover"
                           data-placement="right" data-animation="pop" data-target="webuiPopover"
                           data-title="More Information" data-target="webuiPopover9"
                           data-content="<small> <table class='table'>
                                    <tbody>
                                    <tr>
                                        <td>Total no. of Colleges</td>
                                        <td>{!! count($colleges) !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Total no. of Departments</td>
                                        <td>{!! count($departments) !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Total no. of Faculty Members</td>
                                        <td>{!! count($allFaculty) !!} </td>
                                    </tr>
        
                                    </tbody>
                                    </table> </small>
                                 "><i class="icon wb-info-circle" aria-hidden="true"></i>
                        </a>

                    </center>
                </h4>

                @if(session('activeTab')==1)
                    {{-- notification --}}

                    @if(!empty(session('notification')))
                        <br>
                        <?php $notif = session('notification');?>
                        @if(strpos($notif,'SUCCESS')===false)
                            <div class="alert alert-alt alert-danger alert-dismissible"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @else
                            <div class="alert alert-alt alert-success alert-dismissible"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @endif
                    @endif
                @endif
                <hr>

                <dt>Chancellor</dt>
                @if(!empty($chancellor[0]->last_name))
                    <dd>
                        <em> {{$chancellor[0]->last_name.', '.$chancellor[0]->first_name.' '.$chancellor[0]->middle_name[0].'.' }}</em>
                    </dd>
                @else
                    <dd><em>None</em></dd>
                @endif


                <dt>Vice Chancellor</dt>
                @if(!empty($vchancellor[0]->last_name))
                    <dd>
                        <em> {{$vchancellor[0]->last_name.', '.$vchancellor[0]->first_name.' '.$vchancellor[0]->middle_name[0].'.' }}</em>
                    </dd>
                @else
                    <dd><em>None</em></dd>
                @endif
            </dl>
        </div>
        <hr>

        {{-- update university button --}}
        <div class="btn-group pull-right">

        </div>
    </div>
</div>