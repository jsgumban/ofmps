{{-- FORM CATEGORY 3 MODAL --}}
<div class="modal fade modal-info  example-modal-lg" id="UpdateFC2-modal-1" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                {{-- RIBBONS --}}
                @if (Request::is('*/ReviseForm') || Request::is('*/EvaluateForm') || Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-rejected">
                        <span class="ribbon-inner">REJECTED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-revise">
                        <span class="ribbon-inner">NEEDS REVISION</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-notyet">
                        <span class="ribbon-inner">NOT YET EVALUATED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-accept">
                        <span class="ribbon-inner">ACCEPTED</span>
                    </div>
                @endif

                <h4 class="modal-title">A. PUBLISHED SCHOLARLY WORKS </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <label class="control-label" for="selectMulti">Category</label>
                    <select class="form-control" id="UpdateFC2-SubSubCat-1" disabled>
                        <option value=1>01. Authorship of a paper in a refereed publication/creative work
                        </option>
                        <option value=2>02. Presentation of a paper in an international conference</option>
                        <option value=3>03. Presentation of a paper in a national conference</option>
                        <option value=4>04. Presentation of a paper at the department, college or university</option>
                    </select>
                </div>

                {{-- SUBCATEGORY 1--}}
                <div id="UpdateFC2-divID-1-1" class="UpdateFC2-divClass-1">
                    <form action="#" method="POST" id="UpdateFC2-form-1-1"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-1-1">
                                <option value="ISI Journal">1.1. ISI Journal
                                </option>
                                <option value="Book (Reputable international/ISI publisher) ">1.2. Book (Reputable
                                    international / ISI publisher)
                                </option>
                                <option value="Non-ISI Journal, Reputable international publication">1.3. Non-ISI
                                    Journal, Reputable international publication
                                </option>
                                <option value="Monographs (Reputable international publisher)">1.4. Monographs
                                    (Reputable international publisher)
                                </option>
                                <option value="Chapter in book (Reputable international/ISI publisher)">1.5. Chapter in
                                    book (Reputable international/ISI publisher)
                                </option>
                                <option value="Book (Reputable national publisher)">1.6. Book (reputable national
                                    publisher)
                                </option>
                                <option value="Chapter in book (reputable national publisher)">1.7. Chapter in book
                                    (reputable national publisher)
                                </option>
                                <option value="Non-ISI article in national/regional journal">1.8. Non-ISI article in
                                    national/regional journal
                                </option>
                                <option value="Proceedings of an international conference">1.9. Proceedings of an
                                    international conference
                                </option>
                                <option value="Proceedings of a national conference">1.10. Proceedings of a national
                                    conference
                                </option>
                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-1-1">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" id="UpdateFC2-author-1-1">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" id="UpdateFC2-date_published-1-1">
                        </div>


                        {{-- Title, vol. No. of Journal/Publication where annotated bibliography appears --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication</label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication" id="UpdateFC2-bibliography-1-1"
                                    >
                        </div>

                        {{-- Nature Of Publication --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature Of Publication (e.g
                                international) </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature Of Publication" id="UpdateFC2-nature_ofpublication-1-1">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-1-1">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC2-other_details-1-1">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-1-1">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-1-1" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-1-1" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-1-1" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif

                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-1-1">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-1-1"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-1-1">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-1-1"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 2--}}
                <div id="UpdateFC2-divID-1-2" class="UpdateFC2-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-1-2"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 2) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-1-2">
                                <option value="Keynote presentation">2.1. Keynote presentation
                                </option>
                                <option value="Plenary talk/ Invited Talk">2.2. Plenary talk/ Invited talk</option>
                                <option value="Contributed paper">2.3. Contributed paper</option>
                                <option value="Poster">2.4. Poster
                                </option>
                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-1-2">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" id="UpdateFC2-author-1-2">
                        </div>

                        {{-- Date of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Presentation</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Presentation" id="UpdateFC2-date_ofpresentation-1-2">
                        </div>

                        {{-- Place of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Presentation</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Presentation" id="UpdateFC2-place_ofpresentation-1-2">
                        </div>


                        {{-- Title of Conference --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of Conference </label>
                            <input type="text" class="form-control" name="title_ofconference"
                                   placeholder="Title of Conference" id="UpdateFC2-title_ofconference-1-2">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-1-2">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC2-other_details-1-2">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-1-2">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-1-2" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-1-2" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-1-2" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-1-2">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-1-2"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-1-2">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-1-2"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 3--}}
                <div id="UpdateFC2-divID-1-3" class="UpdateFC2-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-1-3"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 3) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-1-3">
                                <option value="Keynote presentation">3.1. Keynote presentation
                                </option>
                                <option value="Plenary talk/ Invited Talk">3.2. Plenary talk/ Invited Talk</option>
                                <option value="Contributed paper">3.3. Contributed paper</option>
                                <option value="Poster">3.4. Poster
                                </option>
                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-1-3">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" id="UpdateFC2-author-1-3">
                        </div>

                        {{-- Date of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Presentation</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Presentation" id="UpdateFC2-date_ofpresentation-1-3">
                        </div>

                        {{-- Place of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Presentation</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Presentation" id="UpdateFC2-place_ofpresentation-1-3">
                        </div>


                        {{-- Title of Conference --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of Conference </label>
                            <input type="text" class="form-control" name="title_ofconference"
                                   placeholder="Title of Conference" id="UpdateFC2-title_ofconference-1-3">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-1-3">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC2-other_details-1-3">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-1-3">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-1-3" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-1-3" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-1-3" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-1-3">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-1-3"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-1-3">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-1-3"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 4--}}
                <div id="UpdateFC2-divID-1-4" class="UpdateFC2-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-1-4"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 4) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-1-4">
                                <option value="Foreign university ">4.1. Foreign university
                                </option>
                                <option value="Local University ">4.2. Local University</option>
                                <option value="College/Department">4.3. College/Department</option>

                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-1-4">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" id="UpdateFC2-author-1-4">
                        </div>

                        {{-- Date of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Presentation</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Presentation" id="UpdateFC2-date_ofpresentation-1-4">
                        </div>

                        {{-- Place of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Presentation</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Presentation" id="UpdateFC2-place_ofpresentation-1-4">
                        </div>


                        {{-- Title of Conference --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of Conference </label>
                            <input type="text" class="form-control" name="title_ofconference"
                                   placeholder="Title of Conference" id="UpdateFC2-title_ofconference-1-4">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-1-4">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC2-other_details-1-4">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-1-4">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-1-4" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-1-4" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-1-4" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-1-4">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-1-4"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-1-4">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-1-4"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

            </div>


        </div>
    </div>
</div>

<div class="modal fade modal-info  example-modal-lg" id="UpdateFC2-modal-2" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                {{-- RIBBONS --}}
                @if (Request::is('*/ReviseForm') || Request::is('*/EvaluateForm')  || Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-rejected">
                        <span class="ribbon-inner">REJECTED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-revise">
                        <span class="ribbon-inner">NEEDS REVISION</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-notyet">
                        <span class="ribbon-inner">NOT YET EVALUATED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-accept">
                        <span class="ribbon-inner">ACCEPTED</span>
                    </div>
                @endif

                <h4 class="modal-title">B. CREATIVE WORKS </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <label class="control-label" for="selectMulti">Category</label>
                    <select class="form-control" id="UpdateFC2-SubSubCat-2" disabled>
                        <option value=1>01. Literature
                        </option>
                        <option value=2>02. Fine Arts</option>
                        <option value=3>03. Music and Dance</option>
                        <option value=4>04. Architecture</option>
                        <option value=5>05. Theatre Arts</option>
                        <option value=6>06. Radio, Television and Related Media</option>
                        <option value=7>07. Film</option>
                        <option value=8>08. Scholarly Work in the Arts</option>
                    </select>
                </div>

                {{-- SUBCATEGORY 1: LITERATURE --}}
                <div id="UpdateFC2-divID-2-1" class="UpdateFC2-divClass-2">
                    <form action="#" method="POST" id="UpdateFC2-form-2-1"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-2-1">
                                <option value="Authorship: Book">1.1.1. Authorship: Book
                                </option>
                                <option value="Authorship : Short works">1.1.2. Authorship : Short Works</option>
                                <option value="Translations : Book">1.2.1. Translations : Book</option>
                                <option value="Translations : Short Works">1.2.2. Translations : Short Works</option>
                                <option value="Adaptations : Book">1.3.1. Adaptations : Book</option>
                                <option value="Adaptations : Short Works">1.3.2. Adaptations : Short Works</option>
                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-2-1">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required id="UpdateFC2-author-2-1">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Publication</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC2-date_published-2-1">
                        </div>


                        {{-- Publisher --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required id="UpdateFC2-publisher-2-1"
                                    >
                        </div>

                        {{-- Nature Of Publication --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature Of Publication (e.g
                                international) </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature Of Publication" required id="UpdateFC2-nature_ofpublication-2-1">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-2-1">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC2-other_details-2-1">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-2-1">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-2-1" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-2-1" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-2-1" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">
                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-2-1">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-1"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-2-1">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-1"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 2: FINE ARTS --}}
                <div id="UpdateFC2-divID-2-2" class="UpdateFC2-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-2-2"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 2) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-2-2">
                                <option value="Authorship: Studio Arts">2.1.1. Authorship : Studio Arts
                                </option>
                                <option value="Authorship : Graphic Design/Illustration">2.1.2. Authorship : Graphic
                                    Design/Illustration
                                </option>
                                <option value="Authorship : Photography/Photo Advertising">2.1.3. Authorship :
                                    Photography/Photo Advertising
                                </option>
                                <option value="Authorship : Industrial Design">2.1.4. Authorship :
                                    Industrial Design
                                </option>
                                <option value="Artistic Direction">2.2.0. Artistic Direction</option>
                                <option value="Specialized Work">2.3.0. Specialized Work</option>
                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-2-2">
                        </div>


                        {{--Date of Publication/Exhibition/Prototyping --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of
                                Publication/Exhibition</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Publication/Exhibition" required
                                   id="UpdateFC2-date_ofpresentation-2-2">
                        </div>

                        {{-- Place of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of
                                Publication/Exhibition</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Publication/Exhibition" required
                                   id="UpdateFC2-place_ofpresentation-2-2">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-2-2">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC2-other_details-2-2">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-2-2">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-2-2" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-2-2" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-2-2" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-2-2">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-2"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-2-2">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-2"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>


                {{-- SUBCATEGORY 3: MUSIC AND DANCE --}}
                <div id="UpdateFC2-divID-2-3" class="UpdateFC2-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-2-3"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 3) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-2-3">
                                <option value="Authorship : Composition">3.1.1. Authorship : Composition
                                </option>
                                <option value="Authorship : Choreography">3.1.2. Authorship : Choreography</option>
                                <option value="Performance">3.2.0. Performance</option>
                                <option value="Artistic Direction : Major Production">3.3.1. Artistic Direction : Major
                                    Production
                                </option>
                                <option value="Artistic Direction : Major Festival or Event">3.3.2. Artistic Direction :
                                    Major Festival or Event
                                </option>
                                <option value="Artistic Direction : Recording Production">3.3.3. Artistic Direction :
                                    Recording Production
                                </option>
                                <option value="Restaging of a Major Work">3.4.0. Restaging of a Major Work</option>
                                <option value="Games and Exercises">3.5.0. Games and Exercises</option>
                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title" id="UpdateFC2-title-2-3"
                                   placeholder="Title" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" id="UpdateFC2-other_details-2-3">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-2-3">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-2-3">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-2-3" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-2-3" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-2-3" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-2-3">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-3"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-2-3">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-3"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 4: ARCHITECTURE --}}
                <div id="UpdateFC2-divID-2-4" class="UpdateFC2-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-2-4"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 4) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-2-4">
                                <option value="Large- Scale Projects (Built)">4.1.0. Large- Scale Projects (Built)
                                </option>
                                <option value="Mid-Scale Projects (Built)">4.2.0 Mid-Scale Projects (Built)</option>
                                <option value="Small-Scale Project (Built)">4.3.0 Small-Scale Project (Built)</option>
                                <option value="Unbuilt Projects (Winning or published Competition Entries)">4.4.0
                                    Unbuilt Projects (Winning or published Competition Entries)
                                </option>
                                <option value="Additional Points for Winning Built Projects (Winning or Published Competition Entries)">
                                    4.5.0 Additional Points for Winning Built Projects (Winning or Published Competition
                                    Entries)
                                </option>

                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of Project</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title of Project" required id="UpdateFC2-title-2-4">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" required id="UpdateFC2-other_details-2-4">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-2-4">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-2-4">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-2-4" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-2-4" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-2-4" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-2-4">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-4"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-2-4">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-4"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 5: THEATRE ARTS --}}
                <div id="UpdateFC2-divID-2-5" class="UpdateFC2-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-2-5"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 5) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-2-5">
                                <option value="Direction">5.1.0. Direction</option>
                                <option value="Design">5.2.0. Design</option>
                                <option value="Acting">5.3.0. Acting</option>
                                <option value="Restaging">5.4.0. Restaging</option>
                                <option value="Artistic Direction (Festivals/Special Programs and Events)">5.5.0.
                                    Artistic Direction (Festivals/Special Programs and Events)
                                </option>

                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-2-5">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" required id="UpdateFC2-other_details-2-5">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-2-5">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-2-5">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-2-5" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-2-5" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-2-5" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-2-5">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-5"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-2-5">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-5"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 6: Radio, Television and Related Media --}}
                <div id="UpdateFC2-divID-2-6" class="UpdateFC2-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-2-6"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 6) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-2-6">
                                <option value="Direction">6.1.0 Direction</option>
                                <option value="Scriptwriting">6.2.0 Scriptwriting</option>
                                <option value="Aspects of Production : Extended Program">6.3.1 Aspects of Production :
                                    Extended Program
                                </option>
                                <option value="Aspects of Production : Full-Length and Short">6.3.2 Aspects of
                                    Production : Full-Length and Short Program
                                </option>


                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-2-6">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" required id="UpdateFC2-other_details-2-6">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-2-6">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-2-6">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c2radio1-2-6" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c2radio2-2-6" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c2radio3-2-6" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC2-reviseDiv-2-6">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-6"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC2-revise-2-6">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-6"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 7: Film --}}
                <div id="UpdateFC2-divID-2-7" class="UpdateFC2-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC2-form-2-7"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 7) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-2-7">
                                <option value="Direction">7.1.0. Direction</option>
                                <option value="Scriptwriting">2.2.0. Scriptwriting</option>
                                <option value="Aspects of Production ">7.3.0. Aspects of Production</option>
                                <option value="Artistic Direction ">7.4.0. Artistic Direction</option>
                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC2-title-2-7">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" required id="UpdateFC2-other_details-2-7">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC2-points-2-7">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC2-proofs-2-7id=" UpdateFC2-proofs-2-8
                        "">
                        <label class="control-label" for="selectMulti">Attachment(s)
                            <ul class="list-unstyled list-inline">
                                <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                    <div class="radio-custom radio-primary">
                                        <input type="radio" id="c2radio1-2-7" name="proofAction" checked="" value=1>
                                        <label for="inputRadioNormal">Do nothing/Add more</label>
                                    </div>
                                </li>
                                <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                    <div class="radio-custom radio-success">
                                        <input type="radio" id="c2radio2-2-7" name="proofAction" value=2>
                                        <label for="inputRadioNormal">Remove and Replace</label>
                                    </div>
                                </li>
                                <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                    for="inputRadioNormal">
                                    <div class="radio-custom radio-danger">
                                        <input type="radio" id="c2radio3-2-7" name="proofAction" value=3>
                                        <label>Remove all</label>
                                    </div>
                                </li>

                            </ul>
                        </label>

                        <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                            <input type="text" class="form-control" placeholder="Browse.." readonly="">
                            <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                   class="" onchange="checkFile()">
                        </div>
                </div>


                {{-- BUTTONS --}}
                @if(Request::is('*/EvaluateForm'))
                    {!! Form::hidden('action', 'EvaluateForm') !!}
                    <div class="row">
                        <div class="col-lg-12 form-group" id="evaluationButtons">
                            <div class="col-md-6" align="left">
                                <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                       data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                       data-points="0"/>
                                <span>Accept</span> &nbsp;&nbsp;

                                <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                       data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                <span>Reject</span> &nbsp;&nbsp;

                                @if(Auth::user()->position==1)
                                    <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                           data-plugin="labelauty" data-label="false" value="3" data-points="0"/>
                                    <span>Revise</span>
                                @endif
                            </div>
                            <div class="col-md-6" align="right">
                                <button type="submit" class="btn btn-info">Save Changes</button>
                            </div>
                        </div>
                    </div>
                @elseif(Request::is('*/ReviseForm'))
                    {!! Form::hidden('action', 'ReviseForm') !!}
                    <div class="modal-footer">
                        {{-- SUBMIT BUTTON --}}
                        <div id="UpdateFC2-reviseDiv-2-7">
                            <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-7"
                               onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                Entry</a>
                            {{-- CANCEL BUTTON --}}
                            <button type="submit" class="btn btn-success" id="UpdateFC2-revise-2-7">Revise</button>
                        </div>
                    </div>
                @else
                    <div class="modal-footer">
                        <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-7"
                           onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                            Entry</a>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </div>
                    @endif

                    </form>
            </div>

            {{-- SUBCATEGORY 8: Scholarly Work in the Arts --}}
            <div id="UpdateFC2-divID-2-8" class="UpdateFC2-divClass-2" style="display:none;">
                <form action="#" method="POST" id="UpdateFC2-form-2-8"
                      enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    {{-- HIDDEN ATTRIBUTES --}}
                    {!! Form::hidden('category', 2) !!}
                    {!! Form::hidden('subcategory', 8) !!}

                    <div class="col-lg-12 form-group">

                        <select class="form-control" name="subsubcategory" id="UpdateFC2-subsubcategory-2-8">
                            <option value="Authorship : Book">8.1.0. Authorship : Book</option>
                            <option value="Authorship : Short Works">8.2.0. Authorship : Short Works</option>
                        </select>

                    </div>

                    {{-- Title --}}
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="selectMulti">Title</label>
                        <input type="text" class="form-control" name="title"
                               placeholder="Title" required id="UpdateFC2-title-2-8">
                    </div>

                    {{-- Author --}}
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="selectMulti">Author(s)</label>
                        <input type="text" class="form-control" name="author"
                               placeholder="Author" required id="UpdateFC2-author-2-8">
                    </div>

                    {{-- Date Published --}}
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="selectMulti">Date Published</label>
                        <input type="date" class="form-control" name="date_published"
                               placeholder="Date Published" required id="UpdateFC2-date_published-2-8">
                    </div>


                    {{-- Title, vol. No. of Journal/Publication where annotated bibliography appears --}}
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="selectMulti">Title, Vol. No. of
                            Journal/Publication</label>
                        <input type="text" class="form-control" name="bibliography"
                               placeholder="Title, Vol. No. of Journal/Publication" required
                               id="UpdateFC2-bibliography-2-8"
                                >
                    </div>

                    {{-- Nature Of Publication --}}
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="selectMulti">Nature Of Publication (e.g
                            international) </label>
                        <input type="text" class="form-control" name="nature_ofpublication"
                               placeholder="Nature Of Publication" required id="UpdateFC2-nature_ofpublication-2-8">
                    </div>

                    {{-- Points--}}
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="selectMulti">Points</label>
                        <input type="number" step="0.01" class="form-control" name="points"
                               placeholder="Points" min="0" max="100" required id="UpdateFC2-points-2-8">
                    </div>


                    {{-- Other Details --}}
                    <div class="col-md-12 form-group">
                        <label class="control-label" for="selectMulti">Other Details </label>
                        <input type="text" class="form-control" name="other_details"
                               placeholder="Other Details" id="UpdateFC2-other_details-2-8">
                    </div>


                    {{-- Proofs--}}
                    <div class="col-md-12 form-group" id="UpdateFC2-proofs-2-8">
                        <label class="control-label" for="selectMulti">Attachment(s)
                            <ul class="list-unstyled list-inline">
                                <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                    <div class="radio-custom radio-primary">
                                        <input type="radio" id="c2radio1-2-8" name="proofAction" checked="" value=1>
                                        <label for="inputRadioNormal">Do nothing/Add more</label>
                                    </div>
                                </li>
                                <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                    <div class="radio-custom radio-success">
                                        <input type="radio" id="c2radio2-2-8" name="proofAction" value=2>
                                        <label for="inputRadioNormal">Remove and Replace</label>
                                    </div>
                                </li>
                                <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                    for="inputRadioNormal">
                                    <div class="radio-custom radio-danger">
                                        <input type="radio" id="c2radio3-2-8" name="proofAction" value=3>
                                        <label>Remove all</label>
                                    </div>
                                </li>

                            </ul>
                        </label>

                        <div class="form-group form-material form-material-file" id="c2BrowseDiv1">

                            <input type="text" class="form-control" placeholder="Browse.." readonly="">
                            <input type="file" id="inputFileUpdateCat2" name="file[]" multiple=""
                                   class="" onchange="checkFile()">
                        </div>
                    </div>


                    {{-- BUTTONS --}}
                    @if(Request::is('*/EvaluateForm'))
                        {!! Form::hidden('action', 'EvaluateForm') !!}
                        <div class="row">
                            <div class="col-lg-12 form-group" id="evaluationButtons">
                                <div class="col-md-6" align="left">
                                    <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                           data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                           data-points="0"/>
                                    <span>Accept</span> &nbsp;&nbsp;

                                    <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                           data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                    <span>Reject</span> &nbsp;&nbsp;

                                    @if(Auth::user()->position==1)
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="3" data-points="0"/>
                                        <span>Revise</span>
                                    @endif
                                </div>
                                <div class="col-md-6" align="right">
                                    <button type="submit" class="btn btn-info">Save Changes</button>
                                </div>
                            </div>
                        </div>
                    @elseif(Request::is('*/ReviseForm'))
                        {!! Form::hidden('action', 'ReviseForm') !!}
                        <div class="modal-footer">
                            {{-- SUBMIT BUTTON --}}
                            <div id="UpdateFC2-reviseDiv-2-8">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-8"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                {{-- CANCEL BUTTON --}}
                                <button type="submit" class="btn btn-success" id="UpdateFC2-revise-2-8">Revise</button>
                            </div>
                        </div>
                    @else
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC2-delete-2-8"
                               onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                Entry</a>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    @endif
                </form>
            </div>

        </div>
    </div>
</div>
</div>
@if(Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $("[id^=UpdateFC2-]").attr('disabled', true);
        $("[id^=UpdateFC2-proofs-]").css('display', "none");
        $(".btn").css('display', "none");
        @if(Auth::user()->position==4)
            $("[id^=chancellorEvaluation-]").css('display', "block");
        @endif
    </script>
@endif

@if(Request::is('*/EvaluateForm'))
    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $("[id^=UpdateFC2-]").attr('disabled', true);
        $("[id^=UpdateFC2-proofs-]").css('display', "none");
    </script>

    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("input[name*='evaluateAction']").click(function (e) {
                // accept
                if (this.value == 1) {
                    $("[id^=UpdateFC2-points-]").attr('disabled', false);
                    $("[id^=UpdateFC2-points-]").css('background-color', '#FFEFD5');
                    $("[id^=UpdateFC2-points-]").css('color', 'black');
                }

                // reject or revise
                else if (this.value == 2 || this.value == 3) {
                    points = $("[id^=labelauty]").attr("data-points");
                    $("[id^=UpdateFC2-points]").val(points);
                    $("[id^=UpdateFC2-points]").attr('disabled', true);
                    $("[id^=UpdateFC2-points-]").css('background-color', '');
                    $("[id^=UpdateFC2-points-]").css('color', '');
                }


            });
        });
    </script>
@endif


<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=c2radio]").click(function (e) {
            if (this.value == 3) {
                $("[id^=c2BrowseDiv]").css('display', 'none');
            } else {
                $("[id^=c2BrowseDiv]").css('display', 'block');
            }
        });
    });
</script>


<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=inputFileUpdateCat2]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }
            }
        });
    });
</script>
