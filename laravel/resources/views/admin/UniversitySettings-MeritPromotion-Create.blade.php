<div class="modal fade modal-info" id="exampleModalWarning" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">


        {{-- FORM --}}

        <form action={!! $preLink.'meritpromotion/SaveDate' !!} method="POST">
            {!! csrf_field() !!}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">ADD CALL FOR PROMOTION</h4>
                </div>

                <div class="modal-body">
                    {{-- FORM --}}

                    <div class="col-md-6 form-group">
                        <label class="control-label" for="selectMulti">Start</label>
                        <input type="date" class="form-control" name="mpromotion_date_start"
                               required>
                    </div>

                    <div class="col-md-6 form-group">
                        <label class="control-label" for="selectMulti">End</label>
                        <input type="date" class="form-control" name="mpromotion_date_end"
                               required>
                    </div>

                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Details</label>
                        <textarea class="form-control" name="details" rows="3"></textarea>
                    </div>


                </div>
                <div class="modal-footer">


                    {{-- SUBMIT BUTTON --}}
                    <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>


                    {{-- CANCEL BUTTON --}}
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>