@if((Request::is('*/EvaluateForm')) || (Request::is('*/MonitorForm')) || (Request::is('*/ReviseForm')) ||  Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') ||  Request::is('*/CHANCELLORMonitorForm') )

        <!-- REMARKS -->
<div class="panel">
    <div class="panel-body">
        <ol class="dd-list">
            <h4 class="example-title"><b>REMARKS</b></h4>
            <br>

            <div class="pearls row">
                <div class="pearl current col-xs-3 disabled" aria-expanded="true disabled"
                     id="{{'form-remarks-deptchair-'.$formID}}"
                     role="button" data-toggle="modal" data-target="#deptchair_remarks">
                    <div class="pearl-icon"><i class="icon wb-user" aria-hidden="true"></i><span
                                class="badge up badge-danger"></span></div>
                    <span class="pearl-title"><small><i class="icon wb-check-circle" aria-hidden="true"
                                                        style="font-size: 12px; color:#46be8a; display: none"
                                                        id="{{'check-remarks-deptchair-'.$formID}}"></i>&nbsp;DAPC
                        </small></span>
                    @if(!empty($MpromotionForm->deptchair_remarks))
                        <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
                        <script type="text/javascript">
                            $("[id^=form-remarks-deptchair-]").attr('class', 'pearl current col-xs-3');
                            $("[id^=check-remarks-deptchair-]").css('display', 'inline-block');
                            @if($MpromotionForm->form_status==5)
                                 $("[id^=check-remarks-deptchair-]").attr('class', 'icon wb-refresh');
                            $("[id^=check-remarks-deptchair-]").css('color', 'red');
                            @endif

                            @if($MpromotionForm->form_status==1.5)
                                 $("[id^=check-remarks-deptchair-]").attr('class', 'icon wb-close');
                            $("[id^=check-remarks-deptchair-]").css('color', 'red');
                            @endif
                        </script>
                    @endif
                </div>
                <div class="pearl current col-xs-3 disabled" aria-expanded="false" id="{{'form-remarks-dean-'.$formID}}"
                     role="button" data-toggle="modal" data-target="#dean_remarks">
                    <div class="pearl-icon"><i class="icon wb-user" aria-hidden="true"></i></div>
                    <span class="pearl-title"><small><i class="icon wb-check-circle" aria-hidden="true"
                                                        style="font-size: 12px; color:#46be8a; display: none"
                                                        id="{{'check-remarks-dean-'.$formID}}"></i>&nbsp;CAPC
                        </small></span>
                    @if(!empty($MpromotionForm->dean_remarks))
                        <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
                        <script type="text/javascript">
                            $("[id^=form-remarks-dean-]").attr('class', 'pearl current col-xs-3');
                            $("[id^=check-remarks-dean-]").css('display', 'inline-block');
                            @if($MpromotionForm->form_status==2.5)
                                 $("[id^=check-remarks-dean-]").attr('class', 'icon wb-close');
                            $("[id^=check-remarks-dean-]").css('color', 'red');
                            @endif
                        </script>
                    @endif
                </div>
                <div class="pearl col-xs-3 disabled" aria-expanded="false" id="{{'form-remarks-vchancellor-'.$formID}}"
                     role="button" data-toggle="modal" data-target="#vchancellor_remarks">
                    <div class="pearl-icon"><i class="icon wb-user" aria-hidden="true"></i></div>
                    <span class="pearl-title"><small><i class="icon wb-check-circle" aria-hidden="true"
                                                        style="font-size: 12px; color:#46be8a; display: none"
                                                        id="{{'check-remarks-vchancellor-'.$formID}}"></i>&nbsp;UAPFC
                        </small></span>
                    @if(!empty($MpromotionForm->vchancellor_remarks))
                        <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
                        <script type="text/javascript">
                            $("[id^=form-remarks-vchancellor-]").attr('class', 'pearl current col-xs-3');
                            $("[id^=check-remarks-vchancellor-]").css('display', 'inline-block');
                            @if($MpromotionForm->form_status==3.5)
                                 $("[id^=check-remarks-vchancellor-]").attr('class', 'icon wb-close');
                            $("[id^=check-remarks-vchancellor-]").css('color', 'red');
                            @endif
                        </script>
                    @endif
                </div>
                <div class="pearl col-xs-3 disabled" aria-expanded="false" id="{{'form-remarks-chancellor-'.$formID}}"
                     role="button" data-toggle="modal" data-target="#chancellor_remarks">
                    <div class="pearl-icon"><i class="icon wb-user" aria-hidden="true"></i></div>
                    <span class="pearl-title"><small><i class="icon wb-check-circle" aria-hidden="true"
                                                        style="font-size: 12px; color:#46be8a; display: none"
                                                        id="{{'check-remarks-chancellor-'.$formID}}"></i>&nbsp;CHANCELLOR
                        </small></span>
                    @if(!empty($MpromotionForm->chancellor_remarks))
                        <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
                        <script type="text/javascript">
                            $("[id^=form-remarks-chancellor-]").attr('class', 'pearl current col-xs-3');
                            $("[id^=check-remarks-chancellor-]").css('display', 'inline-block');
                            @if($MpromotionForm->form_status==7)
                                 $("[id^=check-remarks-chancellor-]").attr('class', 'icon wb-close');
                            $("[id^=check-remarks-chancellor-]").css('color', 'red');
                            @endif

                        </script>
                    @endif
                </div>
            </div>
        </ol>
    </div>
</div>

<!-- LEGEND -->
<div class="panel">
    <div class="panel-body">
        <ol class="dd-list">
            <h4 class="example-title"><b>LEGEND</b></h4>
            <table class='table'>
                <tbody>
                @if (Request::is('*/EvaluateForm') || Request::is('*/CHANCELLORMonitorForm'))
                    <tr>
                        <td><i class="icon glyphicon glyphicon-hand-right"></i>&nbsp;&nbsp;Entry accepted</td>
                        <td align="right">
                            <i class="icon wb-check-circle" aria-hidden="true"
                               style="font-size: 18px; color:#800000"></i>
                        </td>
                    </tr>
                @endif

                <tr>

                    <td><i class="icon glyphicon glyphicon-hand-right"></i>&nbsp;&nbsp;Entry rejected</td>
                    <td align="right">
                        <i class="icon wb-stop" aria-hidden="true" style="font-size: 20px; color:#f96868"></i>
                    </td>
                </tr>

                <tr>

                    <td><i class="icon glyphicon glyphicon-hand-right"></i>&nbsp;&nbsp;Entry needs revision</td>
                    <td align="right">
                        <i class="icon wb-stop" aria-hidden="true" style="font-size: 20px; color:#46be8a"></i>
                    </td>
                </tr>

                @if (Request::is('*/EvaluateForm') || Request::is('*/CHANCELLORMonitorForm'))
                    <tr>
                        <td><i class="icon glyphicon glyphicon-hand-right"></i>&nbsp;&nbsp;Entry not evaluated yet</td>
                        <td align="right">
                            <i class="icon wb-stop" aria-hidden="true" style="font-size: 20px; color:#76838f"></i>
                        </td>
                    </tr>
                @endif


                </tbody>
            </table>
        </ol>
    </div>
</div>
@endif