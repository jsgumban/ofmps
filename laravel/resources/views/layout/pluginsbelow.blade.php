<script src="{{ asset('/vendor/toastr/toastr.js') }}"></script>
<script src="{{ asset('/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('/vendor/html5sortable/html.sortable.js') }} "></script>
<script src="{{ asset('/vendor/nestable/jquery.nestable.js') }} "></script>
<script src="{{ asset('/vendor/webui-popover/jquery.webui-popover.min.js') }}"></script>
<script src="{{ asset('/vendor/jquery-labelauty/jquery-labelauty.js') }}"></script>
{{-- UNIVERSITY SETTINGS TABS --}}
<script src="{{ asset('/vendor/skycons/skycons.js') }}"></script>
<script src="{{ asset('/vendor/chartist-js/chartist.min.js') }}"></script>
<script src="{{ asset('/vendor/intro-js/intro.js') }}"></script>
{{-- PAGE TO APPEAR --}}
<script src="{{ asset('/vendor/screenfull/screenfull.js') }}"></script>






