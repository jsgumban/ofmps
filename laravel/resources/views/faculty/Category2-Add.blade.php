{{-- FORM CATEGORY 3 MODAL --}}
<div class="modal fade modal-info  example-modal-lg" id="exampleModalWarning3" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">A. PUBLISHED SCHOLARLY WORKS </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="FormCat2Cat1" onclick="FormCat2Cat1ShowDiv()"
                            onkeydown="FormCat2Cat1ShowDiv()"
                            onkeydown="FormCat2Cat1ShowDiv()" onkeypress="FormCat2Cat1ShowDiv()">
                        <option value=1>01. Authorship of a paper in a refereed publication
                        </option>
                        <option value=2>02. Presentation of a paper in an international conference</option>
                        <option value=3>03. Presentation of a paper in a national conference</option>
                        <option value=4>04. Presentation of a paper at the department, college or university</option>
                    </select>
                </div>

                {{-- SUBCATEGORY 1--}}
                <div id="1FormCat2Cat1" class="FormCat2Cat1ShowDivTargetDiv">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subsubcategory">
                                <option value="ISI Journal">1.1. ISI Journal
                                </option>
                                <option value="Book (Reputable international/ISI publisher) ">1.2. Book (Reputable
                                    international / ISI publisher)
                                </option>
                                <option value="Non-ISI Journal, Reputable international publication">1.3. Non-ISI
                                    Journal, Reputable international publication
                                </option>
                                <option value="Monographs (Reputable international publisher)">1.4. Monographs
                                    (Reputable international publisher)
                                </option>
                                <option value="Chapter in book (Reputable international/ISI publisher)">1.5. Chapter in
                                    book (Reputable international/ISI publisher)
                                </option>
                                <option value="Book (Reputable national publisher)">1.6. Book (reputable national
                                    publisher)
                                </option>
                                <option value="Chapter in book (reputable national publisher)">1.7. Chapter in book
                                    (reputable national publisher)
                                </option>
                                <option value="Non-ISI article in national/regional journal">1.8. Non-ISI article in
                                    national/regional journal
                                </option>
                                <option value="Proceedings of an international conference">1.9. Proceedings of an
                                    international conference
                                </option>
                                <option value="Proceedings of a national conference">1.10. Proceedings of a national
                                    conference
                                </option>
                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published">
                        </div>


                        {{-- Title, vol. No. of Journal/Publication where annotated bibliography appears --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication</label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication"
                                    >
                        </div>

                        {{-- Nature Of Publication --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature Of Publication (e.g
                                international) </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature Of Publication">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover" data-placement="right"
                                   data-content=" 
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>ISI</td>
                                            <td>90each</td>
                                        </tr>

                                        <tr>
                                            <td>Non-ISI reputable international publication</td>
                                            <td>60each</td>
                                        </tr>

                                        <tr>
                                            <td>Book/Chapter in book (reputable International/ISI publisher) </td>
                                            <td>60each</td>
                                        </tr>

                                        <tr>
                                            <td>Monographs (reputable international publisher)</td>
                                            <td>60each</td>
                                        </tr>

                                        <tr>
                                            <td>Book/Chapter in book (reputable national publisher)</td>
                                            <td>30each</td>
                                        </tr>

                                        <tr>
                                            <td>Non-ISI article in national/regional journal</td>
                                            <td>30each</td>
                                        </tr>

                                        <tr>
                                            <td>Proceedings of an international conference</td>
                                            <td>30each</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>Proceedings of a national conference</td>
                                            <td>20each</td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="90" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 2--}}
                <div id="2FormCat2Cat1" class="FormCat2Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 2) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Keynote presentation">2.1. Keynote presentation
                                </option>
                                <option value="Plenary talk/ Invited Talk">2.2. Plenary talk/ Invited talk</option>
                                <option value="Contributed paper">2.3. Contributed paper</option>
                                <option value="Poster">2.4. Poster
                                </option>
                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author">
                        </div>

                        {{-- Date of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Presentation</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Presentation">
                        </div>

                        {{-- Place of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Presentation</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Presentation">
                        </div>


                        {{-- Title of Conference --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of Conference </label>
                            <input type="text" class="form-control" name="title_ofconference"
                                   placeholder="Title of Conference">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover" data-placement="right"
                                   data-content=" 
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Plenary talk/Solo Performance</td>
                                            <td>10each</td>
                                        </tr>

                                        <tr>
                                            <td>Invited Talk/Keynote presentation/Performance in a group</td>
                                            <td>8each</td>
                                        </tr>

                                        <tr>
                                            <td>Contributed paper (oral/poster)/ Guest performance</td>
                                            <td>6each</td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>

                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="10" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 3--}}
                <div id="3FormCat2Cat1" class="FormCat2Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 3) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Keynote presentation">3.1. Keynote presentation
                                </option>
                                <option value="Plenary talk/ Invited Talk">3.2. Plenary talk/ Invited Talk</option>
                                <option value="Contributed paper">3.3. Contributed paper</option>
                                <option value="Poster">3.4. Poster
                                </option>
                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author">
                        </div>

                        {{-- Date of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Presentation</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Presentation">
                        </div>

                        {{-- Place of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Presentation</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Presentation">
                        </div>


                        {{-- Title of Conference --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of Conference </label>
                            <input type="text" class="form-control" name="title_ofconference"
                                   placeholder="Title of Conference">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover" data-placement="right"
                                   data-content=" 
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Plenary talk</td>
                                            <td>8each</td>
                                        </tr>

                                        <tr>
                                            <td>Invited Talk/Keynote presentation</td>
                                            <td>6each</td>
                                        </tr>

                                        <tr>
                                            <td>Contributed paper (oral/poster)/ Guest performance</td>
                                            <td>2each</td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="8" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 4--}}
                <div id="4FormCat2Cat1" class="FormCat2Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 4) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Foreign university ">4.1. Foreign university
                                </option>
                                <option value="Local University ">4.2. Local University</option>
                                <option value="College/Department">4.3. College/Department</option>

                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author">
                        </div>

                        {{-- Date of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Presentation</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Presentation">
                        </div>

                        {{-- Place of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Presentation</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Presentation">
                        </div>


                        {{-- Title of Conference --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of Conference </label>
                            <input type="text" class="form-control" name="title_ofconference"
                                   placeholder="Title of Conference">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover" data-placement="right"
                                   data-content=" 
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Foreign university </td>
                                            <td>2each</td>
                                        </tr>

                                        <tr>
                                            <td>Local university</td>
                                            <td>1each</td>
                                        </tr>

                                        <tr>
                                            <td>College/Department</td>
                                            <td>1each</td>
                                        </tr>
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

            </div>


        </div>
    </div>
</div>

<div class="modal fade modal-info  example-modal-lg" id="exampleModalWarning4" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">B. CREATIVE WORKS </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <label class="control-label" for="selectMulti">Category</label>
                    <select class="form-control" id="FormCat2Cat2" onclick="FormCat2Cat2ShowDiv()"
                            onkeydown="FormCat2Cat2ShowDiv()"
                            onkeydown="FormCat2Cat2ShowDiv()" onkeypress="FormCat2Cat2ShowDiv()">
                        <option value=1>01. Literature
                        </option>
                        <option value=2>02. Fine Arts</option>
                        <option value=3>03. Music and Dance</option>
                        <option value=4>04. Architecture</option>
                        <option value=5>05. Theatre Arts</option>
                        <option value=6>06. Radio, Television and Related Media</option>
                        <option value=7>07. Film</option>
                        <option value=8>08. Scholarly Work in the Arts</option>
                    </select>
                </div>

                {{-- SUBCATEGORY 1: LITERATURE --}}
                <div id="1FormCat2Cat2" class="FormCat2Cat2ShowDivTargetDiv">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Authorship: Book">1.1.1. Authorship: Book
                                </option>
                                <option value="Authorship : Short works">1.1.2. Authorship : Short Works</option>
                                <option value="Translations : Book">1.2.1. Translations : Book</option>
                                <option value="Translations : Short Works">1.2.2. Translations : Short Works</option>
                                <option value="Adaptations : Book">1.3.1. Adaptations : Book</option>
                                <option value="Adaptations : Short Works">1.3.2. Adaptations : Short Works</option>
                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Publication</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>


                        {{-- Publisher --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required
                                    >
                        </div>

                        {{-- Nature Of Publication --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature Of Publication (e.g
                                international) </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature Of Publication" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">
                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 2: FINE ARTS --}}
                <div id="2FormCat2Cat2" class="FormCat2Cat2ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 2) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Authorship: Studio Arts">2.1.1. Authorship : Studio Arts
                                </option>
                                <option value="Authorship : Graphic Design/Illustration">2.1.2. Authorship : Graphic
                                    Design/Illustration
                                </option>
                                <option value="Authorship : Photography/Photo Advertising">2.1.3. Authorship :
                                    Photography/Photo Advertising
                                </option>
                                <option value="Authorship : Industrial Design">2.1.4. Authorship :
                                    Industrial Design
                                </option>
                                <option value="Artistic Direction">2.2.0. Artistic Direction</option>
                                <option value="Specialized Work">2.3.0. Specialized Work</option>
                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{--Date of Publication/Exhibition/Prototyping --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of
                                Publication/Exhibition</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Publication/Exhibition" required>
                        </div>

                        {{-- Place of Presentation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of
                                Publication/Exhibition</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Publication/Exhibition" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>


                {{-- SUBCATEGORY 3: MUSIC AND DANCE --}}
                <div id="3FormCat2Cat2" class="FormCat2Cat2ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 3) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Authorship : Composition">3.1.1. Authorship : Composition
                                </option>
                                <option value="Authorship : Choreography">3.1.2. Authorship : Choreography</option>
                                <option value="Performance">3.2.0. Performance</option>
                                <option value="Artistic Direction : Major Production">3.3.1. Artistic Direction : Major
                                    Production
                                </option>
                                <option value="Artistic Direction : Major Festival or Event">3.3.2. Artistic Direction :
                                    Major Festival or Event
                                </option>
                                <option value="Artistic Direction : Recording Production">3.3.3. Artistic Direction :
                                    Recording Production
                                </option>
                                <option value="Restaging of a Major Work">3.4.0. Restaging of a Major Work</option>
                                <option value="Games and Exercises">3.5.0. Games and Exercises</option>
                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 4: ARCHITECTURE --}}
                <div id="4FormCat2Cat2" class="FormCat2Cat2ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 4) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Large- Scale Projects (Built)">4.1.0. Large- Scale Projects (Built)
                                </option>
                                <option value="Mid-Scale Projects (Built)">4.2.0 Mid-Scale Projects (Built)</option>
                                <option value="Small-Scale Project (Built)">4.3.0 Small-Scale Project (Built)</option>
                                <option value="Unbuilt Projects (Winning or published Competition Entries)">4.4.0
                                    Unbuilt Projects (Winning or published Competition Entries)
                                </option>
                                <option value="Additional Points for Winning Built Projects (Winning or Published Competition Entries)">
                                    4.5.0 Additional Points for Winning Built Projects (Winning or Published Competition
                                    Entries)
                                </option>

                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of Project</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title of Project" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 5: THEATRE ARTS --}}
                <div id="5FormCat2Cat2" class="FormCat2Cat2ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 5) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Direction">5.1.0. Direction</option>
                                <option value="Design">5.2.0. Design</option>
                                <option value="Acting">5.3.0. Acting</option>
                                <option value="Restaging">5.4.0. Restaging</option>
                                <option value="Artistic Direction (Festivals/Special Programs and Events)">5.5.0.
                                    Artistic Direction (Festivals/Special Programs and Events)
                                </option>

                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 6: Radio, Television and Related Media --}}
                <div id="6FormCat2Cat2" class="FormCat2Cat2ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 6) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Direction">6.1.0 Direction</option>
                                <option value="Scriptwriting">6.2.0 Scriptwriting</option>
                                <option value="Aspects of Production : Extended Program">6.3.1 Aspects of Production :
                                    Extended Program
                                </option>
                                <option value="Aspects of Production : Full-Length and Short">6.3.2 Aspects of
                                    Production : Full-Length and Short Program
                                </option>


                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 7: Film --}}
                <div id="7FormCat2Cat2" class="FormCat2Cat2ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 7) !!}

                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="subsubcategory">
                                <option value="Direction">7.1.0. Direction</option>
                                <option value="Scriptwriting">2.2.0. Scriptwriting</option>
                                <option value="Aspects of Production ">7.3.0. Aspects of Production</option>
                                <option value="Artistic Direction ">7.4.0. Artistic Direction</option>
                            </select>
                        </div>

                        {{-- Title1 --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Details" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 8: Scholarly Work in the Arts --}}
                <div id="8FormCat2Cat2" class="FormCat2Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat2' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 8) !!}

                        <div class="col-lg-12 form-group">

                            <select class="form-control" name="subsubcategory">
                                <option value="Authorship : Book">8.1.0. Authorship : Book</option>
                                <option value="Authorship : Short Works">8.2.0. Authorship : Short Works</option>
                            </select>

                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>


                        {{-- Title, vol. No. of Journal/Publication where annotated bibliography appears --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication</label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication" required
                                    >
                        </div>

                        {{-- Nature Of Publication --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature Of Publication (e.g
                                international) </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature Of Publication" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points</label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat2" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=inputFileAddCat2]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }

            }
        });
    });
</script>

{{-- FORMCAT2 CAT1 --}}
<script type="text/javascript">
    function FormCat2Cat1ShowDiv() {

        // make all the divs hidden
        var a = document.getElementsByClassName("FormCat2Cat1ShowDivTargetDiv");
        var b;
        for (b = 0; b < a.length; b++) {
            a[b].style.display = "none";
        }

        // make the selected value appear
        var selectFormCat2Cat1 = document.getElementById("FormCat2Cat1");

        var valueFormCat2Cat1 = selectFormCat2Cat1.options[selectFormCat2Cat1.selectedIndex].value;
        var divID = valueFormCat2Cat1 + "FormCat2Cat1";
        // alert(divID);
        document.getElementById(divID).style.display = "block";

    }

    // elem.onchange = function(){
    //     var showdiv = document.getElementById(val);
    //     showdiv.style.display = (this.value == val) ? "block":"none";
    // };

</script>

{{-- FORMCAT2 CAT2 --}}
<script type="text/javascript">
    function FormCat2Cat2ShowDiv() {

        // make all the divs hidden
        var a = document.getElementsByClassName("FormCat2Cat2ShowDivTargetDiv");
        var b;
        for (b = 0; b < a.length; b++) {
            a[b].style.display = "none";
        }

        // make the selected value appear
        var selectFormCat2Cat2 = document.getElementById("FormCat2Cat2");

        var valueFormCat2Cat2 = selectFormCat2Cat2.options[selectFormCat2Cat2.selectedIndex].value;
        var divID = valueFormCat2Cat2 + "FormCat2Cat2";
        // alert(divID);
        document.getElementById(divID).style.display = "block";

    }

    // elem.onchange = function(){
    //     var showdiv = document.getElementById(val);
    //     showdiv.style.display = (this.value == val) ? "block":"none";
    // };

</script>