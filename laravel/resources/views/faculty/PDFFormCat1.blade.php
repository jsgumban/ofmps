{{-- FORM CATEGORY 1 --}}
{{-- CATEGORY 1 --}}
<h3>I. TEACHING PERFORMANCE/OUTPUT</h3>

<h4>A. Performance Rating (SATE) </h4>
<table>
    <thead>
    <tr>
        <th>Weighted Avarage Rating</th>
        <th align="right">Points</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($performances as $performance)
        @if($performance->category==1 && $performance->subcategory==1 )
            <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                <td>{{ $performance->WARating }}</td>
                <td align="right">{{ $performance->points }}</td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>

{{-- CATEGORY 2 --}}
<br><br>
<h4>B. Teaching Portfolio/Other activities related to teaching</h4>
<div class="page-break">

    {{-- FC1C2SUBCAT1 --}}
    <div>
        <h5>1. Authorship of a UP (any UP unit) level textbook /audio-visual materials for one-semester course
            (recommended
            by unit for use in a course)
        </h5>
        <table>
            <thead>
            <tr>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published/Publisher</th>
                <th>Course Title</th>
                <th>Semester/AY used</th>
                <th align="right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==1 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->title}}</td>
                        <td>{{$performance->author}}</td>
                        <td>{{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                        <td>{{$performance->course_title}}</td>
                        <td>{{$performance->semester_used.'/ '.$performance->ay_used}}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>


    {{-- FC1C2SUBCAT2 --}}
    <br><br>

    <div>
        <h5> 2. Authorship of a UP laboratory/studies manual</h5>
        <table>
            <thead>
            <tr>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published/Publisher</th>
                <th>Course Title</th>
                <th>Semester/AY used</th>
                <th align="right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==2 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->title}}</td>
                        <td>{{$performance->author}}</td>
                        <td>{{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                        <td>{{$performance->course_title}}</td>
                        <td>{{$performance->semester_used.'/ '.$performance->ay_used}}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

    {{-- FC1C2SUBCAT3 --}}
    <br><br>

    <div>
        <h5> 3. Authorship of Modules (including wrap around text)</h5>
        <table>
            <thead>
            <tr>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published/Publisher</th>
                <th>Course Title</th>
                <th>Semester/AY used</th>
                <th align="right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==3 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->title}}</td>
                        <td>{{$performance->author}}</td>
                        <td>{{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                        <td>{{$performance->course_title}}</td>
                        <td>{{$performance->semester_used.'/ '.$performance->ay_used}}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

    {{-- FC1C2SUBCAT4 --}}
    <br><br>

    <div>
        <h5> 4. Revision/updating of UP textbook/laboratory/studio manuals</h5>
        <table>
            <thead>
            <tr>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published/Publisher</th>
                <th>Course Title</th>
                <th>Semester/AY used</th>
                <th align="right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==4 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->title}}</td>
                        <td>{{$performance->author}}</td>
                        <td>{{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                        <td>{{$performance->course_title}}</td>
                        <td>{{$performance->semester_used.'/ '.$performance->ay_used}}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>


    {{-- FC1C2SUBCAT5 --}}
    <br><br>

    <div>
        <h5> 5. Authorship of book review </h5>
        <table>
            <thead>
            <tr>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published</th>
                <th align="right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==5 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->title}}</td>
                        <td>{{$performance->author}}</td>
                        <td>{{date('F d, Y', strtotime( $performance->date_published ))}}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

    {{-- FC1C2SUBCAT6 --}}
    <br><br>

    <div>
        <h5> 6. Authorship of case studies </h5>
        <table>
            <thead>
            <tr>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published/Publisher</th>
                <th align="right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==6 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->title}}</td>
                        <td>{{$performance->author}}</td>
                        <td>{{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>


    {{-- FC1C2SUBCAT7 --}}
    <br><br>

    <div>
        <h5> 7. Authorship of annotated bibliography </h5>
        <table>
            <thead>
            <tr>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published</th>
                <th>Title, Vol. No. of Journal/Publication</th>
                <th>Nature of Publication</th>
                <th align="right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==7 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->title}}</td>
                        <td>{{$performance->author}}</td>
                        <td>{{date('F d, Y', strtotime( $performance->date_published ))}}</td>
                        <td>{{ $performance->bibliography }}</td>
                        <td>{{ $performance->nature_ofpublication }}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

    {{-- FC1C2SUBCAT8 --}}
    <br><br>

    <div>
        <h5> 8. Department endorsed audio-visual materials/course readers/laboratory/studio/manual/learning
            objects/computer software for instruction </h5>
        <table>
            <thead>
            <tr>
                <th>Title Material</th>
                <th align="right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==8 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->title}}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

    {{-- FC1C2SUBCAT9 --}}
    <br><br>

    <div>
        <h5>9. For each dissertation advisee graduated </h5>
        <table>
            <thead>
            <tr>
                <th>As</th>
                <th>Name Of Graduate</th>
                <th>Year Graduated</th>
                <th align="right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($performances as $performance)
                @if($performance->category==2 && $performance->subcategory==9 )
                    <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$performance->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                        <td>{{$performance->as_what}}</td>
                        <td>{{$performance->name_ofgraduate}}</td>
                        <td>{{$performance->year_graduated}}</td>
                        <td align="right">{{$performance->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
        <div>

            {{-- FC1C2SUBCAT10 --}}
            <br><br>

            <div>
                <h5>10. For each M.S. thesis advisee graduated </h5>
                <table>
                    <thead>
                    <tr>
                        <th>As</th>
                        <th>Name Of Graduate</th>
                        <th>Year Graduated</th>
                        <th align="right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)
                        @if($performance->category==2 && $performance->subcategory==10 )
                            <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td>{{$performance->as_what}}</td>
                                <td>{{$performance->name_ofgraduate}}</td>
                                <td>{{$performance->year_graduated}}</td>
                                <td align="right">{{$performance->points}}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>


            {{-- FC1C2SUBCAT11--}}
            <br><br>

            <div>
                <h5>11. Undergraduate Thesis/Special Problem Adviser: for every undergraduate thesis advisee who
                    graduated (max of 6 students per year)</h5>
                <table>
                    <thead>
                    <tr>
                        <th>As</th>
                        <th>Name Of Graduate</th>
                        <th>Year Graduated</th>
                        <th align="right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)
                        @if($performance->category==2 && $performance->subcategory==11 )
                            <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td>{{$performance->as_what}}</td>
                                <td>{{$performance->name_ofgraduate}}</td>
                                <td>{{$performance->year_graduated}}</td>
                                <td align="right">{{$performance->points}}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>

            {{-- FC1C2SUBCAT12 --}}
            <br><br>

            <div>
                <h5>12. Number of preparations per year; (SATE not lower than 2.0 in the combination of courses)</h5>
                <table>
                    <thead>
                    <tr>

                        <th># of Preparations</th>
                        <th align="right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)
                        @if($performance->category==2 && $performance->subcategory==12 )
                            <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td>{{$performance->number_ofpreparations}}</td>
                                <td align="right">{{$performance->points}}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>

            {{-- FC1C2SUBCAT13 --}}

            <br><br>

            <div>
                <h5>13. Other contributions to teaching</h5>
                <table>
                    <thead>
                    <tr>

                        <th>Other Contributions</th>
                        <th align="right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)
                        @if($performance->category==2 && $performance->subcategory==13 )
                            <tr data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td>{{$performance->other_contributions}}</td>
                                <td align="right">{{$performance->points}}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>


        </div>
        <div class="page-break"></div>
