@extends('app')
@section('title', 'OFMPS | MY PROFILE')
@section('content')
    @if(Auth::user())
        @if((Auth::user()->position)==5)
            <?php $preLink = FOLDERNAME . '/admin/'; ?>
            <?php $currentUser = 'admin'; ?>
        @elseif((Auth::user()->position)==4)
            <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
            <?php $currentUser = 'chancellor'; ?>
        @elseif((Auth::user()->position)==3)
            <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
            <?php $currentUser = 'vchancellor'; ?>
        @elseif((Auth::user()->position)==2)
            <?php $preLink = FOLDERNAME . '/dean/'; ?>
            <?php $currentUser = 'dean'; ?>
        @elseif((Auth::user()->position)==1)
            <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
            <?php $currentUser = 'deptchair'; ?>
        @elseif((Auth::user()->position)==0)
            <?php $preLink = FOLDERNAME . '/faculty/'; ?>
            <?php $currentUser = 'faculty'; ?>
        @endif
    @endif

    <div class="page animsition"><br>

        <div class="page-content">
            <div class="panel panel-bordered">
                <header class="panel-heading">
                    <h3 class="panel-title">
                        <b> MY PROFILE </b>
                    </h3>
                </header>
                <div class="panel-body">

                    {{-- NOTIFICATION --}}
                    @if(!empty(session('notification')))
                        <?php $notif = session('notification');?>
                        @if(strpos($notif,'SUCCESS')===false)
                            <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @else
                            <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @endif
                    @endif

                    {{-- CONTENT --}}
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="editableUser">
                            <tbody>


                            <tr>
                                <td>Name</td>
                                <td>
                                    {!! $user->first_name. ' '.$user->middle_name[0]. '. ' . $user->last_name !!}
                                </td>
                            </tr>

                            <tr>
                                <td>Gender</td>
                                <td>
                                    {!! $user->gender!!}
                                </td>
                            </tr>


                            <tr>
                                <td style="width:35%">Employee Code</td>
                                <td style="width:65%">
                                    {!! $user->employee_code !!}
                                </td>
                            </tr>


                            @if($user->position<5)
                                <tr>
                                    <td>Position</td>
                                    <td>
                                        @if(($user->position)==5)
                                            <?php echo 'System Administrator'; ?>
                                        @elseif(($user->position)==4)
                                            <?php echo 'Chancellor'; ?>
                                        @elseif(($user->position)==3)
                                            <?php echo 'Vice Chancellor'; ?>
                                        @elseif(($user->position)==2)
                                            <?php echo 'Dean'; ?>
                                        @elseif(($user->position)==1)
                                            <?php echo 'Department Chairperson'; ?>
                                        @elseif(($user->position)==0)
                                            <?php echo 'Faculty Member'; ?>
                                        @endif
                                    </td>
                                </tr>
                            @endif


                            <tr>
                                <td>User Type</td>
                                <td>
                                    @if(($user->position)==5)
                                        <?php echo 'System Administrator'; ?>
                                    @elseif(($user->position)<5)
                                        <?php echo 'Faculty User'; ?>
                                    @endif
                                </td>
                            </tr>


                            @if(($user->position)<5)
                                <tr>
                                    <td>Rank and Step</td>
                                    <td>
                                        {!! $user->rank.' '.$user->step !!}
                                    </td>
                                </tr>

                                <tr>
                                    <td>College</td>
                                    <td>
                                        {!! $collegeBelongs[0]->college_name!!}
                                    </td>
                                </tr>

                                <tr>
                                    <td>Department</td>
                                    <td>
                                        {!! $user->department_belongs !!}
                                    </td>
                                </tr>

                                <tr>
                                    <td>Last Promotion</td>
                                    <td>
                                        {!! $user->last_promotion !!}
                                    </td>
                                </tr>
                            @endif


                            <tr>
                                <td>Birthday</td>
                                <td>
                                    {{ date('F d, Y', strtotime($user->birthday )) }}
                                </td>
                            </tr>

                            <tr>
                                <td>Address</td>
                                <td>
                                    {!! $user->address !!}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="media">
                        <div align="right">
                            <a href="{!! $preLink.'profile/UpdateMyProfile' !!} ">
                                <button type="button" class="btn btn-info ladda-button" data-style="expand-right"
                                        data-plugin="ladda">
                                    <span class="ladda-label"><i class="icon wb-pencil margin-right-10"
                                                                 aria-hidden="true"></i>Update Profile</span>
                                    <span class="ladda-spinner"></span>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection