<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performance_tbl extends Model
{
    protected $table = 'performance_tbl';
    public $timestamps = false;

}
