<div class="list-group" data-plugin="nav-tabs" role="tablist">
    <div class="dd" data-plugin="nestable">
        @include('includes.Notification')
        @include('includes.Notes')
        <ol class="dd-list">
            @include('includes.Back-Button')

            <li class="dd-item dd-item-alt dd-collapsed" data-id="1">
                <div class="dd-handle"></div>
                <div class="dd-content">
                    <a data-toggle="tab" href="#category-1" aria-controls="category-1"
                       role="tab">
                        <font color="#76838f"><b>I. TEACHING PERFORMANCE/OUTPUT</b></font>
                    </a>
                    {{-- CATEGORY 1 TOTAL SCORE  --}}
                    <span class="pull-right">
                                                    <span class="badge badge-success"
                                                          id="cat1PointsDisplay">  {{$cat1Total }}</span>
                                                </span>
                </div>
                <ol class="dd-list">
                    <li class="dd-item dd-item-alt " data-id="4">
                        <div class="dd-handle"></div>
                        <div class="dd-content">
                            @if($disableSateBtn==0)
                                <button type="button" id="SubCatAddButton-FC1C1"
                                        class="btn btn-icon btn-xs btn-success"
                                        data-target="#exampleModalWarning1"
                                        data-toggle="modal"><i
                                            class="icon wb-plus" aria-hidden="true"></i>
                                </button>
                            @else
                                <a href="javascript: void(0)" data-toggle="tooltip"
                                   title="Not allowed">
                                    <button type="button" id="SubCatAddButton-FC1C1"
                                            class="btn btn-icon btn-xs btn-success"
                                            disabled><i
                                                class="icon wb-plus" aria-hidden="true"></i>
                                    </button>
                                </a>
                            @endif

                            &nbsp;
                            <a data-toggle="tab" href="#category-1"
                               aria-controls="category-1"
                               role="tab">
                                A. Performance Rating (SATE)
                            </a>
                        </div>
                    </li>
                    <li class="dd-item dd-item-alt" data-id="5" data-foo="bar">
                        <div class="dd-handle"></div>
                        <div class="dd-content">
                            <button type="button" class="btn btn-icon btn-xs btn-success"
                                    id="SubCatAddButton-FC1C2"
                                    data-target="#exampleModalWarning2"
                                    data-toggle="modal"><i
                                        class="icon wb-plus" aria-hidden="true"></i>
                            </button>
                            &nbsp;
                            <a data-toggle="tab" href="#category-1"
                               aria-controls="category-1"
                               role="tab">
                                B. Teaching Portfolio/Other Activities
                            </a>
                        </div>
                    </li>
                </ol>
            </li>

            <li class="dd-item dd-item-alt dd-collapsed" data-id="1">
                <div class="dd-handle"></div>
                <div class="dd-content">
                    <a data-toggle="tab" href="#category-2" aria-controls="category-1"
                       role="tab">
                        <font color="#76838f"><b>II. SCHOLARLY OR CREATIVE WORK</b></font>
                    </a>
                    {{-- CATEGORY 2 TOTAL SCORE  --}}
                    <span class="pull-right">
                                                    <span class="badge badge-info"
                                                          id="cat1PointsDisplay">  {{$cat2Total }}</span>
                                                </span>
                </div>
                <ol class="dd-list">
                    <li class="dd-item dd-item-alt" data-id="4">
                        <div class="dd-handle"></div>
                        <div class="dd-content">
                            <button type="button" class="btn btn-icon btn-xs btn-info"
                                    data-target="#exampleModalWarning3"
                                    id="SubCatAddButton-FC2C1"
                                    data-toggle="modal"><i
                                        class="icon wb-plus" aria-hidden="true"></i>
                            </button>
                            &nbsp;
                            <a data-toggle="tab" href="#category-2"
                               aria-controls="category-2"
                               role="tab">
                                A. Published Scholarly Works
                            </a>
                        </div>
                    </li>
                    <li class="dd-item dd-item-alt" data-id="5" data-foo="bar">
                        <div class="dd-handle"></div>
                        <div class="dd-content">
                            <button type="button" class="btn btn-icon btn-xs btn-info"
                                    id="SubCatAddButton-FC2C2"
                                    data-target="#exampleModalWarning4" data-toggle="modal">
                                <i
                                        class="icon wb-plus" aria-hidden="true"></i>
                            </button>
                            &nbsp;
                            <a data-toggle="tab" href="#category-2"
                               aria-controls="category-2"
                               role="tab">
                                B. Creative Works
                            </a>

                        </div>
                    </li>
                </ol>
            </li>

            <li class="dd-item dd-item-alt dd-collapsed" data-id="1">
                <div class="dd-handle"></div>
                <div class="dd-content">
                    <a data-toggle="tab" href="#category-3" aria-controls="category-1"
                       role="tab">
                        <font color="#76838f"><b>III. SERVICE</b></font>
                    </a>
                    {{-- CATEGORY 2 TOTAL SCORE  --}}
                    <span class="pull-right">
                                                    <span class="badge badge-warning"
                                                          id="cat1PointsDisplay">  {{$cat3Total }}</span>
                                                </span>
                </div>
                <ol class="dd-list">
                    <li class="dd-item dd-item-alt" data-id="4">
                        <div class="dd-handle"></div>
                        <div class="dd-content">
                            <button type="button" class="btn btn-icon btn-xs btn-warning"
                                    id="SubCatAddButton-FC3C1"
                                    data-target="#exampleModalWarning5" data-toggle="modal">
                                <i
                                        class="icon wb-plus" aria-hidden="true"></i>
                            </button>
                            &nbsp;
                            <a data-toggle="tab" href="#category-3"
                               aria-controls="category-3"
                               role="tab">
                                A. Department/College/University
                            </a>
                        </div>
                    </li>
                    <li class="dd-item dd-item-alt" data-id="5" data-foo="bar">
                        <div class="dd-handle"></div>
                        <div class="dd-content">
                            <button type="button" class="btn btn-icon btn-xs btn-warning"
                                    id="SubCatAddButton-FC3C2"
                                    data-target="#exampleModalWarning6" data-toggle="modal">
                                <i
                                        class="icon wb-plus" aria-hidden="true"></i>
                            </button>
                            &nbsp;
                            <a data-toggle="tab" href="#category-3"
                               aria-controls="category-3"
                               role="tab">
                                B. International/Regional/National
                            </a>
                        </div>
                    </li>
                </ol>
            </li>

            <li class="dd-item dd-item-alt dd-collapsed" data-id="1">
                <div class="dd-handle"></div>
                <div class="dd-content">
                    <a data-toggle="tab" href="#category-4" aria-controls="category-1"
                       role="tab">
                        <font color="#76838f"><b>IV. PROFESSIONAL GROWTH</b></font>
                    </a>
                    {{-- CATEGORY 2 TOTAL SCORE  --}}
                    <span class="pull-right">
                                                    <span class="badge badge-danger"
                                                          id="cat1PointsDisplay">  {{$cat4Total }}</span>
                                                </span>
                </div>
                <ol class="dd-list">
                    <li class="dd-item dd-item-alt" data-id="4">
                        <div class="dd-handle"></div>
                        <div class="dd-content">
                            <button type="button" class="btn btn-icon btn-xs btn-danger"
                                    id="SubCatAddButton-FC4C1"
                                    data-target="#exampleModalWarning7" data-toggle="modal">
                                <i
                                        class="icon wb-plus" aria-hidden="true"></i>
                            </button>
                            &nbsp;
                            <a data-toggle="tab" href="#category-4"
                               aria-controls="category-4"
                               role="tab">
                                A. Professional Growth
                            </a>
                        </div>
                    </li>
                </ol>
            </li>
        </ol>
    </div>
    <span class="label label-dark pull-right"> TOTAL : {{$overAllTotal.' Points' }}</span><br>
</div>

