{{-- FORM CATEGORY 3 MODAL --}}
<div class="modal fade modal-warning  example-modal-lg" id="UpdateFC3-modal-1" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                {{-- RIBBONS --}}
                @if (Request::is('*/ReviseForm') || Request::is('*/EvaluateForm') || Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-rejected">
                        <span class="ribbon-inner">REJECTED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-revise">
                        <span class="ribbon-inner">NEEDS REVISION</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-notyet">
                        <span class="ribbon-inner">NOT YET EVALUATED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-accept">
                        <span class="ribbon-inner">ACCEPTED</span>
                    </div>
                @endif
                <h4 class="modal-title">A. SERVICE TO DEPARTMENT/COLLEGE/UNIVERSITY </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="UpdateFC3-SubSubCat-1" disabled>
                        <option value=1>01. Regular administrative positions in the College/Institute/Department
                        </option>
                        <option value=2>02. Membership in a standing committee of the
                            Institute/Department/College/University
                        </option>
                        <option value=3>03. Resource Generation (at least PhP 1M; over and above research funds
                            contributed to the unit, college or university)
                        </option>
                        <option value=4>04. Officer-in-Charge to the Chancellor/Vice-Chancellor/Dean/Director/College
                            Secretary/Department Head
                        </option>
                    </select>
                </div>

                {{-- SUBCATEGORY 1--}}
                <div id="UpdateFC3-divID-1-1" class="UpdateFC3-divClass-1">
                    <form action="#" method="POST" id="UpdateFC3-form-1-1"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Designation</label>
                            <select class="form-control" name="designation" id="UpdateFC3-designation-1-1">
                                <option value="Chancellor">1.1. Chancellor
                                </option>
                                <option value="Vice Chancellor">1.2. Vice Chancellor
                                </option>
                                <option value="Dean">1.3. Dean
                                </option>
                                <option value="Associate Dean">1.4. Associate Dean
                                </option>
                                <option value="University Registrar">1.5. University Registrar
                                </option>
                                <option value="College Secretaries">1.6. College Secretaries
                                </option>
                                <option value="Department Heads">1.7. Department Heads
                                </option>
                                <option value="Assistant College Secretary/Academic/Administrative Unit Director">1.8.
                                    Assistant College Secretary/Academic/Administrative Unit Director
                                </option>
                                <option value="Deputy Directors/Assistant to the Chair">1.9. Deputy Directors/Assistant
                                    to the Chair
                                </option>
                                <option value="Program Coordinators">1.10. Program Coordinators
                                </option>
                                <option value="Chair of Standing Committees">1.11. Chair of Standing Committees
                                </option>
                            </select>
                        </div>

                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required id="UpdateFC3-period_covered-1-1">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover" data-placement="right"
                                   data-content="
                                        <small><small>  <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> Chancellor/Vice Chancellor/Deans </td>
                                            <td>20max/year</td>
                                        </tr>
                                        <tr>
                                            <td> Associate  Deans/University Registrar/ CollegeSecretaries/Department Heads </td>
                                            <td>17.5max/year</td>
                                        </tr>

                                        <tr>
                                            <td> Assistant College Secretary/Academic/Administrative Unit Director</td>
                                            <td>15max/year</td>
                                        </tr>
                                        
                                        <tr>
                                            <td> Deputy Directors/Assistant to the Chair </td>
                                            <td>12.5max/year</td>
                                        </tr>

                                        <tr>
                                            <td>Program Coordinators   </td>
                                            <td>10.5max/year</td>
                                        </tr>

                                        <tr>
                                            <td> Chair of Standing Committees/Course Coordinators   </td>
                                            <td>5max/year</td>
                                        </tr>

                                        </tbody>
                                        </table> </small></small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="20" required id="UpdateFC3-points-1-1">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC3-other_details-1-1">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC3-proofs-1-1">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c3radio1-1-1" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c3radio2-1-1" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c3radio3-1-1" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c3BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-warning">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC3-reviseDiv-1-1">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-1-1"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC3-revise-1-1">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-1-1"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>


                {{-- SUBCATEGORY 2--}}
                <div id="UpdateFC3-divID-1-2" class="UpdateFC3-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC3-form-1-2"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 2) !!}


                        {{-- Name of Committee --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Committee</label>
                            <input type="text" class="form-control" name="name_ofcommittee"
                                   placeholder="Name of Committee" required id="UpdateFC3-name_ofcommittee-1-2">
                        </div>

                        {{-- Nature of Committee --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Committee</label>
                            <input type="text" class="form-control" name="nature_ofcommittee"
                                   placeholder="Nature of Committee" required id="UpdateFC3-nature_ofcommittee-1-2">
                        </div>


                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required id="UpdateFC3-period_covered-1-2">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small><table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <tr>
                                            <td> Membership in a standing committee of the Institute/ Department/ College/University   </td>
                                            <td>6 committees max/year</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="6" required id="UpdateFC3-points-1-2">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC3-other_details-1-2">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC3-proofs-1-2">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c3radio1-1-2" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c3radio2-1-2" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c3radio3-1-2" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c3BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-warning">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC3-reviseDiv-1-2">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-1-2"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC3-revise-1-2">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-1-2"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 3 --}}
                <div id="UpdateFC3-divID-1-3" class="UpdateFC3-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC3-form-1-3"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 3) !!}


                        {{-- Funding Source --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Funding Source</label>
                            <input type="text" class="form-control" name="funding_source"
                                   placeholder="Funding Source" required id="UpdateFC3-funding_source-1-3">
                        </div>

                        {{-- Purpose of Fund --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Purpose of Fund</label>
                            <input type="text" class="form-control" name="purpose_offund"
                                   placeholder="Purpose of Fund" required id="UpdateFC3-purpose_offund-1-3">
                        </div>


                        {{-- Amount Donated --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Amount Donated</label>
                            <input type="text" class="form-control" name="ammount_donated"
                                   placeholder="Amount Donated" required id="UpdateFC3-ammount_donated-1-3">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small>  <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Resource Generation over and above research funds contributed to the unit, college or university </td>
                                            <td>1 pt every Php20,000</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC3-points-1-3">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC3-other_details-1-3">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC3-proofs-1-3">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c3radio1-1-3" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c3radio2-1-3" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c3radio3-1-3" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c3BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-warning">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC3-reviseDiv-1-3">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-1-3"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC3-revise-1-3">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-1-3"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 4--}}
                <div id="UpdateFC3-divID-1-4" class="UpdateFC3-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC3-form-1-4"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 4) !!}


                        {{-- Designation of OIC --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Designation of OIC</label>
                            <input type="text" class="form-control" name="designation_ofoic"
                                   placeholder="Designation of OIC" required id="UpdateFC3-designation_ofoic-1-4">
                        </div>

                        {{-- Level  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Level</label>
                            <input type="text" class="form-control" name="level"
                                   placeholder="Level " required id="UpdateFC3-level-1-4">
                        </div>


                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required id="UpdateFC3-period_covered-1-4">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Officer-in-charge to the Chancellor/Vice Chancellor/Dean/ Director/College Secretary/Department Head </td>
                                            <td>0.5 pt/week (or 5 working days)</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required id="UpdateFC3-points-1-4">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC3-other_details-1-4">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC3-proofs-1-4">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c3radio1-1-4" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c3radio2-1-4" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c3radio3-1-4" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c3BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-warning">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC3-reviseDiv-1-4">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-1-4"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC3-revise-1-4">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-1-4"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>


            </div>
        </div>
    </div>
</div>

{{-- FORM CATEGORY 3 MODAL --}}
<div class="modal fade modal-warning  example-modal-lg" id="UpdateFC3-modal-2" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                {{-- RIBBONS --}}
                @if (Request::is('*/ReviseForm') || Request::is('*/EvaluateForm') || Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-rejected">
                        <span class="ribbon-inner">REJECTED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-revise">
                        <span class="ribbon-inner">NEEDS REVISION</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-notyet">
                        <span class="ribbon-inner">NOT YET EVALUATED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-accept">
                        <span class="ribbon-inner">ACCEPTED</span>
                    </div>
                @endif

                <h4 class="modal-title">B. SERVICE TO INTERNATIONAL/NATIONAL/REGIONAL COMMUNITY </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="UpdateFC3-SubSubCat-2" disabled>
                        <option value=1>01. Presidency/Chairmanship of Professional association/organization
                        </option>
                        <option value=2>02. Position of responsibility in Professional association/organization</option>
                    </select>
                </div>

                {{-- SUBCATEGORY 1--}}
                <div id="UpdateFC3-divID-2-1" class="UpdateFC3-divClass-2">
                    <form action="#" method="POST" id="UpdateFC3-form-2-1"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subsubcategory" id="UpdateFC3-subsubcategory-2-1">
                                <option value=" International/Regional">1.1. International/Regional
                                </option>
                                <option value="National (Local)">1.2. National (Local)
                                </option>

                            </select>
                        </div>


                        {{-- Name of Organization --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Organization</label>
                            <input type="text" class="form-control" name="name_oforganization"
                                   placeholder="Name of Organization" required id="UpdateFC3-name_oforganization-2-1">
                        </div>

                        {{-- Designation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Designation </label>
                            <input type="text" class="form-control" name="designation"
                                   placeholder="Designation " required id="UpdateFC3-designation-2-1">
                        </div>


                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required id="UpdateFC3-period_covered-2-1">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>International/Regional</td>
                                            <td>4pts</td>
                                        </tr>
                                        <tr>
                                            <td>National (Local) </td>
                                            <td>2pts</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required id="UpdateFC3-points-2-1">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC3-other_details-2-1">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC3-proofs-2-1">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c3radio1-2-1" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c3radio2-2-1" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c3radio3-2-1" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c3BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-warning">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC3-reviseDiv-2-1">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-2-1"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC3-revise-2-1">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-2-1"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- SUBCATEGORY 2--}}
                <div id="UpdateFC3-divID-2-2" class="UpdateFC3-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC3-form-2-2"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 2) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subsubcategory" id="UpdateFC3-subsubcategory-2-2">
                                <option value="International/Regional">2.1. International/Regional
                                </option>
                                <option value="National (Local)">2.2. National (Local)
                                </option>

                            </select>
                        </div>


                        {{-- >Name of Organization --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Organization</label>
                            <input type="text" class="form-control" name="name_oforganization"
                                   placeholder="Name of Organization" required id="UpdateFC3-name_oforganization-2-2">
                        </div>

                        {{-- Designation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Designation </label>
                            <input type="text" class="form-control" name="designation"
                                   placeholder="Designation " required id="UpdateFC3-designation-2-2">
                        </div>


                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required id="UpdateFC3-period_covered-2-2">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>International/Regional</td>
                                            <td>4pts</td>
                                        </tr>
                                        <tr>
                                            <td>National (Local) </td>
                                            <td>2pts</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required id="UpdateFC3-points-2-2">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC3-other_details-2-2">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC3-proofs-2-2">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c3radio1-2-2" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c3radio2-2-2" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c3radio3-2-2" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c3BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-warning">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC3-reviseDiv-2-2">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-2-2"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC3-revise-2-2">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC3-delete-2-2"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $("[id^=UpdateFC3-]").attr('disabled', true);
        $("[id^=UpdateFC3-proofs-]").css('display', "none");
        $(".btn").css('display', "none");
        @if(Auth::user()->position==4)
            $("[id^=chancellorEvaluation-]").css('display', "block");
        @endif
    </script>
@endif

@if(Request::is('*/EvaluateForm'))
    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $("[id^=UpdateFC3-]").attr('disabled', true);
        $("[id^=UpdateFC3-proofs-]").css('display', "none");
    </script>

    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("input[name*='evaluateAction']").click(function (e) {
                // accept
                if (this.value == 1) {
                    $("[id^=UpdateFC3-points-]").attr('disabled', false);
                    $("[id^=UpdateFC3-points-]").css('background-color', '#FFEFD5');
                    $("[id^=UpdateFC3-points-]").css('color', 'black');
                }

                // reject or revise
                else if (this.value == 2 || this.value == 3) {
                    points = $("[id^=labelauty]").attr("data-points");
                    $("[id^=UpdateFC3-points]").val(points);
                    $("[id^=UpdateFC3-points]").attr('disabled', true);
                    $("[id^=UpdateFC3-points-]").css('background-color', '');
                    $("[id^=UpdateFC3-points-]").css('color', '');
                }
            });
        });
    </script>
@endif

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=c3radio]").click(function (e) {
            if (this.value == 3) {
                $("[id^=c3BrowseDiv]").css('display', 'none');
            } else {
                $("[id^=c3BrowseDiv]").css('display', 'block');
            }
        });
    });
</script>

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=inputFileUpdateCat3]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }

            }
        });
    });
</script>

