<?php
Route::group(['middleware' => 'dean'], function () {
    Route::get('dean/home', 'DeanController@index');

    /////////////////
    // OWN PROFILE //
    /////////////////
    Route::get('dean/profile/MyProfile', ['uses' => 'AdminController@MyProfile']);
    Route::get('dean/profile/UpdateMyProfile', ['uses' => 'AdminController@UpdateMyProfile']);
    Route::post('dean/profile/SaveUpdateMyProfile', ['uses' => 'AdminController@SaveUpdateMyProfile']);
    Route::get('dean/profile/UpdateMyPassword', ['uses' => 'AdminController@UpdateMyPassword']);
    Route::post('dean/profile/SaveUpdateMyPassword', ['uses' => 'AdminController@SaveUpdateMyPassword']);

    /////////////////////
    // FACULTY MEMBERS //
    /////////////////////
    Route::get('dean/dapc/{id}/RetrieveUserData', ['uses' => 'DeptChairController@RetrieveUserData']);
    Route::get('dean/dapc/{id}/RetrieveSubmittedForms', ['uses' => 'DeptChairController@RetrieveSubmittedForms']);
    Route::get('dean/meritpromotion/{id}/RetrieveUserInfo', ['uses' => 'DeptChairController@RetrieveUserInfo']);

    /////////////////////
    // MERIT PROMOTION //
    /////////////////////
    Route::get('dean/capc', ['uses' => 'DeanController@CAPC']);
    Route::get('dean/capc/{id}/ListOfSubmittedForms', ['uses' => 'DeanController@ListOfSubmittedForms']);
    
    Route::get('dean/meritpromotion', ['uses' => 'FacultyController@MeritPromotion']);
    Route::post('dean/meritpromotion/AddNewForm', ['uses' => 'FacultyController@AddNewForm']);
    Route::get('dean/meritpromotion/{id}/FillOutMeritPromotion', ['uses' => 'FacultyController@FillOutMeritPromotionCheck']);
    Route::get('dean/meritpromotion/SaveAsDraft', ['uses' => 'FacultyController@SaveAsDraft']);
    Route::get('dean/meritpromotion/{id}/SubmitDeptChair', ['uses' => 'FacultyController@SubmitDeptChair']);
    Route::get('dean/meritpromotion/{id}/MonitorForm', ['uses' => 'FacultyController@MonitorForm']);

    Route::get('dean/meritpromotion/{id}/EvaluateForm', ['uses' => 'DeanController@EvaluateForm']);
    Route::post('dean/meritpromotion/{id}/RejectCAPC', ['uses' => 'DeanController@RejectCAPC']);
    Route::post('dean/meritpromotion/{id}/SubmitVChancellor', ['uses' => 'DeanController@SubmitVChancellor']);
    Route::get('dean/meritpromotion/{id}/RetrieveFormInfo', ['uses' => 'DeptChairController@RetrieveFormInfo']);
    Route::get('dean/meritpromotion/{id}/CAPCMonitorForm', ['uses' => 'DeanController@CAPCMonitorForm']);

    Route::get('dean/meritpromotion/{id}/generateZip', ['uses' => 'FacultyController@generateZip']);
    Route::get('dean/meritpromotion/{id}/generatePDF', ['uses' => 'FacultyController@generatePDF']);
    Route::get('dean/meritpromotion/{id}/ReviseForm', ['uses' => 'FacultyController@ReviseForm']);
    Route::get('dean/meritpromotion/{id}/UpdateForm', ['uses' => 'FacultyController@UpdateForm']);
    Route::get('dean/meritpromotion/{id}/DeleteDraftForm', ['uses' => 'FacultyController@DeleteDraftForm']);

    Route::post('dean/meritpromotion/{id}/SubmitFormCat1', ['uses' => 'FacultyController@SubmitFormCat1']);
    Route::post('dean/meritpromotion/{id}/SubmitFormCat2', ['uses' => 'FacultyController@SubmitFormCat2']);
    Route::post('dean/meritpromotion/{id}/SubmitFormCat3', ['uses' => 'FacultyController@SubmitFormCat3']);
    Route::post('dean/meritpromotion/{id}/SubmitFormCat4', ['uses' => 'FacultyController@SubmitFormCat4']);

    Route::get('dean/meritpromotion/{id}/RetrieveCat1', ['uses' => 'FacultyController@RetrieveCat1']);
    Route::get('dean/meritpromotion/{id}/RetrieveCat2', ['uses' => 'FacultyController@RetrieveCat2']);
    Route::get('dean/meritpromotion/{id}/RetrieveCat3', ['uses' => 'FacultyController@RetrieveCat3']);
    Route::get('dean/meritpromotion/{id}/RetrieveCat4', ['uses' => 'FacultyController@RetrieveCat4']);

    Route::post('dean/meritpromotion/{id}/UpdateFormCat1', ['uses' => 'FacultyController@UpdateFormCat1']);
    Route::post('dean/meritpromotion/{id}/UpdateFormCat2', ['uses' => 'FacultyController@UpdateFormCat2']);
    Route::post('dean/meritpromotion/{id}/UpdateFormCat3', ['uses' => 'FacultyController@UpdateFormCat3']);
    Route::post('dean/meritpromotion/{id}/UpdateFormCat4', ['uses' => 'FacultyController@UpdateFormCat4']);

    Route::get('dean/meritpromotion/{id}/DeleteEntryCat1', ['uses' => 'FacultyController@DeleteEntryCat1']);
    Route::get('dean/meritpromotion/{id}/DeleteEntryCat2', ['uses' => 'FacultyController@DeleteEntryCat2']);
    Route::get('dean/meritpromotion/{id}/DeleteEntryCat3', ['uses' => 'FacultyController@DeleteEntryCat3']);
    Route::get('dean/meritpromotion/{id}/DeleteEntryCat4', ['uses' => 'FacultyController@DeleteEntryCat4']);
});
?>