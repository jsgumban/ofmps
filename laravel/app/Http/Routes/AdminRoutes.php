<?php
Route::group(['middleware' => 'admin'], function () {
    
    Route::get('admin/home', 'AdminController@index');

    ////////////////
    // MY PROFILE //
    ////////////////
    Route::get('admin/profile/MyProfile', ['uses' => 'AdminController@MyProfile']);
    Route::get('admin/profile/UpdateMyProfile', ['uses' => 'AdminController@UpdateMyProfile']);
    Route::post('admin/profile/SaveUpdateMyProfile', ['uses' => 'AdminController@SaveUpdateMyProfile']);
    Route::get('admin/profile/UpdateMyPassword', ['uses' => 'AdminController@UpdateMyPassword']);
    Route::post('admin/profile/SaveUpdateMyPassword', ['uses' => 'AdminController@SaveUpdateMyPassword']);

    /////////////////////////
    // UNIVERSITY SETTINGS //
    /////////////////////////
    Route::get('admin/university/UniversitySettings', ['uses' => 'AdminController@UniversitySettings']);
    Route::post('admin/university/SaveUpdateUniversity', ['uses' => 'AdminController@SaveUpdateUniversity']);
    Route::get('admin/university/{id}/RemoveCollege', ['uses' => 'AdminController@RemoveCollege']);
    Route::post('admin/university/SaveNewCollege', ['uses' => 'AdminController@SaveNewCollege']);
    Route::get('admin/university/{id}/ViewMoreInfoCollege', ['uses' => 'AdminController@ViewMoreInfoCollege']);
    Route::post('admin/university/{id}/SaveUpdatedCollege', ['uses' => 'AdminController@SaveUpdatedCollege']);
    Route::get('admin/university/{id}/RetrieveCollege', ['uses' => 'AdminController@RetrieveCollege']);
    Route::get('admin/university/{id}/RetrieveCollegeDean', ['uses' => 'AdminController@RetrieveCollegeDean']);

    Route::get('admin/university/{id}/RemoveDepartment', ['uses' => 'AdminController@RemoveDepartment']);
    Route::post('admin/university/SaveNewDepartment', ['uses' => 'AdminController@SaveNewDepartment']);
    Route::get('admin/university/{id}/ViewMoreInfoDepartment', ['uses' => 'AdminController@ViewMoreInfoDepartment']);
    Route::post('admin/university/{id}/SaveUpdatedDepartment', ['uses' => 'AdminController@SaveUpdatedDepartment']);
    Route::get('admin/university/{id}/RetrieveDepartment', ['uses' => 'AdminController@RetrieveDepartment']);
    Route::get('admin/university/{id}/RetrieveDeptChair', ['uses' => 'AdminController@RetrieveDeptChair']);

    Route::get('admin/dapc/{id}/RetrieveUserData', ['uses' => 'DeptChairController@RetrieveUserData']);
    Route::get('admin/userprofiles/{id}/DeleteProfile', ['uses' => 'AdminController@DeleteProfile']);
    Route::get('admin/userprofiles/{id}/ResetPassword', ['uses' => 'AdminController@ResetPassword']);
    Route::post('admin/userprofiles/SaveNewUser', ['uses' => 'AdminController@SaveNewUser']);
    Route::post('admin/userprofiles/{employee_code}/SaveProfile', ['uses' => 'AdminController@SaveProfile']);

    /////////////////////
    // MERIT PROMOTION //
    /////////////////////
    Route::get('admin/meritpromotion/ViewMeritPromotionSettings', ['uses' => 'AdminController@ViewMeritPromotionSettings']);
    Route::post('admin/meritpromotion/SaveDate', ['uses' => 'AdminController@SaveDate']);
    Route::get('admin/meritpromotion/{id}/DeleteDate', ['uses' => 'AdminController@DeleteDate']);
    Route::post('admin/meritpromotion/{id}/UpdateDate', ['uses' => 'AdminController@UpdateDate']);
});
?>