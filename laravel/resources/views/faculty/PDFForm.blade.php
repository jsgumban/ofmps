<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        font-size: 11px;
        background: #fff;
        width: 100%;
        border-collapse: collapse;
        text-align: left;
    }

    [data-isaccepteddeptchair=

    2
    ]
    {
        background: gray
    ;
        color: black
    ;
    }

    [data-isaccepteddean=

    2
    ]
    {
        background: gray
    ;
        color: black
    ;
    }

    [data-isacceptedvchancellor=

    2
    ]
    {
        background: gray
    ;
        color: black
    ;
    }

    th {
        font-size: 12px;
        font-weight: normal;
        color: black;
        padding: 10px 8px;
        border-bottom: 1px solid #6678b1;
        font-weight: bold;
        text-align: left;
    }

    td {
        border-bottom: 1px solid #6678b1;
        color: black;
        padding: 6px 8px;
    }

    .page-break {
        page-break-after: always;

    }

    body {
        width: 100%;
    }

    h1, h2, h3, h4, h5 {
        margin-top: 0px;
        margin-bottom: 2px;
    }

    @page {
        margin-left: 4em;
        margin-right: 4em;
        margin-bottom: 4em;
    }

</style>

<body>
<h3 align="center"><b>UNIVERSITY OF THE PHILIPPINES IN MINDANAO</b></h3>

<h3 align="center"><b>ACCOMPLISHMENT FORM FOR MERIT PROMOTION FOR FACULTY</b></h3>

Date: {{date('F d, Y', strtotime( $mpromotionFormInfo->fillOut_date ))}} <br>
Name: {{$usersInfo[0]->first_name.' '.$usersInfo[0]->middle_name[0].'. '.$usersInfo[0]->last_name}}<br>
Rank and Step: {{ $usersInfo[0]->rank.' '.$usersInfo[0]->step }}<br>
College/Department: {{$collegeName[0]->college_name.'/'.$usersInfo[0]->department_belongs}}<br>
Date of last promotion/merit/increase/original appointment: {{ $usersInfo[0]->last_promotion }}<br><br>

@include('faculty.PDFFormCat1')
@include('faculty.PDFFormCat2')
@include('faculty.PDFFormCat3')
@include('faculty.PDFFormCat4')
</body>


