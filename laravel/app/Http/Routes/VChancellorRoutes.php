<?php
Route::group(['middleware' => 'vchancellor'], function () {
    Route::get('vchancellor/home', 'VChancellorController@index');

    /////////////////
    // OWN PROFILE //
    /////////////////
    Route::get('vchancellor/profile/MyProfile', ['uses' => 'AdminController@MyProfile']);
    Route::get('vchancellor/profile/UpdateMyProfile', ['uses' => 'AdminController@UpdateMyProfile']);
    Route::post('vchancellor/profile/SaveUpdateMyProfile', ['uses' => 'AdminController@SaveUpdateMyProfile']);
    Route::get('vchancellor/profile/UpdateMyPassword', ['uses' => 'AdminController@UpdateMyPassword']);
    Route::post('vchancellor/profile/SaveUpdateMyPassword', ['uses' => 'AdminController@SaveUpdateMyPassword']);

    /////////////////////
    // FACULTY MEMBERS //
    /////////////////////
    Route::get('vchancellor/dapc/{id}/RetrieveUserData', ['uses' => 'DeptChairController@RetrieveUserData']);
    Route::get('vchancellor/dapc/{id}/RetrieveSubmittedForms', ['uses' => 'DeptChairController@RetrieveSubmittedForms']);
    Route::get('vchancellor/meritpromotion/{id}/RetrieveUserInfo', ['uses' => 'DeptChairController@RetrieveUserInfo']);

    /////////////////////
    // MERIT PROMOTION //
    /////////////////////
    Route::get('vchancellor/uapfc', ['uses' => 'VChancellorController@UAPFC']);
    Route::get('vchancellor/uapfc/{id}/ListOfSubmittedForms', ['uses' => 'VChancellorController@ListOfSubmittedForms']);

    Route::get('vchancellor/meritpromotion', ['uses' => 'FacultyController@MeritPromotion']);
    Route::post('vchancellor/meritpromotion/AddNewForm', ['uses' => 'FacultyController@AddNewForm']);
    Route::get('vchancellor/meritpromotion/{id}/FillOutMeritPromotion', ['uses' => 'FacultyController@FillOutMeritPromotion']);
    Route::get('vchancellor/meritpromotion/SaveAsDraft', ['uses' => 'FacultyController@SaveAsDraft']);
    Route::get('vchancellor/meritpromotion/{id}/SubmitDeptChair', ['uses' => 'FacultyController@SubmitDeptChair']);
    Route::get('vchancellor/meritpromotion/{id}/MonitorForm', ['uses' => 'FacultyController@MonitorForm']);

    Route::get('vchancellor/meritpromotion/{id}/EvaluateForm', ['uses' => 'VChancellorController@EvaluateForm']);
    Route::post('vchancellor/meritpromotion/{id}/RejectUAPFC', ['uses' => 'VChancellorController@RejectUAPFC']);
    Route::post('vchancellor/meritpromotion/{id}/SubmitChancellor', ['uses' => 'VChancellorController@SubmitChancellor']);
    Route::get('vchancellor/meritpromotion/{id}/RetrieveFormInfo', ['uses' => 'DeptChairController@RetrieveFormInfo']);
    Route::get('vchancellor/meritpromotion/{id}/UAPFCMonitorForm', ['uses' => 'VChancellorController@UAPFCMonitorForm']);
    
    Route::get('vchancellor/meritpromotion/{id}/generateZip', ['uses' => 'FacultyController@generateZip']);
    Route::get('vchancellor/meritpromotion/{id}/generatePDF', ['uses' => 'FacultyController@generatePDF']);
    Route::get('vchancellor/meritpromotion/{id}/ReviseForm', ['uses' => 'FacultyController@ReviseForm']);
    Route::get('vchancellor/meritpromotion/{id}/UpdateForm', ['uses' => 'FacultyController@UpdateForm']);
    Route::get('vchancellor/meritpromotion/{id}/DeleteDraftForm', ['uses' => 'FacultyController@DeleteDraftForm']);

    Route::post('vchancellor/meritpromotion/{id}/SubmitFormCat1', ['uses' => 'FacultyController@SubmitFormCat1']);
    Route::post('vchancellor/meritpromotion/{id}/SubmitFormCat2', ['uses' => 'FacultyController@SubmitFormCat2']);
    Route::post('vchancellor/meritpromotion/{id}/SubmitFormCat3', ['uses' => 'FacultyController@SubmitFormCat3']);
    Route::post('vchancellor/meritpromotion/{id}/SubmitFormCat4', ['uses' => 'FacultyController@SubmitFormCat4']);

    Route::get('vchancellor/meritpromotion/{id}/RetrieveCat1', ['uses' => 'FacultyController@RetrieveCat1']);
    Route::get('vchancellor/meritpromotion/{id}/RetrieveCat2', ['uses' => 'FacultyController@RetrieveCat2']);
    Route::get('vchancellor/meritpromotion/{id}/RetrieveCat3', ['uses' => 'FacultyController@RetrieveCat3']);
    Route::get('vchancellor/meritpromotion/{id}/RetrieveCat4', ['uses' => 'FacultyController@RetrieveCat4']);

    Route::post('vchancellor/meritpromotion/{id}/UpdateFormCat1', ['uses' => 'FacultyController@UpdateFormCat1']);
    Route::post('vchancellor/meritpromotion/{id}/UpdateFormCat2', ['uses' => 'FacultyController@UpdateFormCat2']);
    Route::post('vchancellor/meritpromotion/{id}/UpdateFormCat3', ['uses' => 'FacultyController@UpdateFormCat3']);
    Route::post('vchancellor/meritpromotion/{id}/UpdateFormCat4', ['uses' => 'FacultyController@UpdateFormCat4']);

    Route::get('vchancellor/meritpromotion/{id}/DeleteEntryCat1', ['uses' => 'FacultyController@DeleteEntryCat1']);
    Route::get('vchancellor/meritpromotion/{id}/DeleteEntryCat2', ['uses' => 'FacultyController@DeleteEntryCat2']);
    Route::get('vchancellor/meritpromotion/{id}/DeleteEntryCat3', ['uses' => 'FacultyController@DeleteEntryCat3']);
    Route::get('vchancellor/meritpromotion/{id}/DeleteEntryCat4', ['uses' => 'FacultyController@DeleteEntryCat4']);
});
?>