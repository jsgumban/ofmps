@extends('app')
@section('title', 'OFMPS | CHANCELLOR')
@section('content')

    @if(Auth::user())
        @if((Auth::user()->position)==5)
            <?php $preLink = FOLDERNAME . '/admin/'; ?>
            <?php $currentUser = 'admin'; ?>
        @elseif((Auth::user()->position)==4)
            <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
            <?php $currentUser = 'chancellor'; ?>
        @elseif((Auth::user()->position)==3)
            <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
            <?php $currentUser = 'vchancellor'; ?>
        @elseif((Auth::user()->position)==2)
            <?php $preLink = FOLDERNAME . '/dean/'; ?>
            <?php $currentUser = 'dean'; ?>
        @elseif((Auth::user()->position)==1)
            <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
            <?php $currentUser = 'deptchair'; ?>
        @elseif((Auth::user()->position)==0)
            <?php $preLink = FOLDERNAME . '/faculty/'; ?>
            <?php $currentUser = 'faculty'; ?>
        @endif
    @endif


    <div class="page animsition"><br>

        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <div class="panel">
                        <div class="panel-body">

                            <p style="color:black"><i class="icon fa-cog" aria-hidden="true"></i>
                                &nbsp;CHANCELLOR (UP MINDANAO)</p>

                            <div class="list-group" data-plugin="nav-tabs" role="tablist">
                                <a class="list-group-item active" data-toggle="tab" href="#category-1"
                                   aria-controls="category-1" role="tab" id="tab-category-1"><i class="icon fa-bullhorn" aria-hidden="true"></i>CALL FOR PROMOTION</a>
                                <a class="list-group-item" data-toggle="tab" href="#category-2"
                                   aria-controls="category-2" role="tab" id="tab-category-2"><i class="icon fa-users" aria-hidden="true"></i>FACULTY MEMBERS</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                @include('chancellor.CHANCELLOR-FacultyMembers-View')
                                @include('chancellor.CHANCELLOR-CallForPromotionDates')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(!empty(session('activeTab')))
        <script type="text/javascript">
            document.getElementById("tab-category-1").setAttribute("class", "list-group-item");
            document.getElementById("tab-category-2").setAttribute("class", "list-group-item");
            document.getElementById("category-1").setAttribute("class", "tab-pane");
            document.getElementById("category-2").setAttribute("class", "tab-pane");

            // document.getElementById("tab-category-4").setAttribute("class", "tab-pane");
            document.getElementById("tab-category-" +{{session('activeTab')}}).setAttribute("class", "list-group-item active");
            document.getElementById("category-" +{{session('activeTab')}}).setAttribute("class", "tab-pane active in");
        </script>
    @endif


@endsection