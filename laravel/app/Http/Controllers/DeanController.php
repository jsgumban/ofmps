<?php

namespace App\Http\Controllers;

use App\College;
use App\Department;
use App\Http\Requests;
use App\Mpromotion;
use App\MpromotionForm;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;
use Validator;

class DeanController extends Controller
{
    public function CallForPromtion()
    {
        try {
            $id = DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
            if (!empty($id)) {
                $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.id=' . $id[0]->id);
                if (!empty($isThereCallForPromotion)) {
                    $startDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_start);
                    $endDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_end);
                    $currentDate = strtotime(date("Y-m-d"));
                    $mpromotion = Mpromotion::find($isThereCallForPromotion[0]->id);

                    if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                        // call for promotion status: on-going
                        $mpromotion->mpromotion_status = 0;
                        $mpromotion->save();
                    } else if ($currentDate > $endDate) {
                        // call for promotion status: finished
                        $mpromotion->mpromotion_status = 1;
                        $mpromotion->save();
                    } else if ($startDate > $currentDate) {
                        // call for promotion status: not yet started
                        $mpromotion->mpromotion_status = 2;
                        $mpromotion->save();
                    }
                }
            }
        } catch (Exception $e) {
            $isThereCallForPromotion = "";
        }
    }

    public function index()
    {
        $this->CallForPromtion();
        return view('pages.welcome');
    }

    public function ListOfSubmittedForms($id)
    {
        $this->CallForPromtion();
        $college = DB::select('SELECT * FROM colleges WHERE colleges.dean = \'' . Auth::user()->id . '\'');
        $department = DB::select('SELECT * FROM departments WHERE
                                    departments.college_belongs = \'' . $college[0]->college_name . '\'
                                ');

        $departmentInfo = Department::find($department[0]->id);
        $submittedForms = DB::select('SELECT
                    mpromotion_forms.id,
                    mpromotion_forms.user_id,
                    mpromotion_forms.form_status,
                    mpromotion_forms.lastPromotion_date,
                    mpromotion_forms.fillOut_date,
                    mpromotion_forms.form_name,
                    mpromotion_forms.submit_date_deptchair,
                    mpromotion_forms.callForPromotion_ID,
                    mpromotion_forms.callforpromotion_date_start,
                    mpromotion_forms.callforpromotion_date_end,
                    mpromotion_forms.faculty_total,
                    mpromotion_forms.initial_rank,
                    mpromotion_forms.initial_step,
                    users.first_name,
                    users.middle_name,
                    users.last_name,
                    users.department_belongs
                    FROM
                    mpromotion_forms
                    INNER JOIN users ON mpromotion_forms.user_id = users.id
                    INNER JOIN departments ON users.department_belongs = departments.department_name
                    WHERE
                    mpromotion_forms.user_id = users.id AND
                    (mpromotion_forms.form_status >= 2 and mpromotion_forms.form_status != 5) AND
                    mpromotion_forms.callForPromotion_ID = \'' . $id . '\' AND
                    users.department_belongs = departments.department_name AND
                    departments.college_belongs = \'' . $college[0]->college_name . '\' 
                    ORDER BY mpromotion_forms.form_status ASC           
        ');
        $callforpromotion = DB::select('SELECT
                            mpromotion.id,
                            mpromotion.mpromotion_status,
                            mpromotion.mpromotion_date_end,
                            mpromotion.mpromotion_date_start,
                            mpromotion.details
                            FROM
                            mpromotion
                            WHERE
                            mpromotion.id = \'' . $id . '\'
                            ');
        return view('dean.CAPC-ListOfSubmittedForms', compact('submittedForms', 'department', 'college', 'callforpromotion'));
    }

    public function EvaluateForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if (!empty($MpromotionForm)) {
            $user = User::find($MpromotionForm->user_id);
            $college = DB::select('SELECT * FROM colleges WHERE colleges.dean = \'' . Auth::user()->id . '\'');
            $department = DB::select('SELECT * FROM departments WHERE
                                        departments.college_belongs = \'' . $college[0]->college_name . '\'
                                    ');
            if ($user->department_belongs == $department[0]->department_name && $MpromotionForm->form_status == 2) {
                $this->facultyController = new FacultyController();
                return $this->facultyController->FillOutMeritPromotion($id);
            } else {
                return view('pages.error404');
            }
        } else {
            return view('pages.error404');
        }
    }

    public function CAPCMonitorForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if (!empty($MpromotionForm)) {
            $user = User::find($MpromotionForm->user_id);
            $college = DB::select('SELECT * FROM colleges WHERE colleges.dean = \'' . Auth::user()->id . '\'');
            $department = DB::select('SELECT * FROM departments WHERE
                                        departments.college_belongs = \'' . $college[0]->college_name . '\'
                                    ');
            if ($user->department_belongs == $department[0]->department_name && ($MpromotionForm->form_status > 2 && $MpromotionForm->form_status != 5)) {
                $this->facultyController = new FacultyController();
                return $this->facultyController->FillOutMeritPromotion($id);
            } else {
                return view('pages.error404');
            }
        } else {
            return view('pages.error404');
        }
    }

    public function SubmitVChancellor($id, Request $request)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        $MpromotionForm->form_status = 3;
        $MpromotionForm->dean_points = $MpromotionForm->faculty_total;
        $MpromotionForm->dean_remarks = Input::get('dean_remarks');

        $files = $request->file('file');
        $attachmentFileNames = "";
        $attachmentNamesSpit[] = "";
        if (!(empty($files[0]))) {
            foreach ($files as $file):
                $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                $file->move('uploads', $fileName);
                $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
            endforeach;
        }

        $MpromotionForm->dean_remarks_attachments = $attachmentFileNames;

        $MpromotionForm->save();
        $notification = "SUCCESS : Form has been successfull submitted to the vice chancellor.";
        return redirect('/dean/capc/' . Input::get('callForPromotion_ID') . '/ListOfSubmittedForms')->with('notification', $notification);
    }

    public function RejectCAPC($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        $MpromotionForm->form_status = 2.5;
        $MpromotionForm->dean_remarks = Input::get('dean_remarks');
        $MpromotionForm->save();
        $notification = "SUCCESS : Form has been successfull rejected.";
        return redirect('/dean/capc/' . Input::get('callForPromotion_ID') . '/ListOfSubmittedForms')->with('notification', $notification);
    }

    public function CAPC()
    {
        $this->CallForPromtion();
        $departments = DB::select('SELECT * FROM departments order by department_name');
        $collegeID = DB::select('SELECT
                                    colleges.id, departments.department_name, departments.deptchair, departments.college_belongs
                                    FROM
                                    users
                                    INNER JOIN departments ON users.department_belongs = departments.department_name
                                    INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                    WHERE
                                    users.department_belongs = departments.department_name AND
                                    departments.college_belongs = colleges.college_name AND
                                    colleges.dean  = \'' . Auth::user()->id . '\'');
        $college = College::find($collegeID[0]->id);
        $collegeInfo = College::find($collegeID[0]->id);
        $users = DB::select('SELECT users.id,
                                    users.department_belongs,
                                    users.employee_code,
                                    users.first_name,
                                    users.middle_name,
                                    users.last_name,
                                    users.birthday,
                                    users.rank,
                                    users.step,
                                    users.position,
                                    users.address,
                                    users.remember_token,
                                    users.created_at,
                                    users.updated_at
                                    FROM users as users 
                                    INNER JOIN departments ON users.department_belongs = departments.department_name
                                    INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                    WHERE
                                    users.department_belongs = departments.department_name AND
                                    colleges.college_name = \'' . $collegeID[0]->college_belongs . '\'');
        $numberOfDepartmentsPerCollege = DB::select('SELECT
                                        departments.id
                                        FROM
                                        departments
                                        INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                        WHERE
                                        departments.college_belongs = colleges.college_name AND
                                        colleges.college_name = \'' . $college->college_name . '\'
                                    ');
        $numberOfFacultiesPerCollege = DB::select('SELECT *
                                        FROM
                                            users
                                        INNER JOIN departments ON users.department_belongs = departments.department_name
                                        INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                        WHERE
                                            users.department_belongs = departments.department_name
                                        AND departments.college_belongs = \'' . $college->college_name . '\'');
        $currentDean = DB::select('SELECT *
                                    FROM
                                        users
                                    INNER JOIN departments ON users.department_belongs = departments.department_name
                                    INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                    WHERE
                                        users.department_belongs = departments.department_name
                                    AND colleges.college_name = departments.college_belongs
                                    AND users.id =\'' . $college->dean . '\'');

        $college1 = DB::select('SELECT * FROM colleges WHERE colleges.dean = \'' . Auth::user()->id . '\'');
        $department = DB::select('SELECT departments.department_name FROM departments WHERE
                                    departments.college_belongs = \'' . $college1[0]->college_name . '\'
                                ');
        $mpromotionDates = DB::select('SELECT * FROM mpromotion ORDER BY mpromotion.mpromotion_status ASC');
        $count = 0;

        // number of forms submitted per mpromotion
        foreach ($mpromotionDates as $mpromotionDate) {
            $temp = DB::select('SELECT
                                *
                                FROM
                                mpromotion_forms
                                INNER JOIN users ON mpromotion_forms.user_id = users.id
                                INNER JOIN departments ON users.department_belongs = departments.department_name
                                WHERE
                                mpromotion_forms.callForPromotion_ID = \'' . $mpromotionDate->id . '\' AND 
                                (mpromotion_forms.form_status >= \'2\' AND mpromotion_forms.form_status != \'5\') AND
                                users.department_belongs = departments.department_name AND
                                departments.college_belongs = \'' . $college1[0]->college_name . '\' 
                                ');
            $forms[$count] = $temp;
            $count++;
        }
        return view('dean.CAPC', compact('collegeInfo', 'users', 'departments', 'numberOfDepartmentsPerCollege',
            'numberOfFacultiesPerCollege',
            'college', 'currentDean',
            'mpromotionDates', 'college1', 'forms'));
    }
}


