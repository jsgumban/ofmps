<div class="modal fade modal-info" id="addNewForm" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">

        {{-- FORM ACTION --}}
        <form action={!! $preLink.'meritpromotion/AddNewForm' !!} method="POST">
            {!! csrf_field() !!}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">FILL OUT NEW FORM</h4>
                </div>
                <div class="modal-body">

                    {{-- FILLED OUT DATE --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">Filled Out Date</label>
                        {!! Form::input('date', 'fillOut_date', date('Y-m-d'), ['class' => 'form-control', 'placeholder' => 'Date','disabled']) !!}
                        {!! Form::hidden('fillOut_date', date('Y-m-d')) !!}
                    </div>

                    {{-- FORM NAME --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">Preffered Form Name</label>
                        <input type="text" id="prefferedFormName" class="form-control" name="form_name"
                               placeholder="Preffered Form Name" required>
                        <input type="checkbox" name="inputCheckboxes" id="defaultFormName"> Default Form Name

                        <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#defaultFormName").click(function (e) {
                                    if ($('#defaultFormName').prop('checked')) {
                                        $('#prefferedFormName').attr('disabled', true);
                                        $('#prefferedFormName').val("");
                                    } else {
                                        $('#prefferedFormName').attr('disabled', false);
                                    }
                                });
                            });
                        </script>
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Create</button>
                </div>
            </div>
        </form>
    </div>
</div>
