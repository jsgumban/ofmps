  <div class="modal fade modal-warning" id="ModalneedsRevisions" aria-hidden="false" aria-labelledby="Needs Revisions"
     role="dialog"
     tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content" action="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/NeedsRevision' !!}"
              method="POST"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! Form::hidden('callForPromotion_ID', $MpromotionForm->callForPromotion_ID) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">DAPC REMARKS</h4>
            </div>
            <div class="ribbon ribbon-reverse ribbon-default">
                <span class="ribbon-inner" id="userInfo-5" style="display: inline"></span>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <textarea class="form-control" rows="5" placeholder="Type your message"
                                  name="deptchair_remarks"></textarea>
                    </div>
                    <div class="col-sm-12" align="right">
                        <button type="submit" class="btn btn-warning">Send back to Faculty Member</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>

<div class="modal fade modal-danger" id="ModalRejectDAPC" aria-hidden="false"
     role="dialog"
     tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content" action="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/RejectDAPC' !!}"
              method="POST"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! Form::hidden('callForPromotion_ID', $MpromotionForm->callForPromotion_ID) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">DAPC REMARKS</h4>
            </div>
            <div class="ribbon ribbon-reverse ribbon-default">
                <span class="ribbon-inner" id="userInfo-5" style="display: inline"></span>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <textarea class="form-control" rows="5" placeholder="Type your message"
                                  name="deptchair_remarks"></textarea>
                    </div>
                    <div class="col-sm-12" align="right">
                        <button type="submit" class="btn btn-danger">Reject Form</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>


<div class="modal fade modal-success" id="ModalsubmitDean" aria-hidden="false" role="dialog"
     tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content" action="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/SubmitDean' !!}"
              method="POST"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! Form::hidden('callForPromotion_ID', $MpromotionForm->callForPromotion_ID) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">DAPC REMARKS</h4>
            </div>
            <div class="ribbon ribbon-reverse ribbon-default">
                <span class="ribbon-inner" id="userInfo-5" style="display: inline"></span>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-lg-12 form-group">
                        <textarea class="form-control" rows="5" placeholder="Type your message" name="deptchair_remarks"
                                  required></textarea>
                    </div>

                    <div class="col-md-12 form-group">
                        <label class="control-label" for="selectMulti">Attachments</label>

                        <div class="form-group form-material form-material-file ">
                            <input type="text" class="form-control" placeholder="Browse.." readonly="">
                            <input type="file" name="file[]" multiple="" id="submissionAttachment-DAPC"
                                   class="" onchange="checkFile()">
                        </div>
                    </div>
                    <div class="col-sm-12" align="right">
                        <button type="submit" class="btn btn-success">Submit to Dean</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>

<div class="modal fade modal-danger" id="ModalRejectCAPC" aria-hidden="false"
     role="dialog"
     tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content" action="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/RejectCAPC' !!}"
              method="POST"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! Form::hidden('callForPromotion_ID', $MpromotionForm->callForPromotion_ID) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">CAPC REMARKS</h4>
            </div>
            <div class="ribbon ribbon-reverse ribbon-default">
                <span class="ribbon-inner" id="userInfo-5" style="display: inline"></span>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <textarea class="form-control" rows="5" placeholder="Type your message"
                                  name="dean_remarks"></textarea>
                    </div>
                    <div class="col-sm-12" align="right">
                        <button type="submit" class="btn btn-danger">Reject Form</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>

<div class="modal fade modal-success" id="ModalsubmitVchancellor" aria-hidden="false" role="dialog"
     tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content" action="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/SubmitVChancellor' !!}"
              method="POST"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! Form::hidden('callForPromotion_ID', $MpromotionForm->callForPromotion_ID) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">CAPC REMARKS</h4>
            </div>
            <div class="ribbon ribbon-reverse ribbon-default">
                <span class="ribbon-inner" id="userInfo-5" style="display: inline"></span>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-lg-12 form-group">
                        <textarea class="form-control" rows="5" placeholder="Type your message" name="dean_remarks"
                                  required></textarea>
                    </div>

                    <div class="col-md-12 form-group">
                        <label class="control-label" for="selectMulti">Attachments</label>

                        <div class="form-group form-material form-material-file ">
                            <input type="text" class="form-control" placeholder="Browse.." readonly="">
                            <input type="file" name="file[]" multiple="" id="submissionAttachment-CAPC"
                                   class="" onchange="checkFile()">
                        </div>
                    </div>
                    <div class="col-sm-12" align="right">
                        <button type="submit" class="btn btn-success">Submit to Vice Chancellor</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>

<div class="modal fade modal-danger" id="ModalRejectUAPFC" aria-hidden="false"
     role="dialog"
     tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content" action="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/RejectUAPFC' !!}"
              method="POST"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! Form::hidden('callForPromotion_ID', $MpromotionForm->callForPromotion_ID) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">CAPC REMARKS</h4>
            </div>
            <div class="ribbon ribbon-reverse ribbon-default">
                <span class="ribbon-inner" id="userInfo-5" style="display: inline"></span>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <textarea class="form-control" rows="5" placeholder="Type your message"
                                  name="vchancellor_remarks"></textarea>
                    </div>
                    <div class="col-sm-12" align="right">
                        <button type="submit" class="btn btn-danger">Reject Form</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>

<div class="modal fade modal-success" id="ModalsubmitChancellor" aria-hidden="false" role="dialog"
     tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content" action="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/SubmitChancellor' !!}"
              method="POST"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! Form::hidden('callForPromotion_ID', $MpromotionForm->callForPromotion_ID) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">UAPFC REMARKS</h4>
            </div>

            <div class="ribbon ribbon-reverse ribbon-default">
                <span class="ribbon-inner" id="userInfo-4" style="display: inline"></span>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">ASSIGN NEW RANK AND STEP</label><br>
                        <span class="label label-outline label-default" id="currentRank-2"></span>

                        <div class="form-group">
                            <select class="form-control focus" required="" id="initial_rank-1" name="initial_rank" >
                                <option value="None"> None</option>
                                <option value="Instructor"> Instructor</option>
                                <option value="Assistant Professor"> Assistant Professor</option>
                                <option value="Associate Professor"> Associate Professor</option>
                                <option value="Professor">Professor</option>
                            </select>
                        </div>
                        <span class="label label-outline label-default" id="currentStep-1"></span>
                        <input type="text" class="form-control focus" id="initial_step-1" placeholder="Step" required=""
                                name="initial_step">
                    </div>

                    <div class="col-lg-12 form-group">
                        <textarea class="form-control" rows="5" placeholder="Type your message"
                                  name="vchancellor_remarks"
                                  required></textarea>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="control-label" for="selectMulti">Attachments</label>

                        <div class="form-group form-material form-material-file ">
                            <input type="text" class="form-control" placeholder="Browse.." readonly="">
                            <input type="file" id="submissionAttachment-UAPFC" name="file[]" multiple=""
                                   class="" onchange="checkFile()">
                        </div>
                    </div>
                    <div class="col-sm-12" align="right">
                        <button type="submit" class="btn btn-success">Submit to Chancellor</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>


<div class="modal fade modal-success" id="ModalapproveChancellor" aria-hidden="false" role="dialog"
     tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form class="modal-content" action="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/ApproveForm' !!}"
              method="POST"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! Form::hidden('callForPromotion_ID', $MpromotionForm->callForPromotion_ID) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">CHANCELLOR REMARKS</h4>
            </div>

            <div class="ribbon ribbon-reverse ribbon-default">
                <span class="ribbon-inner" id="userInfo-5" style="display: inline"></span>
            </div>

            <div class="modal-body">
                <div class="row">

                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">NEWLY ASSIGNED RANK AND STEP</label><br>

                        <span class="label label-outline label-default" id="currentRank-2"></span>

                        <div class="form-group">
                            <select class="form-control focus" required="" id="initial_rank-2" name="initial_rank"
                                    disabled="">
                                <option value="None"> None</option>
                                <option value="Instructor"> Instructor</option>
                                <option value="Assistant Professor"> Assistant Professor</option>
                                <option value="Associate Professor"> Associate Professor</option>
                                <option value="Professor">Professor</option>
                            </select>
                        </div>
                        <span class="label label-outline label-default" id="currentStep-2"></span>
                        <input type="text" class="form-control focus" id="initial_step-2" required=""
                               name="initial_step" disabled="">

                    </div>
                    


                    <div class="col-lg-12 form-group">
                        <textarea class="form-control" rows="5" placeholder="Type your message"
                                  name="chancellor_remarks"
                                  required></textarea>
                    </div>

                    <div class="col-md-12 form-group">
                        <label class="control-label" for="selectMulti">Attachments</label>

                        <div class="form-group form-material form-material-file ">
                            <input type="text" class="form-control" placeholder="Browse.." readonly="">
                            <input type="file" id="submissionAttachment-UAPFC" name="file[]" multiple=""
                                   class="" onchange="checkFile()">
                        </div>
                    </div>

                    <small>
                        <small>
                            <div class="col-md-6" align="left">

                                <input type="radio" class="to-labelauty-icon" name="isApproveForm"
                                       data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                       data-points="0"/ required>
                                <span><big><big><big>APPROVE</big></big></big></span> &nbsp;

                            </div>
                        </small>
                    </small>
                    <div class="col-sm-6" align="right">
                        <button type="submit" class="btn btn-success">Save Changes</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>


<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=submissionAttachment]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }
            }
        });

        @if(Request::is('*/EvaluateForm'))
            $("[id^=initial_rank]").change(function () {
                var selected = $("[id^=initial_rank]").val();
                $('#initial_step-1').attr('disabled', true)
                if (selected == 'None') {
                    $('#initial_step-1').attr('disabled', true);
                    $('#initial_step-1').val('');
                } else {
                    $('#initial_step-1').attr('disabled', false);
                }
            });
        @endif
    });
</script>