@extends('app_login')
@section('title', 'OFMPS | ERROR 404')
@section('content')
@section('title', 'ERROR | Page not found')
<div class="page animsition vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
        {{-- <img class="error-mark animation-slide-top" src="{{ asset('/images/404.png') }}" alt="..."> --}}
        <h1 style="color:RED" class="error-mark">E R R O R 4 0 4</h1>

        <p class="error-advise">PAGE NOT FOUND</p><br>
        <a class="btn btn-danger btn-round" href="{{ FOLDERNAME }}">GO TO HOME PAGE</a>

        <footer class="page-copyright">
            <br><br><br><br><br><br><br><br>
        </footer>
    </div>
</div>
@endsection