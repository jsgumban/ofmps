<?php
Route::group(['middleware' => 'deptchair'], function () {
    Route::get('deptchair/home', 'DeptChairController@index');

    /////////////////
    // OWN PROFILE //
    /////////////////
    Route::get('deptchair/profile/MyProfile', ['uses' => 'AdminController@MyProfile']);
    Route::get('deptchair/profile/UpdateMyProfile', ['uses' => 'AdminController@UpdateMyProfile']);
    Route::post('deptchair/profile/SaveUpdateMyProfile', ['uses' => 'AdminController@SaveUpdateMyProfile']);
    Route::get('deptchair/profile/UpdateMyPassword', ['uses' => 'AdminController@UpdateMyPassword']);
    Route::post('deptchair/profile/SaveUpdateMyPassword', ['uses' => 'AdminController@SaveUpdateMyPassword']);

    /////////////////////
    // FACULTY MEMBERS //
    /////////////////////
    Route::get('deptchair/dapc/{id}/RetrieveUserData', ['uses' => 'DeptChairController@RetrieveUserData']);
    Route::get('deptchair/dapc/{id}/RetrieveSubmittedForms', ['uses' => 'DeptChairController@RetrieveSubmittedForms']);
    Route::get('deptchair/meritpromotion/{id}/RetrieveUserInfo', ['uses' => 'DeptChairController@RetrieveUserInfo']);

    /////////////////////
    // MERIT PROMOTION //
    /////////////////////
    Route::get('deptchair/dapc', ['uses' => 'DeptChairController@DAPC']);
    Route::get('deptchair/dapc/{id}/ListOfSubmittedForms', ['uses' => 'DeptChairController@ListOfSubmittedForms']);

    Route::get('deptchair/meritpromotion', ['uses' => 'FacultyController@MeritPromotion']);
    Route::post('deptchair/meritpromotion/AddNewForm', ['uses' => 'FacultyController@AddNewForm']);
    Route::get('deptchair/meritpromotion/{id}/FillOutMeritPromotion', ['uses' => 'FacultyController@FillOutMeritPromotionCheck']);
    Route::get('deptchair/meritpromotion/SaveAsDraft', ['uses' => 'FacultyController@SaveAsDraft']);
    Route::get('deptchair/meritpromotion/{id}/SubmitDeptChair', ['uses' => 'FacultyController@SubmitDeptChair']);
    Route::get('deptchair/meritpromotion/{id}/MonitorForm', ['uses' => 'FacultyController@MonitorForm']);

    Route::get('deptchair/meritpromotion/{id}/EvaluateForm', ['uses' => 'DeptChairController@EvaluateForm']);
    Route::post('deptchair/meritpromotion/{id}/RejectDAPC', ['uses' => 'DeptChairController@RejectDAPC']);
    Route::post('deptchair/meritpromotion/{id}/SubmitDean', ['uses' => 'DeptChairController@SubmitDean']);
    Route::get('deptchair/meritpromotion/{id}/RetrieveFormInfo', ['uses' => 'DeptChairController@RetrieveFormInfo']);
    Route::get('deptchair/meritpromotion/{id}/DAPCMonitorForm', ['uses' => 'DeptChairController@DAPCMonitorForm']);
    Route::post('deptchair/meritpromotion/{id}/NeedsRevision', ['uses' => 'DeptChairController@NeedsRevision']);
    
    Route::get('deptchair/meritpromotion/{id}/generateZip', ['uses' => 'FacultyController@generateZip']);
    Route::get('deptchair/meritpromotion/{id}/generatePDF', ['uses' => 'FacultyController@generatePDF']);
    Route::get('deptchair/meritpromotion/{id}/ReviseForm', ['uses' => 'FacultyController@ReviseForm']);
    Route::get('deptchair/meritpromotion/{id}/UpdateForm', ['uses' => 'FacultyController@UpdateForm']);
    Route::get('deptchair/meritpromotion/{id}/DeleteDraftForm', ['uses' => 'FacultyController@DeleteDraftForm']);

    Route::post('deptchair/meritpromotion/{id}/SubmitFormCat1', ['uses' => 'FacultyController@SubmitFormCat1']);
    Route::post('deptchair/meritpromotion/{id}/SubmitFormCat2', ['uses' => 'FacultyController@SubmitFormCat2']);
    Route::post('deptchair/meritpromotion/{id}/SubmitFormCat3', ['uses' => 'FacultyController@SubmitFormCat3']);
    Route::post('deptchair/meritpromotion/{id}/SubmitFormCat4', ['uses' => 'FacultyController@SubmitFormCat4']);

    Route::get('deptchair/meritpromotion/{id}/RetrieveCat1', ['uses' => 'FacultyController@RetrieveCat1']);
    Route::get('deptchair/meritpromotion/{id}/RetrieveCat2', ['uses' => 'FacultyController@RetrieveCat2']);
    Route::get('deptchair/meritpromotion/{id}/RetrieveCat3', ['uses' => 'FacultyController@RetrieveCat3']);
    Route::get('deptchair/meritpromotion/{id}/RetrieveCat4', ['uses' => 'FacultyController@RetrieveCat4']);

    Route::post('deptchair/meritpromotion/{id}/UpdateFormCat1', ['uses' => 'FacultyController@UpdateFormCat1']);
    Route::post('deptchair/meritpromotion/{id}/UpdateFormCat2', ['uses' => 'FacultyController@UpdateFormCat2']);
    Route::post('deptchair/meritpromotion/{id}/UpdateFormCat3', ['uses' => 'FacultyController@UpdateFormCat3']);
    Route::post('deptchair/meritpromotion/{id}/UpdateFormCat4', ['uses' => 'FacultyController@UpdateFormCat4']);

    Route::get('deptchair/meritpromotion/{id}/DeleteEntryCat1', ['uses' => 'FacultyController@DeleteEntryCat1']);
    Route::get('deptchair/meritpromotion/{id}/DeleteEntryCat2', ['uses' => 'FacultyController@DeleteEntryCat2']);
    Route::get('deptchair/meritpromotion/{id}/DeleteEntryCat3', ['uses' => 'FacultyController@DeleteEntryCat3']);
    Route::get('deptchair/meritpromotion/{id}/DeleteEntryCat4', ['uses' => 'FacultyController@DeleteEntryCat4']);
});
?>