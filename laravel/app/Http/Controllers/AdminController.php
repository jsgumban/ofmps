<?php

namespace App\Http\Controllers;
use App\College;
use App\Department;
use App\Http\Requests;
use App\Mpromotion;
use App\University;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;
use Response;
use Validator;

class AdminController extends Controller
{

    public function CallForPromtion()
    {
        try {
            $id = DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
            if (!empty($id)) {
                $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.id=' . $id[0]->id);
                if (!empty($isThereCallForPromotion)) {
                    $startDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_start);
                    $endDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_end);
                    $currentDate = strtotime(date("Y-m-d"));
                    $mpromotion = Mpromotion::find($isThereCallForPromotion[0]->id);

                    if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                        // call for promotion status: on-going
                        $mpromotion->mpromotion_status = 0;
                        $mpromotion->save();
                    } else if ($currentDate > $endDate) {
                        // call for promotion status: finished
                        $mpromotion->mpromotion_status = 1;
                        $mpromotion->save();
                    } else if ($startDate > $currentDate) {
                        // call for promotion status: not yet started
                        $mpromotion->mpromotion_status = 2;
                        $mpromotion->save();
                    }
                }
            }
        } catch (Exception $e) {
            $isThereCallForPromotion = "";
        }
    }

    public function index()
    {
        $this->CallForPromtion();
        return view('pages.welcome');
    }

    //////////////////////////
    // MY PROFILE FUNCTIONS //
    //////////////////////////
    public function MyProfile()
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT users.id FROM users WHERE users.employee_code = ' . Auth::user()->employee_code);
        $collegeBelongs = DB::select('SELECT DISTINCT
                                    colleges.college_name
                                    FROM
                                    colleges
                                    INNER JOIN departments ON departments.college_belongs = colleges.college_name
                                    INNER JOIN users ON users.department_belongs = departments.department_name
                                    WHERE
                                    colleges.college_name = departments.college_belongs AND
                                    departments.department_name = \'' . Auth::user()->department_belongs . '\'
                                ');
        $user = User::find($sql[0]->id);
        // var_dump($collegeBelongs);
        return view('pages.MyProfile', compact('user', 'collegeBelongs'));
    }

    public function UpdateMyProfile()
    {
        $this->CallForPromtion();
        $user = User::find(Auth::user()->id);
        $collegeBelongs = DB::select('SELECT DISTINCT
                                    colleges.college_name
                                    FROM
                                    colleges
                                    INNER JOIN departments ON departments.college_belongs = colleges.college_name
                                    INNER JOIN users ON users.department_belongs = departments.department_name
                                    WHERE
                                    colleges.college_name = departments.college_belongs AND
                                    departments.department_name = \'' . Auth::user()->department_belongs . '\'
                                ');
        return view('pages.UpdateMyProfile', compact('user', 'collegeBelongs'));
    }

    public function SaveUpdateMyProfile()
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT users.id FROM users WHERE users.employee_code = ' . Auth::user()->employee_code);
        $user = User::find($sql[0]->id);

        // unchanged employee code
        if ($user->employee_code == Input::get('employee_code')) {
            $flag = 1;
            $user->employee_code = $user->employee_code;
        } else {
            // if employee code already exists
            $sql1 = DB::select('SELECT DISTINCT users.employee_code FROM users WHERE users.employee_code =' . Input::get('employee_code'));
            if (empty($sql1[0])) {
                $user->employee_code = Input::get('employee_code');
                $flag = 2;
            } else {
                $user->employee_code = $user->employee_code;
                $flag = 3;
            }
        }

        $user->first_name = Input::get('first_name');
        $user->middle_name = Input::get('middle_name');
        $user->last_name = Input::get('last_name');
        $user->gender = Input::get('gender');
        $user->birthday = Input::get('birthday');
        $user->address = Input::get('address');
        $user->rank = Input::get('rank');
        $user->position = $user->position;
        $user->save();

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        } else if ($currentUserID == 5) {
            $currentUser = "admin";
        }

        if ($flag == 1) {
            $notification = "SUCCESS : Profile has been successfully updated.";
            return redirect($currentUser . '/profile/MyProfile')->with('notification', $notification);
        } else if ($flag == 2) {
            $notification = "SUCCESS : Profile has been successfully updated.";
            return redirect($currentUser . '/profile/MyProfile')->with('notification', $notification);
        } else if ($flag == 3) {
            $notification = "ERROR : Employee code has already been taken. Please try again.";
            return redirect($currentUser . '/profile/UpdateMyProfile')->with('notification', $notification);
        }
    }

    public function UpdateMyPassword()
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT users.id FROM users WHERE users.employee_code = ' . Auth::user()->employee_code);
        $user = User::find($sql[0]->id);
        return view('pages.UpdateMyPassword', compact('user'));
    }

    public function SaveUpdateMyPassword()
    {
        $this->CallForPromtion();
        $newpassword = Input::get('currentpass');
        $oldpassword = User::find(Auth::user()->id)->password;
        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor"; 
        } else if ($currentUserID == 5) {
            $currentUser = "admin";
        }
        if (Hash::check($newpassword, $oldpassword)) {
            if((Input::get('newpass')== Input::get('confirm'))){
                $user = User::find(Auth::user()->id);
                $user->password = Hash::make(Input::get('newpass'));
                $user->save();
                $notification = "SUCCESS : Password has been successfully updated.";
                return redirect($currentUser . '/profile/UpdateMyPassword/')->with('notification', $notification);
            }else{
                $notification = "ERROR : 'New and Confirm New Password' does not match. Try again.";
                return redirect($currentUser . '/profile/UpdateMyPassword')->with('notification', $notification);
            }
        } else {
            $notification = "ERROR : 'Current Password' does not match. Type the correct password.";
            return redirect($currentUser . '/profile/UpdateMyPassword')->with('notification', $notification);
        }
    }


    public function UniversitySettings()
    {
        ////////////////////
        // university tab //
        ////////////////////
        $this->CallForPromtion();
        //options for selecting new chancellor or vice chancellor
        $candidateForCVCs = DB::select('SELECT * FROM users WHERE users.position = 0 OR users.position = 3 OR users.position = 4 ');
        $allFaculty = DB::select('SELECT *FROM users WHERE users.position <5');

        $colleges = DB::select('SELECT * FROM colleges order by college_name');
        $deans = DB::select('SELECT * FROM users RIGHT JOIN colleges ON users.id = colleges.dean order by college_name');

        // var_dump($deanPerCollege);
        $departments = DB::select('SELECT * FROM departments order by department_name');
        $deptchairs = DB::select('SELECT * FROM users 
            RIGHT JOIN departments ON users.id = departments.deptchair order by department_name');


        // chancellor
        $chancellor = DB::select('SELECT users.id, users.first_name, users.middle_name, users.last_name FROM users , universities WHERE users.id = universities.chancellor');

        // vchancellor
        $vchancellor = DB::select('SELECT users.id, users.first_name, users.middle_name, users.last_name FROM users , universities WHERE users.id = universities.vchancellor');

        // count number departments per college
        $count = 0;
        foreach ($colleges as $college) {
            $countDepratmentPerCollege[$count] = DB::select('SELECT * FROM colleges INNER JOIN departments ON 
                departments.college_belongs = colleges.college_name WHERE colleges.college_name = \'' . $college->college_name . '\'');
            $count++;
        }


        // count number of faculty members under the college
        $count2 = 0;
        foreach ($colleges as $college) {
            $countFacultyPerCollege[$count2] = DB::select('SELECT * FROM users INNER JOIN departments ON users.department_belongs = 
                departments.department_name INNER JOIN colleges ON departments.college_belongs = colleges.college_name WHERE 
                departments.college_belongs = \'' . $college->college_name . '\'');
            $count2++;
        }

        $count3 = 0;
        foreach ($departments as $department) {
            $countFacultyPerDepartment[$count3] = DB::select('SELECT * FROM   users INNER JOIN departments ON users.department_belongs = departments.department_name WHERE 
            users.department_belongs = \'' . $department->department_name . '\'');
            $count3++;
        }


        //////////////////////
        // SYSTEM USERS TAB //
        //////////////////////
        $users = DB::select('SELECT * FROM users WHERE users.delete = 0');
        $adminUsers = DB::select('SELECT * FROM users WHERE users.position = 5');


        //////////////////////////////
        // MERIT PROMOTION SETTINGS //
        //////////////////////////////
        $isAllowedToAddNewDate = DB::select('SELECT * FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
        $mpromotionDates = DB::select('SELECT * FROM mpromotion');

        $forms = array();
        $count = 0;
        if (!empty($mpromotionDates)) {
            foreach ($mpromotionDates as $mpromotionDate) {
                $temp = DB::select('SELECT
                                    *
                                    FROM
                                    mpromotion_forms
                                    WHERE
                                    mpromotion_forms.callForPromotion_ID = \'' . $mpromotionDate->id . '\' AND 
                                    (mpromotion_forms.form_status >= \'1\' AND mpromotion_forms.form_status != \'5\') 
                ');
                $forms[$count] = $temp;
                $count++;
            }
        }

        return view('admin.UniversitySettings', compact('candidateForCVCs', 'colleges', 'deans', 'departments', 'deptchairs',
            'chancellor', 'vchancellor', 'countDepratmentPerCollege', 'countFacultyPerCollege',
            'allFaculty', 'countFacultyPerDepartment',
            'users', 'adminUsers',
            'mpromotionDates', 'isAllowedToAddNewDate', 'forms'));
    }

    public function SaveUpdateUniversity()
    {
        $this->CallForPromtion();
        $university = University::find(1);
        if (Input::get('addAs') == 1) {
            if ($university->vchancellor == Input::get('candidate') && (Input::get('candidate') != "")) {
                $notification = "ERROR :  The user has already been assigned to other position.";
            } else {
                if (!empty($university->chancellor)) {
                    try {
                        $oldChancellor = $university->chancellor;
                        $user = User::find($oldChancellor);
                        $user->position = 0;
                        $user->save();
                    } catch (\Exception $e) {
                        $university->vchancellor = 0;
                        $university->save();
                    }

                }

                if (!empty(Input::get('candidate'))) {
                    $user = User::find(Input::get('candidate'));
                    $user->position = 4;
                    $user->save();
                }

                $university->chancellor = Input::get('candidate');
                $university->save();
                $notification = "SUCCESS :  University has been successfully updated.";
            }
        } else if (Input::get('addAs') == 2) {
            if ($university->chancellor == Input::get('candidate') && (Input::get('candidate') != "")) {
                $notification = "ERROR :  The user has already been assigned to other position.";
            } else {
                if (!empty($university->vchancellor)) {
                    $oldVChancellor = $university->vchancellor;
                    try {
                        $user = User::find($oldVChancellor);
                        $user->position = 0;
                        $user->save();
                    } catch (\Exception $e) {
                        $university->vchancellor = 0;
                        $university->save();
                    }
                }
                if (!empty(Input::get('candidate'))) {
                    $user = User::find(Input::get('candidate'));
                    $user->position = 3;
                    $user->save();
                }

                $university->vchancellor = Input::get('candidate');
                $university->save();
                $notification = "SUCCESS :  University has been successfully updated.";
            }
        }
        $activeTab = 1;
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }


    ///////////////////////
    // COLLEGE FUNCTIONS //
    ///////////////////////
    public function RemoveCollege($id)
    {
        $this->CallForPromtion();
        try {
            College::destroy($id);
            $notification = "SUCCESS :  College has been successfully deleted.";
        } catch (\Exception $e) {
            $notification = "ERROR :  College cannot be deleted.";
        }
        $activeTab = 2;
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function SaveNewCollege()
    {
        $this->CallForPromtion();
        //pangitaon kung nagaexists na ang college nga to thru inputted college name
        $sql = DB::select('SELECT colleges.college_name FROM colleges where colleges.college_name =' . '\'' . Input::get('college_name') . '\'');
        if (empty($sql)) {
            $college = new College;
            $college->college_name = Input::get('college_name');
            $college->college_initial = Input::get('college_initial');
            $college->save();
            $activeTab = 2;
            $notification = "SUCCESS :  College has been successfully added.";
            return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
        } else {
            $activeTab = 2;
            $notification = "ERROR : College name has already been taken. Please try again";
            return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
        }
    }

    public function ViewMoreInfoCollege($id)
    {
        $this->CallForPromtion();
        $college = College::find($id);
        $users = DB::select('SELECT users.id,
                                    users.department_belongs,
                                    users.employee_code,
                                    users.`password`,
                                    users.first_name,
                                    users.middle_name,
                                    users.last_name,
                                    users.birthday,
                                    users.rank,
                                    users.step,
                                    users.position,
                                    users.address,
                                    users.remember_token,
                                    users.created_at,
                                    users.updated_at,
                                    users.`delete`,
                                    users.gender
                                    FROM  users INNER JOIN departments ON users.department_belongs = departments.department_name
                                    INNER JOIN colleges ON departments.college_belongs = colleges.college_name WHERE users.department_belongs = departments.department_name AND
                                    departments.college_belongs =\'' . $college->college_name . '\' order by users.position desc');
        $currentDean = DB::select('SELECT *
                                    FROM
                                        users
                                    INNER JOIN departments ON users.department_belongs = departments.department_name
                                    INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                    WHERE
                                        users.department_belongs = departments.department_name
                                    AND colleges.college_name = departments.college_belongs
                                    AND users.id =\'' . $college->dean . '\'');
        $numberOfFacultiesPerCollege = DB::select('SELECT *
                                        FROM
                                            users
                                        INNER JOIN departments ON users.department_belongs = departments.department_name
                                        INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                        WHERE
                                            users.department_belongs = departments.department_name
                                        AND departments.college_belongs = \'' . $college->college_name . '\'');
        $deanChoices = DB::select('SELECT
                                    users.id,
                                    users.first_name,
                                    users.middle_name,
                                    users.last_name
                                    FROM
                                        users
                                    INNER JOIN departments ON users.department_belongs = departments.department_name
                                    INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                    WHERE
                                        users.department_belongs = departments.department_name
                                    AND (users.position = 0
                                    OR users.position = 2)
                                    AND departments.college_belongs = \'' . $college->college_name . '\'');

        $numberOfDepartmentsPerCollege = DB::select('SELECT
                                        departments.id
                                        FROM
                                        departments
                                        INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                        WHERE
                                        departments.college_belongs = colleges.college_name AND
                                        colleges.college_name = \'' . $college->college_name . '\'
                                    ');
        $departments = DB::select('SELECT * FROM departments order by department_name');
        return view('admin.UniversitySettings-College-ViewMoreInfo',
            compact('users', 'college', 'currentDean', 'numberOfFacultiesPerCollege',
                'deanChoices', 'numberOfDepartmentsPerCollege',
                'departments'));
    }

    public function RetrieveCollege($id, Request $request)
    {
        $this->CallForPromtion();
        $college = College::find($id);
        if ($request->ajax()) {
            return $college;
        }
    }

    public function RetrieveCollegeDean($id, Request $request)
    {
        $this->CallForPromtion();
        $college = College::find($id);
        $deanChoices = DB::select('SELECT
                                    users.id,
                                    users.first_name,
                                    users.middle_name,
                                    users.last_name,
                                    colleges.dean
                                    FROM
                                        users
                                    INNER JOIN departments ON users.department_belongs = departments.department_name
                                    INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                    WHERE
                                        users.department_belongs = departments.department_name
                                    AND (users.position = 0
                                    OR users.position = 2)
                                    AND departments.college_belongs = \'' . $college->college_name . '\'');
        if ($request->ajax()) {
            return $deanChoices;
        }
        // dd($deanChoices);
        // echo "string";
    }

    public function SaveUpdatedCollege($id)
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT colleges.college_name FROM colleges where colleges.college_name =' . '\'' . Input::get('college_name') . '\'');
        // updated college name is not on list yet
        if (empty($sql)) {
            $college = College::find($id);


            // change ang position sang old dean chair to 0(faculty)
            if (!empty($college->dean)) {
                try {
                    $oldDean = User::find($college->dean);
                    $oldDean->position = 0;
                    $oldDean->save();
                } catch (\Exception $e) {
                    $college->dean = 0;
                    $college->save();
                }

            }


            $college->college_name = Input::get('college_name');
            $college->college_initial = Input::get('college_initial');

            // if the user chooses not null
            // means ichange ang status ng bagong dean sa user na table
            if ((Input::get('dean')) != NULL) {
                // echo("hey1");
                // find the new dean to the user table
                $college->dean = Input::get('dean');
                $user = User::find(intval(Input::get('dean')));
                $user->position = '2';
                $user->save();
            } else {
                // else walang dapat baguhin
                $college->dean = NULL;
            }


            $college->save();
            $notification = "SUCCESS :  College has been successfully updated.";
            return redirect('/admin/university/' . $id . '/ViewMoreInfoCollege')->with('notification', $notification);
        } else {
            $college = College::find($id);
            if (Input::get('college_name') == $college->college_name) {

                // change ang position sang old dept dean to 0(faculty)
                if (!empty($college->dean)) {
                    try {
                        $oldDean = User::find($college->dean);
                        $oldDean->position = 0;
                        $oldDean->save();
                    } catch (\Exception $e) {
                        $college->dean = 0;
                        $college->save();
                    }
                }


                $college->college_name = $college->college_name;
                $college->college_initial = Input::get('college_initial');


                // if the user chooses not null
                // means ichange ang status ng bagong dean sa user na table
                if ((Input::get('dean')) != NULL) {
                    // echo("hey1");
                    // find the new dean to the user table
                    $college->dean = Input::get('dean');
                    $user = User::find(intval(Input::get('dean')));
                    $user->position = '2';
                    $user->save();
                } else {
                    // else walang dapat baguhin
                    $college->dean = NULL;
                }


                $college->save();
                $notification = "SUCCESS :  College has been successfully updated.";
                $activeTab = 2;
                return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
                // return redirect('/admin/university/' . $id . '/ViewMoreInfoCollege')->with('notification', $notification);
            } else {

                // change ang position sang old dept dean to 0(faculty)
                // change ang position sang old dept dean to 0(faculty)
                if (!empty($college->dean)) {
                    try {
                        $oldDean = User::find($college->dean);
                        $oldDean->position = 0;
                        $oldDean->save();
                    } catch (\Exception $e) {
                        $college->dean = 0;
                        $college->save();
                    }
                }


                $college->college_name = $college->college_name;
                $college->college_initial = Input::get('college_initial');


                // if the user chooses not null
                // means ichange ang status ng bagong dean sa user na table
                if ((Input::get('dean')) != NULL) {
                    // echo("hey1");
                    // find the new dean to the user table
                    $college->dean = Input::get('dean');
                    $user = User::find(intval(Input::get('dean')));
                    $user->position = '2';
                    $user->save();
                } else {
                    // else walang dapat baguhin
                    $college->dean = NULL;
                }


                $college->save();
                $notification = "ERROR : College name has already been taken. Please try again";
                $activeTab = 2;
                return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
                // return redirect('/admin/university/' . $id . '/ViewMoreInfoCollege')->with('notification', $notification);
            }

        }
    }

    //////////////////////////
    // DEPARTMENT FUNCTIONS //
    //////////////////////////
    public function RemoveDepartment($id)
    {
        $this->CallForPromtion();
        try {
            Department::destroy($id);
            $activeTab = 3;
            $notification = "SUCCESS :  Department has been successfully deleted.";
        } catch (\Exception $e) {
            $activeTab = 3;
            $notification = "ERROR :  Department cannot be deleted.";
        }
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function SaveNewDepartment()
    {
        $this->CallForPromtion();
        //pangitaon kung nagaexists na ang college nga to thru inputted college name
        $sql = DB::select('SELECT departments.department_name FROM departments where departments.department_name =' . '\'' . Input::get('department_name') . '\'');
        if (empty($sql)) {
            $department = new Department;
            $department->department_name = Input::get('department_name');
            $department->department_initial = Input::get('department_initial');

            $department->college_belongs = Input::get('college_belongs');
            // echo( Input::get('college_belongs'));
            // var_dump($department);
            $department->save();
            $activeTab = 3;
            $notification = "SUCCESS :  Department has been successfully created.";
            return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
        } else {
            $activeTab = 3;
            $notification = "ERROR : Department name has already been taken. Please try again";
            return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
        }
    }

    public function ViewMoreInfoDepartment($id)
    {
        $this->CallForPromtion();
        $department = Department::find($id);
        $users = DB::select('SELECT users.id,
                                    users.department_belongs,
                                    users.employee_code,
                                    users.`password`,
                                    users.first_name,
                                    users.middle_name,
                                    users.last_name,
                                    users.birthday,
                                    users.rank,
                                    users.step,
                                    users.position,
                                    users.address,
                                    users.remember_token,
                                    users.created_at,
                                    users.updated_at,
                                    users.`delete`,
                                    users.gender
                                    FROM users INNER JOIN departments ON users.department_belongs = departments.department_name WHERE 
                                    users.department_belongs = \'' . $department->department_name . '\' order by users.position desc');

        $colleges = DB::select('SELECT * FROM colleges');

        $candidtatesForDeptChair = DB::select('SELECT  users.id, users.department_belongs, users.employee_code, users.first_name, users.middle_name, users.last_name,
        users.birthday, users.rank, users.step, users.position, users.address, users.remember_token, users.created_at, users.updated_at 
        FROM users INNER JOIN departments ON users.department_belongs = departments.department_name WHERE (users.position = 0 OR users.position = 1)  AND
        users.department_belongs = \'' . $department->department_name . '\'');

        $departments = DB::select('SELECT * FROM departments order by department_name');
        $dc = DB::select('SELECT * 
                        FROM users INNER JOIN departments ON 
                        users.department_belongs = departments.department_name 
                        WHERE users.id = \'' . $department->deptchair . '\'');
        return view('admin.UniversitySettings-Department-ViewMoreInfo', compact('users', 'department', 'departments', 'dc', 'colleges', 'candidtatesForDeptChair'));
    }

    public function RetrieveDepartment($id, Request $request)
    {
        $this->CallForPromtion();
        $department = Department::find($id);
        if ($request->ajax()) {
            return $department;
        }
    }

    public function RetrieveDeptChair($id, Request $request)
    {
        $this->CallForPromtion();
        $department = Department::find($id);
        $depatChairChoices = DB::select('SELECT  users.id, users.department_belongs, users.employee_code, users.first_name, users.middle_name, users.last_name,
        users.birthday, users.rank, users.step, users.position, users.address, users.remember_token, users.created_at, users.updated_at, departments.deptchair 
        FROM users INNER JOIN departments ON users.department_belongs = departments.department_name WHERE (users.position = 0 OR users.position = 1)  AND
        users.department_belongs = \'' . $department->department_name . '\'');
        if ($request->ajax()) {
            return $depatChairChoices;
        }
    }

    public function SaveUpdatedDepartment($id)
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT departments.department_name FROM departments where departments.department_name =' . '\'' . Input::get('department_name') . '\'');
        // updated dept name is not on the list yet
        if (empty($sql)) {
            $department = Department::find($id);

            // change ang position sang old dept chair to 0(faculty)
            if (!empty($department->deptchair)) {
                try {
                    $oldDeptChair = User::find($department->deptchair);
                    $oldDeptChair->position = 0;
                    $oldDeptChair->save();
                } catch (\Exception $e) {
                    $department->deptchair = 0;
                    $department->save();
                }

            }


            $department->department_name = Input::get('department_name');
            $department->department_initial = Input::get('department_initial');
            $department->college_belongs = $department->college_belongs;


            // if the user chooses not null
            // means ichange ang status ng bagong dept chair sa user na table
            if ((Input::get('deptchair')) != NULL) {
                // echo("hey1");
                // find the new dept chair to the user table
                $department->deptchair = Input::get('deptchair');
                $user = User::find(intval(Input::get('deptchair')));
                $user->position = '1';
                $user->save();
            } else {
                // else walang dapat baguhin
                $department->deptchair = NULL;
            }


            $department->save();

            $notification = "SUCCESS :  Department has been successfully updated.";
            $activeTab = 3;
            return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
        } // dept name is not changed
        else {
            $department = Department::find($id);
            if (Input::get('department_name') == $department->department_name) {
                // change ang position sang old dept chair to 0(faculty)
                if (!empty($department->deptchair)) {
                    try {
                        $oldDeptChair = User::find($department->deptchair);
                        $oldDeptChair->position = 0;
                        $oldDeptChair->save();
                    } catch (\Exception $e) {
                        $department->deptchair = 0;
                        $department->save();
                    }

                }


                $department->department_name = $department->department_name;
                $department->department_initial = Input::get('department_initial');
                $department->college_belongs = $department->college_belongs;


                // if the user chooses not null
                // means ichange ang status ng bagong dept chair sa user na table
                if ((Input::get('deptchair')) != NULL) {
                    // echo("hey1");
                    // find the new dept chair to the user table
                    $department->deptchair = Input::get('deptchair');
                    $user = User::find(intval(Input::get('deptchair')));
                    $user->position = '1';
                    $user->save();
                } else {
                    // else walang dapat baguhin
                    $department->deptchair = NULL;
                }


                $department->save();

                $notification = "SUCCESS :  Department has been successfully updated.";
                $activeTab = 3;
                return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
            } // dept chair is already been taken
            else {
                // change ang position sang old dept chair to 0(faculty)
                if (!empty($department->deptchair)) {
                    try {
                        $oldDeptChair = User::find($department->deptchair);
                        $oldDeptChair->position = 0;
                        $oldDeptChair->save();
                    } catch (\Exception $e) {
                        $department->deptchair = 0;
                        $department->save();
                    }

                }

                $department->department_name = $department->department_name;
                $department->department_initial = Input::get('department_initial');
                $department->college_belongs = $department->college_belongs;


                // if the user chooses not null
                // means ichange ang status ng bagong dept chair sa user na table
                if ((Input::get('deptchair')) != NULL) {
                    // echo("hey1");
                    // find the new dept chair to the user table
                    $department->deptchair = Input::get('deptchair');
                    $user = User::find(intval(Input::get('deptchair')));
                    $user->position = '1';
                    $user->save();
                } else {
                    // else walang dapat baguhin
                    $department->deptchair = NULL;
                }

                $department->save();

                $notification = "ERROR : College name has already been taken. Please try again";
                $activeTab = 3;
                return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
            }

        }
    }


    ///////////////////////
    // LIST OF ALL USERS //
    ///////////////////////
    public function DeleteProfile($id)
    {
        $this->CallForPromtion();
        $user = User::find($id);
        if ($user->position == 1) {
            $sqlDepartment = DB::select('SELECT departments.id FROM departments INNER JOIN users ON users.department_belongs = departments.department_name 
            WHERE users.id = \'' . $user->id . '\'');
            $department = Department::find($sqlDepartment[0]->id);
            $department->deptchair = null;
            $department->save();
        } elseif ($user->position == 2) {
            $sqlCollege = DB::select('SELECT colleges.id
                                    FROM
                                    users
                                    INNER JOIN departments ON users.department_belongs = departments.department_name
                                    INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                    WHERE
                                    users.department_belongs = departments.department_name AND
                                    departments.college_belongs = colleges.college_name AND
                                    users.id = \'' . $user->id . '\' 
                                    ');
            $college = College::find($sqlCollege[0]->id);
            $college->dean = null;
            $college->save();
        } elseif ($user->position == 3 || $user->position == 4) {
            $university = University::find(1);
            if ($user->position == 3) {
                $university->vchancellor = null;
            } else {
                $university->chancellor = null;
            }
            $university->save();
        }

        User::destroy($id);
        $notification = "SUCCESS : User has been successfully removed.";
        $activeTab = 4;
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function ResetPassword($id)
    {
        $this->CallForPromtion();
        $user = User::find($id);
        $user->password = Hash::make('upmindanao');
        $user->save();
        $notification = "SUCCESS : Password has been successfully reset.";
        $activeTab = 4;
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function SaveNewUser()
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT users.id FROM users WHERE users.employee_code = ' . Input::get('employee_code'));
        if(empty(Input::get('department_belongs')) && Input::get('position')!=5){
            $activeTab = 4;
            $notification = "ERROR : Faculty member must belong to a department";
            return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
        }else{
            if (empty($sql)) {
                $user = new User;
                $user->employee_code = Input::get('employee_code');
                $user->first_name = Input::get('first_name');
                $user->middle_name = Input::get('middle_name');
                $user->last_name = Input::get('last_name');
                $user->gender = Input::get('gender');
                $user->birthday = Input::get('birthday');
                $user->address = Input::get('address');
                $user->Password = Hash::make('upmindanao');
                $user->department_belongs = Input::get('department_belongs');


                $user->rank = Input::get('rank');
                $user->step = Input::get('step');
                $user->position = Input::get('position');

                $user->save();
                $activeTab = 4;
                $notification = "SUCCESS : New user has been successfully added to the system.";
                return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
            } else {
                $activeTab = 4;
                $notification = "ERROR : Employee code has already been taken. Please try again.";
                return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);

            }
        }
        
    }

    public function SaveProfile($employee_code)
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT users.id FROM users WHERE users.employee_code = ' . $employee_code);
        $user = User::find($sql[0]->id);


        $flag = 0;
        // checks if there's an exisiting employee code
        // based on his updated employee code
        if ($user->employee_code == Input::get('employee_code')) {
            $flag = 1;
            $user->employee_code = $user->employee_code;
        } else {
            // no existing
            $sql1 = DB::select('SELECT DISTINCT users.employee_code FROM users WHERE users.employee_code =' . Input::get('employee_code'));
            if (empty($sql1[0])) {
                $user->employee_code = Input::get('employee_code');
                $flag = 2;
            } // theres an exisiting
            else {
                $user->employee_code = $user->employee_code;
                $flag = 3;
            }
        }


        // change his position if he wants to his department
        if ($user->department_belongs != Input::get('department_belongs')) {
            if ($user->position == 1) {
                $sqlDepartment = DB::select('SELECT departments.id FROM departments INNER JOIN users ON users.department_belongs = departments.department_name 
                WHERE users.id = \'' . $user->id . '\'');
                $department = Department::find($sqlDepartment[0]->id);
                $department->deptchair = null;
                $department->save();
            } elseif ($user->position == 2) {
                $sqlCollege = DB::select('SELECT colleges.id
                                        FROM
                                        users
                                        INNER JOIN departments ON users.department_belongs = departments.department_name
                                        INNER JOIN colleges ON departments.college_belongs = colleges.college_name
                                        WHERE
                                        users.department_belongs = departments.department_name AND
                                        departments.college_belongs = colleges.college_name AND
                                        users.id = \'' . $user->id . '\' 
                                        ');
                $college = College::find($sqlCollege[0]->id);
                $college->dean = null;
                $college->save();
            } elseif ($user->position == 3 || $user->position == 4) {
                $university = University::find(1);
                if ($user->position == 3) {
                    $university->vchancellor = null;
                } else {
                    $university->chancellor = null;
                }
                $university->save();
            }
            $user->position = 0;
        } else {
            $user->position = $user->position;
        }

        $user->first_name = Input::get('first_name');
        $user->middle_name = Input::get('middle_name');
        $user->last_name = Input::get('last_name');
        $user->gender = Input::get('gender');
        $user->birthday = Input::get('birthday');
        $user->address = Input::get('address');
        $user->department_belongs = Input::get('department_belongs');
        $user->step = Input::get('step');
        $user->rank = Input::get('rank');
        $user->save();


        $activeTab = 4;
        if ($flag == 1 || $flag == 2) {
            $notification = "SUCCESS : Profile has been successfully updated.";
        } else if ($flag == 3) {
            $notification = "ERROR : Employee code has already been taken. Please try again.";
        }
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    /////////////////////
    // MERIT PROMOTION //
    /////////////////////
    public function ViewMeritPromotionSettings()
    {
        $this->CallForPromtion();
        $isAllowedToAddNewDate = DB::select('SELECT * FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
        $mpromotionDates = DB::select('SELECT * FROM mpromotion');

        $forms = array();
        $count = 0;
        if (!empty($mpromotionDates)) {
            foreach ($mpromotionDates as $mpromotionDate) {
                $temp = DB::select('SELECT
                                    *
                                    FROM
                                    mpromotion_forms
                                    WHERE
                                    mpromotion_forms.callForPromotion_ID = \'' . $mpromotionDate->id . '\' AND 
                                    (mpromotion_forms.form_status >= \'1\' AND mpromotion_forms.form_status != \'5\') 
                ');
                $forms[$count] = $temp;
                $count++;
            }
        }
        return view('admin.ViewMeritPromotionSettings', compact('mpromotionDates', 'isAllowedToAddNewDate', 'forms'));
    }

    public function SaveDate()
    {
        $this->CallForPromtion();
        try {
            $mpromotion = new Mpromotion;
            $mpromotion->mpromotion_date_start = Input::get('mpromotion_date_start');
            $mpromotion->mpromotion_date_end = Input::get('mpromotion_date_end');
            $mpromotion->details = Input::get('details');

            $startDate = strtotime(Input::get('mpromotion_date_start'));
            $endDate = strtotime(Input::get('mpromotion_date_end'));
            $currentDate = strtotime(date("Y-m-d"));

            if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                $mpromotion->mpromotion_status = 0;
                $mpromotion->save();
            } else if ($currentDate > $endDate) {
                // call for promotion status: finished
                $mpromotion->mpromotion_status = 1;
                $mpromotion->save();
            } else if ($startDate > $currentDate) {
                // call for promotion status: not yet started
                $mpromotion->mpromotion_status = 2;
                $mpromotion->save();
            }
            $notification = "SUCCESS : Call for promotion date has been successfully added.";
        } catch (Exception $e) {
            $notification = "ERROR : There is something wrong with the process. Please try again.";
        }
        $activeTab = 5;
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function DeleteDate($id)
    {
        $this->CallForPromtion();
        try {
            Mpromotion::destroy($id);
            $notification = "SUCCESS :  Date has been successfully deleted.";
        } catch (\Exception $e) {
            $notification = "ERROR :  Date cannot be deleted.";
        }
        $activeTab = 5;
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function UpdateDate($id)
    {
        $this->CallForPromtion();
        try {
            $mpromotion = Mpromotion::find($id);
            $mpromotion->mpromotion_date_start = Input::get('mpromotion_date_start');
            $mpromotion->mpromotion_date_end = Input::get('mpromotion_date_end');
            $mpromotion->details = Input::get('details');

            $startDate = strtotime(Input::get('mpromotion_date_start'));
            $endDate = strtotime(Input::get('mpromotion_date_end'));
            $currentDate = strtotime(date("Y-m-d"));

            if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                $mpromotion->mpromotion_status = 0;
                $mpromotion->save();
            } else if ($currentDate > $endDate) {
                // call for promotion status: finished
                $mpromotion->mpromotion_status = 1;
                $mpromotion->save();
            } else if ($startDate > $currentDate) {
                // call for promotion status: not yet started
                $mpromotion->mpromotion_status = 2;
                $mpromotion->save();
            }

            $notification = "SUCCESS : Call for promotion date has been successfully updated.";
        } catch (\Exception $e) {
            $notification = "ERROR :  There is something wrong with the process. Please try again.";
        }
        $activeTab = 5;
        return redirect('/admin/university/UniversitySettings')->with('notification', $notification)->with('activeTab', $activeTab);
    }
}


