@if(Auth::user())
    @if((Auth::user()->position)==5)
        <?php $preLink = FOLDERNAME . '/admin/'; ?>
        <?php $currentUser = 'admin'; ?>
    @elseif((Auth::user()->position)==4)
        <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
        <?php $currentUser = 'chancellor'; ?>
    @elseif((Auth::user()->position)==3)
        <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
        <?php $currentUser = 'vchancellor'; ?>
    @elseif((Auth::user()->position)==2)
        <?php $preLink = FOLDERNAME . '/dean/'; ?>
        <?php $currentUser = 'dean'; ?>
    @elseif((Auth::user()->position)==1)
        <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
        <?php $currentUser = 'deptchair'; ?>
    @elseif((Auth::user()->position)==0)
        <?php $preLink = FOLDERNAME . '/faculty/'; ?>
        <?php $currentUser = 'faculty'; ?>
    @endif
@endif

<div class="tab-pane" id="category-2" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">
        <h4 id="ASDASD">
            <div class="btn-group pull-right">
                <a href="javascript:void(0)" data-plugin="webuiPopover"
                   data-animation="pop" data-placement="left"
                   data-delay-show="0"
                   data-title="More Information" data-target="webuiPopover9"
                   data-content="<small> <table class='table'>
                                    <tbody>            

                                    <tr>
                                        <td>Total no. of Colleges</td>
                                        <td>{!! count($colleges) !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Total no. of Departments</td>
                                        <td>{!! count($departments) !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Total no. of Faculty Members</td>
                                        <td>{!! count($allFaculty) !!} </td>
                                    </tr>
        
                                    </tbody>
                                    </table> </small>
                                 "><i class="icon wb-info-circle" aria-hidden="true"></i>
                </a>
            </div>
            <center><b>FACULTY MEMBERS (UP MINDANAO)</b><br></center>
        </h4>

        @if(session('activeTab')==2)
            @if(!empty(session('notification')))
                <br>
                <?php $notif = session('notification');?>
                @if(strpos($notif,'SUCCESS')===false)
                    <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @else
                    <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @endif
            @endif
        @endif
        <hr>

        <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Employee Code</th>
                <th>Name</th>
                <th>Department</th>
                <th>Position</th>
                <th>Rank & Step</th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th>#</th>
                <th>Employee Code</th>
                <th>Name</th>
                <th>Department</th>
                <th>Position</th>
                <th>Rank & Step</th>
            </tr>
            </tfoot>

            <tbody>
            <?php $count = 1; ?>
            @foreach ($users as $user)
                <tr id="{{'MoreInfo-'.$user->id}}" data-target="#ModalMInfo-modal-1"
                    data-toggle="modal">
                    <td role="button"> <?php echo $count++; ?> </td>
                    <td role="button"> {{ $user->employee_code }} </td>
                    <td role="button"> {{ $user->last_name.', '. $user->first_name. ' '.$user->middle_name[0].'.'  }} </td>
                    <td role="button"> {{ $user->department_belongs }} </td>
                    <td role="button">
                        @if(($user->position)==0)
                            <?php echo 'Faculty Member'; ?>
                        @elseif(($user->position)==1)
                            <?php echo 'Department Chairperson'; ?>
                        @elseif(($user->position)==2)
                            <?php echo 'College Dean'; ?>
                        @elseif(($user->position)==3)
                            <?php echo 'Vice Chancellor'; ?>
                        @elseif(($user->position)==4)
                            <?php echo 'Chancellor'; ?>
                        @endif
                    </td>
                    <td role="button">
                        {{ $user->rank.' '.$user->step }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>


    </div>
</div>
@include('deptchair.FacultyMembersMoreInfo-Script')
@include('deptchair.FacultyMembersMoreInfo-View')
