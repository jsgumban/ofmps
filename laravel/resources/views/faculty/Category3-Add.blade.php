{{-- FORM CATEGORY 3 MODAL --}}
<div class="modal fade modal-warning  example-modal-lg" id="exampleModalWarning5" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">A. SERVICE TO DEPARTMENT/COLLEGE/UNIVERSITY </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="FormCat3Cat1" onclick="FormCat3Cat1ShowDiv()"
                            onkeydown="FormCat3Cat1ShowDiv()"
                            onkeydown="FormCat3Cat1ShowDiv()" onkeypress="FormCat3Cat1ShowDiv()">
                        <option value=1>01. Regular administrative positions in the College/Institute/Department
                        </option>
                        <option value=2>02. Membership in a standing committee of the
                            Institute/Department/College/University
                        </option>
                        <option value=3>03. Resource Generation (at least PhP 1M; over and above research funds
                            contributed to the unit, college or university)
                        </option>
                        <option value=4>04. Officer-in-Charge to the Chancellor/Vice-Chancellor/Dean/Director/College
                            Secretary/Department Head
                        </option>
                    </select>
                </div>

                {{-- SUBCATEGORY 1--}}
                <div id="1FormCat3Cat1" class="FormCat3Cat1ShowDivTargetDiv">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat3' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Designation</label>
                            <select class="form-control" name="designation">
                                <option value="Chancellor">1.1. Chancellor
                                </option>
                                <option value="Vice Chancellor">1.2. Vice Chancellor
                                </option>
                                <option value="Dean">1.3. Dean
                                </option>
                                <option value="Associate Dean">1.4. Associate Dean
                                </option>
                                <option value="University Registrar">1.5. University Registrar
                                </option>
                                <option value="College Secretaries">1.6. College Secretaries
                                </option>
                                <option value="Department Heads">1.7. Department Heads
                                </option>
                                <option value="Assistant College Secretary/Academic/Administrative Unit Director">1.8.
                                    Assistant College Secretary/Academic/Administrative Unit Director
                                </option>
                                <option value="Deputy Directors/Assistant to the Chair">1.9. Deputy Directors/Assistant
                                    to the Chair
                                </option>
                                <option value="Program Coordinators">1.10. Program Coordinators
                                </option>
                                <option value="Chair of Standing Committees">1.11. Chair of Standing Committees
                                </option>
                            </select>
                        </div>

                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small><small>  <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> Chancellor/Vice Chancellor/Deans </td>
                                            <td>20max/year</td>
                                        </tr>
                                        <tr>
                                            <td> Associate  Deans/University Registrar/ CollegeSecretaries/Department Heads </td>
                                            <td>17.5max/year</td>
                                        </tr>

                                        <tr>
                                            <td> Assistant College Secretary/Academic/Administrative Unit Director</td>
                                            <td>15max/year</td>
                                        </tr>
                                        
                                        <tr>
                                            <td> Deputy Directors/Assistant to the Chair </td>
                                            <td>12.5max/year</td>
                                        </tr>

                                        <tr>
                                            <td>Program Coordinators   </td>
                                            <td>10.5max/year</td>
                                        </tr>

                                        <tr>
                                            <td> Chair of Standing Committees/Course Coordinators   </td>
                                            <td>5max/year</td>
                                        </tr>

                                        </tbody>
                                        </table> </small></small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="20" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Submit</button>
                        </div>

                    </form>
                </div>


                {{-- SUBCATEGORY 2--}}
                <div id="2FormCat3Cat1" class="FormCat3Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat3' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 2) !!}


                        {{-- Name of Committee --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Committee</label>
                            <input type="text" class="form-control" name="name_ofcommittee"
                                   placeholder="Name of Committee" required>
                        </div>

                        {{-- Nature of Committee --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Committee</label>
                            <input type="text" class="form-control" name="nature_ofcommittee"
                                   placeholder="Nature of Committee" required>
                        </div>


                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small><table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <tr>
                                            <td> Membership in a standing committee of the Institute/ Department/ College/University   </td>
                                            <td>6 committees max/year</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="6" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 3 --}}
                <div id="3FormCat3Cat1" class="FormCat3Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat3' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 3) !!}


                        {{-- Funding Source --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Funding Source</label>
                            <input type="text" class="form-control" name="funding_source"
                                   placeholder="Funding Source" required>
                        </div>

                        {{-- Purpose of Fund --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Purpose of Fund</label>
                            <input type="text" class="form-control" name="purpose_offund"
                                   placeholder="Purpose of Fund" required>
                        </div>


                        {{-- Amount Donated --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Amount Donated</label>
                            <input type="text" class="form-control" name="ammount_donated"
                                   placeholder="Amount Donated" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small>  <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Resource Generation over and above research funds contributed to the unit, college or university </td>
                                            <td>1 pt every Php20,000</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0"  required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 4--}}
                <div id="4FormCat3Cat1" class="FormCat3Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat3' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}
                        {!! Form::hidden('subcategory', 4) !!}


                        {{-- Designation of OIC --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Designation of OIC</label>
                            <input type="text" class="form-control" name="designation_ofoic"
                                   placeholder="Designation of OIC" required>
                        </div>

                        {{-- Level  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Level</label>
                            <input type="text" class="form-control" name="level"
                                   placeholder="Level " required>
                        </div>


                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Officer-in-charge to the Chancellor/Vice Chancellor/Dean/ Director/College Secretary/Department Head </td>
                                            <td>0.5 pt/week (or 5 working days)</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0"  required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Submit</button>
                        </div>

                    </form>
                </div>


            </div>
        </div>
    </div>
</div>

{{-- FORM CATEGORY 3 MODAL --}}
<div class="modal fade modal-warning  example-modal-lg" id="exampleModalWarning6" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">B. SERVICE TO INTERNATIONAL/NATIONAL/REGIONAL COMMUNITY </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="FormCat3Cat2" onclick="FormCat3Cat2ShowDiv()"
                            onkeydown="FormCat3Cat2ShowDiv()"
                            onkeydown="FormCat3Cat2ShowDiv()" onkeypress="FormCat3Cat2ShowDiv()">
                        <option value=1>01. Presidency/Chairmanship of Professional association/organization
                        </option>
                        <option value=2>02. Position of responsibility in Professional association/organization</option>
                    </select>
                </div>

                {{-- SUBCATEGORY 1--}}
                <div id="1FormCat3Cat2" class="FormCat3Cat2ShowDivTargetDiv">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat3' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subsubcategory">
                                <option value=" International/Regional">1.1. International/Regional
                                </option>
                                <option value="National (Local)">1.2. National (Local)
                                </option>

                            </select>
                        </div>


                        {{-- Name of Organization --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Organization</label>
                            <input type="text" class="form-control" name="name_oforganization"
                                   placeholder="Name of Organization" required>
                        </div>

                        {{-- Designation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Designation </label>
                            <input type="text" class="form-control" name="designation"
                                   placeholder="Designation " required>
                        </div>


                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>International/Regional</td>
                                            <td>4pts</td>
                                        </tr>
                                        <tr>
                                            <td>National (Local) </td>
                                            <td>2pts</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="4" max="" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- SUBCATEGORY 2--}}
                <div id="2FormCat3Cat2" class="FormCat3Cat2ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat3' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 2) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subsubcategory">
                                <option value="International/Regional">2.1. International/Regional
                                </option>
                                <option value="National (Local)">2.2. National (Local)
                                </option>

                            </select>
                        </div>


                        {{-- >Name of Organization --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Organization</label>
                            <input type="text" class="form-control" name="name_oforganization"
                                   placeholder="Name of Organization" required>
                        </div>

                        {{-- Designation --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Designation </label>
                            <input type="text" class="form-control" name="designation"
                                   placeholder="Designation " required>
                        </div>


                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered</label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>International/Regional</td>
                                            <td>4pts</td>
                                        </tr>
                                        <tr>
                                            <td>National (Local) </td>
                                            <td>2pts</td>
                                        </tr>


                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat3" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=inputFileAddCat3]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }

            }
        });
    });
</script>

{{-- FORMCAT2 CAT1 --}}
<script type="text/javascript">
    function FormCat3Cat1ShowDiv() {

        // make all the divs hidden
        var a = document.getElementsByClassName("FormCat3Cat1ShowDivTargetDiv");
        var b;
        for (b = 0; b < a.length; b++) {
            a[b].style.display = "none";
        }

        // make the selected value appear
        var selectFormCat3Cat1 = document.getElementById("FormCat3Cat1");

        var valueFormCat3Cat1 = selectFormCat3Cat1.options[selectFormCat3Cat1.selectedIndex].value;
        var divID = valueFormCat3Cat1 + "FormCat3Cat1";
        // alert(divID);
        document.getElementById(divID).style.display = "block";

    }

    // elem.onchange = function(){
    //     var showdiv = document.getElementById(val);
    //     showdiv.style.display = (this.value == val) ? "block":"none";
    // };
</script>

{{-- FORMCAT2 CAT2 --}}
<script type="text/javascript">
    function FormCat3Cat2ShowDiv() {

        // make all the divs hidden
        var a = document.getElementsByClassName("FormCat3Cat2ShowDivTargetDiv");
        var b;
        for (b = 0; b < a.length; b++) {
            a[b].style.display = "none";
        }

        // make the selected value appear
        var selectFormCat3Cat2 = document.getElementById("FormCat3Cat2");

        var valueFormCat3Cat2 = selectFormCat3Cat2.options[selectFormCat3Cat2.selectedIndex].value;
        var divID = valueFormCat3Cat2 + "FormCat3Cat2";
        // alert(divID);
        document.getElementById(divID).style.display = "block";

    }

    // elem.onchange = function(){
    //     var showdiv = document.getElementById(val);
    //     showdiv.style.display = (this.value == val) ? "block":"none";
    // };
</script>

