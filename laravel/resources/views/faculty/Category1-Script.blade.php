<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("[id^=UpdateFC1-tr]").click(function (e) {
            e.preventDefault();

            // REVISE FUNCTION INITIALIZATION
            @if (Request::is('*/ReviseForm'))
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=UpdateFC1-]").attr('disabled', false);
            @endif

            // UPLOAD ATTACHMENT INITIALIZATION
            $('input:radio[name="proofAction"][value="1"]').prop('checked', true);
            $("[id^=c1BrowseDiv]").css('display', 'block');

            var id = (this.id).split('-');
            var category = id[2];
            var subCategory = id[3];
            var entryID = id[4];

            var retrieveDataUrl = "{{ $preLink.'meritpromotion/' }}" + entryID + '/RetrieveCat1';
            var submitButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/UpdateFormCat1';
            var deleteButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/DeleteEntryCat1';

            var x = document.getElementsByClassName("UpdateFC1-divClass-2");
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById("UpdateFC1-divID-" + subCategory).style.display = "block";

            $.get(retrieveDataUrl, function (data) {

                // REVISE FORM 
                @if (Request::is('*/ReviseForm'))
                    // initialize
                var entryStatus = data.isAccepted_deptchair;
                // submit buttons - textfields - upload section
                $('[id^=UpdateFC1-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC1-]").attr('disabled', true);
                $("[id^=UpdateFC1-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                // needs revision
                if (entryStatus == 3) {
                    $('[id^=UpdateFC1-reviseDiv-]').css('display', 'block');
                    $("[id^=UpdateFC1-]").attr('disabled', false);
                    $("[id^=UpdateFC1-proofs-]").css('display', "block");
                    $("[id^=ribbon-revise]").css('display', "block");
                    $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                } else if (entryStatus == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                }
                @endif

                @if (Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    // submit buttons - textfields - upload section
                $('[id^=UpdateFC1-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC1-]").attr('disabled', true);
                $("[id^=UpdateFC1-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                if (data.isAccepted_deptchair == 3) {
                    $("[id^=ribbon-revise]").css('display', "block");
                    $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                } else if (data.isAccepted_deptchair == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                } else if (data.isAccepted_dean == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                } else if (data.isAccepted_vchancellor == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (UAPFC)");
                }
                @endif

                @if (Request::is('*/EvaluateForm'))
                    // checkbox
                $('[id^=labelauty-][value="1"]').prop("checked", false);
                $('[id^=labelauty-][value="2"]').prop("checked", false);
                $('[id^=labelauty-][value="3"]').prop("checked", false);

                // submit buttons - textfields - upload section
                $('[id^=UpdateFC1-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC1-]").attr('disabled', true);
                $("[id^=UpdateFC1-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                // points
                // data-points - check box purposes
                $("[id^=UpdateFC1-points-]").attr('disabled', true);
                $("[id^=UpdateFC1-points-]").css('background-color', '');
                $("[id^=UpdateFC1-points-]").css('color', '');
                $("[id^=labelauty]").attr("data-points", data.points);

                var formStatus = ({{ $MpromotionForm->form_status }});

                // FORM IS CURRENTLY EVALUATED BY THE DEPTCHAIR
                if (formStatus == 1) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    // accepted
                    if (entryStatusDC == 1) {
                        $("[id^=UpdateFC1-points-]").css('background-color', '#FFEFD5');
                        $("[id^=UpdateFC1-points-]").css('color', 'black');
                        $("[id^=UpdateFC1-points-]").attr('disabled', false);
                        $('[id^=labelauty-][value="1"]').prop("checked", true);

                        $("[id^=ribbon-accept]").css('display', "block");
                        $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (DAPC)");
                    }

                    // rejected
                    else if (entryStatusDC == 2) {
                        $("[id^=UpdateFC1-points-]").css('background-color', '');
                        $("[id^=UpdateFC1-points-]").css('color', '');
                        $("[id^=UpdateFC1-points-]").attr('disabled', true);
                        $('[id^=labelauty-][value="2"]').prop("checked", true);

                        $("[id^=ribbon-rejected]").css('display', "block");
                        $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                    }

                    // needs revision
                    else if (entryStatusDC == 3) {
                        $("[id^=UpdateFC1-points-]").css('background-color', '');
                        $("[id^=UpdateFC1-points-]").css('color', '');
                        $("[id^=UpdateFC1-points-]").attr('disabled', true);
                        $('[id^=labelauty-][value="3"]').prop("checked", true);


                        $("[id^=ribbon-revise]").css('display', "block");
                        $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                    }

                    // not yet evaluated
                    else if (entryStatusDC == null) {
                        $("[id^=UpdateFC1-points-]").css('background-color', '');
                        $("[id^=UpdateFC1-points-]").css('color', '');
                        $("[id^=UpdateFC1-points-]").attr('disabled', true);

                        $("[id^=ribbon-notyet]").css('display', "block");
                        $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (DAPC)");
                    }
                }

                // FORM IS CURRENTLY EVALUATED BY THE DEAN
                else if (formStatus == 2) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    var entryStatusDean = data.isAccepted_dean;
                    $("[id^=evaluationButtons]").css('display', "block");

                    if (entryStatusDC == 2) {
                        $("[id^=UpdateFC1-points-]").css('background-color', '');
                        $("[id^=UpdateFC1-points-]").css('color', '');
                        $("[id^=UpdateFC1-points-]").attr('disabled', true);
                        $("[id^=evaluationButtons]").css('display', "none");

                        $("[id^=ribbon-rejected]").css('display', "block");
                        $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                    } else {
                        if (entryStatusDean == 1) {
                            $("[id^=UpdateFC1-points-]").css('background-color', '#FFEFD5');
                            $("[id^=UpdateFC1-points-]").css('color', 'black');
                            $("[id^=UpdateFC1-points-]").attr('disabled', false);
                            $('[id^=labelauty-][value="1"]').prop("checked", true);


                            $("[id^=ribbon-accept]").css('display', "block");
                            $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (CAPC)");
                        }

                        else if (entryStatusDean == 2) {
                            $("[id^=UpdateFC1-points-]").css('background-color', '');
                            $("[id^=UpdateFC1-points-]").css('color', '');
                            $("[id^=UpdateFC1-points-]").attr('disabled', true);
                            $('[id^=labelauty-][value="2"]').prop("checked", true);

                            $("[id^=ribbon-rejected]").css('display', "block");
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                        }

                        else if (entryStatusDean == null) {
                            $("[id^=UpdateFC1-points-]").css('background-color', '');
                            $("[id^=UpdateFC1-points-]").css('color', '');
                            $("[id^=UpdateFC1-points-]").attr('disabled', true);

                            $("[id^=ribbon-notyet]").css('display', "block");
                            $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (CAPC)");
                        }
                    }
                }

                // FORM IS CURRENTLY EVALUATED BY THE VCHANCELLOR
                else if (formStatus == 3 || formStatus == 4) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    var entryStatusDean = data.isAccepted_dean;
                    var entryStatusVchancellor = data.isAccepted_vchancellor;

                    @if(Auth::user()->position==4)
                        $("[id^=evaluationButtons]").css('display', "none");
                    @else
                        $("[id^=evaluationButtons]").css('display', "block");
                    @endif


                    if (entryStatusDC == 2 || entryStatusDean == 2) {
                        $("[id^=UpdateFC1-points-]").css('background-color', '');
                        $("[id^=UpdateFC1-points-]").css('color', '');
                        $("[id^=UpdateFC1-points-]").attr('disabled', true);
                        $("[id^=evaluationButtons]").css('display', "none");

                        $("[id^=ribbon-rejected]").css('display', "block");

                        if (entryStatusDC == 2) {
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                        }

                        if (entryStatusDean == 2) {
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                        }
                    } else {
                        if (entryStatusVchancellor == 1) {

                            $('[id^=labelauty-][value="1"]').prop("checked", true);


                            $("[id^=ribbon-accept]").css('display', "block");

                            @if(Auth::user()->position==4)
                                $("[id^=ribbon-accept] .ribbon-inner").html("ENTRY ACCEPTED");
                            $("[id^=UpdateFC1-points-]").attr('disabled', true);
                            $("[id^=UpdateFC1-points-]").css('background-color', '');
                            $("[id^=UpdateFC1-points-]").css('color', '');
                            @else
                                $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (UAPFC)");
                            $("[id^=UpdateFC1-points-]").attr('disabled', false);
                            $("[id^=UpdateFC1-points-]").css('background-color', '#FFEFD5');
                            $("[id^=UpdateFC1-points-]").css('color', 'black');
                            @endif


                        }

                        else if (entryStatusVchancellor == 2) {
                            $("[id^=UpdateFC1-points-]").css('background-color', '');
                            $("[id^=UpdateFC1-points-]").css('color', '');
                            $("[id^=UpdateFC1-points-]").attr('disabled', true);
                            $('[id^=labelauty-][value="2"]').prop("checked", true);

                            $("[id^=ribbon-rejected]").css('display', "block");
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (UAPFC)");
                        }

                        else if (entryStatusVchancellor == null) {
                            $("[id^=UpdateFC1-points-]").css('background-color', '');
                            $("[id^=UpdateFC1-points-]").css('color', '');
                            $("[id^=UpdateFC1-points-]").attr('disabled', true);

                            $("[id^=ribbon-notyet]").css('display', "block");
                            $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (UAPFC)");
                        }
                    }
                }
                @endif

            

                // RETRIEVE ENTRY INFORMATION
                if (category == 1) {
                    $('#UpdateFC1-WARating-1-1').val(data.WARating);
                    $('#UpdateFC1-points-1-1').val(data.points);
                    $('#UpdateFC1-form-1-1').attr('action', submitButtonUrl);
                    $('#UpdateFC1-delete-1-1').attr('href', deleteButtonUrl);
                } else if (category == 2) {
                    $('#UpdateFC1-SubSubCat-2').val(subCategory);
                    if (subCategory == 1) {
                        $('#UpdateFC1-title-2-1').val(data.title);
                        $('#UpdateFC1-author-2-1').val(data.author);
                        $('#UpdateFC1-date_published-2-1').val(data.date_published);
                        $('#UpdateFC1-publisher-2-1').val(data.publisher);
                        $('#UpdateFC1-semester_used-2-1').val(data.semester_used);
                        $('#UpdateFC1-ay_used-2-1').val(data.ay_used);
                        $('#UpdateFC1-points-2-1').val(data.points);
                        $('#UpdateFC1-course_title-2-1').val(data.course_title);
                        $('#UpdateFC1-form-2-1').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-1').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-1').attr('disabled', true);
                            $('#c1radio3-2-1').attr('disabled', true);
                        }
                    } else if (subCategory == 2) {
                        $('#UpdateFC1-title-2-2').val(data.title);
                        $('#UpdateFC1-author-2-2').val(data.author);
                        $('#UpdateFC1-date_published-2-2').val(data.date_published);
                        $('#UpdateFC1-publisher-2-2').val(data.publisher);
                        $('#UpdateFC1-semester_used-2-2').val(data.semester_used);
                        $('#UpdateFC1-ay_used-2-2').val(data.ay_used);
                        $('#UpdateFC1-points-2-2').val(data.points);
                        $('#UpdateFC1-course_title-2-2').val(data.course_title);


                        $('#UpdateFC1-form-2-2').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-2').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-2').attr('disabled', true);
                            $('#c1radio3-2-2').attr('disabled', true);
                        }


                    } else if (subCategory == 3) {
                        $('#UpdateFC1-title-2-3').val(data.title);
                        $('#UpdateFC1-author-2-3').val(data.author);
                        $('#UpdateFC1-date_published-2-3').val(data.date_published);
                        $('#UpdateFC1-publisher-2-3').val(data.publisher);
                        $('#UpdateFC1-semester_used-2-3').val(data.semester_used);
                        $('#UpdateFC1-ay_used-2-3').val(data.ay_used);
                        $('#UpdateFC1-points-2-3').val(data.points);
                        $('#UpdateFC1-course_title-2-3').val(data.course_title);

                        $('#UpdateFC1-form-2-3').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-3').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-3').attr('disabled', true);
                            $('#c1radio3-2-3').attr('disabled', true);
                        }


                    } else if (subCategory == 4) {
                        $('#UpdateFC1-title-2-4').val(data.title);
                        $('#UpdateFC1-author-2-4').val(data.author);
                        $('#UpdateFC1-date_published-2-4').val(data.date_published);
                        $('#UpdateFC1-publisher-2-4').val(data.publisher);
                        $('#UpdateFC1-semester_used-2-4').val(data.semester_used);
                        $('#UpdateFC1-ay_used-2-4').val(data.ay_used);
                        $('#UpdateFC1-points-2-4').val(data.points);
                        $('#UpdateFC1-course_title-2-4').val(data.course_title);

                        $('#UpdateFC1-form-2-4').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-4').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-4').attr('disabled', true);
                            $('#c1radio3-2-4').attr('disabled', true);
                        }
                    } else if (subCategory == 5) {
                        $('#UpdateFC1-title-2-5').val(data.title);
                        $('#UpdateFC1-author-2-5').val(data.author);
                        $('#UpdateFC1-date_published-2-5').val(data.date_published);
                        $('#UpdateFC1-points-2-5').val(data.points);

                        $('#UpdateFC1-form-2-5').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-5').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-5').attr('disabled', true);
                            $('#c1radio3-2-5').attr('disabled', true);
                        }
                    } else if (subCategory == 6) {
                        $('#UpdateFC1-title-2-6').val(data.title);
                        $('#UpdateFC1-author-2-6').val(data.author);
                        $('#UpdateFC1-date_published-2-6').val(data.date_published);
                        $('#UpdateFC1-points-2-6').val(data.points);
                        $('#UpdateFC1-publisher-2-6').val(data.publisher);

                        $('#UpdateFC1-form-2-6').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-6').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-6').attr('disabled', true);
                            $('#c1radio3-2-6').attr('disabled', true);
                        }
                    } else if (subCategory == 7) {
                        $('#UpdateFC1-title-2-7').val(data.title);
                        $('#UpdateFC1-author-2-7').val(data.author);
                        $('#UpdateFC1-date_published-2-7').val(data.date_published);
                        $('#UpdateFC1-bibliography-2-7').val(data.bibliography);
                        $('#UpdateFC1-nature_ofpublication-2-7').val(data.nature_ofpublication);
                        $('#UpdateFC1-points-2-7').val(data.points);

                        $('#UpdateFC1-form-2-7').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-7').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-7').attr('disabled', true);
                            $('#c1radio3-2-7').attr('disabled', true);
                        }
                    } else if (subCategory == 8) {
                        $('#UpdateFC1-title-2-8').val(data.title);
                        $('#UpdateFC1-points-2-8').val(data.points);

                        $('#UpdateFC1-form-2-8').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-8').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-8').attr('disabled', true);
                            $('#c1radio3-2-8').attr('disabled', true);
                        }
                    } else if (subCategory == 9) {
                        $('#UpdateFC1-as_what-2-9').val(data.as_what);
                        $('#UpdateFC1-name_ofgraduate-2-9').val(data.name_ofgraduate);
                        $('#UpdateFC1-year_graduated-2-9').val(data.year_graduated);
                        $('#UpdateFC1-points-2-9').val(data.points);

                        $('#UpdateFC1-form-2-9').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-9').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-9').attr('disabled', true);
                            $('#c1radio3-2-9').attr('disabled', true);
                        }
                    } else if (subCategory == 10) {
                        $('#UpdateFC1-as_what-2-10').val(data.as_what);
                        $('#UpdateFC1-name_ofgraduate-2-10').val(data.name_ofgraduate);
                        $('#UpdateFC1-year_graduated-2-10').val(data.year_graduated);
                        $('#UpdateFC1-points-2-10').val(data.points);

                        $('#UpdateFC1-form-2-10').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-10').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-10').attr('disabled', true);
                            $('#c1radio3-2-10').attr('disabled', true);
                        }
                    } else if (subCategory == 11) {
                        $('#UpdateFC1-as_what-2-11').val(data.as_what);
                        $('#UpdateFC1-name_ofgraduate-2-11').val(data.name_ofgraduate);
                        $('#UpdateFC1-year_graduated-2-11').val(data.year_graduated);
                        $('#UpdateFC1-points-2-11').val(data.points);

                        $('#UpdateFC1-form-2-11').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-11').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-11').attr('disabled', true);
                            $('#c1radio3-2-11').attr('disabled', true);
                        }
                    } else if (subCategory == 12) {
                        $('#UpdateFC1-number_ofpreparations-2-12').val(data.number_ofpreparations);
                        $('#UpdateFC1-points-2-12').val(data.points);

                        $('#UpdateFC1-form-2-12').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-12').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-12').attr('disabled', true);
                            $('#c1radio3-2-12').attr('disabled', true);
                        }
                    } else if (subCategory == 13) {
                        $('#UpdateFC1-other_contributions-2-13').val(data.other_contributions);
                        $('#UpdateFC1-points-2-13').val(data.points);

                        $('#UpdateFC1-form-2-13').attr('action', submitButtonUrl);
                        $('#UpdateFC1-delete-2-13').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c1radio2-2-13').attr('disabled', true);
                            $('#c1radio3-2-13').attr('disabled', true);
                        }
                    }
                }
            });


        });
    });
</script>

