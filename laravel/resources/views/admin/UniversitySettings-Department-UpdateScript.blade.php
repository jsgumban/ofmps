<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=UpdateDepartment-]").click(function (e) {
            e.preventDefault();
            var id = (this.id).split('-');
            var entryID = id[1];

            // alert(this.id);depatChairChoices
            var retrieveDataUrl = "{{ $preLink.'university/' }}" + entryID + '/RetrieveDepartment';
            var retrieveDataUrl2 = "{{ $preLink.'university/' }}" + entryID + '/RetrieveDeptChair';
            var submitButtonUrl = "{{ $preLink.'university/'}}" + entryID + '/SaveUpdatedDepartment';
            // var deleteButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/DeleteEntryCat4';

            $('#UpdateDeparmentForm').attr('action', submitButtonUrl);
            $.get(retrieveDataUrl, function (data) {
                $('#UpdateDepartmentName').val(data.department_name);
                $('#UpdateDepartmentInitials').val(data.department_initial);
                $('#college_belongs').val(data.college_belongs);

                $('[id^=UpdateDepartmentDean]').attr("id", "UpdateDepartment" + data.id);
            });


            $.get(retrieveDataUrl2, function (data) {
                $('[id^=UpdateDeptChair]').empty();
                $('[id^=UpdateDeptChair]').append("<option value=''>" + "None" + "</option>");

                for (x = 0; x < data.length; x++) {
                    $('[id^=UpdateDeptChair]').append("<option value='" + data[x].id + "'>" + data[x].last_name + ", " + data[x].first_name + " " + data[x].middle_name[0] + "." + "</option>");
                }
                $('[id^=UpdateDeptChair]').val(data[0].deptchair);
            });


        });
    });
</script>