<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $fillable = [
    	'first_name',
    	'middle_name',
    	'last_name',  
    	'employee_code',
    	'birthday'

    ];

    public $timestamps = false;
    protected $table = 'faculties';
}
