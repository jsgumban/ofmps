@extends('app')
@section('title', 'OFMPS |  HOME')
@section('content')

    @if(Auth::user())
        @if((Auth::user()->position)==5)
            <?php $preLink = '/admin/'; ?>
            <?php $currentUser = 'admin'; ?>
        @elseif((Auth::user()->position)==4)
            <?php $preLink = '/chancellor/'; ?>
            <?php $currentUser = 'chancellor'; ?>
        @elseif((Auth::user()->position)==3)
            <?php $preLink = '/vchancellor/'; ?>
            <?php $currentUser = 'vchancellor'; ?>
        @elseif((Auth::user()->position)==2)
            <?php $preLink = '/dean/'; ?>
            <?php $currentUser = 'dean'; ?>
        @elseif((Auth::user()->position)==1)
            <?php $preLink = '/deptchair/'; ?>
            <?php $currentUser = 'deptchair'; ?>
        @elseif((Auth::user()->position)==0)
            <?php $preLink = '/faculty/'; ?>
            <?php $currentUser = 'faculty'; ?>
        @endif
    @endif

    <div class="page">
        <div class="page-header ">
            <div class="widget widget-shadow">
                <div class="example example-well ">
                    <div class="page-header text-center">
                        <small>{{ date('F d, Y  h:i:sa') }}</small>
                        <h1 class="page-title">WELCOME TO UPMIN OFMPS!</h1>

                        <p class="page-description">
                            UNIVERSITY OF THE PHILIPPINES MINDANAO ONLINE FACULTY MERIT PROMOTION SYSTEM
                        </p>
                    </div>

                    @if((!empty($isThereCallForPromotion)) && ($isThereCallForPromotion[0]->mpromotion_status!=1))
                        <div class="widget-content white bg-facebook padding-20 height-full">
                            <center><big>ATTENTION : CALL FOR PROMOTOTION! </big></center>
                            <hr>
                            <br>

                            <dl class="dl-horizontal">
                                <dt>DETAILS</dt>
                                <dd>{{ $isThereCallForPromotion[0]->details }}</dd>

                                <dt>START DATE</dt>

                                <dd>{{ date('F d, Y', strtotime($isThereCallForPromotion[0]->mpromotion_date_start )) }}</dd>

                                <dt>END DATE</dt>
                                <dd>{{ date('F d, Y', strtotime($isThereCallForPromotion[0]->mpromotion_date_end ))}}</dd>

                                <dt>STATUS</dt>
                                <dd>
                                    @if($isThereCallForPromotion[0]->mpromotion_status==0)
                                        <span class="label label-success">ON-GOING</span>
                                    @elseif($isThereCallForPromotion[0]->mpromotion_status==1)
                                        <span class="label label-danger">FINISHED</span>
                                    @elseif($isThereCallForPromotion[0]->mpromotion_status==2)
                                        <span class="label label-warning">NOT YET STARTED</span>
                                    @endif
                                </dd>

                                @if($currentUser!="admin")
                                    <dt>ACTION</dt>
                                    <dd>

                                        <a href={!! $preLink.'meritpromotion/MeritPromotionForms' !!}>
                                            FILL OUT NOW!
                                        </a>
                                        <i class="icon wb-pencil" aria-hidden="true"></i>

                                    </dd>
                                @endif
                            </dl>
                        </div>
                    @endif

                </div>

            </div>
        </div>
    </div>

@endsection