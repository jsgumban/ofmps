<div class="modal fade modal-info" id="updateCollege" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">


        {{-- FORM --}}
        <form action="#" method="POST" id="UpdateCollegeForm">
            {!! csrf_field() !!}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">UPDATE COLLEGE</h4>
                </div>

                <div class="modal-body">


                    {{-- COLLGE NAME --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">College</label>
                        <input type="text" class="form-control" name="college_name"
                               placeholder="College Name" required id="UpdateCollegeName">
                        {{-- {!! Form::text('college_name', $college->college_name, ['class' => 'form-control', 'required']) !!} --}}
                    </div>

                    {{-- COLLEGE INITIALS --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Initials</label>
                        <input type="text" class="form-control" name="college_initial"
                               placeholder="College Initials" required id="UpdateCollegeInitials">
                        {{-- {!! Form::text('college_initial', $college->college_initial, ['class' => 'form-control', 'required']) !!} --}}
                    </div>


                    {{-- DEAN CANDIDATES --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">College Dean</label>

                        <select class="form-control" id="UpdateCollegeDean" name="dean">
                            <option value=""> None</option>
                        </select>
                    </div>


                </div>

                <div class="modal-footer">


                    {{-- SUBMIT BUTTON --}}
                    <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>


                    {{-- CANCEL BUTTON --}}
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>


            </div>

        </form>
    </div>
</div>