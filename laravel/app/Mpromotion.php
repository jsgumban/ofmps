<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mpromotion extends Model
{
    protected $table = 'mpromotion';
    public $timestamps = false;
    protected $fillable = ['mpromotion_date', 'mpromotion_status'];
}
