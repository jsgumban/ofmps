<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    protected $table = 'colleges';
    public $timestamps = false;
    protected $fillable = ['college_name', 'college_initial'];
}
