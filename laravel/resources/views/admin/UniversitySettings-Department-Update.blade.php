<div class="modal fade modal-info" id="updateDepartment" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">


        {{-- FORM --}}
        <form action="#" method="POST" id="UpdateDeparmentForm">
            {!! csrf_field() !!}
            {{-- {{ }} --}}


            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">UPDATE DEPARTMENT</h4>
                </div>

                <div class="modal-body">


                    {{-- COLLGE BELONGS --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">College</label>
                        <input type="text" class="form-control" name="college_belongs"
                               placeholder="College Belongs" required id="college_belongs" disabled="">
                    </div>

                    {{-- DEPARTMENT NAME --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Department Name</label>
                        <input type="text" class="form-control" name="department_name"
                               placeholder="Department Name" required id="UpdateDepartmentName">
                    </div>


                    {{-- DEPARTMENT INITIALS --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Initials</label>
                        <input type="text" class="form-control" name="department_initial"
                               placeholder="Initials" required id="UpdateDepartmentInitials">

                    </div>


                    {{-- CANDIDATES --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">Department Chairperson </label>
                        <select class="form-control" id="UpdateDeptChair" name="deptchair">
                            <option value=""> None</option>
                        </select>
                    </div>


                </div>
                <div class="modal-footer">


                    {{-- SUBMIT BUTTON --}}
                    <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>


                    {{-- CANCEL BUTTON --}}
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>