<div class="modal fade modal-info" id="addNewDepartment" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">


        {{-- FORM --}}
        <form action={!! $preLink.'university/SaveNewDepartment' !!} method="POST">
            {!! csrf_field() !!}


            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">CREATE NEW DEPARTMENT</h4>
                </div>

                <div class="modal-body">


                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">College </label>
                        <select class="form-control" name="college_belongs" id="college_belongs">
                            @foreach($colleges as $college)
                                <option value="{{ $college->college_name }}">  {!! $college->college_name !!}</option>
                            @endforeach
                        </select>


                    </div>


                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Department Name </label>
                        <input type="text" class="form-control" name="department_name" placeholder="Department Name"
                               required>
                    </div>


                    {{-- DEPARTMENT INITIAL NAME --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Initial</label>
                        <input type="text" class="form-control" name="department_initial" placeholder="Initials">
                    </div>


                </div>
                <div class="modal-footer">


                    {{-- SUBMIT BUTTON --}}
                    <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>


                    {{-- CANCEL BUTTON --}}
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>