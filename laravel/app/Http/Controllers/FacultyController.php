<?php
namespace App\Http\Controllers;

use App\Creativework_attrib;
use App\Creativework_tbl;
use App\Http\Requests;
use App\Mpromotion;
use App\MpromotionForm;
use App\Performance_attrib;
use App\Performance_tbl;
use App\PGrowth_attrib;
use App\PGrowth_tbl;
use App\Service_attrib;
use App\Service_tbl;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;
use Input;
use PDF;
use Redirect;
use Response;
use Storage;
use Validator;
use Zipper;

// use Illuminate\Support\Facades\Request;
class FacultyController extends Controller
{
    public function CallForPromtion()
    {
        try {
            $id = DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
            if (!empty($id)) {
                $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.id=' . $id[0]->id);
                if (!empty($isThereCallForPromotion)) {
                    $startDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_start);
                    $endDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_end);
                    $currentDate = strtotime(date("Y-m-d"));
                    $mpromotion = Mpromotion::find($isThereCallForPromotion[0]->id);

                    if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                        // call for promotion status: on-going
                        $mpromotion->mpromotion_status = 0;
                        $mpromotion->save();
                    } else if ($currentDate > $endDate) {
                        // call for promotion status: finished
                        $mpromotion->mpromotion_status = 1;
                        $mpromotion->save();
                    } else if ($startDate > $currentDate) {
                        // call for promotion status: not yet started
                        $mpromotion->mpromotion_status = 2;
                        $mpromotion->save();
                    }
                }
            }
        } catch (Exception $e) {
            $isThereCallForPromotion = "";
        }
    }

    public function index()
    {
        $this->CallForPromtion();
        return view('pages.welcome');
    }

    public function ReviseForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if(!empty($MpromotionForm)){
            if ($MpromotionForm->form_status == 5) {
                if($MpromotionForm->user_id==Auth::user()->id){
                    $this->facultyController = new FacultyController();
                    return $this->facultyController->FillOutMeritPromotion($id);
                }else{
                    return view('pages.error404');
                }
            } else {
                return view('pages.error404');
            }
        }else{
            return view('pages.error404');
        }
    }

    public function FillOutMeritPromotion($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);

        $curentMpromotionID = DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0');
        $isThereCallForPromotion="";
        $isSubmittedToDeptChairOnThatCFP="";
        if (!empty($curentMpromotionID)) {
            if($MpromotionForm->form_status>0){
                $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.mpromotion_status=0 AND 
                    mpromotion.id=\''.$MpromotionForm->callForPromotion_ID. '\'');
            }else{
                $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.mpromotion_status=0');
            }
            $isSubmittedToDeptChairOnThatCFP = DB::select('SELECT mpromotion_forms.id FROM mpromotion_forms WHERE mpromotion_forms.form_status >=1 AND mpromotion_forms.form_status !=5 AND
                mpromotion_forms.callForPromotion_ID = \'' . $curentMpromotionID[0]->id .
                '\'' . 'AND mpromotion_forms.user_id =\'' . Auth::user()->id . '\'');
        }

        $performances = DB::select('SELECT
                        performance_attrib.id,
                        performance_attrib.performance_tblID,
                        performance_attrib.category,
                        performance_attrib.subcategory,
                        performance_attrib.WARating,
                        performance_attrib.points,
                        performance_attrib.title,
                        performance_attrib.author,
                        performance_attrib.date_published,
                        performance_attrib.publisher,
                        performance_attrib.course_title,
                        performance_attrib.semester_used,
                        performance_attrib.ay_used,
                        performance_attrib.nature_ofpublication,
                        performance_attrib.bibliography,
                        performance_attrib.year_graduated,
                        performance_attrib.name_ofgraduate,
                        performance_attrib.number_ofpreparations,
                        performance_attrib.other_contributions,
                        performance_attrib.as_what,
                        performance_attrib.attachment_filenames,
                        performance_attrib.isAccepted_deptchair,
                        performance_attrib.isAccepted_dean,
                        performance_attrib.isAccepted_vchancellor



                        FROM
                            performance_attrib
                        INNER JOIN performance_tbl ON performance_attrib.performance_tblID = performance_tbl.id
                        INNER JOIN mpromotion_forms ON performance_tbl.mpromotion_formID = mpromotion_forms.id
                        WHERE
                            performance_attrib.performance_tblID = performance_tbl.id
                        AND performance_tbl.mpromotion_formID =\'' . $id . '\'
                        AND mpromotion_forms.user_id = \'' . $MpromotionForm->user_id . '\'
            ');

        $creativeworks = DB::select('SELECT
                        creativework_attrib.id,
                        creativework_attrib.creativework_tblID,
                        creativework_attrib.category,
                        creativework_attrib.subcategory,
                        creativework_attrib.subsubcategory,
                        creativework_attrib.title,
                        creativework_attrib.author,
                        creativework_attrib.date_published,
                        creativework_attrib.bibliography,
                        creativework_attrib.nature_ofpublication,
                        creativework_attrib.points,
                        creativework_attrib.attachment_filenames,
                        creativework_attrib.other_details,
                        creativework_attrib.date_ofpresentation,
                        creativework_attrib.place_ofpresentation,
                        creativework_attrib.title_ofconference,
                        creativework_attrib.publisher,
                        creativework_attrib.isAccepted_deptchair,
                        creativework_attrib.isAccepted_dean,
                        creativework_attrib.isAccepted_vchancellor
                        FROM
                        creativework_attrib
                        INNER JOIN creativework_tbl ON creativework_attrib.creativework_tblID = creativework_tbl.id
                        INNER JOIN mpromotion_forms ON creativework_tbl.mpromotion_formID = mpromotion_forms.id
                        WHERE
                            creativework_attrib.creativework_tblID = creativework_tbl.id
                        AND creativework_tbl.mpromotion_formID =\'' . $id . '\'
                        AND mpromotion_forms.user_id = \'' . $MpromotionForm->user_id . '\'' . 'ORDER BY creativework_attrib.subsubcategory
            ');

        $services = DB::select('SELECT
                        service_attrib.id,
                        service_attrib.category,
                        service_attrib.subcategory,
                        service_attrib.designation,
                        service_attrib.period_covered,
                        service_attrib.points,
                        service_attrib.service_tblID,
                        service_attrib.other_details,
                        service_attrib.attachment_filenames,
                        service_attrib.name_ofcommittee,
                        service_attrib.nature_ofcommittee,
                        service_attrib.funding_source,
                        service_attrib.ammount_donated,
                        service_attrib.purpose_offund,
                        service_attrib.designation_ofoic,
                        service_attrib.level,
                        service_attrib.name_oforganization,
                        service_attrib.subsubcategory,
                        service_attrib.isAccepted_deptchair,
                        service_attrib.isAccepted_dean,
                        service_attrib.isAccepted_vchancellor

                        FROM
                        service_attrib
                        INNER JOIN service_tbl ON service_attrib.service_tblID = service_tbl.id
                        INNER JOIN mpromotion_forms ON service_tbl.mpromotion_formID = mpromotion_forms.id
                        WHERE
                            service_attrib.service_tblID = service_tbl.id
                        AND service_tbl.mpromotion_formID =\'' . $id . '\'
                        AND mpromotion_forms.user_id = \'' . $MpromotionForm->user_id . '\''
        );

        $pgrowths = DB::select('SELECT
                        pgrowth_attrib.id,
                        pgrowth_attrib.pgrowth_tblID,
                        pgrowth_attrib.category,
                        pgrowth_attrib.subcategory,
                        pgrowth_attrib.name_ofevent,
                        pgrowth_attrib.subsubcategory,
                        pgrowth_attrib.points,
                        pgrowth_attrib.other_details,
                        pgrowth_attrib.attachment_filenames,
                        pgrowth_attrib.title,
                        pgrowth_attrib.author,
                        pgrowth_attrib.nature_ofpublication,
                        pgrowth_attrib.date_published,
                        pgrowth_attrib.bibliography,
                        pgrowth_attrib.date_ofpresentation,
                        pgrowth_attrib.place_ofpresentation,
                        pgrowth_attrib.participation,
                        pgrowth_attrib.period_covered,
                        pgrowth_attrib.duration_ofappointment,
                        pgrowth_attrib.isAccepted_deptchair,
                        pgrowth_attrib.isAccepted_dean,
                        pgrowth_attrib.isAccepted_vchancellor

                        FROM
                        pgrowth_attrib
                        INNER JOIN pgrowth_tbl ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                        INNER JOIN mpromotion_forms ON pgrowth_tbl.mpromotion_formID = mpromotion_forms.id
                        WHERE
                            pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                        AND pgrowth_tbl.mpromotion_formID =\'' . $id . '\'
                        AND mpromotion_forms.user_id = \'' . $MpromotionForm->user_id . '\''
        );

        $cat1Total = 0;
        $cat2Total = 0;
        $cat3Total = 0;
        $cat4Total = 0;
        $overAllTotal = 0;
        $disableSateBtn = 0;

        if (!empty($performances)) {
            foreach ($performances as $performance) {
                if ($performance->category == 1 || $performance->category == 2) {
                    if ($performance->isAccepted_deptchair != 2 && $performance->isAccepted_dean != 2 && $performance->isAccepted_vchancellor != 2) {
                        $cat1Total = $cat1Total + $performance->points;
                    }

                }
                if ($performance->category == 1) {
                    $disableSateBtn = 1;
                }
            }
        }

        if (!empty($creativeworks)) {
            foreach ($creativeworks as $creativework) {
                if ($creativework->category == 1 || $creativework->category == 2) {

                    if ($creativework->isAccepted_deptchair != 2 && $creativework->isAccepted_dean != 2 && $creativework->isAccepted_vchancellor != 2) {
                        $cat2Total = $cat2Total + $creativework->points;
                    }
                }
            }
        }

        if (!empty($services)) {
            foreach ($services as $service) {
                if ($service->category == 1 || $service->category == 2) {
                    if ($service->isAccepted_deptchair != 2 && $service->isAccepted_dean != 2 && $service->isAccepted_vchancellor != 2) {
                        $cat3Total = $cat3Total + $service->points;
                    }
                }
            }
        }

        if (!empty($pgrowths)) {
            foreach ($pgrowths as $pgrowth) {
                if ($pgrowth->isAccepted_deptchair != 2 && $pgrowth->isAccepted_dean != 2 && $pgrowth->isAccepted_vchancellor != 2) {
                    $cat4Total = $cat4Total + $pgrowth->points;
                }
            }
        }

        $overAllTotal = $cat1Total + $cat2Total + $cat3Total + $cat4Total;

        $MpromotionForm = MpromotionForm::find($id);
        $MpromotionForm->faculty_total = $overAllTotal;
        $MpromotionForm->save();
        $formID = $id;

        $user = User::find($MpromotionForm->user_id);

        /*
            identfies if all entries in the form are already 
            evaluated before allowing to sumbmit it to 
            the next evaluator
        */
        // deptchair to dean
        if (Auth::user()->position == 1) {
            // enable/disable needs revision button
            $isEnableSubmitDean1 = DB::select('SELECT
                                        performance_attrib.isAccepted_deptchair
                                        FROM
                                        performance_attrib
                                        INNER JOIN performance_tbl ON performance_attrib.performance_tblID = performance_tbl.id
                                        WHERE
                                        performance_attrib.performance_tblID = performance_tbl.id AND
                                        performance_attrib.isAccepted_deptchair  IS NULL  AND
                                        performance_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitDean2 = DB::select('SELECT
                                        creativework_attrib.isAccepted_deptchair
                                        FROM
                                        creativework_attrib
                                        INNER JOIN creativework_tbl ON creativework_attrib.creativework_tblID = creativework_tbl.id
                                        WHERE
                                        creativework_attrib.creativework_tblID = creativework_tbl.id AND
                                        creativework_attrib.isAccepted_deptchair IS NULL AND
                                        creativework_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitDean3 = DB::select('SELECT
                                        service_attrib.isAccepted_deptchair
                                        FROM
                                        service_attrib
                                        INNER JOIN service_tbl ON service_attrib.service_tblID = service_tbl.id
                                        WHERE
                                        service_attrib.service_tblID = service_tbl.id AND
                                        service_attrib.isAccepted_deptchair IS NULL AND
                                        service_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitDean4 = DB::select('SELECT
                                        pgrowth_attrib.isAccepted_deptchair
                                        FROM
                                        pgrowth_attrib
                                        INNER JOIN pgrowth_tbl ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                                        WHERE
                                        pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id AND
                                        pgrowth_attrib.isAccepted_deptchair IS NULL  AND
                                        pgrowth_tbl.mpromotion_formID =\'' . $id . '\'
            ');


            $isEnableReviseButton1 = DB::select('SELECT
                                        performance_attrib.isAccepted_deptchair
                                        FROM
                                        performance_attrib
                                        INNER JOIN performance_tbl ON performance_attrib.performance_tblID = performance_tbl.id
                                        WHERE
                                        performance_attrib.performance_tblID = performance_tbl.id AND
                                        performance_attrib.isAccepted_deptchair = 3  AND
                                        performance_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableReviseButton2 = DB::select('SELECT
                                        creativework_attrib.isAccepted_deptchair
                                        FROM
                                        creativework_attrib
                                        INNER JOIN creativework_tbl ON creativework_attrib.creativework_tblID = creativework_tbl.id
                                        WHERE
                                        creativework_attrib.creativework_tblID = creativework_tbl.id AND
                                        creativework_attrib.isAccepted_deptchair = 3 AND
                                        creativework_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableReviseButton3 = DB::select('SELECT
                                        service_attrib.isAccepted_deptchair
                                        FROM
                                        service_attrib
                                        INNER JOIN service_tbl ON service_attrib.service_tblID = service_tbl.id
                                        WHERE
                                        service_attrib.service_tblID = service_tbl.id AND
                                        service_attrib.isAccepted_deptchair = 3 AND
                                        service_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableReviseButton4 = DB::select('SELECT
                                        pgrowth_attrib.isAccepted_deptchair
                                        FROM
                                        pgrowth_attrib
                                        INNER JOIN pgrowth_tbl ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                                        WHERE
                                        pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id AND
                                        pgrowth_attrib.isAccepted_deptchair = 3  AND
                                        pgrowth_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            if (empty($isEnableReviseButton1) && empty($isEnableReviseButton2) && empty($isEnableReviseButton3) && empty($isEnableReviseButton4)) {
                $isEnableReviseButton = 0;
            } else {
                $isEnableReviseButton = 1;
            }

            if (empty($isEnableSubmitDean1) && empty($isEnableSubmitDean2) && empty($isEnableSubmitDean3) && empty($isEnableSubmitDean4)) {
                $isEnableSubmitDean = 1;
            } else {
                $isEnableSubmitDean = 0;
            }
        }

        // dean to the vice chancellor
        if (Auth::user()->position == 2) {
            // searches for unevaluated entries
            $isEnableSubmitVChancellor1 = DB::select('SELECT
                                        performance_attrib.isAccepted_dean
                                        FROM
                                        performance_attrib
                                        INNER JOIN performance_tbl ON performance_attrib.performance_tblID = performance_tbl.id
                                        WHERE
                                        performance_attrib.performance_tblID = performance_tbl.id AND
                                        performance_attrib.isAccepted_dean  IS NULL  AND performance_attrib.isAccepted_deptchair !=2 AND
                                        performance_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitVChancellor2 = DB::select('SELECT
                                        creativework_attrib.isAccepted_dean
                                        FROM
                                        creativework_attrib
                                        INNER JOIN creativework_tbl ON creativework_attrib.creativework_tblID = creativework_tbl.id
                                        WHERE
                                        creativework_attrib.creativework_tblID = creativework_tbl.id AND 
                                        creativework_attrib.isAccepted_dean IS NULL AND creativework_attrib.isAccepted_deptchair !=2 AND
                                        creativework_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitVChancellor3 = DB::select('SELECT
                                        service_attrib.isAccepted_dean
                                        FROM
                                        service_attrib
                                        INNER JOIN service_tbl ON service_attrib.service_tblID = service_tbl.id
                                        WHERE
                                        service_attrib.service_tblID = service_tbl.id AND 
                                        service_attrib.isAccepted_dean IS NULL AND service_attrib.isAccepted_deptchair !=2 AND
                                        service_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitVChancellor4 = DB::select('SELECT
                                        pgrowth_attrib.isAccepted_dean
                                        FROM
                                        pgrowth_attrib
                                        INNER JOIN pgrowth_tbl ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                                        WHERE
                                        pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id AND
                                        pgrowth_attrib.isAccepted_dean IS NULL  AND pgrowth_attrib.isAccepted_deptchair !=2 AND
                                        pgrowth_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            if (empty($isEnableSubmitVChancellor1) && empty($isEnableSubmitVChancellor2) && empty($isEnableSubmitVChancellor3) && empty($isEnableSubmitVChancellor4)) {
                $isEnableSubmitVChancellor = 1;
            } else {
                $isEnableSubmitVChancellor = 0;
            }
        }


        // vice chancellor to the chancellor
        if (Auth::user()->position == 3) {
            // searches for unrejected, unevaluated entries 
            // if there's any, disable 'send button'
            $isEnableSubmitChancellor1 = DB::select('SELECT
                                        performance_attrib.isAccepted_vchancellor
                                        FROM
                                        performance_attrib
                                        INNER JOIN performance_tbl ON performance_attrib.performance_tblID = performance_tbl.id
                                        WHERE
                                        performance_attrib.performance_tblID = performance_tbl.id AND
                                        performance_attrib.isAccepted_vchancellor  IS NULL  AND performance_attrib.isAccepted_deptchair !=2 AND performance_attrib.isAccepted_dean !=2 AND 
                                        performance_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitChancellor2 = DB::select('SELECT
                                        creativework_attrib.isAccepted_vchancellor
                                        FROM
                                        creativework_attrib
                                        INNER JOIN creativework_tbl ON creativework_attrib.creativework_tblID = creativework_tbl.id
                                        WHERE
                                        creativework_attrib.creativework_tblID = creativework_tbl.id AND
                                        creativework_attrib.isAccepted_vchancellor IS NULL AND creativework_attrib.isAccepted_deptchair !=2 AND creativework_attrib.isAccepted_dean !=2 AND 
                                        creativework_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitChancellor3 = DB::select('SELECT
                                        service_attrib.isAccepted_vchancellor
                                        FROM
                                        service_attrib
                                        INNER JOIN service_tbl ON service_attrib.service_tblID = service_tbl.id
                                        WHERE
                                        service_attrib.service_tblID = service_tbl.id AND
                                        service_attrib.isAccepted_vchancellor IS NULL AND service_attrib.isAccepted_deptchair !=2 AND service_attrib.isAccepted_dean !=2 AND 
                                        service_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            $isEnableSubmitChancellor4 = DB::select('SELECT
                                        pgrowth_attrib.isAccepted_vchancellor
                                        FROM
                                        pgrowth_attrib
                                        INNER JOIN pgrowth_tbl ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                                        WHERE
                                        pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id AND
                                        pgrowth_attrib.isAccepted_vchancellor IS NULL  AND pgrowth_attrib.isAccepted_deptchair !=2 AND pgrowth_attrib.isAccepted_dean !=2 AND 
                                        pgrowth_tbl.mpromotion_formID =\'' . $id . '\'
            ');

            if (empty($isEnableSubmitChancellor1) && empty($isEnableSubmitChancellor2) && empty($isEnableSubmitChancellor3) && empty($isEnableSubmitChancellor4)) {
                $isEnableSubmitChancellor = 1;
            } else {
                $isEnableSubmitChancellor = 0;
            }
        }

        // identifies the current user
        if (Auth::user()) {
            if ((Auth::user()->position) == 5) {
                $preLink = '/admin/';
                $currentUser = 'admin';
            } else if ((Auth::user()->position) == 4) {
                $preLink = '/chancellor/';
                $currentUser = 'chancellor';
            } else if ((Auth::user()->position) == 3) {
                $preLink = '/vchancellor/';
                $currentUser = 'vchancellor';
            } else if ((Auth::user()->position) == 2) {
                $preLink = '/dean/';
                $currentUser = 'dean';
            } else if ((Auth::user()->position) == 1) {
                $preLink = '/deptchair/';
                $currentUser = 'deptchair';
            } else if ((Auth::user()->position) == 0) {
                $preLink = '/faculty/';
                $currentUser = 'faculty';
            }
        }

        return view('faculty.FillOutMeritPromotion', compact('formID', 'performances',
            'cat1Points', 'cat1Total', 'disableSateBtn', 'SubmitDean', 'MpromotionForm',
            'creativeworks', 'cat2Total',
            'services', 'cat3Total',
            'pgrowths', 'cat4Total', 'overAllTotal', 'isThereCallForPromotion', 'isSubmittedToDeptChairOnThatCFP',
            'user', 'isEnableSubmitDean', 'isEnableSubmitVChancellor', 'isEnableSubmitChancellor', 'isEnableReviseButton', 'preLink', 'currentUser'));
    }

    public function FillOutMeritPromotionCheck($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if(!empty($MpromotionForm)){
            if ($MpromotionForm->form_status == 0) {
                if($MpromotionForm->user_id==Auth::user()->id){
                    return $this->FillOutMeritPromotion($id);
                }else{
                    return view('pages.error404');
                }
            } else {
                return view('pages.error404');
            }
        }else{
            return view('pages.error404');
        }
        
    }

    public function MonitorForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if(!empty($MpromotionForm)){
            if ($MpromotionForm->form_status > 0) {
                if($MpromotionForm->user_id==Auth::user()->id){
                    return $this->FillOutMeritPromotion($id);
                }else{
                    return view('pages.error404');
                }
            } else {
                return view('pages.error404');
            }
        }else{
            return view('pages.error404');
        }
        
    }

    public function UpdateForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if(!empty($MpromotionForm)){
            if ($MpromotionForm->form_status == 0) {
                if($MpromotionForm->user_id==Auth::user()->id){
                    return $this->FillOutMeritPromotion($id);
                }else{
                    return view('pages.error404');
                }
            } else {
                return view('pages.error404');
            }
        }else{
            return view('pages.error404');
        }
        
    }

    public function generateZip($id, Request $request)
    {
        $this->CallForPromtion();
        $usersInfo = DB::select('SELECT
                                    users.id,
                                    users.department_belongs,
                                    users.employee_code,
                                    users.first_name,
                                    users.middle_name,
                                    users.last_name,
                                    users.birthday,
                                    users.rank,
                                    users.step,
                                    users.position,
                                    users.address,
                                    users.remember_token,
                                    users.created_at,
                                    users.updated_at
                                    FROM
                                        users
                                    INNER JOIN mpromotion_forms ON mpromotion_forms.user_id = users.id
                                    WHERE
                                        users.id = mpromotion_forms.user_id
                                    AND mpromotion_forms.id = \'' . $id . '\'
        ');

        $creativework = DB::select('SELECT GROUP_CONCAT(creativework_attrib.attachment_filenames SEPARATOR \'\') as creativeworkString FROM creativework_attrib INNER JOIN 
            creativework_tbl ON creativework_attrib.creativework_tblID = creativework_tbl.id WHERE creativework_attrib.creativework_tblID = creativework_tbl.id AND
            creativework_tbl.mpromotion_formID =\'' . $id . '\'');

        $pgrowth = DB::select('SELECT GROUP_CONCAT(pgrowth_attrib.attachment_filenames SEPARATOR \'\') as pgrowthString FROM pgrowth_attrib INNER JOIN 
            pgrowth_tbl ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id WHERE pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id AND
            pgrowth_tbl.mpromotion_formID =\'' . $id . '\'');

        $performance = DB::select('SELECT GROUP_CONCAT(performance_attrib.attachment_filenames SEPARATOR \'\') as performanceString FROM performance_attrib INNER JOIN 
            performance_tbl ON performance_attrib.performance_tblID = performance_tbl.id WHERE performance_attrib.performance_tblID = performance_tbl.id AND
            performance_tbl.mpromotion_formID =\'' . $id . '\'');

        $service = DB::select('SELECT GROUP_CONCAT(service_attrib.attachment_filenames SEPARATOR \'\') as serviceString FROM service_attrib INNER JOIN 
            service_tbl ON service_attrib.service_tblID = service_tbl.id WHERE service_attrib.service_tblID = service_tbl.id AND
            service_tbl.mpromotion_formID =\'' . $id . '\'');

        $deptchair = DB::select('SELECT GROUP_CONCAT( deptchair_remarks_attachments SEPARATOR \'\') AS 
            deptchairString FROM mpromotion_forms WHERE mpromotion_forms.id =\'' . $id . '\'');

        $dean = DB::select('SELECT GROUP_CONCAT( dean_remarks_attachments SEPARATOR \'\') AS 
            deanString FROM mpromotion_forms WHERE mpromotion_forms.id =\'' . $id . '\'');

        $vchancellor = DB::select('SELECT GROUP_CONCAT( vchancellor_remarks_attachments SEPARATOR \'\') AS 
            vchancellorString FROM mpromotion_forms WHERE mpromotion_forms.id =\'' . $id . '\'');

        $chancellor = DB::select('SELECT GROUP_CONCAT( chancellor_remarks_attachments SEPARATOR \'\') AS 
            chancellorString FROM mpromotion_forms WHERE mpromotion_forms.id =\'' . $id . '\'');

        $pgrowthAttachments = explode("+++", $pgrowth[0]->pgrowthString);
        $creativeworkAttachments = explode("+++", $creativework[0]->creativeworkString);
        $performanceAttachments = explode("+++", $performance[0]->performanceString);
        $serviceAttachments = explode("+++", $service[0]->serviceString);

        $deptchairAttachments = explode("+++", $deptchair[0]->deptchairString);
        $deanAttachments = explode("+++", $dean[0]->deanString);
        $vchancellorAttachments = explode("+++", $vchancellor[0]->vchancellorString);
        $chancellorAttachments = explode("+++", $chancellor[0]->chancellorString);

        $files = array();
        $count = 0;
        if (!empty($pgrowthAttachments)) {
            foreach ($pgrowthAttachments as $pgrowthAttachment) {
                if ($pgrowthAttachment != "") {
                    $files[$count] = 'uploads/' . $pgrowthAttachment;
                    $count++;
                }
            }
        }

        if (!empty($creativeworkAttachments)) {
            foreach ($creativeworkAttachments as $creativeworkAttachment) {
                if ($creativeworkAttachment != "") {
                    $files[$count] = 'uploads/' . $creativeworkAttachment;
                    $count++;
                }
            }
        }

        if (!empty($performanceAttachments)) {
            foreach ($performanceAttachments as $performanceAttachment) {
                if ($performanceAttachment != "") {
                    $files[$count] = 'uploads/' . $performanceAttachment;
                    $count++;
                }
            }
        }

        if (!empty($serviceAttachments)) {
            foreach ($serviceAttachments as $serviceAttachment) {
                if ($serviceAttachment != "") {
                    $files[$count] = 'uploads/' . $serviceAttachment;
                    $count++;
                }
            }
        }

        if (!empty($deptchairAttachments)) {
            foreach ($deptchairAttachments as $deptchairAttachment) {
                if ($deptchairAttachment != "") {
                    $files[$count] = 'uploads/' . $deptchairAttachment;
                    $count++;
                }
            }
        }

        if (!empty($deanAttachments)) {
            foreach ($deanAttachments as $deanAttachment) {
                if ($deanAttachment != "") {
                    $files[$count] = 'uploads/' . $deanAttachment;
                    $count++;
                }
            }
        }

        if (!empty($vchancellorAttachments)) {
            foreach ($vchancellorAttachments as $vchancellorAttachment) {
                if ($vchancellorAttachment != "") {
                    $files[$count] = 'uploads/' . $vchancellorAttachment;
                    $count++;
                }
            }
        }

        if (!empty($chancellorAttachments)) {
            foreach ($chancellorAttachments as $chancellorAttachment) {
                if ($chancellorAttachment != "") {
                    $files[$count] = 'uploads/' . $chancellorAttachment;
                    $count++;
                }
            }
        }


        $zipName = 'Form' . $id . '_' . $usersInfo[0]->last_name . '_attachments';
        $zipPathName = 'zipped/' . $zipName . '.zip';
        if (file_exists($zipPathName)) {
            unlink($zipPathName);
        }
        if (empty($files)) {
            $files = 'uploads/no_attachments_yet.txt';
        }
        Zipper::make($zipPathName)->add($files);
        if ($request->ajax()) {
            return $zipPathName;
        }
    }

    public function generatePDF($id)
    {
        $this->CallForPromtion();
        $performances = DB::select('SELECT
                        performance_attrib.id,
                        performance_attrib.performance_tblID,
                        performance_attrib.category,
                        performance_attrib.subcategory,
                        performance_attrib.WARating,
                        performance_attrib.points,
                        performance_attrib.title,
                        performance_attrib.author,
                        performance_attrib.date_published,
                        performance_attrib.publisher,
                        performance_attrib.course_title,
                        performance_attrib.semester_used,
                        performance_attrib.ay_used,
                        performance_attrib.nature_ofpublication,
                        performance_attrib.bibliography,
                        performance_attrib.year_graduated,
                        performance_attrib.name_ofgraduate,
                        performance_attrib.number_ofpreparations,
                        performance_attrib.other_contributions,
                        performance_attrib.as_what,
                        performance_attrib.attachment_filenames,
                        performance_attrib.isAccepted_deptchair,
                        performance_attrib.isAccepted_dean,
                        performance_attrib.isAccepted_vchancellor

                        FROM
                            performance_attrib
                        INNER JOIN performance_tbl ON performance_attrib.performance_tblID = performance_tbl.id
                        INNER JOIN mpromotion_forms ON performance_tbl.mpromotion_formID = mpromotion_forms.id
                        WHERE
                            performance_attrib.performance_tblID = performance_tbl.id
                        AND performance_tbl.mpromotion_formID  = \'' . $id . '\'');

        $creativeworks = DB::select('SELECT
                            creativework_attrib.id,
                            creativework_attrib.creativework_tblID,
                            creativework_attrib.category,
                            creativework_attrib.subcategory,
                            creativework_attrib.subsubcategory,
                            creativework_attrib.title,
                            creativework_attrib.author,
                            creativework_attrib.date_published,
                            creativework_attrib.bibliography,
                            creativework_attrib.nature_ofpublication,
                            creativework_attrib.points,
                            creativework_attrib.attachment_filenames,
                            creativework_attrib.other_details,
                            creativework_attrib.date_ofpresentation,
                            creativework_attrib.place_ofpresentation,
                            creativework_attrib.title_ofconference,
                            creativework_attrib.publisher,
                            creativework_attrib.isAccepted_deptchair,
                            creativework_attrib.isAccepted_dean,
                            creativework_attrib.isAccepted_vchancellor
                            
                            FROM
                            creativework_attrib
                            INNER JOIN creativework_tbl ON creativework_attrib.creativework_tblID = creativework_tbl.id
                            INNER JOIN mpromotion_forms ON creativework_tbl.mpromotion_formID = mpromotion_forms.id
                            WHERE
                                creativework_attrib.creativework_tblID = creativework_tbl.id
                            AND creativework_tbl.mpromotion_formID =\'' . $id . '\'
        ');

        $services = DB::select('SELECT
                        service_attrib.id,
                        service_attrib.category,
                        service_attrib.subcategory,
                        service_attrib.designation,
                        service_attrib.period_covered,
                        service_attrib.points,
                        service_attrib.service_tblID,
                        service_attrib.other_details,
                        service_attrib.attachment_filenames,
                        service_attrib.name_ofcommittee,
                        service_attrib.nature_ofcommittee,
                        service_attrib.funding_source,
                        service_attrib.ammount_donated,
                        service_attrib.purpose_offund,
                        service_attrib.designation_ofoic,
                        service_attrib.level,
                        service_attrib.name_oforganization,
                        service_attrib.subsubcategory,
                        service_attrib.isAccepted_deptchair,
                        service_attrib.isAccepted_dean,
                        service_attrib.isAccepted_vchancellor
                        
                        FROM
                        service_attrib
                        INNER JOIN service_tbl ON service_attrib.service_tblID = service_tbl.id
                        INNER JOIN mpromotion_forms ON service_tbl.mpromotion_formID = mpromotion_forms.id
                        WHERE
                            service_attrib.service_tblID = service_tbl.id
                        AND service_tbl.mpromotion_formID =\'' . $id . '\'
        ');

        $pgrowths = DB::select('SELECT
                    pgrowth_attrib.id,
                    pgrowth_attrib.pgrowth_tblID,
                    pgrowth_attrib.category,
                    pgrowth_attrib.subcategory,
                    pgrowth_attrib.name_ofevent,
                    pgrowth_attrib.subsubcategory,
                    pgrowth_attrib.points,
                    pgrowth_attrib.other_details,
                    pgrowth_attrib.attachment_filenames,
                    pgrowth_attrib.title,
                    pgrowth_attrib.author,
                    pgrowth_attrib.nature_ofpublication,
                    pgrowth_attrib.date_published,
                    pgrowth_attrib.bibliography,
                    pgrowth_attrib.date_ofpresentation,
                    pgrowth_attrib.place_ofpresentation,
                    pgrowth_attrib.participation,
                    pgrowth_attrib.period_covered,
                    pgrowth_attrib.duration_ofappointment,
                    pgrowth_attrib.isAccepted_deptchair,
                    pgrowth_attrib.isAccepted_dean,
                    pgrowth_attrib.isAccepted_vchancellor
                    FROM
                    pgrowth_attrib
                    INNER JOIN pgrowth_tbl ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                    INNER JOIN mpromotion_forms ON pgrowth_tbl.mpromotion_formID = mpromotion_forms.id
                    WHERE
                        pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                    AND pgrowth_tbl.mpromotion_formID =\'' . $id . '\'
        ');

        $usersInfo = DB::select('SELECT
                                    users.id,
                                    users.department_belongs,
                                    users.employee_code,
                                    users.first_name,
                                    users.middle_name,
                                    users.last_name,
                                    users.birthday,
                                    users.rank,
                                    users.step,
                                    users.position,
                                    users.address,
                                    users.remember_token,
                                    users.created_at,
                                    users.updated_at,
                                    users.last_promotion
                                    FROM
                                        users
                                    INNER JOIN mpromotion_forms ON mpromotion_forms.user_id = users.id
                                    WHERE
                                        users.id = mpromotion_forms.user_id
                                    AND mpromotion_forms.id = \'' . $id . '\'
        ');

        $collegeName = DB::select('SELECT
                            colleges.college_name,
                            colleges.college_initial
                            FROM
                            colleges
                            INNER JOIN departments ON departments.college_belongs = colleges.college_name
                            WHERE
                            departments.department_name = \'' . $usersInfo[0]->department_belongs . '\'');

        $mpromotionFormInfo = MpromotionForm::find($id);

        $pdf = PDF::loadView('faculty.PDFForm', compact('performances', 'creativeworks',
            'services', 'pgrowths', 'usersInfo', 'mpromotionFormInfo', 'collegeName'))->setPaper('a4')->setOrientation('landscape')->setWarnings(false);
        return $pdf->stream("Form" . $id . "_" . $usersInfo[0]->last_name . '_GeneratedForm');
    }

    public function removeAttachmentsFromServer($id)
    {
        $this->CallForPromtion();
        $creativework = DB::select('SELECT GROUP_CONCAT(creativework_attrib.attachment_filenames SEPARATOR \'\') as creativeworkString FROM creativework_attrib INNER JOIN
            creativework_tbl ON creativework_attrib.creativework_tblID = creativework_tbl.id WHERE creativework_attrib.creativework_tblID = creativework_tbl.id AND
            creativework_tbl.mpromotion_formID =\'' . $id . '\'');

        $pgrowth = DB::select('SELECT GROUP_CONCAT(pgrowth_attrib.attachment_filenames SEPARATOR \'\') as pgrowthString FROM pgrowth_attrib INNER JOIN
            pgrowth_tbl ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id WHERE pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id AND
            pgrowth_tbl.mpromotion_formID =\'' . $id . '\'');

        $performance = DB::select('SELECT GROUP_CONCAT(performance_attrib.attachment_filenames SEPARATOR \'\') as performanceString FROM performance_attrib INNER JOIN
            performance_tbl ON performance_attrib.performance_tblID = performance_tbl.id WHERE performance_attrib.performance_tblID = performance_tbl.id AND
            performance_tbl.mpromotion_formID =\'' . $id . '\'');

        $service = DB::select('SELECT GROUP_CONCAT(service_attrib.attachment_filenames SEPARATOR \'\') as serviceString FROM service_attrib INNER JOIN
            service_tbl ON service_attrib.service_tblID = service_tbl.id WHERE service_attrib.service_tblID = service_tbl.id AND
            service_tbl.mpromotion_formID =\'' . $id . '\'');

        $pgrowthAttachments = explode("+++", $pgrowth[0]->pgrowthString);
        $creativeworkAttachments = explode("+++", $creativework[0]->creativeworkString);
        $performanceAttachments = explode("+++", $performance[0]->performanceString);
        $serviceAttachments = explode("+++", $service[0]->serviceString);

        if (!empty($pgrowthAttachments)) {
            foreach ($pgrowthAttachments as $pgrowthAttachment) {
                if ($pgrowthAttachment != "") {
                    unlink('uploads/' . $pgrowthAttachment);
                }
            }
        }

        if (!empty($creativeworkAttachments)) {
            foreach ($creativeworkAttachments as $creativeworkAttachment) {
                if ($creativeworkAttachment != "") {
                    unlink('uploads/' . $creativeworkAttachment);
                }
            }
        }

        if (!empty($performanceAttachments)) {
            foreach ($performanceAttachments as $performanceAttachment) {
                if ($performanceAttachment != "") {
                    unlink('uploads/' . $performanceAttachment);
                }
            }
        }

        if (!empty($serviceAttachments)) {
            foreach ($serviceAttachments as $serviceAttachment) {
                if ($serviceAttachment != "") {
                    unlink('uploads/' . $serviceAttachment);
                }
            }
        }

        MpromotionForm::destroy($id);
    }

    public function AddNewForm()
    {
        $this->CallForPromtion();
        $mpromotionForm = new MpromotionForm;
        $mpromotionForm->user_id = Auth::user()->id;
        $mpromotionForm->form_status = 0;
        $mpromotionForm->fillOut_date = Input::get('fillOut_date');
        $mpromotionForm->form_name = Input::get('form_name');
        $mpromotionForm->save();

        // default form name
        if (empty(Input::get('form_name'))) {
            $mpromotionForm->form_name = 'Form-' . $mpromotionForm->id;
        } else {
            $mpromotionForm->form_name = Input::get('form_name');
        }
        $mpromotionForm->save();
        $notification = "SUCCESS : New form has been successfully created.";


        // PERFORMANCE TABLE
        $Performance_tbl = new Performance_tbl;
        $Performance_tbl->mpromotion_formID = $mpromotionForm->id;
        $Performance_tbl->save();

        // CREATIVE WORK TABLE
        $Creativework_tbl = new Creativework_tbl;
        $Creativework_tbl->mpromotion_formID = $mpromotionForm->id;
        $Creativework_tbl->save();

        // SERVICE TABLE
        $Service_tbl = new Service_tbl;
        $Service_tbl->mpromotion_formID = $mpromotionForm->id;
        $Service_tbl->save();

        // SERVICE TABLE
        $PGrowth_tbl = new PGrowth_tbl;
        $PGrowth_tbl->mpromotion_formID = $mpromotionForm->id;
        $PGrowth_tbl->save();

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }

        return redirect($currentUser . '/meritpromotion/' . $mpromotionForm->id . '/FillOutMeritPromotion')->with('notification', $notification);
    }

    public function DeleteDraftForm($id)
    {
        $this->CallForPromtion();
        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $this->removeAttachmentsFromServer($id);

        $activeTab = 2;
        $notification = "SUCCESS :  Draft form has been successfully deleted.";
        return redirect($currentUser . '/meritpromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function SaveAsDraft()
    {
        $this->CallForPromtion();
        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $activeTab = 2;
        $notification = "SUCCESS : Form has been successfully saved as draft";
        return redirect($currentUser . '/meritpromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function SubmitDeptChair($id)
    {
        $this->CallForPromtion();
        $mpromotionForm = MpromotionForm::find($id);
        $mpromotionForm->form_status = 1;
        $mpromotionForm->submit_date_deptchair = date('Y-m-d');

        $curentMpromotionID = DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0');
        $callforpromotion = Mpromotion::find($curentMpromotionID[0]->id);

        $mpromotionForm->callForPromotion_ID = $curentMpromotionID[0]->id;
        $mpromotionForm->callforpromotion_date_start = $callforpromotion->mpromotion_date_start;
        $mpromotionForm->callforpromotion_date_end = $callforpromotion->mpromotion_date_end;

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }

        $mpromotionForm->save();
        $activeTab = 1;
        $notification = "SUCCESS : Form has successfully  submitted to the Department Chairperson";
        return redirect($currentUser . '/meritpromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function MeritPromotion()
    {
        $this->CallForPromtion();
        $submittedForms = DB::select('SELECT * FROM mpromotion_forms WHERE  mpromotion_forms.user_id= \'' . Auth::user()->id . '\'' .
            'and  mpromotion_forms.form_status>0 ORDER BY mpromotion_forms.id ASC');

        $OwnDraftForms = DB::select('SELECT * FROM mpromotion_forms WHERE mpromotion_forms.form_status = 0 AND mpromotion_forms.user_id = \'' . Auth::user()->id . '\'' .
            'ORDER BY mpromotion_forms.fillOut_date ASC');
        return view('faculty.MeritPromotion', compact('submittedForms'), compact('OwnDraftForms'));
    }

    ////////////////
    // CATEGORY 4 //
    ////////////////
    public function SubmitFormCat4($id, Request $request)
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT pgrowth_tbl.id FROM pgrowth_tbl INNER JOIN mpromotion_forms 
            ON pgrowth_tbl.mpromotion_formID = mpromotion_forms.id WHERE mpromotion_forms.id = ' . $id);

        $PGrowth_attrib = new PGrowth_attrib;
        $PGrowth_attrib->pgrowth_tblID = $sql[0]->id;

        $PGrowth_attrib->category = Input::get('category');
        $PGrowth_attrib->subcategory = Input::get('subcategory');
        $PGrowth_attrib->other_details = Input::get('other_details');
        $PGrowth_attrib->points = Input::get('points');
        $PGrowth_attrib->subsubcategory = Input::get('subsubcategory');
        $PGrowth_attrib->name_ofevent = Input::get('name_ofevent');
        $PGrowth_attrib->title = Input::get('title');
        $PGrowth_attrib->author = Input::get('author');
        $PGrowth_attrib->date_published = Input::get('date_published');
        $PGrowth_attrib->bibliography = Input::get('bibliography');
        $PGrowth_attrib->nature_ofpublication = Input::get('nature_ofpublication');
        $PGrowth_attrib->place_ofpresentation = Input::get('place_ofpresentation');
        $PGrowth_attrib->date_ofpresentation = Input::get('date_ofpresentation');
        $PGrowth_attrib->participation = Input::get('participation');
        $PGrowth_attrib->period_covered = Input::get('period_covered');
        $PGrowth_attrib->duration_ofappointment = Input::get('duration_ofappointment');

        $files = $request->file('file');

        $attachmentFileNames = "";
        $attachmentNamesSpit[] = "";

        if (!(empty($files[0]))) {
            foreach ($files as $file):
                $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                $file->move('uploads', $fileName);
                $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
            endforeach;
            $PGrowth_attrib->attachment_filenames = $attachmentFileNames;
        }
        $PGrowth_attrib->save();
        $activeTab = 4;

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $notification = "SUCCESS : Form has been successfully  updated.";
        return redirect($currentUser . '/meritpromotion/' . $id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function RetrieveCat4($id, Request $request)
    {
        $this->CallForPromtion();
        $PGrowth_attrib = PGrowth_attrib::find($id);
        if ($request->ajax()) {
            return $PGrowth_attrib;
        }
    }

    public function UpdateFormCat4($id, Request $request)
    {
        $this->CallForPromtion();
        $activeTab = 4;
        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }

        $formID = DB::select('SELECT
                            mpromotion_forms.id
                            FROM
                            mpromotion_forms
                            INNER JOIN pgrowth_tbl ON pgrowth_tbl.mpromotion_formID = mpromotion_forms.id
                            INNER JOIN pgrowth_attrib ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                            WHERE
                            mpromotion_forms.id = pgrowth_tbl.mpromotion_formID AND
                            pgrowth_tbl.id = pgrowth_attrib.pgrowth_tblID AND
                            pgrowth_attrib.id = \'' . $id . '\'');

        if (Input::get('action') == "EvaluateForm") {
            $PGrowth_attrib = PGrowth_attrib::find($id);
            $PGrowth_attrib->category = $PGrowth_attrib->category;
            $PGrowth_attrib->subcategory = $PGrowth_attrib->subcategory;
            $PGrowth_attrib->other_details = $PGrowth_attrib->other_details;
            $PGrowth_attrib->points = $PGrowth_attrib->points;
            $PGrowth_attrib->subsubcategory = $PGrowth_attrib->subsubcategory;
            $PGrowth_attrib->name_ofevent = $PGrowth_attrib->name_ofevent;
            $PGrowth_attrib->title = $PGrowth_attrib->title;
            $PGrowth_attrib->author = $PGrowth_attrib->author;
            $PGrowth_attrib->date_published = $PGrowth_attrib->date_published;
            $PGrowth_attrib->bibliography = $PGrowth_attrib->bibliography;
            $PGrowth_attrib->nature_ofpublication = $PGrowth_attrib->nature_ofpublication;
            $PGrowth_attrib->place_ofpresentation = $PGrowth_attrib->place_ofpresentation;
            $PGrowth_attrib->date_ofpresentation = $PGrowth_attrib->date_ofpresentation;
            $PGrowth_attrib->participation = $PGrowth_attrib->participation;
            $PGrowth_attrib->period_covered = $PGrowth_attrib->period_covered;
            $PGrowth_attrib->duration_ofappointment = $PGrowth_attrib->duration_ofappointment;
            $PGrowth_attrib->attachment_filenames = $PGrowth_attrib->attachment_filenames;

            // accept
            if (Input::get('evaluateAction') == 1) {
                if ($currentUser == "deptchair") {
                    $PGrowth_attrib->isAccepted_deptchair = 1;
                } else if ($currentUser == "dean") {
                    $PGrowth_attrib->isAccepted_dean = 1;
                } else if ($currentUser == "vchancellor") {
                    $PGrowth_attrib->isAccepted_vchancellor = 1;
                }
                $PGrowth_attrib->points = Input::get('points');
            } // reject
            else if ((Input::get('evaluateAction') == 2)) {
                if ($currentUser == "deptchair") {
                    $PGrowth_attrib->isAccepted_deptchair = 2;
                } else if ($currentUser == "dean") {
                    $PGrowth_attrib->isAccepted_dean = 2;
                } else if ($currentUser == "vchancellor") {
                    $PGrowth_attrib->isAccepted_vchancellor = 2;
                }
                $PGrowth_attrib->points = $PGrowth_attrib->points;
            } // needs revision
            else if ((Input::get('evaluateAction') == 3)) {
                if ($currentUser == "deptchair") {
                    $PGrowth_attrib->isAccepted_deptchair = 3;
                } else if ($currentUser == "dean") {
                    $PGrowth_attrib->isAccepted_dean = 3;
                } else if ($currentUser == "vchancellor") {
                    $PGrowth_attrib->isAccepted_vchancellor = 3;
                }
                $PGrowth_attrib->points = $PGrowth_attrib->points;
            }

            $PGrowth_attrib->save();
            if (!empty((Input::get('evaluateAction')))) {
                $notification = "SUCCESS : Form has been successfully  updated.";
            } else {
                $notification = "ERROR : There's something wrong. Plese try again.";
            }
            return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/EvaluateForm')->with('notification', $notification)->with('activeTab', $activeTab);
        } else {
            $PGrowth_attrib = PGrowth_attrib::find($id);
            $PGrowth_attrib->category = Input::get('category');
            $PGrowth_attrib->subcategory = Input::get('subcategory');
            $PGrowth_attrib->other_details = Input::get('other_details');
            $PGrowth_attrib->points = Input::get('points');
            $PGrowth_attrib->subsubcategory = Input::get('subsubcategory');
            $PGrowth_attrib->name_ofevent = Input::get('name_ofevent');
            $PGrowth_attrib->title = Input::get('title');
            $PGrowth_attrib->author = Input::get('author');
            $PGrowth_attrib->date_published = Input::get('date_published');
            $PGrowth_attrib->bibliography = Input::get('bibliography');
            $PGrowth_attrib->nature_ofpublication = Input::get('nature_ofpublication');
            $PGrowth_attrib->place_ofpresentation = Input::get('place_ofpresentation');
            $PGrowth_attrib->date_ofpresentation = Input::get('date_ofpresentation');
            $PGrowth_attrib->participation = Input::get('participation');
            $PGrowth_attrib->period_covered = Input::get('period_covered');
            $PGrowth_attrib->duration_ofappointment = Input::get('duration_ofappointment');

            $files = $request->file('file');
            $attachmentFileNames = "";
            $attachmentNamesSpit[] = "";
            $oldFiles = $PGrowth_attrib->attachment_filenames;
            if (!(empty($files[0]))) {
                $oldFiles = $PGrowth_attrib->attachment_filenames;
                foreach ($files as $file):
                    $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                    $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                    $file->move('uploads', $fileName);
                    $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
                endforeach;
            }


            if (Input::get('proofAction') == 1) {
                $PGrowth_attrib->attachment_filenames = $attachmentFileNames . $oldFiles;
            } else if (Input::get('proofAction') == 2) {
                $deteleThisFiles = explode("+++", $PGrowth_attrib->attachment_filenames);
                $counter = 0;
                foreach ($deteleThisFiles as $file):
                    if ($file != "") {
                        unlink('uploads/' . $file);
                    }
                endforeach;
                $PGrowth_attrib->attachment_filenames = $attachmentFileNames;
            } else {
                $deteleThisFiles = explode("+++", $PGrowth_attrib->attachment_filenames);
                $counter = 0;
                foreach ($deteleThisFiles as $file):
                    if ($file != "") {
                        unlink('uploads/' . $file);
                    }
                endforeach;
                $PGrowth_attrib->attachment_filenames = NULL;
            }

            $PGrowth_attrib->save();


            $notification = "SUCCESS : Form has been successfully  updated.";
            if (Input::get('action') == "ReviseForm") {
                return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/ReviseForm')->with('notification', $notification)->with('activeTab', $activeTab);
            } else {
                return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
            }

        }
    }

    public function DeleteEntryCat4($id)
    {
        $this->CallForPromtion();
        $formID = DB::select('SELECT
                            mpromotion_forms.id
                            FROM
                            mpromotion_forms
                            INNER JOIN pgrowth_tbl ON pgrowth_tbl.mpromotion_formID = mpromotion_forms.id
                            INNER JOIN pgrowth_attrib ON pgrowth_attrib.pgrowth_tblID = pgrowth_tbl.id
                            WHERE
                            mpromotion_forms.id = pgrowth_tbl.mpromotion_formID AND
                            pgrowth_tbl.id = pgrowth_attrib.pgrowth_tblID AND
                            pgrowth_attrib.id = \'' . $id . '\'');


        $PGrowth_attrib = PGrowth_attrib::find($id);
        $deteleThisFiles = explode("+++", $PGrowth_attrib->attachment_filenames);
        $counter = 0;
        foreach ($deteleThisFiles as $file):
            if ($file != "") {
                unlink('uploads/' . $file);
            }
        endforeach;

        $activeTab = 4;
        PGrowth_attrib::destroy($id);

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $notification = "SUCCESS :  Entry has been successfully deleted.";
        return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    ////////////////
    // CATEGORY 3 //
    ////////////////
    public function SubmitFormCat3($id, Request $request)
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT service_tbl.id FROM service_tbl INNER JOIN mpromotion_forms 
            ON service_tbl.mpromotion_formID = mpromotion_forms.id WHERE mpromotion_forms.id = ' . $id);

        $Service_attrib = new Service_attrib;
        $Service_attrib->service_tblID = $sql[0]->id;

        $Service_attrib->category = Input::get('category');
        $Service_attrib->subcategory = Input::get('subcategory');
        $Service_attrib->other_details = Input::get('other_details');
        $Service_attrib->designation = Input::get('designation');
        $Service_attrib->period_covered = Input::get('period_covered');
        $Service_attrib->points = Input::get('points');
        $Service_attrib->name_ofcommittee = Input::get('name_ofcommittee');
        $Service_attrib->nature_ofcommittee = Input::get('nature_ofcommittee');
        $Service_attrib->funding_source = Input::get('funding_source');
        $Service_attrib->purpose_offund = Input::get('purpose_offund');
        $Service_attrib->ammount_donated = Input::get('ammount_donated');
        $Service_attrib->designation_ofoic = Input::get('designation_ofoic');
        $Service_attrib->level = Input::get('level');
        $Service_attrib->name_oforganization = Input::get('name_oforganization');
        $Service_attrib->subsubcategory = Input::get('subsubcategory');

        $files = $request->file('file');
        $attachmentFileNames = "";
        $attachmentNamesSpit[] = "";
        if (!(empty($files[0]))) {
            foreach ($files as $file):
                $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                $file->move('uploads', $fileName);
                $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
            endforeach;
            $Service_attrib->attachment_filenames = $attachmentFileNames;
        }
        $Service_attrib->save();
        $activeTab = 3;

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $notification = "SUCCESS : Form has been successfully  updated.";
        return redirect($currentUser . '/meritpromotion/' . $id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function RetrieveCat3($id, Request $request)
    {
        $this->CallForPromtion();
        $Service_attrib = Service_attrib::find($id);
        if ($request->ajax()) {
            return $Service_attrib;
        }
    }

    public function UpdateFormCat3($id, Request $request)
    {
        $this->CallForPromtion();
        $activeTab = 3;
        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }

        $formID = DB::select('SELECT
                            mpromotion_forms.id
                            FROM
                            mpromotion_forms
                            INNER JOIN service_tbl ON service_tbl.mpromotion_formID = mpromotion_forms.id
                            INNER JOIN service_attrib ON service_attrib.service_tblID = service_tbl.id
                            WHERE
                            mpromotion_forms.id = service_tbl.mpromotion_formID AND
                            service_tbl.id = service_attrib.service_tblID AND
                            service_attrib.id = \'' . $id . '\'');

        if (Input::get('action') == "EvaluateForm") {
            $Service_attrib = Service_attrib::find($id);
            $Service_attrib->category = $Service_attrib->category;
            $Service_attrib->subcategory = $Service_attrib->subcategory;
            $Service_attrib->other_details = $Service_attrib->other_details;
            $Service_attrib->designation = $Service_attrib->designation;
            $Service_attrib->period_covered = $Service_attrib->period_covered;
            $Service_attrib->points = $Service_attrib->points;
            $Service_attrib->name_ofcommittee = $Service_attrib->name_ofcommittee;
            $Service_attrib->nature_ofcommittee = $Service_attrib->nature_ofcommittee;
            $Service_attrib->funding_source = $Service_attrib->funding_source;
            $Service_attrib->purpose_offund = $Service_attrib->purpose_offund;
            $Service_attrib->ammount_donated = $Service_attrib->ammount_donated;
            $Service_attrib->designation_ofoic = $Service_attrib->designation_ofoic;
            $Service_attrib->level = $Service_attrib->level;
            $Service_attrib->name_oforganization = $Service_attrib->name_oforganization;
            $Service_attrib->subsubcategory = $Service_attrib->subsubcategory;
            $Service_attrib->attachment_filenames = $Service_attrib->attachment_filenames;

            // accept
            if (Input::get('evaluateAction') == 1) {
                if ($currentUser == "deptchair") {
                    $Service_attrib->isAccepted_deptchair = 1;
                } else if ($currentUser == "dean") {
                    $Service_attrib->isAccepted_dean = 1;
                } else if ($currentUser == "vchancellor") {
                    $Service_attrib->isAccepted_vchancellor = 1;
                }

                $Service_attrib->points = Input::get('points');
            } // reject
            else if ((Input::get('evaluateAction') == 2)) {
                if ($currentUser == "deptchair") {
                    $Service_attrib->isAccepted_deptchair = 2;
                } else if ($currentUser == "dean") {
                    $Service_attrib->isAccepted_dean = 2;
                } else if ($currentUser == "vchancellor") {
                    $Service_attrib->isAccepted_vchancellor = 2;
                }
                $Service_attrib->points = $Service_attrib->points;
            } else if ((Input::get('evaluateAction') == 3)) {
                if ($currentUser == "deptchair") {
                    $Service_attrib->isAccepted_deptchair = 3;
                } else if ($currentUser == "dean") {
                    $Service_attrib->isAccepted_dean = 3;
                } else if ($currentUser == "vchancellor") {
                    $Service_attrib->isAccepted_vchancellor = 3;
                }
                $Service_attrib->points = $Service_attrib->points;
            }

            $Service_attrib->save();
            if (!empty((Input::get('evaluateAction')))) {
                $notification = "SUCCESS : Form has been successfully  updated.";
            } else {
                $notification = "ERROR : There's something wrong. Plese try again.";
            }
            return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/EvaluateForm')->with('notification', $notification)->with('activeTab', $activeTab);

        } else {
            $Service_attrib = Service_attrib::find($id);
            $Service_attrib->category = Input::get('category');
            $Service_attrib->subcategory = Input::get('subcategory');
            $Service_attrib->other_details = Input::get('other_details');
            $Service_attrib->designation = Input::get('designation');
            $Service_attrib->period_covered = Input::get('period_covered');
            $Service_attrib->points = Input::get('points');
            $Service_attrib->name_ofcommittee = Input::get('name_ofcommittee');
            $Service_attrib->nature_ofcommittee = Input::get('nature_ofcommittee');
            $Service_attrib->funding_source = Input::get('funding_source');
            $Service_attrib->purpose_offund = Input::get('purpose_offund');
            $Service_attrib->ammount_donated = Input::get('ammount_donated');
            $Service_attrib->designation_ofoic = Input::get('designation_ofoic');
            $Service_attrib->level = Input::get('level');
            $Service_attrib->name_oforganization = Input::get('name_oforganization');
            $Service_attrib->subsubcategory = Input::get('subsubcategory');

            $files = $request->file('file');


            $attachmentFileNames = "";
            $attachmentNamesSpit[] = "";
            $oldFiles = $Service_attrib->attachment_filenames;
            if (!(empty($files[0]))) {
                $oldFiles = $Service_attrib->attachment_filenames;
                foreach ($files as $file):
                    $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                    $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                    $file->move('uploads', $fileName);
                    $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
                endforeach;
            }


            if (Input::get('proofAction') == 1) {
                $Service_attrib->attachment_filenames = $attachmentFileNames . $oldFiles;
            } else if (Input::get('proofAction') == 2) {
                $deteleThisFiles = explode("+++", $Service_attrib->attachment_filenames);
                $counter = 0;
                foreach ($deteleThisFiles as $file):
                    if ($file != "") {
                        unlink('uploads/' . $file);
                    }
                endforeach;
                $Service_attrib->attachment_filenames = $attachmentFileNames;
            } else {
                $deteleThisFiles = explode("+++", $Service_attrib->attachment_filenames);
                $counter = 0;
                foreach ($deteleThisFiles as $file):
                    if ($file != "") {
                        unlink('uploads/' . $file);
                    }
                endforeach;
                $Service_attrib->attachment_filenames = NULL;
            }
            $Service_attrib->save();


            $notification = "SUCCESS : Form has been successfully  updated.";
            if (Input::get('action') == "ReviseForm") {
                return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/ReviseForm')->with('notification', $notification)->with('activeTab', $activeTab);
            } else {
                return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
            }
        }
    }

    public function DeleteEntryCat3($id)
    {
        $this->CallForPromtion();
        $formID = DB::select('SELECT
                            mpromotion_forms.id
                            FROM
                            mpromotion_forms
                            INNER JOIN service_tbl ON service_tbl.mpromotion_formID = mpromotion_forms.id
                            INNER JOIN service_attrib ON service_attrib.service_tblID = service_tbl.id
                            WHERE
                            mpromotion_forms.id = service_tbl.mpromotion_formID AND
                            service_tbl.id = service_attrib.service_tblID AND
                            service_attrib.id = \'' . $id . '\'');

        $Service_attrib = Service_attrib::find($id);
        $deteleThisFiles = explode("+++", $Service_attrib->attachment_filenames);
        $counter = 0;
        foreach ($deteleThisFiles as $file):
            if ($file != "") {
                unlink('uploads/' . $file);
            }
        endforeach;

        $activeTab = 3;
        Service_attrib::destroy($id);

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        }

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $notification = "SUCCESS :  Entry has been successfully deleted.";
        return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    ////////////////
    // CATEGORY 2 //
    ////////////////
    public function SubmitFormCat2($id, Request $request)
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT creativework_tbl.id FROM creativework_tbl INNER JOIN mpromotion_forms 
            ON creativework_tbl.mpromotion_formID = mpromotion_forms.id WHERE mpromotion_forms.id = ' . $id);

        $Creativework_attrib = new Creativework_attrib;
        $Creativework_attrib->creativework_tblID = $sql[0]->id;

        $Creativework_attrib->category = Input::get('category');
        $Creativework_attrib->subcategory = Input::get('subcategory');
        $Creativework_attrib->subsubcategory = Input::get('subsubcategory');
        $Creativework_attrib->title = Input::get('title');
        $Creativework_attrib->author = Input::get('author');
        $Creativework_attrib->date_published = Input::get('date_published');
        $Creativework_attrib->bibliography = Input::get('bibliography');
        $Creativework_attrib->nature_ofpublication = Input::get('nature_ofpublication');
        $Creativework_attrib->other_details = Input::get('other_details');
        $Creativework_attrib->points = Input::get('points');
        $Creativework_attrib->publisher = Input::get('publisher');
        $Creativework_attrib->date_ofpresentation = Input::get('date_ofpresentation');
        $Creativework_attrib->place_ofpresentation = Input::get('place_ofpresentation');
        $Creativework_attrib->title_ofconference = Input::get('title_ofconference');

        $files = $request->file('file');
        $attachmentFileNames = "";
        $attachmentNamesSpit[] = "";
        if (!(empty($files[0]))) {
            foreach ($files as $file):
                $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                $file->move('uploads', $fileName);
                $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
            endforeach;
            $Creativework_attrib->attachment_filenames = $attachmentFileNames;
        }
        $Creativework_attrib->save();
        $activeTab = 2;

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $notification = "SUCCESS : Form has been successfully  updated.";
        return redirect($currentUser . '/meritpromotion/' . $id . '/FillOutMeritPromotion')->with('notification', $notification)
            ->with('activeTab', $activeTab);
    }

    public function RetrieveCat2($id, Request $request)
    {
        $this->CallForPromtion();
        $Creativework_attrib = Creativework_attrib::find($id);
        if ($request->ajax()) {
            return $Creativework_attrib;
        }
    }

    public function UpdateFormCat2($id, Request $request)
    {
        $this->CallForPromtion();
        $activeTab = 2;
        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }

        $formID = DB::select('SELECT
                            mpromotion_forms.id
                            FROM
                            mpromotion_forms
                            INNER JOIN creativework_tbl ON creativework_tbl.mpromotion_formID = mpromotion_forms.id
                            INNER JOIN creativework_attrib ON creativework_attrib.creativework_tblID = creativework_tbl.id
                            WHERE
                            mpromotion_forms.id = creativework_tbl.mpromotion_formID AND
                            creativework_tbl.id = creativework_attrib.creativework_tblID AND
                            creativework_attrib.id = \'' . $id . '\'');
        // FUNCTION: Evaluate Form
        if (Input::get('action') == "EvaluateForm") {

            $Creativework_attrib = Creativework_attrib::find($id);
            $Creativework_attrib->category = $Creativework_attrib->category;
            $Creativework_attrib->subcategory = $Creativework_attrib->subcategory;
            $Creativework_attrib->subsubcategory = $Creativework_attrib->subsubcategory;
            $Creativework_attrib->title = $Creativework_attrib->title;
            $Creativework_attrib->author = $Creativework_attrib->author;
            $Creativework_attrib->date_published = $Creativework_attrib->date_published;
            $Creativework_attrib->bibliography = $Creativework_attrib->bibliography;
            $Creativework_attrib->nature_ofpublication = $Creativework_attrib->nature_ofpublication;
            $Creativework_attrib->other_details = $Creativework_attrib->other_details;
            $Creativework_attrib->publisher = $Creativework_attrib->publisher;
            $Creativework_attrib->date_ofpresentation = $Creativework_attrib->date_ofpresentation;
            $Creativework_attrib->place_ofpresentation = $Creativework_attrib->place_ofpresentation;
            $Creativework_attrib->title_ofconference = $Creativework_attrib->title_ofconference;
            $Creativework_attrib->attachment_filenames = $Creativework_attrib->attachment_filenames;
            // accept
            if (Input::get('evaluateAction') == 1) {
                if ($currentUser == "deptchair") {
                    $Creativework_attrib->isAccepted_deptchair = 1;
                } else if ($currentUser == "dean") {
                    $Creativework_attrib->isAccepted_dean = 1;
                } else if ($currentUser == "vchancellor") {
                    $Creativework_attrib->isAccepted_vchancellor = 1;
                }
                $Creativework_attrib->points = Input::get('points');
            } // reject
            else if ((Input::get('evaluateAction') == 2)) {
                if ($currentUser == "deptchair") {
                    $Creativework_attrib->isAccepted_deptchair = 2;
                } else if ($currentUser == "dean") {
                    $Creativework_attrib->isAccepted_dean = 2;
                } else if ($currentUser == "vchancellor") {
                    $Creativework_attrib->isAccepted_vchancellor = 2;
                }
                $Creativework_attrib->points = $Creativework_attrib->points;
            } // needs revision
            else if ((Input::get('evaluateAction') == 3)) {
                if ($currentUser == "deptchair") {
                    $Creativework_attrib->isAccepted_deptchair = 3;
                } else if ($currentUser == "dean") {
                    $Creativework_attrib->isAccepted_dean = 3;
                } else if ($currentUser == "vchancellor") {
                    $Creativework_attrib->isAccepted_vchancellor = 3;
                }
                $Creativework_attrib->points = $Creativework_attrib->points;
            }


            $Creativework_attrib->save();
            if (!empty((Input::get('evaluateAction')))) {
                $notification = "SUCCESS : Form has been successfully  updated.";
            } else {
                $notification = "ERROR : There's something wrong. Plese try again.";
            }
            return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/EvaluateForm')->with('notification', $notification)->with('activeTab', $activeTab);
        } else {
            $Creativework_attrib = Creativework_attrib::find($id);
            $Creativework_attrib->category = Input::get('category');
            $Creativework_attrib->subcategory = Input::get('subcategory');
            $Creativework_attrib->subsubcategory = Input::get('subsubcategory');
            $Creativework_attrib->title = Input::get('title');
            $Creativework_attrib->author = Input::get('author');
            $Creativework_attrib->date_published = Input::get('date_published');
            $Creativework_attrib->bibliography = Input::get('bibliography');
            $Creativework_attrib->nature_ofpublication = Input::get('nature_ofpublication');
            $Creativework_attrib->other_details = Input::get('other_details');
            $Creativework_attrib->points = Input::get('points');
            $Creativework_attrib->publisher = Input::get('publisher');
            $Creativework_attrib->date_ofpresentation = Input::get('date_ofpresentation');
            $Creativework_attrib->place_ofpresentation = Input::get('place_ofpresentation');
            $Creativework_attrib->title_ofconference = Input::get('title_ofconference');


            $files = $request->file('file');
            $attachmentFileNames = "";
            $attachmentNamesSpit[] = "";
            $oldFiles = $Creativework_attrib->attachment_filenames;
            if (!(empty($files[0]))) {
                $oldFiles = $Creativework_attrib->attachment_filenames;
                foreach ($files as $file):
                    $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                    $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;

                    // for saving all the filenames in one variable in the database
                    $file->move('uploads', $fileName);
                    $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
                endforeach;
            }

            if (Input::get('proofAction') == 1) {
                $Creativework_attrib->attachment_filenames = $attachmentFileNames . $oldFiles;
            } else if (Input::get('proofAction') == 2) {
                $deteleThisFiles = explode("+++", $Creativework_attrib->attachment_filenames);
                $counter = 0;
                foreach ($deteleThisFiles as $file):
                    if ($file != "") {
                        unlink('uploads/' . $file);
                    }
                endforeach;
                $Creativework_attrib->attachment_filenames = $attachmentFileNames;
            } else {
                $deteleThisFiles = explode("+++", $Creativework_attrib->attachment_filenames);
                $counter = 0;
                foreach ($deteleThisFiles as $file):
                    if ($file != "") {
                        unlink('uploads/' . $file);
                    }
                endforeach;
                $Creativework_attrib->attachment_filenames = NULL;
            }

            $Creativework_attrib->save();
            $notification = "SUCCESS : Form has been successfully  updated.";
            if (Input::get('action') == "ReviseForm") {
                return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/ReviseForm')->with('notification', $notification)->with('activeTab', $activeTab);
            } else {
                return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
            }
        }
    }

    public function DeleteEntryCat2($id)
    {
        $this->CallForPromtion();
        $formID = DB::select('SELECT
                            mpromotion_forms.id
                            FROM
                            mpromotion_forms
                            INNER JOIN creativework_tbl ON creativework_tbl.mpromotion_formID = mpromotion_forms.id
                            INNER JOIN creativework_attrib ON creativework_attrib.creativework_tblID = creativework_tbl.id
                            WHERE
                            mpromotion_forms.id = creativework_tbl.mpromotion_formID AND
                            creativework_tbl.id = creativework_attrib.creativework_tblID AND
                            creativework_attrib.id = \'' . $id . '\'');

        $Creativework_attrib = Creativework_attrib::find($id);
        $deteleThisFiles = explode("+++", $Creativework_attrib->attachment_filenames);
        $counter = 0;
        foreach ($deteleThisFiles as $file):
            if ($file != "") {
                unlink('uploads/' . $file);
            }
        endforeach;

        $activeTab = 2;
        Creativework_attrib::destroy($id);

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $notification = "SUCCESS :  Entry has been successfully deleted.";
        return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    ////////////////
    // CATEGORY 1 //
    ////////////////
    public function SubmitFormCat1($id, Request $request)
    {
        $this->CallForPromtion();
        $sql = DB::select('SELECT performance_tbl.id FROM performance_tbl INNER JOIN mpromotion_forms
            ON performance_tbl.mpromotion_formID = mpromotion_forms.id WHERE mpromotion_forms.id = ' . $id);

        $Performance_attrib = new Performance_attrib;
        $Performance_attrib->performance_tblID = $sql[0]->id;

        $Performance_attrib->category = Input::get('category');
        $Performance_attrib->subcategory = Input::get('subcategory');
        $Performance_attrib->WARating = Input::get('WARating');
        $Performance_attrib->title = Input::get('title');
        $Performance_attrib->author = Input::get('author');
        $Performance_attrib->date_published = Input::get('date_published');
        $Performance_attrib->publisher = Input::get('publisher');
        $Performance_attrib->course_title = Input::get('course_title');
        $Performance_attrib->semester_used = Input::get('semester_used');
        $Performance_attrib->ay_used = Input::get('ay_used');
        $Performance_attrib->nature_ofpublication = Input::get('nature_ofpublication');
        $Performance_attrib->bibliography = Input::get('bibliography');
        $Performance_attrib->as_what = Input::get('as_what');
        $Performance_attrib->year_graduated = Input::get('year_graduated');
        $Performance_attrib->name_ofgraduate = Input::get('name_ofgraduate');
        $Performance_attrib->number_ofpreparations = Input::get('number_ofpreparations');
        $Performance_attrib->other_contributions = Input::get('other_contributions');
        $Performance_attrib->points = Input::get('points');


        $files = $request->file('file');


        $attachmentFileNames = "";
        $attachmentNamesSpit[] = "";
        if (!(empty($files[0]))) {
            foreach ($files as $file):
                $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                $file->move('uploads', $fileName);
                $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
            endforeach;
            $Performance_attrib->attachment_filenames = $attachmentFileNames;
        }
        $Performance_attrib->save();
        $activeTab = 1;

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $notification = "SUCCESS : Form has been successfully  updated.";
        return redirect($currentUser . '/meritpromotion/' . $id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }

    public function RetrieveCat1($id, Request $request)
    {
        $this->CallForPromtion();
        $Performance_attrib = Performance_attrib::find($id);
        if ($request->ajax()) {
            return $Performance_attrib;
        }
    }

    public function UpdateFormCat1($id, Request $request)
    {
        $this->CallForPromtion();
        $activeTab = 1;
        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $formID = DB::select('SELECT
                                mpromotion_forms.id
                                FROM
                                mpromotion_forms
                                INNER JOIN performance_tbl ON performance_tbl.mpromotion_formID = mpromotion_forms.id
                                INNER JOIN performance_attrib ON performance_attrib.performance_tblID = performance_tbl.id
                                WHERE
                                mpromotion_forms.id = performance_tbl.mpromotion_formID AND
                                performance_tbl.id = performance_attrib.performance_tblID AND
                                performance_attrib.id =  \'' . $id . '\'');

        // FUNCTION: Evaluate Form
        if (Input::get('action') == "EvaluateForm") {
            $Performance_attrib = Performance_attrib::find($id);
            $Performance_attrib->category = $Performance_attrib->category;
            $Performance_attrib->subcategory = $Performance_attrib->subcategory;
            $Performance_attrib->WARating = $Performance_attrib->WARating;
            $Performance_attrib->title = $Performance_attrib->title;
            $Performance_attrib->author = $Performance_attrib->author;
            $Performance_attrib->date_published = $Performance_attrib->date_published;
            $Performance_attrib->publisher = $Performance_attrib->publisher;
            $Performance_attrib->course_title = $Performance_attrib->course_title;
            $Performance_attrib->semester_used = $Performance_attrib->semester_used;
            $Performance_attrib->ay_used = $Performance_attrib->ay_used;
            $Performance_attrib->nature_ofpublication = $Performance_attrib->nature_ofpublication;
            $Performance_attrib->bibliography = $Performance_attrib->bibliography;
            $Performance_attrib->as_what = $Performance_attrib->as_what;
            $Performance_attrib->year_graduated = $Performance_attrib->year_graduated;
            $Performance_attrib->name_ofgraduate = $Performance_attrib->name_ofgraduate;
            $Performance_attrib->number_ofpreparations = $Performance_attrib->number_ofpreparations;
            $Performance_attrib->other_contributions = $Performance_attrib->other_contributions;
            $Performance_attrib->attachment_filenames = $Performance_attrib->attachment_filenames;

            // accept
            if (Input::get('evaluateAction') == 1) {
                if ($currentUser == "deptchair") {
                    $Performance_attrib->isAccepted_deptchair = 1;
                } else if ($currentUser == "dean") {
                    $Performance_attrib->isAccepted_dean = 1;
                } else if ($currentUser == "vchancellor") {
                    $Performance_attrib->isAccepted_vchancellor = 1;
                }
                $Performance_attrib->points = Input::get('points');
            } // reject
            else if ((Input::get('evaluateAction') == 2)) {
                if ($currentUser == "deptchair") {
                    $Performance_attrib->isAccepted_deptchair = 2;
                } else if ($currentUser == "dean") {
                    $Performance_attrib->isAccepted_dean = 2;
                } else if ($currentUser == "vchancellor") {
                    $Performance_attrib->isAccepted_vchancellor = 2;
                }
                $Performance_attrib->points = $Performance_attrib->points;
            } // needs revision
            else if ((Input::get('evaluateAction') == 3)) {
                if ($currentUser == "deptchair") {
                    $Performance_attrib->isAccepted_deptchair = 3;
                } else if ($currentUser == "dean") {
                    $Performance_attrib->isAccepted_dean = 3;
                } else if ($currentUser == "vchancellor") {
                    $Performance_attrib->isAccepted_vchancellor = 3;
                }
                $Performance_attrib->points = $Performance_attrib->points;
            }
            $Performance_attrib->save();
            if (!empty((Input::get('evaluateAction')))) {
                $notification = "SUCCESS : Form has been successfully  updated.";
            } else {
                $notification = "ERROR : There's something wrong. Plese try again.";
            }

            return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/EvaluateForm')->with('notification', $notification)->with('activeTab', $activeTab);
        } // FUNCTION: Update Form
        else {
            $Performance_attrib = Performance_attrib::find($id);
            $Performance_attrib->points = Input::get('points');
            $Performance_attrib->category = Input::get('category');
            $Performance_attrib->subcategory = Input::get('subcategory');
            $Performance_attrib->WARating = Input::get('WARating');
            $Performance_attrib->title = Input::get('title');
            $Performance_attrib->author = Input::get('author');
            $Performance_attrib->date_published = Input::get('date_published');
            $Performance_attrib->publisher = Input::get('publisher');
            $Performance_attrib->course_title = Input::get('course_title');
            $Performance_attrib->semester_used = Input::get('semester_used');
            $Performance_attrib->ay_used = Input::get('ay_used');
            $Performance_attrib->nature_ofpublication = Input::get('nature_ofpublication');
            $Performance_attrib->bibliography = Input::get('bibliography');
            $Performance_attrib->as_what = Input::get('as_what');
            $Performance_attrib->year_graduated = Input::get('year_graduated');
            $Performance_attrib->name_ofgraduate = Input::get('name_ofgraduate');
            $Performance_attrib->number_ofpreparations = Input::get('number_ofpreparations');
            $Performance_attrib->other_contributions = Input::get('other_contributions');

            $files = $request->file('file');
            $attachmentFileNames = "";
            $oldFiles = $Performance_attrib->attachment_filenames;
            $attachmentNamesSpit[] = "";
            if (!(empty($files[0]))) {
                foreach ($files as $file):
                    $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                    $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                    $file->move('uploads', $fileName);
                    $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
                endforeach;
            }

            if (Input::get('proofAction') == 1) {
                $Performance_attrib->attachment_filenames = $attachmentFileNames . $oldFiles;
            } else if (Input::get('proofAction') == 2) {
                $deteleThisFiles = explode("+++", $Performance_attrib->attachment_filenames);
                $counter = 0;
                foreach ($deteleThisFiles as $file):
                    if ($file != "") {
                        unlink('uploads/' . $file);
                    }
                endforeach;
                $Performance_attrib->attachment_filenames = $attachmentFileNames;
            } else {
                $deteleThisFiles = explode("+++", $Performance_attrib->attachment_filenames);
                $counter = 0;
                foreach ($deteleThisFiles as $file):
                    if ($file != "") {
                        unlink('uploads/' . $file);
                    }
                endforeach;
                $Performance_attrib->attachment_filenames = NULL;
            }

            $Performance_attrib->save();
            $notification = "SUCCESS : Form has been successfully  updated.";
            if (Input::get('action') == "ReviseForm") {
                return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/ReviseForm')->with('notification', $notification)->with('activeTab', $activeTab);
            } else {
                return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
            }
        }
    }

    public function DeleteEntryCat1($id)
    {
        $this->CallForPromtion();
        $formID = DB::select('SELECT
                            mpromotion_forms.id
                            FROM
                            mpromotion_forms
                            INNER JOIN performance_tbl ON performance_tbl.mpromotion_formID = mpromotion_forms.id
                            INNER JOIN performance_attrib ON performance_attrib.performance_tblID = performance_tbl.id
                            WHERE
                            mpromotion_forms.id = performance_tbl.mpromotion_formID AND
                            performance_tbl.id = performance_attrib.performance_tblID AND
                            performance_attrib.id =  \'' . $id . '\'
                        ');

        $Performance_attrib = Performance_attrib::find($id);
        $deteleThisFiles = explode("+++", $Performance_attrib->attachment_filenames);
        $counter = 0;
        foreach ($deteleThisFiles as $file):
            if ($file != "") {
                unlink('uploads/' . $file);
            }
        endforeach;

        $activeTab = 1;
        Performance_attrib::destroy($id);

        $currentUserID = Auth::user()->position;
        if ($currentUserID == 0) {
            $currentUser = "faculty";
        } else if ($currentUserID == 1) {
            $currentUser = "deptchair";
        } else if ($currentUserID == 2) {
            $currentUser = "dean";
        } else if ($currentUserID == 3) {
            $currentUser = "vchancellor";
        } else if ($currentUserID == 4) {
            $currentUser = "chancellor";
        }
        $notification = "SUCCESS :  Entry has been successfully deleted.";
        return redirect($currentUser . '/meritpromotion/' . $formID[0]->id . '/FillOutMeritPromotion')->with('notification', $notification)->with('activeTab', $activeTab);
    }
}
