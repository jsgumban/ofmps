@if(Auth::user())
    @if((Auth::user()->position)==5)
        <?php $preLink = FOLDERNAME . '/admin/'; ?>
        <?php $currentUser = 'admin'; ?>
        <?php $currentUser1 = 'SYSTEM ADMIN'; ?>
    @elseif((Auth::user()->position)==4)
        <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
        <?php $currentUser = 'chancellor'; ?>
        <?php $currentUser1 = 'CHANCELLOR'; ?>
    @elseif((Auth::user()->position)==3)
        <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
        <?php $currentUser = 'vchancellor'; ?>
        <?php $currentUser1 = 'VICE CHANCELLOR'; ?>
    @elseif((Auth::user()->position)==2)
        <?php $preLink = FOLDERNAME . '/dean/'; ?>
        <?php $currentUser = 'dean'; ?>
        <?php $currentUser1 = 'COLLEGE DEAN'; ?>
    @elseif((Auth::user()->position)==1)
        <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
        <?php $currentUser = 'deptchair'; ?>
        <?php $currentUser1 = 'DEPT. CHAIRPERSON'; ?>
    @elseif((Auth::user()->position)==0)
        <?php $preLink = FOLDERNAME . '/faculty/'; ?>
        <?php $currentUser = 'faculty'; ?>
        <?php $currentUser1 = 'FACULTY'; ?>
    @endif
@endif


<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega " role="navigation">
    <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
        <i class="icon wb-more-horizontal" aria-hidden="true"></i>
    </button>

    {{-- logo --}}
    <a href={{ FOLDERNAME }}>
        <div class="navbar-header">
            <div class="navbar-brand navbar-brand-center " data-toggle="gridmenu">
                <span class="navbar-brand-text"><big><big>UPMIN OFMPS </big></big></span>
            </div>
        </div>
    </a>

    <div class="navbar-container container-fluid">
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                <li>
                    <a href="{{ FOLDERNAME }}" aria-expanded="false" role="button"> <big> HOME </big></a>
                </li>


                {{-- admin --}}
                @if($currentUser=='admin')
                    <li>
                        <a href={!! $preLink.'university/UniversitySettings' !!}  aria-expanded="false"
                           role="button"><big> UNIVERSITY SETTINGS </big></i></a>
                    </li>
                @endif

                {{-- department chair--}}
                @if($currentUser=='deptchair')
                    <li>
                        <a href={!! $preLink.'dapc' !!} aria-expanded="false"
                           aria-expanded="false"
                           role="button"><big> DAPC </big></a>
                        </a>

                    </li>
                @endif


                {{-- dean --}}
                @if($currentUser=='dean')
                    <li>
                        <a href={!! $preLink.'capc' !!} aria-expanded="false" aria-expanded="false"
                           role="button"><big> CAPC </big></a>
                        </a>
                    </li>
                @endif


                {{-- vchancellor --}}
                @if($currentUser=='vchancellor')
                    <li>
                        <a href={!! $preLink.'uapfc' !!} aria-expanded="false"
                           aria-expanded="false"
                           role="button"><big> UAPFC </big></a>
                        </a>
                    </li>
                @endif

                {{-- chancellor --}}
                @if($currentUser=='chancellor')
                    <li>
                        <a href={!! $preLink.'chancellor' !!}  aria-expanded="false"
                           aria-expanded="false"
                           role="button"><big> CHANCELLOR </big></a>
                        </a>
                    </li>
                @endif

                {{-- merit promotion --}}
                @if($currentUser!='admin')
                    <li>
                        <a href={!! $preLink.'meritpromotion' !!} aria-expanded="false"
                           aria-expanded="false"
                           role="button"><big> MERIT PROMOTION </big></a>
                        </a>
                    </li>
                @endif




                {{-- my profile --}}
                <li class="dropdown">
                    <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
                       role="button">
                        <i class="icon wb-user-circle" aria-hidden="true" style="font-size: 30px;"></i>
                    </a>


                    <ul class="dropdown-menu" role="menu">
                        {{-- DISPLAY FIRST NAME --}}

                        <center><span class="label label-dark">{{$currentUser1}}</span></center>


                        {{-- SHOW OWN PROFILE --}}
                        <li class="divider" role="presentation"></li>
                        <li role="presentation">
                            <a href={!! $preLink.'profile/MyProfile' !!} role="menuitem">
                                <i class="icon wb-user" aria-hidden="true" minlength="9" maxlength="9"></i>
                                My Profile {{'('.Auth::user()->first_name.')'}}
                            </a>
                        </li>


                        {{-- CHANGE PASSWORD FUNCTION HERE --}}
                        <li role="presentation">
                            <a href={!! $preLink.'profile/UpdateMyPassword' !!} role="menuitem">
                                <i class="icon fa-key" aria-hidden="true"></i>
                                Update Password
                            </a>
                        </li>

                        {{-- LOGOUT FUNCTION HERE --}}
                        <li class="divider" role="presentation"></li>
                        <li role="presentation">
                            <a href={{ FOLDERNAME."/auth/logout" }} role="menuitem"><i class="icon wb-power"
                                                                                       aria-hidden="true"></i>
                                Logout</a>
                        </li>
                    </ul>
                </li>

                <li class="hidden-xs" id="toggleFullscreen">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </li>
            </ul>
        </div>
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
                <div class="form-group">
                    <div class="input-search">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="site-search" placeholder="Search...">
                        <button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search"
                                data-toggle="collapse" aria-label="Close"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>