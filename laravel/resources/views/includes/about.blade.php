{{-- LOCATED IN THE LOG-IN PAGE --}}
<div class="col-lg-4 col-md-6">
    <div class="example-wrap">
        <div class="example">
            <div class="modal fade" id="exampleAccrodionModal" aria-hidden="true"
                 aria-labelledby="exampleAccrodionModal"
                 role="dialog" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="panel-group panel-group-continuous margin-0" id="exampleAccrodion1"
                             aria-multiselectable="true" role="tablist">
                            <div class="panel">

                                <div class="panel-heading" id="exampleHeadingFirst" role="tab">
                                    <a class="panel-title" data-parent="#exampleAccrodion1" data-toggle="collapse"
                                       href="#exampleCollapseFirst" aria-controls="exampleCollapseFirst"
                                       aria-expanded="false">
                                        <i class="md-palette"></i> ABOUT UPMIN OFMPS
                                    </a>
                                </div>

                                <div class="panel-collapse collapse in" id="exampleCollapseFirst"
                                     aria-labelledby="exampleHeadingFirst"
                                     role="tabpanel">
                                    <div class="panel-body" style="color:gray">
                                        Remissius verear, sabinum concessum linguam umbram saepe vendibiliora quoddam
                                        terroribus,
                                        intellegere rectas videmus octavio sapientiam,
                                        mox magistra provincias chaere videre coerceri
                                        efficiat, corpus mediocriterne sinat beatam denique
                                        maiora menandro, iniucundus fastidium, aliquem
                                        probatus benivolentiam omnium summum facultas fuga,
                                        debitis mirari menandri domus molita. Maximisque
                                        interpretaris multoque exercitus tractatas, commemorandis
                                        pecunias.
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-heading" id="exampleHeadingSecond" role="tab">
                                    <a class="panel-title collapsed" data-parent="#exampleAccrodion1"
                                       data-toggle="collapse"
                                       href="#exampleCollapseSecond" aria-controls="exampleCollapseSecond"
                                       aria-expanded="false">
                                        <i class="md-place"></i> ABOUT UP MINDANAO
                                    </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapseSecond"
                                     aria-labelledby="exampleHeadingSecond"
                                     role="tabpanel">
                                    <div class="panel-body" style="color:gray">
                                        Diuturnitatem chremes gratia macedonum referatur intellegitur t ea industriae
                                        plus,
                                        ex videmus praetereat ratio mediocrem pro orestem,
                                        ipsam lictores perpetiuntur aperiri benivolentiam,
                                        nusquam ignaviamque athenis m plato videamus, liberatione
                                        scientia nihilo aristotelem quoquo consumere latinam,
                                        successerit certa morte menandro delectatum noster
                                        impetu videri senserit, infinitum iudicatum misisti
                                        conectitur, voce proficiscuntur.
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-heading" id="exampleHeadingThird" role="tab">
                                    <a class="panel-title collapsed" data-parent="#exampleAccrodion1"
                                       data-toggle="collapse"
                                       href="#exampleCollapseThird" aria-controls="exampleCollapseThird"
                                       aria-expanded="false">
                                        <i class="md-loupe"></i> ABOUT THE DEVELOPER
                                    </a>
                                </div>

                                <div class="panel-collapse collapse" id="exampleCollapseThird"
                                     aria-labelledby="exampleHeadingThird"
                                     role="tabpanel">
                                    <div class="panel-body" style="color:gray">
                                        Audire scribimus spe platonis longinquitate evertunt scribi, notionem doleamus
                                        assentiar mortis lucilius, exedunt. Finitum genus
                                        coniunctione vidisse, ipsam grate studuisse respondendum
                                        ignorant probabo atomum. Corrumpit mortem instructus
                                        totam familiarem tertium voluntates consilia aperiam
                                        disputata, plena animumque ius supplicii incurrunt
                                        laboribus, rationis dedocendi incurreret illam
                                        triari utrisque eos commodius. Assentiar magnitudinem.
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>