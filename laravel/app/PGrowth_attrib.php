<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PGrowth_attrib extends Model
{
    protected $table = 'pgrowth_attrib';
    public $timestamps = false;

}
