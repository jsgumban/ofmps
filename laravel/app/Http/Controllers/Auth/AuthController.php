<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mpromotion;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    public function CallForPromtion(){
        try{
            $id=DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
            if(!empty($id)){
                $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.id='.$id[0]->id);
                if(!empty($isThereCallForPromotion)){
                    $startDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_start);
                    $endDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_end);
                    $currentDate = strtotime(date("Y-m-d"));
                    $mpromotion = Mpromotion::find($isThereCallForPromotion[0]->id);
                
                    if (($currentDate >= $startDate) && ($currentDate <= $endDate)){
                        // call for promotion status: on-going
                        $mpromotion->mpromotion_status=0;
                        $mpromotion->save();
                    }else if($currentDate>$endDate){
                        // call for promotion status: finished
                        $mpromotion->mpromotion_status=1;
                        $mpromotion->save();
                    }else if($startDate>$currentDate){
                        // call for promotion status: not yet started
                        $mpromotion->mpromotion_status=2;
                        $mpromotion->save();
                    }
                }
            }
        }catch (Exception $e) {
            $isThereCallForPromotion = "";
        }
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    /**
     * Overriding postLogin() from Auth/AuthenticatesAndRegistersUsers
     */
    protected $username = 'employee_code';

    public function __construct()
    {
        $this->CallForPromtion();
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $this->CallForPromtion();
        return Validator::make($data, [
            'name' => 'required|max:255',
            'password' => 'required|confirmed|min:6',

            'employee_code' => 'required|max:9|',
            'position' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $this->CallForPromtion();
        return User::create([
            'name' => $data['name'],
            'password' => bcrypt($data['password']),

            'employee_code' => $data['employee_code'],
            'position' => $data['position'],
        ]);
    }


}
