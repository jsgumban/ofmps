@if(Auth::user())
    @if((Auth::user()->position)==5)
        <?php $preLink = FOLDERNAME . '/admin/'; ?>
        <?php $currentUser = 'admin'; ?>
    @elseif((Auth::user()->position)==4)
        <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
        <?php $currentUser = 'chancellor'; ?>
    @elseif((Auth::user()->position)==3)
        <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
        <?php $currentUser = 'vchancellor'; ?>
    @elseif((Auth::user()->position)==2)
        <?php $preLink = FOLDERNAME . '/dean/'; ?>
        <?php $currentUser = 'dean'; ?>
    @elseif((Auth::user()->position)==1)
        <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
        <?php $currentUser = 'deptchair'; ?>
    @elseif((Auth::user()->position)==0)
        <?php $preLink = FOLDERNAME . '/faculty/'; ?>
        <?php $currentUser = 'faculty'; ?>
    @endif
@endif

<div class="tab-pane active" id="category-1" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">
        <h4 id="ASDASD">
            <center><b>SUBMITTED FORMS</b><br></center>
        </h4>

        @if(session('activeTab')==1)
            @if(!empty(session('notification')))
                <br>
                <?php $notif = session('notification');?>
                @if(strpos($notif,'SUCCESS')===false)
                    <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @else
                    <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @endif
            @endif
        @endif
        <hr>

        <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Date Submitted</th>
                <th>Filled Out Date</th>
                <th>Call for Promotion Date</th>
                <th>Form Status</th>
                <th>Points</th>
                <th>
                    <center>
                        Action
                    </center>
                </th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th>#</th>
                <th>Date Submitted</th>
                <th>Filled Out Date</th>
                <th>Call for Promotion Date</th>
                <th>Form Status</th>
                <th>Points</th>
                <th>
                    <center>
                        Action
                    </center>
                </th>
            </tr>
            </tfoot>


            <tbody>
            <?php $count = 1; ?>
            @foreach ($submittedForms as $submittedForm)
                <tr>
                    <td> <?php echo $count++; ?>  </td>
                    <td> {{ date('F d, Y', strtotime($submittedForm->submit_date_deptchair)) }}  </td>
                    <td>{{ date('F d, Y', strtotime($submittedForm->fillOut_date)) }}</td>
                    <td> {{ date('F d, Y', strtotime($submittedForm->callforpromotion_date_start)).' - '.date('F d, Y', strtotime($submittedForm->callforpromotion_date_end)) }} </td>
                    <td>
                        @if($submittedForm->form_status==1)
                            <span class="label  label-info">DAPC Level</span>
                        @elseif($submittedForm->form_status==1.5)
                            <span class="label  label-danger">DAPC Level - REJECTED</span>
                        @elseif($submittedForm->form_status==2)
                            <span class="label  label-primary">CAPC Level</span>
                        @elseif($submittedForm->form_status==2.5)
                            <span class="label  label-danger">CAPC Level - REJECTED</span>
                        @elseif($submittedForm->form_status==3)
                            <span class="label  label-success">UAPFC Level</span>
                        @elseif($submittedForm->form_status==3.5)
                            <span class="label  label-danger">UAPFC Level - REJECTED</span>
                        @elseif($submittedForm->form_status==4)
                            <span class="label  label-danger">Chancellor Level</span>
                        @elseif($submittedForm->form_status==5)
                            <span class="label  label-danger">Needs Revisions</span>
                        @elseif($submittedForm->form_status==6)
                            <span class="label  label-danger"> 
                                @if(empty($submittedForm->initial_rank))
                                    (0)
                                @else
                                    {{ "(".$submittedForm->initial_rank." ".$submittedForm->initial_step.")" }}
                                @endif
                            </span>
                        @elseif($submittedForm->form_status==7)
                            <span class="label  label-danger">Chancellor Level - REJECTED</span>
                        @endif
                    </td>
                    <td><span class="label label-dark">  {{$submittedForm->faculty_total}}</span></td>
                    <td align="center">
                        @if($submittedForm->form_status!=5)
                            {{-- MONITOR DRAFT FORM --}}
                            <a href="{!! $preLink.'meritpromotion/'.$submittedForm->id.'/MonitorForm' !!}"

                               data-toggle="tooltip" title="Monitor Form">
                                <button type="button"
                                        class="btn btn-icon social-instagram  btn-sm btn-sm  btn-sm"
                                        title=""><i class="icon wb-eye" aria-hidden="true"
                                                    style="font-size: 15px;"></i></button>
                            </a>
                        @endif

                        @if($submittedForm->form_status==5)
                            {{-- Revise Form --}}
                            <a href="{!! $preLink.'meritpromotion/'.$submittedForm->id.'/ReviseForm' !!}"
                               data-toggle="tooltip" title="Revise Form">
                                <button type="button"
                                        class="btn btn-icon btn-danger  btn-sm btn-sm  btn-sm"
                                        title=""><i class="icon wb-refresh" aria-hidden="true"></i></button>
                            </a>
                        @endif

                        {{-- GENERATE PDF --}}
                        <a href="{!! $preLink.'meritpromotion/'.$submittedForm->id.'/generatePDF' !!}"
                           data-toggle="tooltip" title="Form(pdf)" target="_blank">
                            <button type="button"
                                    class="btn btn-icon btn-warning  btn-sm btn-sm  btn-sm"
                                    ><i class="icon fa-file-pdf-o"
                                        aria-hidden="true"
                                        style="font-size: 15px;"></i></button>
                        </a>

                        {{-- DOWNLOAD ATTACHMENTS --}}
                        <a
                                data-toggle="tooltip" title="Attachments(zip)"
                                id="{{'zip'.$count.'-'.$submittedForm->id}}">
                            <button type="button"
                                    class="btn btn-icon btn-info btn-active  btn-sm btn-sm  btn-sm"
                                    ><i class="icon fa-file-zip-o "
                                        aria-hidden="true"
                                        style="font-size: 15px;"></i></button>
                        </a>


                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=zip]").click(function (e) {
            var id = (this.id).split('-');
            var formID = id[1];
            var generateZipURL = "{{ $preLink.'meritpromotion/'}}" + formID + '/generateZip';
            var downloadZipURL = "{{ $preLink.'meritpromotion/'}}" + formID + '/downloadZip';
            $.get(generateZipURL, function (data) {
                window.location = "{{ ZIPLOCATION }}" + "/" + data;
            });
        });
    });
</script>