<h3>II. TEACHING PERFORMANCE/OUTPUT</h3>
<div class="page-break">
    <h4>A. PUBLISHED SCHOLARLY WORK </h4>

    <div>
        <h5>1. Authorship of a paper in a refereed publication
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published</th>
                <th>Title, Vol. No. of Journal/Publication</th>
                <th>Nature of Publication</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==1 && $creativework->subcategory==1)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td> {{$creativework->subsubcategory}} </td>
                        <td> {{$creativework->title}} </td>
                        <td> {{$creativework->author}} </td>
                        <td> {{$creativework->date_published}} </td>
                        <td> {{$creativework->bibliography}} </td>
                        <td> {{$creativework->nature_ofpublication}} </td>
                        <td> {{$creativework->other_details}} </td>
                        <td align="right"> {{$creativework->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 2. Presentation of a paper in an international conference</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date and Place of Presentation</th>
                <th>Title of Conference</th>
                <th>Other Details</th>

                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==1 && $creativework->subcategory==2)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->author}}</td>
                        <td>{{$creativework->date_ofpresentation.'/'.$creativework->place_ofpresentation}}</td>
                        <td>{{$creativework->title_ofconference}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 3. Presentation of a paper in a national conference</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date and Place of Presentation</th>
                <th>Title of Conference</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==1 && $creativework->subcategory==3)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->author}}</td>
                        <td>{{$creativework->date_ofpresentation.'/'.$creativework->place_ofpresentation}}</td>
                        <td>{{$creativework->title_ofconference}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 4. Presentation of a paper at the department, college or university</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date and Place of Presentation</th>
                <th>Title of Conference</th>
                <th>Other Details</th>

                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==1 && $creativework->subcategory==4)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->author}}</td>
                        <td>{{$creativework->date_ofpresentation.'/'.$creativework->place_ofpresentation}}</td>
                        <td>{{$creativework->title_ofconference}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>


    <h4>B. CREATIVE WORK </h4>

    <div>
        <h5>1. Literature
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date of Publication/Publisher/Nature of Publication</th>
                <th>Other Details</th>

                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==2 && $creativework->subcategory==1)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->author}}</td>
                        <td>{{$creativework->date_published.'/'.$creativework->publisher.'/'.$creativework->nature_ofpublication}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right"> {{$creativework->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 2. Fine Arts</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>

                <th>Date and Place of Publication/Exhibition</th>
                <th>Other Details</th>


                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==2 && $creativework->subcategory==2)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->date_ofpresentation.'/'.$creativework->place_ofpresentation}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 3. Music and Dance</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==2 && $creativework->subcategory==3)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 4. Architecture</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==2 && $creativework->subcategory==4)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 5. Theatre Arts</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==2 && $creativework->subcategory==5)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 6. Radio, Television and Related Media</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==2 && $creativework->subcategory==6)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 7. Film</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==2 && $creativework->subcategory==7)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 8. Scholarly Work in the Arts</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published</th>
                <th>Title, Vol. No. of Journal/Publication</th>
                <th>Nature of Publication</th>
                <th>Other Details(s)</th>
                <th style="text-align:right"> Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($creativeworks as $creativework)
                @if($creativework->category==2 && $creativework->subcategory==8)
                    <tr data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                        <td>{{$creativework->subsubcategory}}</td>
                        <td>{{$creativework->title}}</td>
                        <td>{{$creativework->author}}</td>
                        <td>{{$creativework->date_published}}</td>
                        <td>{{$creativework->bibliography}}</td>
                        <td>{{$creativework->nature_ofpublication}}</td>
                        <td>{{$creativework->other_details}}</td>
                        <td align="right">{{$creativework->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>
</div>

