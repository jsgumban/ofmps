<div class="tab-pane" id="category-5" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">
        <h4 id="ASDASD">
            <div class="btn-group pull-right">
                @if(empty($isAllowedToAddNewDate))
                    <span data-toggle="tooltip" data-target="#id" title="">
                                <button type="button" data-target="#exampleModalWarning" data-toggle="modal"

                                        class="btn social-facebook">

                                    <span><i class="icon wb-plus-circle" aria-hidden="true"></i> Add New</span>
                                </button>
                            </span>

                    {{-- NOT ALLOWED TO ADD NEW DATE --}}
                @else
                    <a href="javascript: void(0)" data-toggle="tooltip"
                       title="Not allowed">
                        <button type="button" data-target="#exampleModalWarning" data-toggle="tooltip"

                                class="btn social-facebook" disabled="true">

                            <span><i class="icon wb-plus-circle" aria-hidden="true"></i> Add New</span>
                        </button>
                    </a>

                    @endif
                    </span>
            </div>
            <center><b>CALL FOR PROMOTION</b><br></center>

        </h4>
        @if(session('activeTab')==5)
            @if(!empty(session('notification')))
                <br>
                <?php $notif = session('notification');?>
                @if(strpos($notif,'SUCCESS')===false)
                    <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @else
                    <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @endif
            @endif
        @endif

        <hr>

        <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Start</th>
                <th>End</th>
                <th>Status</th>
                <th>Form count</th>

                <th>
                    <center> Action</center>
                </th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th>#</th>
                <th>Start</th>
                <th>End</th>
                <th>Status</th>
                <th>Form count</th>

                <th>
                    <center> Action</center>
                </th>
            </tr>
            </tfoot>


            <tbody>
            <?php $count = 1; ?>
            @foreach ($mpromotionDates as $mpromotionDate)
                <tr>
                    <td> <?php echo $count++; ?>  </td>
                    {{-- <td> {{ $mpromotionDate->mpromotion_date }}</td> --}}
                    <td> {{ date('F d, Y', strtotime($mpromotionDate->mpromotion_date_start )) }}</td>
                    <td> {{ date('F d, Y', strtotime($mpromotionDate->mpromotion_date_end )) }}</td>
                    <td>
                        @if($mpromotionDate->mpromotion_status == 0)
                            <span class="label label-success">ON-GOING</span>
                        @elseif($mpromotionDate->mpromotion_status == 1)
                            <span class="label label-danger ">FINISHED</span>
                        @elseif($mpromotionDate->mpromotion_status == 2)
                            <span class="label label-warning ">NOT YET STARTED</span>
                        @endif
                    </td>
                    <td><span class="label label-dark">{{ count($forms[$count-2])}}</span></td>
                    <td>
                        <center>
                            {{-- UPDATE DISABLE --}}
                            @if($mpromotionDate->mpromotion_status==1)
                                <a href="javascript: void(0)" data-toggle="tooltip"
                                   title="Not allowed to update">
                                    <button type="button" class="btn btn-icon btn-info btn-sm "
                                            disabled="true"><i
                                                class="icon wb-refresh" aria-hidden="true"
                                                style="font-size: 15px;"></i></button>
                                </a>
                                {{-- UPDATE ENABLE --}}
                            @else
                                <button type="button" class="btn btn-icon btn-info btn-sm"
                                        data-target="#exampleModalWarning1" data-toggle="modal"><i
                                            class="icon wb-refresh" aria-hidden="true"
                                            style="font-size: 15px;" data-toggle="tooltip"
                                            title="Update"></i></button>


                            @endif

                            {{-- DELETE --}}
                            @if($mpromotionDate->mpromotion_status==1)
                                <a href="javascript: void(0)"
                                   data-toggle="tooltip" title="Not allowed to delete">
                                    <button type="button"
                                            class="btn btn-icon btn-danger  btn-sm btn-sm  btn-sm"
                                            disabled=""><i
                                                class="icon wb-trash" aria-hidden="true"
                                                style="font-size: 15px;"></i></button>
                                </a>
                            @else
                                <a href='{!! $preLink.'meritpromotion/'.$mpromotionDate->id.'/DeleteDate' !!}'
                                   onclick
                                   ="
                                        if (! confirm('Are you sure do you want remove this entry?')) { return false; }
                                        " data-toggle="tooltip" title="Delete">
                                    <button type="button"
                                            class="btn btn-icon btn-danger  btn-sm btn-sm  btn-sm"><i
                                                class="icon wb-trash" aria-hidden="true"
                                                style="font-size: 15px;"></i></button>
                                </a>
                            @endif
                        </center>
                    </td>

                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>

@if(!empty($mpromotionDate))
    @include('admin.UniversitySettings-MeritPromotion-Update')
@endif