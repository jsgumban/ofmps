{{-- FORM CATEGORY 4 MODAL --}}
<div class="modal fade modal-danger  example-modal-lg" id="UpdateFC4-modal-1" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                {{-- RIBBONS --}}
                @if (Request::is('*/ReviseForm') || Request::is('*/EvaluateForm') || Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-rejected">
                        <span class="ribbon-inner">REJECTED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-revise">
                        <span class="ribbon-inner">NEEDS REVISION</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-notyet">
                        <span class="ribbon-inner">NOT YET EVALUATED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-accept">
                        <span class="ribbon-inner">ACCEPTED</span>
                    </div>
                @endif

                <h4 class="modal-title">A. PROFESSIONAL GROWTH </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="UpdateFC4-SubSubCat-1" disabled>
                        <option value=1>01. Workshop/Training/Conference Organizer or Chair
                        </option>
                        <option value=2>02. Authorship of a reference book in one's field of discipline</option>
                        <option value=3>03. Authorship of a college-level textbook/laboratory/course manual</option>
                        <option value=4>04. Authorship of High School and Elementary Manual/textbook</option>

                        <option value=5>05. Popular presentations and popularized lectures on topics within the
                            discipline
                        </option>
                        <option value=6>06. Resource person in print or broadcast media presentations</option>
                        <option value=7>07. Trainor/sports official within area of specialization</option>
                        <option value=8>08. Membership in technical committee</option>
                        <option value=9>09. Advisorship to Professional Organization</option>


                        <option value=10>10. Active Participation in outreach program related to own specialization
                        </option>
                        <option value=11>11. Resource person in meetings/symposiums/extension activities related to
                            issues within your discipline
                        </option>
                        <option value=12>12. Attendance in International/Regional meeting/conference/training
                            course/workshop
                        </option>
                        <option value=13>13. Attendance in National/University meetings/conference/training course
                            workshop
                        </option>
                        <option value=14>14. Highly specialized training related to discipline with contribution to the
                            college (duration of at least 2 weeks)
                        </option>
                        <option value=15>15. Patents</option>

                        <option value=16>16. Citations (indicate which journal)</option>
                        <option value=17>17. Awards</option>
                        <option value=18>18. Membership in Editorial Board</option>
                        <option value=19>19. Referee to a publication</option>
                        <option value=20>20. Research Fellowship/Visiting Professor Appointment in a reputable
                            university (with MOA)
                        </option>
                        <option value=21>21. Other related services to the community</option>
                    </select>
                </div>

                {{-- CATEGORY 1--}}
                <div id="UpdateFC4-divID-1-1" class="UpdateFC4-divClass-1">
                    <form action="#" method="POST" id="UpdateFC4-form-1-1"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-1">
                                <option value="Chair (International)">1.1. Chair (International)
                                </option>
                                <option value="Member (International)">1.2. Member (International)
                                </option>
                                <option value="Chair (National)">1.3. Chair (National)
                                </option>
                                <option value="Member (National)">1.4. Member (National)
                                </option>
                            </select>
                        </div>

                        {{-- Name of Workshop/Training/Conference --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Workshop/Training/Conference</label>
                            <input type="text" class="form-control" name="name_ofevent"
                                   placeholder="Name of Workshop/Training/Conference" required
                                   id="UpdateFC4-name_ofevent-1-1">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> Chair (International)</td>
                                            <td>4 each</td>

                                        </tr>
                                        <tr>
                                            <td>Member (International)</td>
                                            <td>2 each</td>
                                        </tr>

                                        <tr>
                                            <td>Chair (National)</td>
                                            <td>2.5 each</td>
                                        </tr>

                                        <tr>
                                            <td>Member (National)</td>
                                            <td>1 each</td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required id="UpdateFC4-points-1-1">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-1">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-1">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-1" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-1" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-1" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">
                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-1">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-1"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-1">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-1"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>


                {{-- CATEGORY 2--}}
                <div id="UpdateFC4-divID-1-2" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-2"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}

                        {{-- Title of the Reference Book  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of the Reference Book </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title of the Reference Book" required id="UpdateFC4-title-1-2">
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required id="UpdateFC4-author-1-2">
                        </div>

                        {{-- Date Published  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published/Lectured </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC4-date_published-1-2">
                        </div>

                        {{--Title, Vol. No. of Journal/Publication   --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication" required
                                   id="UpdateFC4-bibliography-1-2">
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required
                                   id="UpdateFC4-nature_ofpublication-1-2">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of a reference book in one's field of discipline</td>
                                            <td>4 each</td>

                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required id="UpdateFC4-points-1-2">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-2">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-2">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-2" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-2" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-2" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-2">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-2"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-2">Revise
                                    </button>
                                </div>
                            </div>
                        @else

                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-2"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 3--}}
                <div id="UpdateFC4-divID-1-3" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-3"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 3) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-3">
                                <option value="Textbook">3.1. Textbook
                                </option>
                                <option value="Manual">3.2. Manual
                                </option>

                            </select>
                        </div>

                        {{-- Title  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC4-title-1-3">
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required id="UpdateFC4-author-1-3">
                        </div>

                        {{-- Date Published  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC4-date_published-1-3">
                        </div>

                        {{--Title, Vol. No. of Journal/Publication   --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication" required
                                   id="UpdateFC4-bibliography-1-3">
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required
                                   id="UpdateFC4-nature_ofpublication-1-3">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Textbook  </td>
                                            <td>5 each</td>
                                        </tr>

                                        <tr>
                                            <td>Manual   </td>
                                            <td>4 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC4-points-1-3">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-3">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-3">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-3" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-3" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-3" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-3">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-3"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-3">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-3"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 4--}}
                <div id="UpdateFC4-divID-1-4" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-4"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 4) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-4">
                                <option value="Textbook">4.1. Textbook
                                </option>
                                <option value="Manual">4.2. Manual
                                </option>
                            </select>
                        </div>

                        {{-- Title  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC4-title-1-4">
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required id="UpdateFC4-author-1-4">
                        </div>

                        {{-- Date Published  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC4-date_published-1-4">
                        </div>

                        {{--Title, Vol. No. of Journal/Publication   --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication" required
                                   id="UpdateFC4-bibliography-1-4">
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required
                                   id="UpdateFC4-nature_ofpublication-1-4">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Textbook  </td>
                                            <td>5 each</td>
                                        </tr>

                                        <tr>
                                            <td>Manual   </td>
                                            <td>4 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC4-points-1-4">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-4">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-4">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-4" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-4" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-4" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-4">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-4"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-4">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-4"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 5 --}}
                <div id="UpdateFC4-divID-1-5" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-5"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 5) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-5">
                                <option value="Authorship of articles">5.1. Authorship of articles
                                </option>
                                <option value="Popular lectures within the discipline">5.2. Popular lectures within the
                                    discipline
                                </option>

                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC4-title-1-5">
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required id="UpdateFC4-author-1-5">
                        </div>

                        {{-- Date Published  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published/Lectured </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC4-date_published-1-5">
                        </div>

                        {{--Name of Publication/Conference/Seminar   --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of
                                Publication/Conference/Seminar </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Name of Publication/Conference/Seminar" required
                                   id="UpdateFC4-bibliography-1-5">
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required
                                   id="UpdateFC4-nature_ofpublication-1-5">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of articles  </td>
                                            <td>1 each</td>
                                        </tr>

                                        <tr>
                                            <td>Popular lectures within the discipline   </td>
                                            <td>1 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required id="UpdateFC4-points-1-5">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-5">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-5">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-5" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-5" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-5" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-5">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-5"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-5">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-5"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 6 --}}
                <div id="UpdateFC4-divID-1-6" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-6"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 6) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-6">
                                <option value="Column writer for national publication">6.1. Column writer for national
                                    publication
                                </option>
                                <option value="Column Writer for local publication">6.2. Column Writer for local
                                    publication
                                </option>

                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC4-title-1-6">
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required id="UpdateFC4-author-1-6">
                        </div>

                        {{-- Date of Presentation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Presentation </label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Presentation" required id="UpdateFC4-date_ofpresentation-1-6">
                        </div>

                        {{-- Place of Presentation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Presentation</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Presentation" required id="UpdateFC4-place_ofpresentation-1-6">
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Presentation </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required
                                   id="UpdateFC4-nature_ofpublication-1-6">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Column writer for national publication</td>
                                            <td>4 max/yr</td>
                                        </tr>

                                        <tr>
                                            <td>Column writer for local publication</td>
                                            <td>2 max/yr</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required id="UpdateFC4-points-1-6">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-6">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-6">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-6" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-6" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-6" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-6">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-6"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-6">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-6"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 7 --}}
                <div id="UpdateFC4-divID-1-7" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-7"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 7) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-7">
                                <option value="International">7.1. International
                                </option>
                                <option value="National/Local">7.2. National/Local
                                </option>

                            </select>
                        </div>

                        {{-- Name of Training  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Training </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Training" required id="UpdateFC4-title-1-7">
                        </div>

                        {{-- Date of Training  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Training </label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Training" required id="UpdateFC4-date_ofpresentation-1-7">
                        </div>

                        {{-- Place of Training  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Training</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Training" required id="UpdateFC4-place_ofpresentation-1-7">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>International</td>
                                            <td>4 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/Local</td>
                                            <td>2 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required id="UpdateFC4-points-1-7">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-7">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-7">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-7" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-7" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-7" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-7">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-7"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-7">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-7"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 8 --}}
                <div id="UpdateFC4-divID-1-8" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-8"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 8) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-8">
                                <option value="International/Regional">8.1. International/Regional
                                </option>
                                <option value="National/University (Local)">8.2. National/University (Local)
                                </option>

                            </select>
                        </div>

                        {{-- Name of Committee  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Committee </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Committee" required id="UpdateFC4-title-1-8">
                        </div>

                        {{-- Date of Date of Appointment  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Appointment </label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Date of Appointment" required
                                   id="UpdateFC4-date_ofpresentation-1-8">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> International/Regional</td>
                                            <td>2.5 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/University (Local)</td>
                                            <td>2 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2.5" required id="UpdateFC4-points-1-8">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-8">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-8">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-8" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-8" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-8" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-8">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-8"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-8">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-8"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 9 --}}
                <div id="UpdateFC4-divID-1-9" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-9"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 9) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-9">
                                <option value="International/Regional">9.1. International/Regional
                                </option>
                                <option value="National/University (Local)">9.2. National/University (Local)
                                </option>

                            </select>
                        </div>

                        {{-- Name of Organization  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Organization </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Organization" required id="UpdateFC4-title-1-9">
                        </div>

                        {{-- Date --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date" required id="UpdateFC4-date_ofpresentation-1-9">
                        </div>

                        {{-- Place --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place </label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place" required id="UpdateFC4-place_ofpresentation-1-9">
                        </div>

                        {{-- Nature of Organization --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Organization </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Organization" required
                                   id="UpdateFC4-nature_ofpublication-1-9">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> International/Regional</td>
                                            <td>2 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/University (Local)</td>
                                            <td>1 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2" required id="UpdateFC4-points-1-9">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-9">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-9">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-9" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-9" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-9" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-9">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-9"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-9">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-9"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 10 --}}
                <div id="UpdateFC4-divID-1-10" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-10"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 10) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-10">
                                <option value="International/Regional">10.1. International/Regional
                                </option>
                                <option value="National/University (Local)">10.2. National/University (Local)
                                </option>

                            </select>
                        </div>

                        {{-- Name of Program  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Program </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Program" required id="UpdateFC4-title-1-10">
                        </div>

                        {{-- Participation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Participation
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. member etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="participation"
                                   placeholder="Participation" required id="UpdateFC4-participation-1-10">
                        </div>

                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required id="UpdateFC4-date_ofpresentation-1-10">
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required id="UpdateFC4-place_ofpresentation-1-10">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> International/Regional</td>
                                            <td>2 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/University (Local)</td>
                                            <td>1 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2" required id="UpdateFC4-points-1-10">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-10">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-10">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-10" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-10" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-10" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-10">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-10"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-10">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-10"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 11 --}}
                <div id="UpdateFC4-divID-1-11" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-11"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 11) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-11">
                                <option value="International/Regional">11.1. International/Regional
                                </option>
                                <option value="National/University (Local)">11.2. National/University (Local)
                                </option>

                            </select>
                        </div>

                        {{-- Name of Activity  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Activity </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Activity" required id="UpdateFC4-title-1-11">
                        </div>

                        {{-- Participation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Participation
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. member etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="participation"
                                   placeholder="Participation" required id="UpdateFC4-participation-1-11">
                        </div>

                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required id="UpdateFC4-date_ofpresentation-1-11">
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required id="UpdateFC4-place_ofpresentation-1-11">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> International/Regional</td>
                                            <td>2 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/University (Local)</td>
                                            <td>1 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2" required id="UpdateFC4-points-1-11">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-11">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-11">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-11" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-11" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-11" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-11">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-11"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-11">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-11"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 12 --}}
                <div id="UpdateFC4-divID-1-12" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-12"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 12) !!}

                        {{-- Name of Conference  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Conference </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Conference" required id="UpdateFC4-title-1-12">
                        </div>


                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required id="UpdateFC4-date_ofpresentation-1-12">
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required id="UpdateFC4-place_ofpresentation-1-12">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Attendance in International/Regional meetings/conference/workshop </td>
                                            <td>1 each</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required id="UpdateFC4-points-1-12">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-12">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-12">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-12" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-12" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-12" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-12">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-12"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-12">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-12"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 13 --}}
                <div id="UpdateFC4-divID-1-13" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-13"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 13) !!}

                        {{-- Name of Conference  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Conference </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Conference" required id="UpdateFC4-title-1-13">
                        </div>


                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required id="UpdateFC4-date_ofpresentation-1-13">
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required id="UpdateFC4-place_ofpresentation-1-13">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Attendance in International/Regional meetings/conference/workshop </td>
                                            <td>1 each</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required id="UpdateFC4-points-1-13">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-13">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-13">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-13" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-13" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-13" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-13">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-13"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-13">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-13"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 14 --}}
                <div id="UpdateFC4-divID-1-14" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-14"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 14) !!}

                        {{-- Name of Conference  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Conference </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Conference" required id="UpdateFC4-title-1-14">
                        </div>


                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required id="UpdateFC4-date_ofpresentation-1-14">
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required id="UpdateFC4-place_ofpresentation-1-14">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Highly specialized training related to discipline with contribution to the college (duration of at least 2 weeks)</td>
                                            <td>1 each</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required id="UpdateFC4-points-1-14">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-14">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-14">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-14" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-14" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-14" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-14">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-14"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-14">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-14"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- CATEGORY 15 --}}
                <div id="UpdateFC4-divID-1-15" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-15"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 15) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-15">
                                <option value="Patents approved">15.1. Patents approved
                                </option>
                                <option value="Patents filed and stamped at the IPO (National) or equivalent office in foreign countries">
                                    15.2. Patents filed and stamped at the IPO (National) or equivalent office in
                                    foreign countries
                                </option>


                            </select>
                        </div>

                        {{-- Name of Pantent  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Pantent </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Pantent" required id="UpdateFC4-title-1-15">
                        </div>


                        {{-- Nature of Patent --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Patent</label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Patent" required id="UpdateFC4-nature_ofpublication-1-15">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Patents approved </td>
                                            <td>90 each</td>
                                        </tr>
                                        <tr>
                                            <td>Patents filed and stamped at the IPO (National) or equivalent office in foreign countries </td>
                                            <td>45 each</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="90" required id="UpdateFC4-points-1-15">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-15">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-15">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-15" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-15" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-15" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-15">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-15"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-15">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-15"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 16 --}}
                <div id="UpdateFC4-divID-1-16" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-16"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 16) !!}

                        {{-- Name of Citation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Citation </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Citation" required id="UpdateFC4-title-1-16">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Citations (indicate which article, journal/book)</td>
                                            <td>5 per citation</td>
                                        </tr>
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC4-points-1-16">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-16">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-16">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-16" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-16" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-16" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-16">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-16"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-16">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-16"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 17 --}}
                <div id="UpdateFC4-divID-1-17" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-17"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 17) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-17">
                                <option value="Honorific Awards: International Recognition">17.1.1 Honorific Awards:
                                    International Recognition
                                </option>
                                <option value="Honorific Awards: National Recognition">17.1.2 Honorific Awards: National
                                    Recognition
                                </option>
                                <option value="Honorific Awards: Local/Regional Recognition">17.1.3 Honorific Awards:
                                    Local/Regional Recognition
                                </option>
                                <option value="Competitive Awards: International Recognition">17.2.1 Competitive Awards:
                                    International Recognition
                                </option>
                                <option value="Competitive Awards: National Recognition">17.2.2 Competitive Awards:
                                    National Recognition
                                </option>
                                <option value="Competitive Awards: Local/Regional Recognition">17.2.3 Competitive
                                    Awards: Local/Regional Recognition
                                </option>


                            </select>
                        </div>

                        {{-- Name of Award  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Name of Award </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Award" required id="UpdateFC4-title-1-17">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr  class='success'>
                                            <td><b>HONORIFIC AWARDS</b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>International recognition(i.e., Caldicott Award, Ramon Magsaysay, SEA Write Awards) </td>
                                            <td>90pts</td>
                                        </tr>

                                        <tr>
                                            <td>National Recognition (i.e.,National Artist, National Scientist,/TOYM, Gawad Palanca, NAST Academician, etc.) </td>
                                            <td>60pts</td>
                                        </tr>

                                        <tr>
                                            <td>Local/Regional (i.e., Datu Bago Award, etc.) </td>
                                            <td>30pts</td>
                                        </tr>


                                        <tr class='success'>
                                            <td><b>COMPETITIVE AWARDS</b></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td>International award </td>
                                            <td>60pts</td>
                                        </tr>

                                        <tr>
                                            <td>National (i.e., Gawad Urian, Philippine Free Press Literary Contest, Metrobank Outstanding Teacher Award, Gawad Saka, CHED REPUBLICA, Best Research Papers in National Conferences) </td>
                                            <td>30pts</td>
                                        </tr>


                                        <tr>
                                            <td>Local/Regional (i.e., Gawad Chancellor, Best Oral/Poster e.g., SMARRDEC, etc.) </td>
                                            <td>15pts</td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="90" required id="UpdateFC4-points-1-17">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-17">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-17">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-17" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-17" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-17" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-17">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-17"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-17">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-17"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 18 --}}
                <div id="UpdateFC4-divID-1-18" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-18"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 18) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Refereed Publication (journal,
                                proceedings)/other print media</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-18">
                                <option value="Editor-in-Chief of an ISI-accredited scientific publication">18.1.1.
                                    Editor-in-Chief of an ISI-accredited scientific publication
                                </option>
                                <option value="Editor-in-Chief of creative print media (international)">18.1.2.
                                    Editor-in-Chief of creative print media (international)
                                </option>
                                <option value="Editor-in-Chief of non-ISI publication">18.1.3. Editor-in-Chief of
                                    non-ISI
                                    publication
                                </option>
                                <option value="Editor-in-chief of Creative Print Media (National)">18.1.4.
                                    Editor-in-chief of Creative Print Media (National)
                                </option>
                                <option value="Editorial Board Member">18.1.5 Editorial Board Member
                                </option>


                            </select>
                        </div>

                        {{-- Name of Publication  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Name of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Publication" required id="UpdateFC4-title-1-18">
                        </div>

                        {{-- Nature of Publication  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication" required
                                   id="UpdateFC4-nature_ofpublication-1-18">
                        </div>

                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered </label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required id="UpdateFC4-period_covered-1-18">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                       
                                        <tr>
                                            <td>Editor-in-Chief of an ISI-accredited publication</td>
                                            <td>5pts per year</td>
                                        </tr>

                                        <tr>
                                            <td>Editor-in-Chief of Creative Print Media (international) </td>
                                            <td>5pts per year</td>
                                        </tr>

                                         <tr>
                                            <td>Editor-in-Chief of non-ISI accredited publication  </td>
                                            <td>4pts per year</td>
                                        </tr>

                                        <tr>
                                            <td>Editor-in-Chief of Creative Print Media (national)   </td>
                                            <td>2pts per year</td>
                                        </tr>

                                        <tr>
                                            <td>Editorial Board Member    </td>
                                            <td>1pt per year</td>
                                        </tr>                                         

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC4-points-1-18">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-18">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-18">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-18" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-18" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-18" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-18">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-18"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-18">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-18"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 19 --}}
                <div id="UpdateFC4-divID-1-19" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-19"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 19) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory" id="UpdateFC4-subcategory-1-19">
                                <option value="International: Book">19.1.1 International: Book
                                </option>
                                <option value="International: Article">19.1.2 International: Article
                                </option>
                                <option value="National: Book">19.2.1 National: Book
                                </option>
                                <option value="National: Article">19.2.2 National: Article
                                </option>
                            </select>
                        </div>

                        {{-- Title of Book/Artice  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Title of Book/Artice </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title of Book/Artice" required id="UpdateFC4-title-1-19">
                        </div>

                        {{-- Publisher  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher/Journal Vol./No. </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Publisher/Journal Vol./No." id="UpdateFC4-bibliography-1-19">
                        </div>

                        {{-- Date of Publication  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Publication </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date of Publication" required id="UpdateFC4-date_published-1-19">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr  class='success'>
                                            <td><b>INTERNATIONAL </b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Book   </td>
                                            <td>4pts per book  </td>
                                        </tr>
                                        <tr>
                                            <td>Article   </td>
                                            <td>3pts per article  </td>
                                        </tr>

                                        

                                        <tr class='success'>
                                            <td><b>NATIONAL </b></td>
                                            <td></td>
                                        </tr>

                                       <tr>
                                            <td>Book   </td>
                                            <td>3pts per book  </td>
                                        </tr>
                                        <tr>
                                            <td>Article   </td>
                                            <td>2pts per article  </td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required id="UpdateFC4-points-1-19">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-19">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-19">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-19" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-19" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-19" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-19">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-19"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-19">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-19"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 20 --}}
                <div id="UpdateFC4-divID-1-20" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-20"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 20) !!}



                        {{-- Name of University  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Name of University </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of University" required id="UpdateFC4-title-1-20">
                        </div>

                        {{-- Duration of Appointment  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Duration of Appointment </label>
                            <input type="text" class="form-control" name="duration_ofappointment"
                                   placeholder="Duration of Appointment" required
                                   id="UpdateFC4-duration_ofappointment-1-20">
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <tr>
                                            <td>Research Fellowship /Visiting Professor Appointment in a reputable university (with MOA)    </td>
                                            <td>5pts per appt  </td>
                                        </tr>
                                       

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC4-points-1-20">
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details" id="UpdateFC4-other_details-1-20">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-20">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-20" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-20" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-20" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-20">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-20"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-20">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-20"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

                {{-- CATEGORY 21 --}}
                <div id="UpdateFC4-divID-1-21" class="UpdateFC4-divClass-1" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC4-form-1-21"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 21) !!}

                        {{-- Other Services  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Services</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Other Services" required id="UpdateFC4-title-1-21">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <tr>
                                            <td>Other related services to the community (e.g., reviewer of research/creative work development of discipline or tertiary education/software developed for other agencies, etc.)   </td>
                                            <td>1pt per review</td>
                                        </tr>
                                       

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required id="UpdateFC4-points-1-21">
                        </div>


                        {{--  Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti"> Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder=" Details" id="UpdateFC4-other_details-1-21">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC4-proofs-1-21">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c4radio1-1-21" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c4radio2-1-21" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c4radio3-1-21" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c4BrowseDiv1">
                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-danger">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC4-reviseDiv-1-21">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC4-delete-1-21"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC4-revise-1-21">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC4-delete-1-21"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        @endif

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

@if(Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm') )
    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $("[id^=UpdateFC4-]").attr('disabled', true);
        $("[id^=UpdateFC4-proofs-]").css('display', "none");
        $(".btn").css('display', "none");
        @if(Auth::user()->position==4)
            $("[id^=chancellorEvaluation-]").css('display', "block");
        @endif
    </script>
@endif

@if(Request::is('*/EvaluateForm'))
    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $("[id^=UpdateFC4-]").attr('disabled', true);
        $("[id^=UpdateFC4-proofs-]").css('display', "none");
    </script>

    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("input[name*='evaluateAction']").click(function (e) {
                // accept
                if (this.value == 1) {
                    $("[id^=UpdateFC4-points-]").attr('disabled', false);
                    $("[id^=UpdateFC4-points-]").css('background-color', '#FFEFD5');
                    $("[id^=UpdateFC4-points-]").css('color', 'black');
                }

                // reject
                else if (this.value == 2 || this.value == 3) {
                    points = $("[id^=labelauty]").attr("data-points");
                    $("[id^=UpdateFC4-points]").val(points);
                    $("[id^=UpdateFC4-points]").attr('disabled', true);
                    $("[id^=UpdateFC4-points-]").css('background-color', '');
                    $("[id^=UpdateFC4-points-]").css('color', '');
                }
            });
        });
    </script>
@endif

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=c4radio]").click(function (e) {
            if (this.value == 3) {
                $("[id^=c4BrowseDiv]").css('display', 'none');
            } else {
                $("[id^=c4BrowseDiv]").css('display', 'block');
            }
        });
    });
</script>


<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=inputFileUpdateCat4]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }

            }
        });
    });
</script>



