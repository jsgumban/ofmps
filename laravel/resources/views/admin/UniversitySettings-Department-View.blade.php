<div class="tab-pane" id="category-3" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">
        <h4 id="ASDASD">
            <div class="btn-group pull-right">
                @if(empty($colleges))

                    <a href="javascript: void(0)" data-toggle="tooltip"
                       title="Requires College" role="button" class="btn btn-animate social-facebook dropdown-toggle"
                       disabled>
                        <span><i class="icon wb-plus-circle" aria-hidden="true"></i> Create</span>
                    </a>
                @else
                    <span data-toggle="tooltip" data-target="#id"
                          title="New Department">
                        <button type="button" data-target="#addNewDepartment"
                                data-toggle="modal"
                                class="btn btn-animate social-facebook dropdown-toggle"
                                id="exampleHoverDropdown3" data-toggle="dropdown">
                            <span><i class="icon wb-plus-circle" aria-hidden="true"></i> Create</span>
                        </button>
                        </span>
                @endif
            </div>
            <center><b>LIST OF ALL DEPARTMENTS</b>&nbsp;
                <a href="javascript:void(0)" data-plugin="webuiPopover"
                   href="javascript:void(0)" data-plugin="webuiPopover"
                   data-placement="right" data-animation="pop" data-target="webuiPopover" data-title="More Information"
                   data-target="webuiPopover9"
                   data-content="
                                <small> <table class='table'>
                                <tbody>

                                <tr>
                                    <td>Total no. of Departments</td>
                                    <td>{!! count($departments) !!}</td>
                                </tr>

                                <tr>
                                    <td>Total no. of Faculty Members</td>
                                    <td>{!! count($allFaculty) !!} </td>
                                </tr>
    
                                </tbody>
                                </table> </small>">
                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                </a>

            </center>
        </h4>

        @if(session('activeTab')==3)
            {{-- notification --}}

            @if(!empty(session('notification')))
                <br>
                <?php $notif = session('notification');?>
                @if(strpos($notif,'SUCCESS')===false)
                    <div class="alert alert-alt alert-danger alert-dismissible"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @else
                    <div class="alert alert-alt alert-success alert-dismissible"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @endif
            @endif
        @endif
        <hr>
        @if(empty($departments))
            <br><br><br><br>
        @endif
        <?php $count = 1; ?>
        @foreach ($departments as $department)
            <?php $count++ ?>
            <div class="panel">
                <br>
                <dl class="dl-horizontal">
                    <h4>{{ $department->department_name }}</h4>
                    <dt>Initials</dt>
                    <dd>{{ $department->department_initial }}</dd>


                    <dt>Dept. Chairperson</dt>
                    @if(!empty($deptchairs[$count-2]->last_name))
                        <dd>
                            <em>{{$deptchairs[$count-2]->last_name.', '.$deptchairs[$count-2]->first_name.' '.$deptchairs[$count-2]->middle_name[0].'.' }}</em>
                        </dd>
                    @else
                        <dd><em>None</em></dd>
                    @endif


                    <dt>No. of Faculty Members</dt>
                    <dd>{{ count($countFacultyPerDepartment[$count-2]) }}</dd>

                    <dt>College</dt>
                    <dd>{{ $department->college_belongs }}</dd>


                    <br>
                    <dt>Update Department</dt>
                    <dd>
                        <a id="{{'UpdateDepartment-'.$department->id}}" data-target="#updateDepartment"
                           data-toggle="modal" style="color:green" role="button">Click
                            Here</a>
                    </dd>

                    <dt>More Information</dt>
                    <dd>
                        <a href='{!! FOLDERNAME.'/admin/university/'.$department->id.'/ViewMoreInfoDepartment' !!}'>Click
                            Here</a>
                    </dd>

                    <dt>Remove Department</dt>
                    <dd>
                        <a href='{!! $preLink.'university/'.$department->id.'/RemoveDepartment' !!}'
                           onclick="if (! confirm('Are you sure do you want remove this Department?')) { return false; }
                            " style="color:#e71e1e">Click Here
                        </a>
                    </dd>
                </dl>
                <br>

            </div>
        @endforeach
        <hr>

        {{-- create new department button--}}
        <div class="btn-group pull-right">


        </div>
    </div>
</div>
@include('admin.UniversitySettings-Department-Update')
@include('admin.UniversitySettings-Department-UpdateScript')