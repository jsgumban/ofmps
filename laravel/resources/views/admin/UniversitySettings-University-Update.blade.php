<div class="modal fade modal-info" id="updateUniversity" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        {{-- form --}}
        <form action={!! $preLink.'university/SaveUpdateUniversity' !!} method="POST">
            {!! csrf_field() !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">UPDATE CHANCHELLOR/VICE CHANCELLOR</h4>
                </div>
                <div class="modal-body">
                    {{-- update chancellor/vice chancellor --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Position</label>
                        <select class="form-control" id="addAs" name="addAs" onchange="changeOption()">
                            <option value=1> Chancellor</option>
                            <option value=2> Vice Chancellor</option>
                        </select>
                    </div>
                    {{-- faculty members --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Faculty Member</label>
                        <select class="form-control" id="candidate" name="candidate">
                            <option value=""> None</option>
                            @foreach($candidateForCVCs as $candidateForCVC)
                                <option value="{{ $candidateForCVC->id }}">
                                    {!! $candidateForCVC->last_name.', '.$candidateForCVC->first_name.' '.$candidateForCVC->middle_name[0].'.' !!}
                                </option>
                            @endforeach

                            @if(!empty($chancellor))
                                <script>
                                    document.getElementById("candidate").value = "{!! $chancellor[0]->id !!}";
                                </script>
                            @endif

                            @if(!empty($chancellor) && !empty($vchancellor))
                                <script>
                                    function changeOption() {
                                        if (document.getElementById("addAs").value == 1) {
                                            var x = document.getElementById("candidate").value = "{!! $chancellor[0]->id !!}";
                                        }

                                        else if (document.getElementById("addAs").value == 2) {
                                            var x = document.getElementById("candidate").value = "{!! $vchancellor[0]->id !!}";
                                        }
                                    }
                                </script>
                            @endif

                            @if(!empty($chancellor) && empty($vchancellor))
                                <script>
                                    function changeOption() {
                                        if (document.getElementById("addAs").value == 1) {
                                            var x = document.getElementById("candidate").value = "{!! $chancellor[0]->id !!}";
                                        }

                                        else if (document.getElementById("addAs").value == 2) {
                                            var x = document.getElementById("candidate").value = "";
                                        }
                                    }
                                </script>
                            @endif

                            @if(empty($chancellor) && !empty($vchancellor))
                                <script>
                                    function changeOption() {
                                        if (document.getElementById("addAs").value == 1) {
                                            var x = document.getElementById("candidate").value = "";
                                        }

                                        else if (document.getElementById("addAs").value == 2) {
                                            var x = document.getElementById("candidate").value = "{!! $vchancellor[0]->id !!}";
                                        }
                                    }
                                </script>
                            @endif

                            @if(empty($chancellor) && empty($vchancellor))
                                <script>
                                    function changeOption() {
                                        if (document.getElementById("addAs").value == 1) {
                                            var x = document.getElementById("candidate").value = "";
                                        }

                                        else if (document.getElementById("addAs").value == 2) {
                                            var x = document.getElementById("candidate").value = "";
                                        }
                                    }
                                </script>
                            @endif
                        </select>
                    </div>
                </div>
                {{-- buttons --}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>