{{-- ACTIVE TAB --}}
<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
@if(!empty(session('activeTab')))
    <script type="text/javascript">
        document.getElementById("category-1").setAttribute("class", "tab-pane");
        document.getElementById("category-2").setAttribute("class", "tab-pane");
        document.getElementById("category-3").setAttribute("class", "tab-pane");
        document.getElementById("category-4").setAttribute("class", "tab-pane");
        document.getElementById("category-" +{{session('activeTab')}}).setAttribute("class", "tab-pane active");
    </script>
@endif

{{-- NAVIGATION BAR: DISABLE ALL FUNTION IN THE NAVIGATION BAR --}}
@if (Request::is('*/MonitorForm') || Request::is('*/EvaluateForm') || Request::is('*/ReviseForm')  || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') ||  Request::is('*/CHANCELLORMonitorForm'))
    <script type="text/javascript">
        $(document).ready(function () {
            $("[id^=SubCatAddButton]").ready(function (e) {
                $("[id^=SubCatAddButton]").addClass('disabled');
                $("[id^=SubCatAddButton]").attr('data-target', '');
                $("[id^=SubCatAddLink]").attr('data-toggle', 'tooltip');
                $("[id^=SubCatAddLink]").attr('title', 'Not allowed');
            });
        });
    </script>
@endif

{{-- REMARKS --}}
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=form-remarks]").click(function (e) {
            e.preventDefault();
            // form-remarks-department
            var id = (this.id).split('-');
            var remarksLevel = id[2];
            var formID = id[3];
            var retrieveDataUrl = "{{ $preLink.'meritpromotion/' }}" + formID + '/RetrieveFormInfo';

            // alert(retrieveDataUrl);
            $.get(retrieveDataUrl, function (data) {
                if (remarksLevel == 'deptchair') {
                    $('#form-remarks-1').val(data.deptchair_remarks);
                    $('#remarks_points_deptchair').empty();
                    $('#remarks_points_deptchair').append(data.deptchair_points);

                    var attachmentNamesSpit = data.deptchair_remarks_attachments.split("+++");
                    $("#remarks_attachments_deptchair").empty();
                    for (var x = 0; x < attachmentNamesSpit.length; x++) {
                        if (attachmentNamesSpit[x].length != 0) {
                            var fileType = attachmentNamesSpit[x].substr(attachmentNamesSpit[x].lastIndexOf('.') + 1);
                            $("#remarks_attachments_deptchair").append("<a href=\"/uploads/" + attachmentNamesSpit[x] + "\" target=\"_blank\" title=\"" + fileType + "\"  >" +
                                    "<i class=\"icon wb-tag\" style=\"font-size: 18px\"></i>" + "</a> ");
                        }
                    }
                } else if (remarksLevel == 'dean') {
                    $('#form-remarks-2').val(data.dean_remarks);
                    $('#remarks_points_dean').empty();
                    $('#remarks_points_dean').append(data.dean_points);
                    var attachmentNamesSpit = data.dean_remarks_attachments.split("+++");
                    $("#remarks_attachments_dean").empty();
                    for (var x = 0; x < attachmentNamesSpit.length; x++) {
                        if (attachmentNamesSpit[x].length != 0) {
                            var fileType = attachmentNamesSpit[x].substr(attachmentNamesSpit[x].lastIndexOf('.') + 1);
                            $("#remarks_attachments_dean").append("<a href=\"/uploads/" + attachmentNamesSpit[x] + "\" target=\"_blank\" title=\"" + fileType + "\"  >" +
                                    "<i class=\"icon wb-tag\" style=\"font-size: 18px\"></i>" + "</a> ");
                        }
                    }
                } else if (remarksLevel == 'vchancellor') {
                    $('#form-remarks-3').val(data.vchancellor_remarks);
                    $('#remarks_points_vchancellor').empty();
                    $('#remarks_points_vchancellor').append(data.vchancellor_points);

                    $('#remarks_rank_vchancellor').empty();
                    $('#remarks_rank_vchancellor').append(data.initial_rank);

                    $('#remarks_step_vchancellor').empty();
                    $('#remarks_step_vchancellor').append(data.initial_step);
                    var attachmentNamesSpit = data.vchancellor_remarks_attachments.split("+++");
                    $("#remarks_attachments_vchancellor").empty();
                    for (var x = 0; x < attachmentNamesSpit.length; x++) {
                        if (attachmentNamesSpit[x].length != 0) {
                            var fileType = attachmentNamesSpit[x].substr(attachmentNamesSpit[x].lastIndexOf('.') + 1);
                            $("#remarks_attachments_vchancellor").append("<a href=\"/uploads/" + attachmentNamesSpit[x] + "\" target=\"_blank\" title=\"" + fileType + "\"  >" +
                                    "<i class=\"icon wb-tag\" style=\"font-size: 18px\"></i>" + "</a> ");
                        }
                    }
                } else if (remarksLevel == 'chancellor') {
                    $('#form-remarks-4').val(data.chancellor_remarks);
                    $('#remarks_points_chancellor').empty();
                    $('#remarks_points_chancellor').append(data.chancellor_points);

                    $('#remarks_rank_chancellor').empty();
                    $('#remarks_rank_chancellor').append(data.initial_rank);

                    $('#remarks_step_chancellor').empty();
                    $('#remarks_step_chancellor').append(data.initial_step);

                    var attachmentNamesSpit = data.chancellor_remarks_attachments.split("+++");
                    $("#remarks_attachments_chancellor").empty();


                    if (data.form_status == 6) {
                        $('#check-remarks-chancellor-rank').css('display', 'inline-block');
                        $('#check-remarks-chancellor-step').css('display', 'inline-block');
                    }

                    for (var x = 0; x < attachmentNamesSpit.length; x++) {
                        if (attachmentNamesSpit[x].length != 0) {
                            var fileType = attachmentNamesSpit[x].substr(attachmentNamesSpit[x].lastIndexOf('.') + 1);
                            $("#remarks_attachments_chancellor").append("<a href=\"/uploads/" + attachmentNamesSpit[x] + "\" target=\"_blank\" title=\"" + fileType + "\"  >" +
                                    "<i class=\"icon wb-tag\" style=\"font-size: 18px\"></i>" + "</a> ");
                        }
                    }
                }
            });

        });
    });
</script>

{{-- USER INFO: REMARKS --}}
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=submitChancellorButton], [id^=submitVchancellor], [id^=submitDean], [id^=needsRevisions], [id^=chancellorEvaluation]").click(function (e) {
            e.preventDefault();

            var id = (this.id).split('-');
            var formID = id[1];
            var retrieveDataUrl = "{{ $preLink.'meritpromotion/' }}" + formID + '/RetrieveUserInfo';

            $.get(retrieveDataUrl, function (data) {
                $('[id^=labelauty-][value="1"]').prop("checked", false);
                $('[id^=labelauty-][value="2"]').prop("checked", false);

                $("[id^=userInfo]").empty();
                $("[id^=userInfo]").append(data[0].last_name + ', ' + data[0].first_name + '/ ' + data[0].faculty_total + 'pts.');

                $("[id^=currentRank]").empty();
                $("[id^=currentStep]").empty();

                if ((data[0].rank) != null) {
                    $("[id^=currentRank]").html("Current rank : " + data[0].rank);
                } else {
                    $("[id^=currentRank]").html("Current rank : " + "None");
                }

                if ((data[0].step) != null) {
                    $("[id^=currentStep]").html("Current step : " + data[0].step);
                } else {
                    $("[id^=currentStep]").html("Current step : " + "None");
                }

                if ((data[0].initial_rank) != null) {
                    $("[id^=initial_rank]").val(data[0].initial_rank);
                } else {
                    $("[id^=initial_rank]").val("None");
                }

                if ((data[0].initial_step) != null) {
                    $("[id^=initial_step]").val(data[0].initial_step);
                } else {
                    $("[id^=initial_step]").val("");
                }

                //disable assign new rank and step; total points gained = 0
                if(data[0].faculty_total==0){
                    $("[id^=initial_rank]").attr('disabled', true);
                    $("[id^=initial_step]").attr('disabled', true);
                     
                }
            });
        });
    });
</script>

{{-- MONITORFORM VIEW: BASED LEGEND --}}
@if(Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/ReviseForm') || Request::is('*/CHANCELLORMonitorForm'))
    <script type="text/javascript">
        $(function () {
            $('tr').filter(function () {
                return $(this).data("isaccepteddeptchair") == 2
            }).css({background: '#ffcccc', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddeptchair") == 3
            }).css({background: '#BCED91', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddean") == 2
            }).css({background: '#ffcccc', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isacceptedvchancellor") == 2
            }).css({background: '#ffcccc', color: 'black'});

            @if(Auth::user()->position==4 || $MpromotionForm->form_status==6)
                $('i').filter(function () {
                        return $(this).data("isacceptedvchancellor") == 1
                    }).css({display: 'inline-block'});
            @endif

        });
    </script>
@endif

{{-- EVALUATEFORM VIEW: BASED LEGEND --}}
@if(Request::is('*/EvaluateForm'))
    <script type="text/javascript">
        $(function () {
            var x = "{{'isaccepted'.$currentUser}}";
            // DEPTCHAIR USER
            @if((Auth::user()->position)==1)
                $('tr').filter(function () {
                        return $(this).data("isaccepteddeptchair") == 2
                    }).css({background: '#ffcccc', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddeptchair") == 3
            }).css({background: '#BCED91', color: 'black'});

            $('i').filter(function () {
                return $(this).data("isaccepteddeptchair") == 1
            }).css({display: 'inline-block'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddeptchair") == ''
            }).css({background: '#E4EAEC'});
            @endif

            @if((Auth::user()->position)==2)
                $('tr').filter(function () {
                        return $(this).data("isaccepteddean") == ''
                    }).css({background: '#E4EAEC'});

            $('i').filter(function () {
                return $(this).data("isaccepteddean") == 1
            }).css({display: 'inline-block'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddean") == 2
            }).css({background: '#ffcccc', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddeptchair") == 2
            }).css({background: '#ffcccc', color: 'black'});
            @endif

            @if((Auth::user()->position)==3)
                $('tr').filter(function () {
                        return $(this).data("isacceptedvchancellor") == ''
                    }).css({background: '#E4EAEC'});

            $('i').filter(function () {
                return $(this).data("isacceptedvchancellor") == 1
            }).css({display: 'inline-block'});

            $('tr').filter(function () {
                return $(this).data("isacceptedvchancellor") == 2
            }).css({background: '#ffcccc', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddean") == 2
            }).css({background: '#ffcccc', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddeptchair") == 2
            }).css({background: '#ffcccc', color: 'black'});

            @endif

             @if((Auth::user()->position)==4)
                $('i').filter(function () {
                        return $(this).data("isacceptedvchancellor") == 1
                    }).css({display: 'inline-block'});

            $('tr').filter(function () {
                return $(this).data("isacceptedvchancellor") == 2
            }).css({background: '#ffcccc', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddean") == 2
            }).css({background: '#ffcccc', color: 'black'});

            $('tr').filter(function () {
                return $(this).data("isaccepteddeptchair") == 2
            }).css({background: '#ffcccc', color: 'black'});

            @endif



        });
    </script>
@endif