@extends('app_login')
@section('content')
@section('title', 'OFMPS | LOG IN')


<div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
     data-animsition-out="fade-out">

    <div class="page-content vertical-align-middle">

        <div class="brand">
            <img class="brand-img" src="{{asset('/images/upmindanao_logo.png')}}" style="width: 65%; height: 65%"
                 alt="...">
            <p style="color:black">THE UNIVERSITY OF THE PHILIPPLINES MINDANAO ONLINE FACULTY MERIT PROMOTION SYSTEM</p>
        </div>
        <hr>
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <span class="label label-danger">{!! $error !!}</span><br><br>
            @endforeach
        @endif
        <form class="form-inline" method="POST" action={{FOLDERNAME."/auth/login"}}>
            {!! csrf_field() !!}

            <div class="form-group">
                <label class="sr-only" for="inputUnlabelUsername">Employee code</label>
                <input type="employee_code" class="form-control" id="employee_code" name="employee_code"
                       placeholder="Employee Code" minlength="9" maxlength="9"
                       value="{{ old('employee_code') }}">
            </div>

            <div class="form-group">
                <label class="sr-only" for="inputUnlabelPassword">Password</label>
                <input type="password" class="form-control" id="inputPassword" name="password"
                       placeholder="Password">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" style="background: #5E0000">Login</button>
            </div>
        </form>
        <footer class="page-copyright">
            <br><br>
        </footer>
    </div>
</div>
@endsection