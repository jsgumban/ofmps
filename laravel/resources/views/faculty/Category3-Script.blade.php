<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("[id^=UpdateFC3-tr]").click(function (e) {
            e.preventDefault();

            // REVISE FUNCTION INITIALIZATION
            @if (Request::is('*/ReviseForm'))
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=UpdateFC3-]").attr('disabled', false);
            @endif

            // UPLOAD ATTACHMENT INITIALIZATION
            $('input:radio[name="proofAction"][value="1"]').prop('checked', true);
            $("[id^=c3BrowseDiv]").css('display', 'block');

            var id = (this.id).split('-');
            var category = id[2];
            var subCategory = id[3];
            var entryID = id[4];


            var retrieveDataUrl = "{{ $preLink.'meritpromotion/' }}" + entryID + '/RetrieveCat3';
            var submitButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/UpdateFormCat3';
            var deleteButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/DeleteEntryCat3';

            if (category == 1) {
                var x = document.getElementsByClassName("UpdateFC3-divClass-1");
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById("UpdateFC3-divID-" + category + "-" + subCategory).style.display = "block";
            } else if (category == 2) {
                var x = document.getElementsByClassName("UpdateFC3-divClass-2");
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById("UpdateFC3-divID-" + category + "-" + subCategory).style.display = "block";
            }

            $.get(retrieveDataUrl, function (data) {
                // REVISE FORM 
                @if (Request::is('*/ReviseForm'))
                    // initialize
                var entryStatus = data.isAccepted_deptchair;
                // submit buttons - textfields - upload section
                $('[id^=UpdateFC3-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC3-]").attr('disabled', true);
                $("[id^=UpdateFC3-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                // needs revision
                if (entryStatus == 3) {
                    $('[id^=UpdateFC3-reviseDiv-]').css('display', 'block');
                    $("[id^=UpdateFC3-]").attr('disabled', false);
                    $("[id^=UpdateFC3-proofs-]").css('display', "block");
                    $("[id^=ribbon-revise]").css('display', "block");
                    $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                } else if (entryStatus == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                }
                @endif

                @if (Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    // submit buttons - textfields - upload section
                $('[id^=UpdateFC3-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC3-]").attr('disabled', true);
                $("[id^=UpdateFC3-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                if (data.isAccepted_deptchair == 3) {
                    $("[id^=ribbon-revise]").css('display', "block");
                    $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                } else if (data.isAccepted_deptchair == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                } else if (data.isAccepted_dean == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                } else if (data.isAccepted_vchancellor == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (UAPFC)");
                }
                @endif

                @if (Request::is('*/EvaluateForm'))
                    // checkbox
                $('[id^=labelauty-][value="1"]').prop("checked", false);
                $('[id^=labelauty-][value="2"]').prop("checked", false);
                $('[id^=labelauty-][value="3"]').prop("checked", false);

                // submit buttons - textfields - upload section
                $('[id^=UpdateFC3-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC3-]").attr('disabled', true);
                $("[id^=UpdateFC3-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                // points
                // data-points - check box purposes
                $("[id^=UpdateFC3-points-]").attr('disabled', true);
                $("[id^=UpdateFC3-points-]").css('background-color', '');
                $("[id^=UpdateFC3-points-]").css('color', '');
                $("[id^=labelauty]").attr("data-points", data.points);

                var formStatus = ({{ $MpromotionForm->form_status }});

                // FORM IS CURRENTLY EVALUATED BY THE DEPTCHAIR
                if (formStatus == 1) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    // accepted
                    if (entryStatusDC == 1) {
                        $("[id^=UpdateFC3-points-]").css('background-color', '#FFEFD5');
                        $("[id^=UpdateFC3-points-]").css('color', 'black');
                        $("[id^=UpdateFC3-points-]").attr('disabled', false);
                        $('[id^=labelauty-][value="1"]').prop("checked", true);

                        $("[id^=ribbon-accept]").css('display', "block");
                        $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (DAPC)");
                    }

                    // rejected
                    else if (entryStatusDC == 2) {
                        $("[id^=UpdateFC3-points-]").css('background-color', '');
                        $("[id^=UpdateFC3-points-]").css('color', '');
                        $("[id^=UpdateFC3-points-]").attr('disabled', true);
                        $('[id^=labelauty-][value="2"]').prop("checked", true);

                        $("[id^=ribbon-rejected]").css('display', "block");
                        $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                    }

                    // needs revision
                    else if (entryStatusDC == 3) {
                        $("[id^=UpdateFC3-points-]").css('background-color', '');
                        $("[id^=UpdateFC3-points-]").css('color', '');
                        $("[id^=UpdateFC3-points-]").attr('disabled', true);
                        $('[id^=labelauty-][value="3"]').prop("checked", true);


                        $("[id^=ribbon-revise]").css('display', "block");
                        $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                    }

                    // not yet evaluated
                    else if (entryStatusDC == null) {
                        $("[id^=UpdateFC3-points-]").css('background-color', '');
                        $("[id^=UpdateFC3-points-]").css('color', '');
                        $("[id^=UpdateFC3-points-]").attr('disabled', true);

                        $("[id^=ribbon-notyet]").css('display', "block");
                        $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (DAPC)");
                    }
                }

                // FORM IS CURRENTLY EVALUATED BY THE DEAN
                else if (formStatus == 2) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    var entryStatusDean = data.isAccepted_dean;
                    $("[id^=evaluationButtons]").css('display', "block");

                    if (entryStatusDC == 2) {
                        $("[id^=UpdateFC3-points-]").css('background-color', '');
                        $("[id^=UpdateFC3-points-]").css('color', '');
                        $("[id^=UpdateFC3-points-]").attr('disabled', true);
                        $("[id^=evaluationButtons]").css('display', "none");

                        $("[id^=ribbon-rejected]").css('display', "block");
                        $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                    } else {
                        if (entryStatusDean == 1) {
                            $("[id^=UpdateFC3-points-]").css('background-color', '#FFEFD5');
                            $("[id^=UpdateFC3-points-]").css('color', 'black');
                            $("[id^=UpdateFC3-points-]").attr('disabled', false);
                            $('[id^=labelauty-][value="1"]').prop("checked", true);


                            $("[id^=ribbon-accept]").css('display', "block");
                            $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (CAPC)");
                        }

                        else if (entryStatusDean == 2) {
                            $("[id^=UpdateFC3-points-]").css('background-color', '');
                            $("[id^=UpdateFC3-points-]").css('color', '');
                            $("[id^=UpdateFC3-points-]").attr('disabled', true);
                            $('[id^=labelauty-][value="2"]').prop("checked", true);

                            $("[id^=ribbon-rejected]").css('display', "block");
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                        }

                        else if (entryStatusDean == null) {
                            $("[id^=UpdateFC3-points-]").css('background-color', '');
                            $("[id^=UpdateFC3-points-]").css('color', '');
                            $("[id^=UpdateFC3-points-]").attr('disabled', true);

                            $("[id^=ribbon-notyet]").css('display', "block");
                            $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (CAPC)");
                        }
                    }
                }

                // FORM IS CURRENTLY EVALUATED BY THE VCHANCELLOR
                else if (formStatus == 3 || formStatus == 4) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    var entryStatusDean = data.isAccepted_dean;
                    var entryStatusVchancellor = data.isAccepted_vchancellor;

                    @if(Auth::user()->position==4)
                        $("[id^=evaluationButtons]").css('display', "none");
                    @else
                        $("[id^=evaluationButtons]").css('display', "block");
                    @endif


                    if (entryStatusDC == 2 || entryStatusDean == 2) {
                        $("[id^=UpdateFC3-points-]").css('background-color', '');
                        $("[id^=UpdateFC3-points-]").css('color', '');
                        $("[id^=UpdateFC3-points-]").attr('disabled', true);
                        $("[id^=evaluationButtons]").css('display', "none");

                        $("[id^=ribbon-rejected]").css('display', "block");

                        if (entryStatusDC == 2) {
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                        }

                        if (entryStatusDean == 2) {
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                        }
                    } else {
                        if (entryStatusVchancellor == 1) {

                            $('[id^=labelauty-][value="1"]').prop("checked", true);


                            $("[id^=ribbon-accept]").css('display', "block");

                            @if(Auth::user()->position==4)
                                $("[id^=ribbon-accept] .ribbon-inner").html("ENTRY ACCEPTED");
                            $("[id^=UpdateFC3-points-]").attr('disabled', true);
                            $("[id^=UpdateFC3-points-]").css('background-color', '');
                            $("[id^=UpdateFC3-points-]").css('color', '');
                            @else
                                $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (UAPFC)");
                            $("[id^=UpdateFC3-points-]").attr('disabled', false);
                            $("[id^=UpdateFC3-points-]").css('background-color', '#FFEFD5');
                            $("[id^=UpdateFC3-points-]").css('color', 'black');
                            @endif


                        }

                        else if (entryStatusVchancellor == 2) {
                            $("[id^=UpdateFC3-points-]").css('background-color', '');
                            $("[id^=UpdateFC3-points-]").css('color', '');
                            $("[id^=UpdateFC3-points-]").attr('disabled', true);
                            $('[id^=labelauty-][value="2"]').prop("checked", true);

                            $("[id^=ribbon-rejected]").css('display', "block");
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (UAPFC)");
                        }

                        else if (entryStatusVchancellor == null) {
                            $("[id^=UpdateFC3-points-]").css('background-color', '');
                            $("[id^=UpdateFC3-points-]").css('color', '');
                            $("[id^=UpdateFC3-points-]").attr('disabled', true);

                            $("[id^=ribbon-notyet]").css('display', "block");
                            $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (UAPFC)");
                        }
                    }
                }
                @endif

                // RETRIEVE ENTRY INFORMATION
                if (category == 1) {
                    $('#UpdateFC3-SubSubCat-1').val(subCategory);
                    if (subCategory == 1) {
                        $('#UpdateFC3-designation-1-1').val(data.designation);
                        $('#UpdateFC3-period_covered-1-1').val(data.period_covered);
                        $('#UpdateFC3-points-1-1').val(data.points);
                        $('#UpdateFC3-other_details-1-1').val(data.other_details);

                        $('#UpdateFC3-form-1-1').attr('action', submitButtonUrl);
                        $('#UpdateFC3-delete-1-1').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c3radio2-1-1').attr('disabled', true);
                            $('#c3radio3-1-1').attr('disabled', true);
                        }
                    } else if (subCategory == 2) {
                        $('#UpdateFC3-name_ofcommittee-1-2').val(data.name_ofcommittee);
                        $('#UpdateFC3-nature_ofcommittee-1-2').val(data.nature_ofcommittee);
                        $('#UpdateFC3-period_covered-1-2').val(data.period_covered);
                        $('#UpdateFC3-points-1-2').val(data.points);
                        $('#UpdateFC3-other_details-1-2').val(data.other_details);

                        $('#UpdateFC3-form-1-2').attr('action', submitButtonUrl);
                        $('#UpdateFC3-delete-1-2').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c3radio2-1-2').attr('disabled', true);
                            $('#c3radio3-1-2').attr('disabled', true);
                        }
                    } else if (subCategory == 3) {
                        $('#UpdateFC3-funding_source-1-3').val(data.funding_source);
                        $('#UpdateFC3-purpose_offund-1-3').val(data.purpose_offund);
                        $('#UpdateFC3-ammount_donated-1-3').val(data.ammount_donated);
                        $('#UpdateFC3-points-1-3').val(data.points);
                        $('#UpdateFC3-other_details-1-3').val(data.other_details);

                        $('#UpdateFC3-form-1-3').attr('action', submitButtonUrl);
                        $('#UpdateFC3-delete-1-3').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c3radio2-1-3').attr('disabled', true);
                            $('#c3radio3-1-3').attr('disabled', true);
                        }
                    } else if (subCategory == 4) {
                        $('#UpdateFC3-designation_ofoic-1-4').val(data.designation_ofoic);
                        $('#UpdateFC3-level-1-4').val(data.level);
                        $('#UpdateFC3-period_covered-1-4').val(data.period_covered);
                        $('#UpdateFC3-points-1-4').val(data.points);
                        $('#UpdateFC3-other_details-1-4').val(data.other_details);

                        $('#UpdateFC3-form-1-4').attr('action', submitButtonUrl);
                        $('#UpdateFC3-delete-1-4').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c3radio2-1-4').attr('disabled', true);
                            $('#c3radio3-1-4').attr('disabled', true);
                        }
                    }
                } else if (category == 2) {
                    $('#UpdateFC3-SubSubCat-2').val(subCategory);
                    if (subCategory == 1) {
                        $('#UpdateFC3-subsubcategory-2-1').val(data.subsubcategory);
                        $('#UpdateFC3-name_oforganization-2-1').val(data.name_oforganization);
                        $('#UpdateFC3-designation-2-1').val(data.designation);
                        $('#UpdateFC3-period_covered-2-1').val(data.period_covered);
                        $('#UpdateFC3-points-2-1').val(data.points);
                        $('#UpdateFC3-other_details-2-1').val(data.other_details);

                        $('#UpdateFC3-form-2-1').attr('action', submitButtonUrl);
                        $('#UpdateFC3-delete-2-1').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c3radio2-2-1').attr('disabled', true);
                            $('#c3radio3-2-1').attr('disabled', true);
                        }
                    } else if (subCategory == 2) {
                        $('#UpdateFC3-subsubcategory-2-2').val(data.subsubcategory);
                        $('#UpdateFC3-name_oforganization-2-2').val(data.name_oforganization);
                        $('#UpdateFC3-designation-2-2').val(data.designation);
                        $('#UpdateFC3-period_covered-2-2').val(data.period_covered);
                        $('#UpdateFC3-points-2-2').val(data.points);
                        $('#UpdateFC3-other_details-2-2').val(data.other_details);

                        $('#UpdateFC3-form-2-2').attr('action', submitButtonUrl);
                        $('#UpdateFC3-delete-2-2').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c3radio2-2-2').attr('disabled', true);
                            $('#c3radio3-2-2').attr('disabled', true);
                        }

                    }
                }

            });


        });
    });
</script>