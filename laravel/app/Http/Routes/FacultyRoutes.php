<?php
Route::group(['middleware' => 'faculty'], function () {
    Route::get('faculty/home', 'FacultyController@index');

    /////////////////
    // OWN PROFILE //
    /////////////////
    Route::get('faculty/profile/MyProfile', ['uses' => 'AdminController@MyProfile']);
    Route::get('faculty/profile/UpdateMyProfile', ['uses' => 'AdminController@UpdateMyProfile']);
    Route::post('faculty/profile/SaveUpdateMyProfile', ['uses' => 'AdminController@SaveUpdateMyProfile']);
    Route::get('faculty/profile/UpdateMyPassword', ['uses' => 'AdminController@UpdateMyPassword']);
    Route::post('faculty/profile/SaveUpdateMyPassword', ['uses' => 'AdminController@SaveUpdateMyPassword']);

    /////////////////////
    // MERIT PROMOTION //
    /////////////////////
    Route::get('faculty/meritpromotion', ['uses' => 'FacultyController@MeritPromotion']);
    Route::get('faculty/meritpromotion/MeritPromotionForms', ['uses' => 'FacultyController@MeritPromotionForms']);
    Route::post('faculty/meritpromotion/AddNewForm', ['uses' => 'FacultyController@AddNewForm']);
    Route::get('faculty/meritpromotion/SaveAsDraft', ['uses' => 'FacultyController@SaveAsDraft']);
    Route::get('faculty/meritpromotion/{id}/SubmitDeptChair', ['uses' => 'FacultyController@SubmitDeptChair']);
    Route::get('faculty/meritpromotion/SubmittedForms', ['uses' => 'FacultyController@SubmittedForms']);
    Route::get('faculty/meritpromotion/{id}/ReviseForm', ['uses' => 'FacultyController@ReviseForm']);
    Route::get('faculty/meritpromotion/{id}/FillOutMeritPromotion', ['uses' => 'FacultyController@FillOutMeritPromotionCheck']);
    Route::get('faculty/meritpromotion/{id}/MonitorForm', ['uses' => 'FacultyController@MonitorForm']);
    Route::get('faculty/meritpromotion/{id}/RetrieveFormInfo', ['uses' => 'DeptChairController@RetrieveFormInfo']);
    Route::get('faculty/meritpromotion/{id}/DeleteDraftForm', ['uses' => 'FacultyController@DeleteDraftForm']);

    Route::get('faculty/meritpromotion/{id}/generateZip', ['uses' => 'FacultyController@generateZip']);
    Route::get('faculty/meritpromotion/{id}/generatePDF', ['uses' => 'FacultyController@generatePDF']);
    Route::get('faculty/meritpromotion/{id}/UpdateForm', ['uses' => 'FacultyController@UpdateForm']);

    Route::post('faculty/meritpromotion/{id}/SubmitFormCat1', ['uses' => 'FacultyController@SubmitFormCat1']);
    Route::post('faculty/meritpromotion/{id}/SubmitFormCat2', ['uses' => 'FacultyController@SubmitFormCat2']);
    Route::post('faculty/meritpromotion/{id}/SubmitFormCat3', ['uses' => 'FacultyController@SubmitFormCat3']);
    Route::post('faculty/meritpromotion/{id}/SubmitFormCat4', ['uses' => 'FacultyController@SubmitFormCat4']);

    Route::get('faculty/meritpromotion/{id}/RetrieveCat1', ['uses' => 'FacultyController@RetrieveCat1']);
    Route::get('faculty/meritpromotion/{id}/RetrieveCat2', ['uses' => 'FacultyController@RetrieveCat2']);
    Route::get('faculty/meritpromotion/{id}/RetrieveCat3', ['uses' => 'FacultyController@RetrieveCat3']);
    Route::get('faculty/meritpromotion/{id}/RetrieveCat4', ['uses' => 'FacultyController@RetrieveCat4']);

    Route::post('faculty/meritpromotion/{id}/UpdateFormCat1', ['uses' => 'FacultyController@UpdateFormCat1']);
    Route::post('faculty/meritpromotion/{id}/UpdateFormCat2', ['uses' => 'FacultyController@UpdateFormCat2']);
    Route::post('faculty/meritpromotion/{id}/UpdateFormCat3', ['uses' => 'FacultyController@UpdateFormCat3']);
    Route::post('faculty/meritpromotion/{id}/UpdateFormCat4', ['uses' => 'FacultyController@UpdateFormCat4']);

    Route::get('faculty/meritpromotion/{id}/DeleteEntryCat1', ['uses' => 'FacultyController@DeleteEntryCat1']);
    Route::get('faculty/meritpromotion/{id}/DeleteEntryCat2', ['uses' => 'FacultyController@DeleteEntryCat2']);
    Route::get('faculty/meritpromotion/{id}/DeleteEntryCat3', ['uses' => 'FacultyController@DeleteEntryCat3']);
    Route::get('faculty/meritpromotion/{id}/DeleteEntryCat4', ['uses' => 'FacultyController@DeleteEntryCat4']);
});

?>