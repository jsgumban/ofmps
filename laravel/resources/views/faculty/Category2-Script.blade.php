<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("[id^=UpdateFC2-tr]").click(function (e) {
            e.preventDefault();


            // REVISE FUNCTION INITIALIZATION
            @if (Request::is('*/ReviseForm'))
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=UpdateFC2-]").attr('disabled', false);
            @endif

            // UPLOAD ATTACHMENT INITIALIZATION
            $('input:radio[name="proofAction"][value="1"]').prop('checked', true);
            $("[id^=c2BrowseDiv]").css('display', 'block');


            var id = (this.id).split('-');
            var category = id[2];
            var subCategory = id[3];
            var entryID = id[4];


            var retrieveDataUrl = "{{ $preLink.'meritpromotion/' }}" + entryID + '/RetrieveCat2';
            var submitButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/UpdateFormCat2';
            var deleteButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/DeleteEntryCat2';

            if (category == 1) {
                var x = document.getElementsByClassName("UpdateFC2-divClass-1");
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById("UpdateFC2-divID-" + category + "-" + subCategory).style.display = "block";
            } else if (category == 2) {
                var x = document.getElementsByClassName("UpdateFC2-divClass-2");
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById("UpdateFC2-divID-" + category + "-" + subCategory).style.display = "block";
            }

            $.get(retrieveDataUrl, function (data) {

                // REVISE FORM 
                @if (Request::is('*/ReviseForm'))
                    // initialize
                var entryStatus = data.isAccepted_deptchair;
                // submit buttons - textfields - upload section
                $('[id^=UpdateFC2-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC2-]").attr('disabled', true);
                $("[id^=UpdateFC2-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                // needs revision
                if (entryStatus == 3) {
                    $('[id^=UpdateFC2-reviseDiv-]').css('display', 'block');
                    $("[id^=UpdateFC2-]").attr('disabled', false);
                    $("[id^=UpdateFC2-proofs-]").css('display', "block");
                    $("[id^=ribbon-revise]").css('display', "block");
                    $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                } else if (entryStatus == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                }
                @endif

                @if (Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    // submit buttons - textfields - upload section
                $('[id^=UpdateFC2-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC2-]").attr('disabled', true);
                $("[id^=UpdateFC2-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                if (data.isAccepted_deptchair == 3) {
                    $("[id^=ribbon-revise]").css('display', "block");
                    $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                } else if (data.isAccepted_deptchair == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                } else if (data.isAccepted_dean == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                } else if (data.isAccepted_vchancellor == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (UAPFC)");
                }
                @endif

                @if (Request::is('*/EvaluateForm'))
                    // checkbox
                $('[id^=labelauty-][value="1"]').prop("checked", false);
                $('[id^=labelauty-][value="2"]').prop("checked", false);
                $('[id^=labelauty-][value="3"]').prop("checked", false);

                // submit buttons - textfields - upload section
                $('[id^=UpdateFC2-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC2-]").attr('disabled', true);
                $("[id^=UpdateFC2-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                // points
                // data-points - check box purposes
                $("[id^=UpdateFC2-points-]").attr('disabled', true);
                $("[id^=UpdateFC2-points-]").css('background-color', '');
                $("[id^=UpdateFC2-points-]").css('color', '');
                $("[id^=labelauty]").attr("data-points", data.points);

                var formStatus = ({{ $MpromotionForm->form_status }});

                // FORM IS CURRENTLY EVALUATED BY THE DEPTCHAIR
                if (formStatus == 1) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    // accepted
                    if (entryStatusDC == 1) {
                        $("[id^=UpdateFC2-points-]").css('background-color', '#FFEFD5');
                        $("[id^=UpdateFC2-points-]").css('color', 'black');
                        $("[id^=UpdateFC2-points-]").attr('disabled', false);
                        $('[id^=labelauty-][value="1"]').prop("checked", true);

                        $("[id^=ribbon-accept]").css('display', "block");
                        $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (DAPC)");
                    }

                    // rejected
                    else if (entryStatusDC == 2) {
                        $("[id^=UpdateFC2-points-]").css('background-color', '');
                        $("[id^=UpdateFC2-points-]").css('color', '');
                        $("[id^=UpdateFC2-points-]").attr('disabled', true);
                        $('[id^=labelauty-][value="2"]').prop("checked", true);

                        $("[id^=ribbon-rejected]").css('display', "block");
                        $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                    }

                    // needs revision
                    else if (entryStatusDC == 3) {
                        $("[id^=UpdateFC2-points-]").css('background-color', '');
                        $("[id^=UpdateFC2-points-]").css('color', '');
                        $("[id^=UpdateFC2-points-]").attr('disabled', true);
                        $('[id^=labelauty-][value="3"]').prop("checked", true);


                        $("[id^=ribbon-revise]").css('display', "block");
                        $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                    }

                    // not yet evaluated
                    else if (entryStatusDC == null) {
                        $("[id^=UpdateFC2-points-]").css('background-color', '');
                        $("[id^=UpdateFC2-points-]").css('color', '');
                        $("[id^=UpdateFC2-points-]").attr('disabled', true);

                        $("[id^=ribbon-notyet]").css('display', "block");
                        $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (DAPC)");
                    }
                }

                // FORM IS CURRENTLY EVALUATED BY THE DEAN
                else if (formStatus == 2) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    var entryStatusDean = data.isAccepted_dean;
                    $("[id^=evaluationButtons]").css('display', "block");

                    if (entryStatusDC == 2) {
                        $("[id^=UpdateFC2-points-]").css('background-color', '');
                        $("[id^=UpdateFC2-points-]").css('color', '');
                        $("[id^=UpdateFC2-points-]").attr('disabled', true);
                        $("[id^=evaluationButtons]").css('display', "none");

                        $("[id^=ribbon-rejected]").css('display', "block");
                        $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                    } else {
                        if (entryStatusDean == 1) {
                            $("[id^=UpdateFC2-points-]").css('background-color', '#FFEFD5');
                            $("[id^=UpdateFC2-points-]").css('color', 'black');
                            $("[id^=UpdateFC2-points-]").attr('disabled', false);
                            $('[id^=labelauty-][value="1"]').prop("checked", true);


                            $("[id^=ribbon-accept]").css('display', "block");
                            $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (CAPC)");
                        }

                        else if (entryStatusDean == 2) {
                            $("[id^=UpdateFC2-points-]").css('background-color', '');
                            $("[id^=UpdateFC2-points-]").css('color', '');
                            $("[id^=UpdateFC2-points-]").attr('disabled', true);
                            $('[id^=labelauty-][value="2"]').prop("checked", true);

                            $("[id^=ribbon-rejected]").css('display', "block");
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                        }

                        else if (entryStatusDean == null) {
                            $("[id^=UpdateFC2-points-]").css('background-color', '');
                            $("[id^=UpdateFC2-points-]").css('color', '');
                            $("[id^=UpdateFC2-points-]").attr('disabled', true);

                            $("[id^=ribbon-notyet]").css('display', "block");
                            $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (CAPC)");
                        }
                    }
                }

                // FORM IS CURRENTLY EVALUATED BY THE VCHANCELLOR
                else if (formStatus == 3 || formStatus == 4) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    var entryStatusDean = data.isAccepted_dean;
                    var entryStatusVchancellor = data.isAccepted_vchancellor;

                    @if(Auth::user()->position==4)
                        $("[id^=evaluationButtons]").css('display', "none");
                    @else
                        $("[id^=evaluationButtons]").css('display', "block");
                    @endif


                    if (entryStatusDC == 2 || entryStatusDean == 2) {
                        $("[id^=UpdateFC2-points-]").css('background-color', '');
                        $("[id^=UpdateFC2-points-]").css('color', '');
                        $("[id^=UpdateFC2-points-]").attr('disabled', true);
                        $("[id^=evaluationButtons]").css('display', "none");

                        $("[id^=ribbon-rejected]").css('display', "block");

                        if (entryStatusDC == 2) {
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                        }

                        if (entryStatusDean == 2) {
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                        }
                    } else {
                        if (entryStatusVchancellor == 1) {

                            $('[id^=labelauty-][value="1"]').prop("checked", true);


                            $("[id^=ribbon-accept]").css('display', "block");

                            @if(Auth::user()->position==4)
                                $("[id^=ribbon-accept] .ribbon-inner").html("ENTRY ACCEPTED");
                            $("[id^=UpdateFC2-points-]").attr('disabled', true);
                            $("[id^=UpdateFC2-points-]").css('background-color', '');
                            $("[id^=UpdateFC2-points-]").css('color', '');
                            @else
                                $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (UAPFC)");
                            $("[id^=UpdateFC2-points-]").attr('disabled', false);
                            $("[id^=UpdateFC2-points-]").css('background-color', '#FFEFD5');
                            $("[id^=UpdateFC2-points-]").css('color', 'black');
                            @endif


                        }

                        else if (entryStatusVchancellor == 2) {
                            $("[id^=UpdateFC2-points-]").css('background-color', '');
                            $("[id^=UpdateFC2-points-]").css('color', '');
                            $("[id^=UpdateFC2-points-]").attr('disabled', true);
                            $('[id^=labelauty-][value="2"]').prop("checked", true);

                            $("[id^=ribbon-rejected]").css('display', "block");
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (UAPFC)");
                        }

                        else if (entryStatusVchancellor == null) {
                            $("[id^=UpdateFC2-points-]").css('background-color', '');
                            $("[id^=UpdateFC2-points-]").css('color', '');
                            $("[id^=UpdateFC2-points-]").attr('disabled', true);

                            $("[id^=ribbon-notyet]").css('display', "block");
                            $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (UAPFC)");
                        }
                    }
                }
                @endif

                if (category == 1) {
                    $('#UpdateFC2-SubSubCat-1').val(subCategory);
                    if (subCategory == 1) {
                        $('#UpdateFC2-subsubcategory-1-1').val(data.subsubcategory);
                        $('#UpdateFC2-title-1-1').val(data.title);
                        $('#UpdateFC2-author-1-1').val(data.author);
                        $('#UpdateFC2-date_published-1-1').val(data.date_published);
                        $('#UpdateFC2-bibliography-1-1').val(data.bibliography);
                        $('#UpdateFC2-nature_ofpublication-1-1').val(data.nature_ofpublication);
                        $('#UpdateFC2-points-1-1').val(data.points);
                        $('#UpdateFC2-other_details-1-1').val(data.other_details);

                        $('#UpdateFC2-form-1-1').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-1-1').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-1-1').attr('disabled', true);
                            $('#c2radio3-1-1').attr('disabled', true);
                        }
                    } else if (subCategory == 2) {
                        // alert('hey');
                        $('#UpdateFC2-subsubcategory-1-2').val(data.subsubcategory);
                        $('#UpdateFC2-title-1-2').val(data.title);
                        $('#UpdateFC2-author-1-2').val(data.author);
                        $('#UpdateFC2-date_ofpresentation-1-2').val(data.date_ofpresentation);
                        $('#UpdateFC2-place_ofpresentation-1-2').val(data.place_ofpresentation);
                        $('#UpdateFC2-points-1-2').val(data.points);
                        $('#UpdateFC2-other_details-1-2').val(data.other_details);

                        $('#UpdateFC2-form-1-2').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-1-2').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-1-2').attr('disabled', true);
                            $('#c2radio3-1-2').attr('disabled', true);
                        }
                    } else if (subCategory == 3) {
                        // alert('hey');
                        $('#UpdateFC2-subsubcategory-1-3').val(data.subsubcategory);
                        $('#UpdateFC2-title-1-3').val(data.title);
                        $('#UpdateFC2-author-1-3').val(data.author);
                        $('#UpdateFC2-date_ofpresentation-1-3').val(data.date_ofpresentation);
                        $('#UpdateFC2-place_ofpresentation-1-3').val(data.place_ofpresentation);
                        $('#UpdateFC2-title_ofconference-1-3').val(data.title_ofconference);
                        $('#UpdateFC2-points-1-3').val(data.points);
                        $('#UpdateFC2-other_details-1-3').val(data.other_details);

                        $('#UpdateFC2-form-1-3').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-1-3').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-1-3').attr('disabled', true);
                            $('#c2radio3-1-3').attr('disabled', true);
                        }
                    } else if (subCategory == 4) {
                        $('#UpdateFC2-subsubcategory-1-4').val(data.subsubcategory);
                        $('#UpdateFC2-title-1-4').val(data.title);
                        $('#UpdateFC2-author-1-4').val(data.author);
                        $('#UpdateFC2-date_ofpresentation-1-4').val(data.date_ofpresentation);
                        $('#UpdateFC2-place_ofpresentation-1-4').val(data.place_ofpresentation);
                        $('#UpdateFC2-title_ofconference-1-4').val(data.title_ofconference);
                        $('#UpdateFC2-points-1-4').val(data.points);
                        $('#UpdateFC2-other_details-1-4').val(data.other_details);

                        $('#UpdateFC2-form-1-4').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-1-4').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-1-4').attr('disabled', true);
                            $('#c2radio3-1-4').attr('disabled', true);
                        }
                    }
                } else if (category == 2) {
                    $('#UpdateFC2-SubSubCat-2').val(subCategory);
                    if (subCategory == 1) {
                        $('#UpdateFC2-subsubcategory-2-1').val(data.subsubcategory);
                        $('#UpdateFC2-title-2-1').val(data.title);
                        $('#UpdateFC2-author-2-1').val(data.author);
                        $('#UpdateFC2-date_published-2-1').val(data.date_published);
                        $('#UpdateFC2-publisher-2-1').val(data.publisher);
                        $('#UpdateFC2-nature_ofpublication-2-1').val(data.nature_ofpublication);
                        $('#UpdateFC2-points-2-1').val(data.points);
                        $('#UpdateFC2-other_details-2-1').val(data.other_details);

                        $('#UpdateFC2-form-2-1').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-2-1').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-2-1').attr('disabled', true);
                            $('#c2radio3-2-1').attr('disabled', true);
                        }
                    } else if (subCategory == 2) {
                        $('#UpdateFC2-subsubcategory-2-2').val(data.subsubcategory);
                        $('#UpdateFC2-title-2-2').val(data.title);
                        $('#UpdateFC2-date_ofpresentation-2-2').val(data.date_ofpresentation);
                        $('#UpdateFC2-place_ofpresentation-2-2').val(data.place_ofpresentation);
                        $('#UpdateFC2-points-2-2').val(data.points);
                        $('#UpdateFC2-other_details-2-2').val(data.other_details);

                        $('#UpdateFC2-form-2-2').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-2-2').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-2-2').attr('disabled', true);
                            $('#c2radio3-2-2').attr('disabled', true);
                        }
                    } else if (subCategory == 3) {
                        $('#UpdateFC2-subsubcategory-2-3').val(data.subsubcategory);
                        $('#UpdateFC2-title-2-3').val(data.title);
                        $('#UpdateFC2-other_details-2-3').val(data.other_details);
                        $('#UpdateFC2-points-2-3').val(data.points);

                        $('#UpdateFC2-form-2-3').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-2-3').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-2-3').attr('disabled', true);
                            $('#c2radio3-2-3').attr('disabled', true);
                        }
                    } else if (subCategory == 4) {
                        $('#UpdateFC2-subsubcategory-2-4').val(data.subsubcategory);
                        $('#UpdateFC2-title-2-4').val(data.title);
                        $('#UpdateFC2-other_details-2-4').val(data.other_details);
                        $('#UpdateFC2-points-2-4').val(data.points);

                        $('#UpdateFC2-form-2-4').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-2-4').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-2-4').attr('disabled', true);
                            $('#c2radio3-2-4').attr('disabled', true);
                        }

                    } else if (subCategory == 5) {
                        $('#UpdateFC2-subsubcategory-2-5').val(data.subsubcategory);
                        $('#UpdateFC2-title-2-5').val(data.title);
                        $('#UpdateFC2-other_details-2-5').val(data.other_details);
                        $('#UpdateFC2-points-2-5').val(data.points);

                        $('#UpdateFC2-form-2-5').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-2-5').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-2-5').attr('disabled', true);
                            $('#c2radio3-2-5').attr('disabled', true);
                        }

                    } else if (subCategory == 6) {
                        $('#UpdateFC2-subsubcategory-2-6').val(data.subsubcategory);
                        $('#UpdateFC2-title-2-6').val(data.title);
                        $('#UpdateFC2-other_details-2-6').val(data.other_details);
                        $('#UpdateFC2-points-2-6').val(data.points);

                        $('#UpdateFC2-form-2-6').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-2-6').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-2-6').attr('disabled', true);
                            $('#c2radio3-2-6').attr('disabled', true);
                        }

                    } else if (subCategory == 7) {
                        $('#UpdateFC2-subsubcategory-2-7').val(data.subsubcategory);
                        $('#UpdateFC2-title-2-7').val(data.title);
                        $('#UpdateFC2-other_details-2-7').val(data.other_details);
                        $('#UpdateFC2-points-2-7').val(data.points);

                        $('#UpdateFC2-form-2-7').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-2-7').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-2-7').attr('disabled', true);
                            $('#c2radio3-2-7').attr('disabled', true);
                        }

                    } else if (subCategory == 8) {
                        $('#UpdateFC2-subsubcategory-2-8').val(data.subsubcategory);
                        $('#UpdateFC2-title-2-8').val(data.title);
                        $('#UpdateFC2-author-2-8').val(data.author);
                        $('#UpdateFC2-date_published-2-8').val(data.date_published);
                        $('#UpdateFC2-bibliography-2-8').val(data.bibliography);
                        $('#UpdateFC2-nature_ofpublication-2-8').val(data.nature_ofpublication);
                        $('#UpdateFC2-points-2-8').val(data.points);
                        $('#UpdateFC2-other_details-2-8').val(data.other_details);

                        $('#UpdateFC2-form-2-8').attr('action', submitButtonUrl);
                        $('#UpdateFC2-delete-2-8').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c2radio2-2-8').attr('disabled', true);
                            $('#c2radio3-2-8').attr('disabled', true);
                        }
                    }
                }
            });
        });
    });
</script>