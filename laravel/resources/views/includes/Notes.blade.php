{{-- notes --}}
@if (Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
    <small>
        <div class="alert alert-icon alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="icon wb-info" aria-hidden="true"></i>

            <p>NOTE : You are not allowed to do any updates here</p>
        </div>
    </small>
@endif
@if (Request::is('*/EvaluateForm') && Auth::user()->position!=4 )
    <small>
        <div class="alert alert-icon alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="icon wb-info" aria-hidden="true"></i>

            <p>NOTE : You can only submit the form if there is a call for promotion, all entries are evaluated, and no
                entries need any revisions.</p>
        </div>
    </small>
@endif

@if (Request::is('*/FillOutMeritPromotion') || Request::is('*/UpdateForm'))
    <small>
        <div class="alert alert-icon alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="icon wb-info" aria-hidden="true"></i>

            <p>NOTE : You can only submit if there is a call for promotion.</p>
        </div>
    </small>
@endif