<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="UPMIN OFMPS" content="University of the Philippines Mindanao Online Faculty Merit Pormotion">
    <meta name="author" content="John Hel Gumban">
    <title> @yield('title') </title>
    <link rel="apple-touch-icon" href="{{ asset('/images/upmindanao_logo.png') }}">
    <link rel="shortcut icon" href="{{ asset('/images/upmindanao_logo.png') }}">
    @include('layout.stylesheets')
    @include('layout.pluginsabove')
    @include('layout.styleabove')
    @include('layout.page')
    @include('layout.fonts')
    @include('layout.scriptsabove')
</head>

<body class="page-login layout-full">
@yield('content')
@include('layout.cores')
@include('layout.pluginsbelow')
@include('layout.scriptsbelow')
@include('layout.scriptsbelowbody')
</body>
</html>

{{-- <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).bind("contextmenu",function(e) {
        e.preventDefault();
    });

     $(document).keydown(function(e){
        if(e.which === 123){
           return false;
        }
    });
</script> --}}