<div class="tab-pane" id="category-4" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">
        <h4 id="ASDASD">
            <div class="btn-group pull-right">
                <span data-toggle="tooltip" data-target="#id"
                      title="Create New User">
                    <button type="button" class="btn btn-animate social-facebook"
                            id="{{'MoreInfo-0-addnew'}}" data-target="#ModalMInfo-modal-1"
                            data-toggle="modal"
                            data-toggle="dropdown" title="Create New User">
                        <span><i class="icon fa-user-plus" aria-hidden="true"></i> New User</span>
                    </button>
                </span>
            </div>
            <center><b>LIST OF ALL USERS</b> &nbsp;
                <a href="javascript:void(0)" data-plugin="webuiPopover"
                   data-placement="right" data-animation="pop" data-target="webuiPopover" data-title="More Information"
                   data-target="webuiPopover9"
                   data-content="
                                <small> <table class='table'>
                                <tbody>

                                <tr>
                                    <td>Total no. of Users</td>
                                    <td>{!! count($users) !!}</td>
                                </tr>

                                <tr>
                                    <td>Total no. of Faculty Users</td>
                                    <td>{!! count($allFaculty) !!} </td>
                                </tr>

                                <tr>
                                    <td>Total no. of System Administrators</td>
                                    <td>{!! count($adminUsers) !!} </td>
                                </tr>
    
                                </tbody>
                                </table> </small>">
                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                </a>
            </center>
        </h4>

        @if(session('activeTab')==4)
            {{-- notification --}}

            @if(!empty(session('notification')))
                <br>
                <?php $notif = session('notification');?>
                @if(strpos($notif,'SUCCESS')===false)
                    <div class="alert alert-alt alert-danger alert-dismissible"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @else
                    <div class="alert alert-alt alert-success alert-dismissible"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @endif
            @endif
        @endif
        <hr>

        <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Employee Code</th>
                <th>Name</th>
                <th>User Type</th>
                <th>
                    <center> Action</center>
                </th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th>#</th>
                <th>Employee Code</th>
                <th>Name Name</th>
                <th>User Type</th>
                <th>
                    <center> Action</center>
                </th>
            </tr>
            </tfoot>

            <tbody>
            <?php $count = 1; ?>
            @foreach ($users as $user)
                <tr>
                    <td id="{{'MoreInfo-'.$user->id.'-update'}}" data-target="#ModalMInfo-modal-1"
                        data-toggle="modal" role="button"> <?php echo $count++; ?> </td>
                    <td id="{{'MoreInfo-'.$user->id.'-update'}}" data-target="#ModalMInfo-modal-1"
                        data-toggle="modal" role="button"> {{ $user->employee_code }} </td>
                    <td id="{{'MoreInfo-'.$user->id.'-update'}}" data-target="#ModalMInfo-modal-1"
                        data-toggle="modal"
                        role="button"> {{ $user->last_name.', '. $user->first_name. ' '.$user->middle_name[0].'.'  }} </td>
                    <td id="{{'MoreInfo-'.$user->id.'-update'}}" data-target="#ModalMInfo-modal-1"
                        data-toggle="modal" role="button">
                        @if(($user->position)==5)
                            <?php echo 'System Administrator'; ?>
                        @elseif(($user->position)<5)
                            <?php echo 'Faculty'; ?>
                        @endif
                    </td>


                    <td>
                        <center>
                            {{-- DISABLE ACTIONS TO JOHN HEL (SYSTEM ADMIN) --}}
                            @if($user->employee_code == '999999999')
                                <a href="javascript: void(0)">
                                    <button disabled type="button" class="btn btn-icon btn-info  btn-sm"
                                            title="Reset Password"><i class="icon wb-refresh"
                                                                      aria-hidden="true"
                                                                      style="font-size: 15px;"></i></button>
                                </a>


                                <a href="javascript: void(0)">
                                    <button disabled type="button"
                                            class="btn btn-icon btn-danger  btn-sm btn-sm  btn-sm"
                                            title="Delete Profile"><i class="icon wb-trash"
                                                                      aria-hidden="true"
                                                                      style="font-size: 15px;"></i></button>
                                </a>
                            @else

                                <a href="{!!$preLink.'userprofiles/'.$user->id.'/ResetPassword' !!}" onclick
                                ="
                                            if (! confirm('Are you sure do you want to reset the password?')) { return false; }
                                            " data-toggle="tooltip" title="Reset Password">
                                    <button type="button" class="btn btn-icon btn-info  btn-sm"><i
                                                class="icon wb-refresh" aria-hidden="true"
                                                style="font-size: 15px;"></i></button>
                                </a>
                                <a href="{!!$preLink.'userprofiles/'.$user->id.'/DeleteProfile' !!}" onclick
                                ="
                                            if (! confirm('Are you sure do you want remove this profile?')) { return false; }
                                            " data-toggle="tooltip" title="Remove User">
                                    <button type="button"
                                            class="btn btn-icon btn-danger  btn-sm btn-sm  btn-sm"><i
                                                class="icon wb-trash" aria-hidden="true"
                                                style="font-size: 15px;"></i></button>
                                </a>
                            @endif
                        </center>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('deptchair.FacultyMembersMoreInfo-Script')
@include('deptchair.FacultyMembersMoreInfo-View')