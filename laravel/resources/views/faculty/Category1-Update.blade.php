{{-- CATEGORY1 MODAL --}}
<div class="modal fade modal-success" id="UpdateFC1-modal-1" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">

        {{-- FORM --}}
        <form action="#" method="POST" id="UpdateFC1-form-1-1">
            {!! csrf_field() !!}

            {{-- HIDDEN ATTRIBUTES --}}
            {!! Form::hidden('category', 1) !!}
            {!! Form::hidden('subcategory', 1) !!}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                    {{-- RIBBONS --}}
                    @if (Request::is('*/ReviseForm') || Request::is('*/EvaluateForm') || Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                        <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-rejected">
                            <span class="ribbon-inner">REJECTED</span>
                        </div>
                        <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-revise">
                            <span class="ribbon-inner">NEEDS REVISION</span>
                        </div>

                        <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-notyet">
                            <span class="ribbon-inner">NOT YET EVALUATED</span>
                        </div>

                        <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-accept">
                            <span class="ribbon-inner">ACCEPTED</span>
                        </div>
                    @endif

                    <h4 class="modal-title">A. PERFORMANCE RATING (STATE) </h4>
                </div>

                <div class="modal-body">

                    {{-- WEIGHTED AVERAGE RATING --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">Weighted Average Rating</label>
                        <input type="number" step="0.01" class="form-control" name="WARating"
                               placeholder="Weighted Average Rating" required id="UpdateFC1-WARating-1-1">
                    </div>

                    {{-- POINTS --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">Points
                            <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                               data-animation="pop" data-target="webuiPopover" data-placement="right"
                               data-content="
                                        <small><small> <table class='table'><thead>
                                        <tr>
                                            <th>WEIGHTED AVERAGE RATING</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1.0</td>
                                            <td>20</td>
                                        </tr>

                                        <tr>
                                            <td>1.1</td>
                                            <td>19</td>
                                        </tr>

                                        <tr>
                                            <td>1.2</td>
                                            <td>18</td>
                                        </tr>

                                        <tr>
                                            <td>1.3</td>
                                            <td>17</td>
                                        </tr>

                                        <tr>
                                            <td>1.4</td>
                                            <td>16</td>
                                        </tr>

                                        <tr>
                                            <td>1.5</td>
                                            <td>15</td>
                                        </tr>

                                        <tr>
                                            <td>1.6</td>
                                            <td>14</td>
                                        </tr>

                                        <tr>
                                            <td>1.7</td>
                                            <td>13</td>
                                        </tr>

                                        <tr>
                                            <td>1.8</td>
                                            <td>12</td>
                                        </tr>

                                        <tr>
                                            <td>1.9</td>
                                            <td>11</td>
                                        </tr>

                                        <tr>
                                            <td>2.0</td>
                                            <td>10</td>
                                        </tr>

                                        <tr>
                                            <td>Less than  2.0</td>
                                            <td>0</td>
                                        </tr>
                                        </tbody>
                                        </table> </small></small>">
                                <i class="icon wb-info-circle" aria-hidden="true"></i>
                            </a>
                        </label>
                        <input type="number" step="0.01" class="form-control" name="points" placeholder="Points" min="0" max="20"
                               required id="UpdateFC1-points-1-1">
                    </div>


                    @if(Request::is('*/EvaluateForm'))
                        {!! Form::hidden('action', 'EvaluateForm') !!}
                        <div class="row">
                            <div class="col-lg-12 form-group" id="evaluationButtons">
                                <small>
                                    <div class="col-md-6" align="left">

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </small>
                            </div>
                        </div>
                    @elseif(Request::is('*/ReviseForm'))
                        {!! Form::hidden('action', 'ReviseForm') !!}
                        <div class="modal-footer">
                            {{-- SUBMIT BUTTON --}}
                            <div id="UpdateFC1-reviseDiv-1-1">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-1-1"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                {{-- CANCEL BUTTON --}}
                                <button type="submit" class="btn btn-success" id="UpdateFC1-revise-1-1">Revise</button>
                            </div>
                        </div>
                    @else
                        <div class="modal-footer">
                            {{-- SUBMIT BUTTON --}}
                            <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-1-1"
                               onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                Entry</a>
                            {{-- CANCEL BUTTON --}}
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>


{{-- CATEGORY2 MODAL --}}
<div class="modal fade modal-success  example-modal-lg" id="UpdateFC1-modal-2" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                {{-- RIBBONS --}}
                @if (Request::is('*/ReviseForm') || Request::is('*/EvaluateForm') || Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-rejected">
                        <span class="ribbon-inner">REJECTED</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-revise">
                        <span class="ribbon-inner">NEEDS REVISION</span>
                    </div>
                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-notyet">
                        <span class="ribbon-inner">NOT YET EVALUATED</span>
                    </div>

                    <div class="ribbon ribbon-reverse ribbon-default" style="display:none" id="ribbon-accept">
                        <span class="ribbon-inner">ACCEPTED</span>
                    </div>
                @endif

                <h4 class="modal-title">B. TEACHING PORTFOLIO / OTHER ACTIVITIES RELATED TO TEACHING </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="UpdateFC1-SubSubCat-2" disabled>
                        <option value=1>01. Authorship of a UP level textbook/ audio-visual materials for one- semester
                            course
                        </option>
                        <option value=2>02. Authorship of a UP laboratory/studies manual</option>
                        <option value=3>03. Authorship of Modules (including wrap-around text)</option>
                        <option value=4>04. Revision/updating of UP textbook/laboratory/studio manuals</option>
                        <option value=5>05. Authorship of book review</option>
                        <option value=6>06. Authorship of case studies(Case studies are classroom-tested and with signed
                            company release)
                        </option>
                        <option value=7>07. Authorship of annotated bibliography</option>
                        <option value=8>08. Department-endorsed materials for instructions</option>
                        <option value=9>09. For each dissertation advisee graduated</option>
                        <option value=10>10. For each Master’s thesis advisee graduated</option>
                        <option value=11>11. Undergraduate Thesis/Special Problem/Business Plan: for every advisee who
                            graduated (Max of 6 students/year)
                        </option>
                        <option value=12>12. Number of preparations per year</option>
                        <option value=13>13. Other contributions to teaching</option>
                    </select>
                </div>


                {{-- SUBCATEGORY 1--}}
                <div id="UpdateFC1-divID-1" class="UpdateFC1-divClass-2">
                    <form action="#" method="POST" id="UpdateFC1-form-2-1"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC1-title-2-1">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required id="UpdateFC1-author-2-1">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC1-date_published-2-1">
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required id="UpdateFC1-publisher-2-1">
                        </div>

                        {{-- Course Title--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Course Title</label>
                            <input type="text" class="form-control" name="course_title"
                                   placeholder="Course Title" required id="UpdateFC1-course_title-2-1">
                        </div>

                        {{-- Semester Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Semester Used</label>
                            <select class="form-control" name="semester_used"
                                    placeholder="Semester Used" required id="UpdateFC1-semester_used-2-1">
                                <option value="First Semester"> First Semester
                                </option>
                                <option value="Second Semester">Second Semester</option>
                                <option value="Summer">Summer</option>
                            </select>
                        </div>

                        {{-- AY Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">AY Used</label>
                            <input class="form-control" name="ay_used"
                                   placeholder="AY Used" required id="UpdateFC1-ay_used-2-1">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of a UP (any UP unit) level textbook/ audio-visual materials for one- semester course (recommended by unit for use in a course)</td>
                                            <td>20each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="20" required id="UpdateFC1-points-2-1">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-1">
                            <label class="control-label" for="selectMulti">Attachment(s)

                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-1" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-1" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-1" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>


                            <div class="form-group form-material form-material-file" id="c1BrowseDiv1">
                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="">
                            </div>


                        </div>


                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif


                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-1">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-1"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-1">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-1"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                {{-- CANCEL BUTTON --}}
                                <button type="submit" class="btn btn-success">Save Changes</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 2 --}}
                <div id="UpdateFC1-divID-2" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-2"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 2) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC1-title-2-2">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required id="UpdateFC1-author-2-2">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC1-date_published-2-2">
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required id="UpdateFC1-publisher-2-2">
                        </div>

                        {{-- Course Title--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Course Title</label>
                            <input type="text" class="form-control" name="course_title"
                                   placeholder="Course Title" required id="UpdateFC1-course_title-2-2">
                        </div>

                        {{-- Semester Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Semester Used</label>
                            <select class="form-control" name="semester_used"
                                    placeholder="Semester Used" required id="UpdateFC1-semester_used-2-2">
                                <option value="First Semester"> First Semester
                                </option>
                                <option value="Second Semester">Second Semester</option>
                                <option value="Summer">Summer</option>
                            </select>
                        </div>

                        {{-- AY Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">AY Used</label>
                            <input class="form-control" name="ay_used"
                                   placeholder="AY Used" required id="UpdateFC1-ay_used-2-2">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of a UP laboratory/studies manual</td>
                                            <td>20each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="20"  required id="UpdateFC1-points-2-2">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-2">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-2" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-2" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-2" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- cat1BrowseDiv --}}
                            </label>

                            <div class="form-group form-material form-material-file" id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>

                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif

                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-2">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-2"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-2">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-2"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 3 --}}
                <div id="UpdateFC1-divID-3" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-3"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 3) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC1-title-2-3">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required id="UpdateFC1-author-2-3">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC1-date_published-2-3">
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required id="UpdateFC1-publisher-2-3">
                        </div>

                        {{-- Course Title--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Course Title</label>
                            <input type="text" class="form-control" name="course_title"
                                   placeholder="Course Title" required id="UpdateFC1-course_title-2-3">
                        </div>

                        {{-- Semester Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Semester Used</label>
                            <select class="form-control" name="semester_used"
                                    placeholder="Semester Used" required id="UpdateFC1-semester_used-2-3">
                                <option value="First Semester"> First Semester
                                </option>
                                <option value="Second Semester">Second Semester</option>
                                <option value="Summer">Summer</option>
                            </select>
                        </div>

                        {{-- AY Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">AY Used</label>
                            <input class="form-control" name="ay_used"
                                   placeholder="AY Used" required id="UpdateFC1-ay_used-2-3">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of Modules (including wrap-around text)</td>
                                            <td>8each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="8" required id="UpdateFC1-points-2-3">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-3">

                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-3" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-3" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-3" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- c1BrowseDiv --}}
                            </label>


                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>

                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif

                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-3">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-3"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-3">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-3"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 4 --}}
                <div id="UpdateFC1-divID-4" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-4"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 4) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC1-title-2-4">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required id="UpdateFC1-author-2-4">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC1-date_published-2-4">
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required id="UpdateFC1-publisher-2-4">
                        </div>

                        {{-- Course Title--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Course Title</label>
                            <input type="text" class="form-control" name="course_title"
                                   placeholder="Course Title" id="UpdateFC1-course_title-2-4">
                        </div>

                        {{-- Semester Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Semester Used</label>
                            <select class="form-control" name="semester_used"
                                    placeholder="Semester Used" required id="UpdateFC1-semester_used-2-4">
                                <option value="First Semester"> First Semester
                                </option>
                                <option value="Second Semester">Second Semester</option>
                                <option value="Summer">Summer</option>
                            </select>
                        </div>

                        {{-- AY Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">AY Used</label>
                            <input class="form-control" name="ay_used"
                                   placeholder="AY Used" required id="UpdateFC1-ay_used-2-4">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Revision/updating of UP textbook/laboratory/studio manuals </td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC1-points-2-4">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-4">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-4" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-4" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-4" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- c1BrowseDiv --}}
                            </label>

                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-4">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-4"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-4">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-4"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>


                {{-- SUBCATEGORY 5 --}}
                <div id="UpdateFC1-divID-5" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-5"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 5) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC1-title-2-5">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required id="UpdateFC1-author-2-5">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC1-date_published-2-5">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of book review </td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC1-points-2-5">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-5">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-5" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-5" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-5" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- id="c1BrowseDiv" --}}
                            </label>

                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-5">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-5"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-5">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            {{-- BUTTONS --}}
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-5"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 6 --}}
                <div id="UpdateFC1-divID-6" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-6"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 6) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC1-title-2-6">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required id="UpdateFC1-author-2-6">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC1-date_published-2-6">
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required id="UpdateFC1-publisher-2-6">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of case studies (Case studies are classroom-tested and with signed company release)</td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC1-points-2-6">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-6">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-6" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-6" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-6" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- id="c1BrowseDiv" --}}
                            </label>


                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif

                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-6">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-6"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-6">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-6"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 7 --}}
                <div id="UpdateFC1-divID-7" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-7"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 7) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required id="UpdateFC1-title-2-7">
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required id="UpdateFC1-author-2-7">
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required id="UpdateFC1-date_published-2-7">
                        </div>

                        {{-- Title, vol. No. of Journal/Publication where annotated bibliography appears --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of Journal/Publication Where
                                Annotated Bibliography Appears</label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication"
                                   required id="UpdateFC1-bibliography-2-7">
                        </div>

                        {{-- Nature Of Publication --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Nature Of Publication (e.g
                                international) </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature Of Publication" required id="UpdateFC1-nature_ofpublication-2-7">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of annotated bibliography</td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required id="UpdateFC1-points-2-7">
                        </div>
                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-7">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-7" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-7" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-7" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- id="c1BrowseDiv" --}}
                            </label>


                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif


                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-7">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-7"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-7">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-7"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 8 --}}
                <div id="UpdateFC1-divID-8" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-8"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 8) !!}
                        {{-- Title Of Material --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Title Of Material</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title Of Material" required id="UpdateFC1-title-2-8">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Department-endorsed audio-visual materials/course readers/laboratory/studio/manuals/learning object/computer software for instruction</td>
                                            <td>4each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required id="UpdateFC1-points-2-8">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-8">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-8" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-8" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-8" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- id="c1BrowseDiv" --}}
                            </label>


                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-8">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-8"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-8">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-8"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>


                {{-- SUBCATEGORY 9 --}}
                <div id="UpdateFC1-divID-9" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-9"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 9) !!}
                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="as_what" id="UpdateFC1-as_what-2-9">
                                <option value="As Adviser/Co-adviser">9.1 As Adviser/Co-adviser</option>
                                <option value="As Reader/Panelist/Examiner ">9.2 As Reader/Panelist/Examiner</option>

                            </select>
                        </div>


                        {{-- Name of Graduate --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Graduate</label>
                            <input type="text" class="form-control" name="name_ofgraduate"
                                   placeholder="Name of Graduate" required id="UpdateFC1-name_ofgraduate-2-9">
                        </div>

                        {{-- Year Graduated--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Year Graduated</label>
                            <input type="number" class="form-control" name="year_graduated"
                                   placeholder="Year Graduated" required id="UpdateFC1-year_graduated-2-9">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>As Adviser/Co-adviser</td>
                                            <td>10each</td>
                                        </tr>

                                        <tr>
                                            <td>As Reader/Panelist/Examiner  </td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="10" required id="UpdateFC1-points-2-9">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-6 form-group" id="UpdateFC1-proofs-2-9">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-9" name="proofAction" checked="" value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-9" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-9" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- id="c1BrowseDiv" --}}
                            </label>


                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif

                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-9">
                                    <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-9"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-9">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-9"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 10 --}}
                <div id="UpdateFC1-divID-10" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-10"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 10) !!}
                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="as_what" id="UpdateFC1-as_what-2-10">
                                <option value="As Thesis Adviser/Co-Adviser ">10.1 As Thesis Adviser/Co-Adviser</option>
                                <option value="As Thesis Adviser/Co-Adviser ">10.2 As Thesis Adviser/Co-Adviser</option>
                                <option value="As Thesis Examiner">10.3 As Thesis Examiner</option>
                                <option value="As Strategic Plan Panelist for Master of Management (max of 6 students per year) ">
                                    10.4 As Strategic Plan Panelist for Master of Management (max of 6 students per
                                    year)
                                </option>
                            </select>
                        </div>

                        {{-- Name of Graduate --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Graduate</label>
                            <input type="text" class="form-control" name="name_ofgraduate"
                                   placeholder="Name of Graduate" required id="UpdateFC1-name_ofgraduate-2-10">
                        </div>

                        {{-- Year Graduated--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Year Graduated</label>
                            <input type="number" class="form-control" name="year_graduated"
                                   placeholder="Year Graduated" required id="UpdateFC1-year_graduated-2-10">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>As Thesis Adviser/Co-Adviser</td>
                                            <td>8each</td>
                                        </tr>

                                        <tr>
                                            <td>As Thesis  Reader/Panelist/Critic</td>
                                            <td>4each</td>
                                        </tr>

                                        <tr>
                                            <td>As Thesis Examiner </td>
                                            <td>4each</td>
                                        </tr>

                                        <tr>
                                            <td>As Strategic Plan Panelist for Master of Management (max of 6 students per year)  </td>
                                            <td>4each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="8" required id="UpdateFC1-points-2-10">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-10">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-10" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-10" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-10" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- id="c1BrowseDiv" --}}
                            </label>


                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group">
                                    <div class="col-md-6" align="left" id="evaluationButtons">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif

                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-10">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC1-delete-2-10"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-10">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-10"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>


                {{-- SUBCATEGORY 11 --}}
                <div id="UpdateFC1-divID-11" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-11"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 11) !!}
                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="as_what" id="UpdateFC1-as_what-2-11">
                                <option value="As Adviser/Co-Adviser  (6 units)">11.1 As Adviser/Co-Adviser (6 units)
                                </option>

                                <option value="As Adviser/Co-Adviser  (3 units)">11.3 As Adviser/Co-Adviser (3 units)
                                </option>

                                <option value="As Reader/Panelist/Critic ">11.5 As Reader/Panelist/Critic</option>

                            </select>
                        </div>

                        {{-- Name of Graduate --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Graduate</label>
                            <input type="text" class="form-control" name="name_ofgraduate"
                                   placeholder="Name of Graduate" required id="UpdateFC1-name_ofgraduate-2-11">
                        </div>

                        {{-- Year Graduated--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Year Graduated</label>
                            <input type="number" class="form-control" name="year_graduated"
                                   placeholder="Year Graduated" required id="UpdateFC1-year_graduated-2-11">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>As Adviser /Co-Adviser (6 units</td>
                                            <td>6each</td>
                                        </tr>

                                        <tr>
                                            <td>As Adviser/Co-Adviser (3 units) </td>
                                            <td>3each</td>
                                        </tr>

                                        <tr>
                                            <td>As Reader/Panelist/Critic  </td>
                                            <td>2ach</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="6" required id="UpdateFC1-points-2-11">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-11">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-11" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-11" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-11" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- id="c1BrowseDiv" --}}
                            </label>


                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif


                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-11">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC1-delete-2-11"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-11">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-11"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>


                {{-- SUBCATEGORY 12 --}}
                <div id="UpdateFC1-divID-12" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-12"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 12) !!}

                        {{-- Number of Preparations--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Number of Preparations</label>
                            <input type="number" step="1" class="form-control" name="number_ofpreparations"
                                   placeholder="Number of Preparations" required
                                   id="UpdateFC1-number_ofpreparations-2-12">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1 preparation</td>
                                            <td>2pts</td>
                                        </tr>

                                        <tr>
                                            <td>2 preparations</td>
                                            <td>3pts</td>
                                        </tr>

                                        <tr>
                                            <td>3 preparations</td>
                                            <td>4pts</td>
                                        </tr>

                                        <tr>
                                            <td>4 preparations</td>
                                            <td>5pts</td>
                                        </tr>

                                        <tr>
                                            <td>5 or more preparations</td>
                                            <td>6pts</td>
                                        </tr>
                                        

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="6" required id="UpdateFC1-points-2-12">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-12">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-12" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-12" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-12" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                            </label>

                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-12">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC1-delete-2-12"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-12">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-12"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        @endif
                    </form>
                </div>

                {{-- SUBCATEGORY 13 --}}
                <div id="UpdateFC1-divID-13" class="UpdateFC1-divClass-2" style="display:none;">
                    <form action="#" method="POST" id="UpdateFC1-form-2-13"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 13) !!}
                        {{-- Other Contributions --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Contributions</label>
                            <input type="text" class="form-control" name="other_contributions"
                                   placeholder="Other Contributions" required id="UpdateFC1-other_contributions-2-13">
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Other contributions to teaching to be determined by the Institute/Department</td>
                                            <td>5max/year</td>
                                        </tr>

            
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0"  max="5" required id="UpdateFC1-points-2-13">
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group" id="UpdateFC1-proofs-2-13">
                            <label class="control-label" for="selectMulti">Attachment(s)
                                <ul class="list-unstyled list-inline">
                                    <li data-toggle="tooltip" data-original-title="Do nothing or Add more attachments">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="c1radio1-2-13" name="proofAction" checked=""
                                                   value=1>
                                            <label for="inputRadioNormal">Do nothing/Add more</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove and Replace all attachments">
                                        <div class="radio-custom radio-success">
                                            <input type="radio" id="c1radio2-2-13" name="proofAction" value=2>
                                            <label for="inputRadioNormal">Remove and Replace</label>
                                        </div>
                                    </li>
                                    <li data-toggle="tooltip" data-original-title="Remove all attachments"
                                        for="inputRadioNormal">
                                        <div class="radio-custom radio-danger">
                                            <input type="radio" id="c1radio3-2-13" name="proofAction" value=3>
                                            <label>Remove all</label>
                                        </div>
                                    </li>

                                </ul>
                                {{-- id="c1BrowseDiv" --}}
                            </label>


                            <div class="form-group form-material form-material-file " id="c1BrowseDiv">
                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileUpdateCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        @if(Request::is('*/EvaluateForm'))
                            {!! Form::hidden('action', 'EvaluateForm') !!}
                            <div class="row">
                                <div class="col-lg-12 form-group" id="evaluationButtons">
                                    <div class="col-md-6" align="left">
                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" checked="checked" value="1"
                                               data-points="0"/>
                                        <span>Accept</span> &nbsp;&nbsp;

                                        <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                               data-plugin="labelauty" data-label="false" value="2" data-points="0"/>
                                        <span>Reject</span> &nbsp;&nbsp;

                                        @if(Auth::user()->position==1)
                                            <input type="radio" class="to-labelauty-icon" name="evaluateAction"
                                                   data-plugin="labelauty" data-label="false" value="3"
                                                   data-points="0"/>
                                            <span>Revise</span>
                                        @endif

                                    </div>
                                    <div class="col-md-6" align="right">
                                        <button type="submit" class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        @elseif(Request::is('*/ReviseForm'))
                            {!! Form::hidden('action', 'ReviseForm') !!}
                            <div class="modal-footer">
                                {{-- SUBMIT BUTTON --}}
                                <div id="UpdateFC1-reviseDiv-2-13">
                                    <a href="#" class="btn btn-default btn-dark" role="button"
                                       id="UpdateFC1-delete-2-13"
                                       onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                        Entry</a>
                                    {{-- CANCEL BUTTON --}}
                                    <button type="submit" class="btn btn-success" id="UpdateFC1-revise-2-13">Revise
                                    </button>
                                </div>
                            </div>
                        @else
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default btn-dark" role="button" id="UpdateFC1-delete-2-13"
                                   onclick="if (! confirm('Are you sure do you want remove this entry?')) { return false; }">Delete
                                    Entry</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                    </form>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>


@if(Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') ||Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $("[id^=UpdateFC1-]").attr('disabled', true);
        $("[id^=UpdateFC1-proofs-]").css('display', "none");
        $(".btn").css('display', "none");
        @if(Auth::user()->position==4)
            $("[id^=chancellorEvaluation-]").css('display', "block");
        @endif
    </script>
@endif

@if(Request::is('*/EvaluateForm'))
    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $("[id^=UpdateFC1-]").attr('disabled', true);
        $("[id^=UpdateFC1-proofs-]").css('display', "none");
    </script>

    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("input[name*='evaluateAction']").click(function (e) {
                // accept
                if (this.value == 1) {
                    $("[id^=UpdateFC1-points-]").attr('disabled', false);
                    $("[id^=UpdateFC1-points-]").css('background-color', '#FFEFD5');
                    $("[id^=UpdateFC1-points-]").css('color', 'black');
                }

                // reject
                else if (this.value == 2 || this.value == 3) {
                    points = $("[id^=labelauty]").attr("data-points");
                    $("[id^=UpdateFC1-points]").val(points);
                    $("[id^=UpdateFC1-points]").attr('disabled', true);
                    $("[id^=UpdateFC1-points-]").css('background-color', '');
                    $("[id^=UpdateFC1-points-]").css('color', '');
                }
            });
        });
    </script>
@endif


<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=c1radio]").click(function (e) {
            if (this.value == 3) {
                $("[id^=c1BrowseDiv]").css('display', 'none');
            } else {
                $("[id^=c1BrowseDiv]").css('display', 'block');
            }
        });
    });
</script>


<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=inputFileUpdateCat1]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }

            }
        });
    });
</script>
