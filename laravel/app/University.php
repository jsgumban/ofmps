<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $table = 'universities';
    public $timestamps = false;
    protected $fillable = ['university_name', 'chancellor', 'vchancellor'];
}
