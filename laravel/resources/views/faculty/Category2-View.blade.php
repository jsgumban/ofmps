<div class="tab-pane" id="category-2" role="tabpanel" name="panel2">

    <div class="btn-group pull-right">
        <i class="icon wb-fullscreen" aria-hidden="true" role="button" onclick="shrink2()" style="font-size: 18px;"
           id="maxmin2" data-toggle="tooltip" title="Expand/Shrink"></i>
    </div>


    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">

        <h3><b>II. TEACHING PERFORMANCE/OUTPUT</b></h3>

        <div class="example">


            <h4><b>A. PUBLISHED SCHOLARLY WORK </b></h4>
            <h5>1. Authorship of a paper in a refereed publication </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published</th>
                        <th>Title, Vol. No. of Journal/Publication</th>
                        <th>Nature of Publication</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($creativeworks as $creativework)
                        <?php $counterOuter++; ?>
                        @if($creativework->category==1 && $creativework->subcategory==1)


                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-1-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-1-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->title}}</td>
                                <td id="{{'UpdateFC2-tr-1-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->author}}</td>
                                <td id="{{'UpdateFC2-tr-1-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->date_published}}</td>
                                <td id="{{'UpdateFC2-tr-1-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->bibliography}}</td>
                                <td id="{{'UpdateFC2-tr-1-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->nature_ofpublication}}</td>
                                <td id="{{'UpdateFC2-tr-1-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->other_details}}</td>
                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-1-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>


            <h5>2. Presentation of a paper in an international conference </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date and Place of Presentation</th>
                        <th>Title of Conference</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==1 && $creativework->subcategory==2)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td
                                        id="{{'UpdateFC2-tr-1-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                        data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td
                                        id="{{'UpdateFC2-tr-1-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                        data-toggle="modal">{{$creativework->title}}</td>
                                <td
                                        id="{{'UpdateFC2-tr-1-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                        data-toggle="modal">{{$creativework->author}}</td>
                                <td
                                        id="{{'UpdateFC2-tr-1-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                        data-toggle="modal">{{$creativework->date_ofpresentation.'/'.$creativework->place_ofpresentation}}</td>
                                <td
                                        id="{{'UpdateFC2-tr-1-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                        data-toggle="modal">{{$creativework->title_ofconference}}</td>
                                <td
                                        id="{{'UpdateFC2-tr-1-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                        data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td
                                "id={{'UpdateFC2-tr-1-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                data-toggle="modal" style="text-align:right">
                                <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>

            <h5>3. Presentation of a paper in a national conference </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date and Place of Presentation</th>
                        <th>Title of Conference</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==1 && $creativework->subcategory==3)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-1-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-1-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->title}}</td>
                                <td id="{{'UpdateFC2-tr-1-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->author}}</td>
                                <td id="{{'UpdateFC2-tr-1-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->date_ofpresentation.'/'.$creativework->place_ofpresentation}}</td>
                                <td id="{{'UpdateFC2-tr-1-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->title_ofconference}}</td>
                                <td id="{{'UpdateFC2-tr-1-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-1-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach

                    </tbody>
                </table>
            </div>
            <br>

            <h5>4. Presentation of a paper at the department, college or university </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date and Place of Presentation</th>
                        <th>Title of Conference</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==1 && $creativework->subcategory==4)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-1-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-1-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->title}}</td>
                                <td id="{{'UpdateFC2-tr-1-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->author}}</td>
                                <td id="{{'UpdateFC2-tr-1-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->date_ofpresentation.'/'.$creativework->place_ofpresentation}}</td>
                                <td id="{{'UpdateFC2-tr-1-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->title_ofconference}}</td>
                                <td id="{{'UpdateFC2-tr-1-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-1-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h4><b>B. CREATIVE WORK</b></h4>

            <h5>1. Literature</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date of Publication/Publisher/Nature of Publication</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==2 && $creativework->subcategory==1)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-2-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-2-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->title}}</td>
                                <td id="{{'UpdateFC2-tr-2-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal"> {{$creativework->author}}</td>
                                <td id="{{'UpdateFC2-tr-2-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->date_published.'/'.$creativework->publisher.'/'.$creativework->nature_ofpublication}}</td>
                                <td id="{{'UpdateFC2-tr-2-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-2-1-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach

                    </tbody>
                </table>
            </div>
            <br>

            <h5>2. Fine Arts</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>

                        <th>Date and Place of Publication/Exhibition</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==2 && $creativework->subcategory==2)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-2-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-2-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->title}}</td>

                                <td id="{{'UpdateFC2-tr-2-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->date_ofpresentation.'/'.$creativework->place_ofpresentation}}</td>
                                <td id="{{'UpdateFC2-tr-2-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-2-2-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach

                    </tbody>
                </table>
            </div>
            <br>

            <h5>3. Music and Dance</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==2 && $creativework->subcategory==3)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-2-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-2-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->title}}</td>

                                <td id="{{'UpdateFC2-tr-2-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-2-3-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach

                    </tbody>
                </table>
            </div>
            <br>

            <h5>4. Architecture</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==2 && $creativework->subcategory==4)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-2-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-2-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->title}}</td>

                                <td id="{{'UpdateFC2-tr-2-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-2-4-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach

                    </tbody>
                </table>
            </div>
            <br>

            <h5>5. Theatre Arts</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==2 && $creativework->subcategory==5)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-2-5-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-2-5-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->title}}</td>

                                <td id="{{'UpdateFC2-tr-2-5-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-2-5-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach

                    </tbody>
                </table>
            </div>
            <br>

            <h5>6. Radio, Television and Related Media</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==2 && $creativework->subcategory==6)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-2-6-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-2-6-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->title}}</td>

                                <td id="{{'UpdateFC2-tr-2-6-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-2-6-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>

            <h5>7. Film</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        @if($creativework->category==2 && $creativework->subcategory==7)
                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-2-7-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-2-7-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->title}}</td>

                                <td id="{{'UpdateFC2-tr-2-7-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-2-7-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>

            <h5>8. Scholarly Work in the Arts</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published</th>
                        <th>Title, Vol. No. of Journal/Publication</th>
                        <th>Nature of Publication</th>
                        <th>Other Details(s)</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right"> Points</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($creativeworks as $creativework)
                        <?php $counterOuter++; ?>
                        @if($creativework->category==2 && $creativework->subcategory==8)


                            <tr role="button" data-isaccepteddeptchair="{{$creativework->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$creativework->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$creativework->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC2-tr-2-8-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$creativework->id}} data-isaccepteddeptchair =
                                    "{{$creativework->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$creativework->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$creativework->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden
                                    ="true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$creativework->subsubcategory}}</td>
                                <td id="{{'UpdateFC2-tr-2-8-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->title}}</td>
                                <td id="{{'UpdateFC2-tr-2-8-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->author}}</td>
                                <td id="{{'UpdateFC2-tr-2-8-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->date_published}}</td>
                                <td id="{{'UpdateFC2-tr-2-8-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->bibliography}}</td>
                                <td id="{{'UpdateFC2-tr-2-8-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->nature_ofpublication}}</td>
                                <td id="{{'UpdateFC2-tr-2-8-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal">{{$creativework->other_details}}</td>
                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $creativework->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC2-tr-2-8-'.$creativework->id}}" data-target="#UpdateFC2-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-info">{{$creativework->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>
        </div>
    </div>
</div>

{{-- SHRINKING AND EXPANDING TABLE PANELS --}}
<script type="text/javascript">
    function shrink2() {
        document.getElementById("shrink").style.display = "none";
        document.getElementById("maximize").setAttribute("class", "col-lg-12 col-sm-12");
        document.getElementById("maxmin2").setAttribute("class", "icon wb-fullscreen-exit");
        document.getElementById("maxmin2").setAttribute("onclick", "expand2()");
        // col-lg-8 col-sm-8
    }
    function expand2() {
        document.getElementById("shrink").style.display = "block";
        document.getElementById("maximize").setAttribute("class", "col-lg-8 col-sm-8");
        document.getElementById("maxmin2").setAttribute("class", "icon wb-fullscreen");
        document.getElementById("maxmin2").setAttribute("onclick", "shrink2()");
    }
</script>