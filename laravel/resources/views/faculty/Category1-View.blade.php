<div class="tab-pane active " id="category-1" role="tabpanel" name="panel1">

    <div class="btn-group pull-right">
        <i class="icon wb-fullscreen" aria-hidden="true" role="button" onclick="shrink1()" style="font-size: 18px;"
           id="maxmin1" data-toggle="tooltip" title="Expand/Shrink"></i>
    </div>

    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">

        <h3><b>I. TEACHING PERFORMANCE/OUTPUT</b></h3>

        <div class="example">
            <h4><b>A. PERFORMANCE RATING </b></h4>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>WEIGHTED AVERAGE RATING</th>
                        <th style="text-align:right">Points</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 0 ?>
                    @foreach ($performances as $performance)

                        @if($performance->category==1 && $performance->subcategory==1 )
                            <tr id="{{'UpdateFC1-tr-1-1-'.$performance->id}}" data-target="#UpdateFC1-modal-1"
                                data-toggle="modal" role="button"
                                data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td>
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{ $performance->WARating }}
                                </td>
                                <td style="text-align:right">
                                    <div class="label label-table label-success">
                                        {{ $performance->points }}
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h4><b>B. TEACHING PORTFOLIO/OTHER ACTIVITIES RELATED TO TEACHING</b>

            </h4>

            <h5>1. Authorship of a UP (any UP unit) level textbook /audio-visual materials for one- semester course
                (recommended by unit for use in a course)</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published/Publisher</th>
                        <th>Course Title</th>
                        <th>Semester/AY used</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>
                    @foreach ($performances as $performance)
                        <?php $counterOuter++; ?>
                        @if($performance->category==2 && $performance->subcategory==1 )
                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-1-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->title}}</td>
                                <td id="{{'UpdateFC1-tr-2-1-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->author}}</td>
                                <td id="{{'UpdateFC1-tr-2-1-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                                <td id="{{'UpdateFC1-tr-2-1-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->course_title}}</td>
                                <td id="{{'UpdateFC1-tr-2-1-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->semester_used.'/ '.$performance->ay_used}}</td>


                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file!=null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>

                                <td style="text-align:right"
                                    id="{{'UpdateFC1-tr-2-1-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>2. Authorship of a UP laboratory/studies manual</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published/ Publisher</th>
                        <th>Course Title</th>
                        <th>Semester/AY used</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==2 )

                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-2-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-isdeptChairAccepted="{{$performance->isAccepted_deptchair}}"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->title}}</a></td>
                                <td id="{{'UpdateFC1-tr-2-2-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->author}}</td>
                                <td id="{{'UpdateFC1-tr-2-2-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                                <td id="{{'UpdateFC1-tr-2-2-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->course_title}}</td>
                                <td id="{{'UpdateFC1-tr-2-2-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->semester_used.'/ '.$performance->ay_used}}</td>


                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>
                                <td id="{{'UpdateFC1-tr-2-2-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>3. Authorship of Modules (including wrap around text)</h5>

            <div class="table-responsive ">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published/Publisher</th>
                        <th>Course Title</th>
                        <th>Semester/AY used</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==3 )
                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-3-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->title}}</a></td>
                                <td id="{{'UpdateFC1-tr-2-3-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->author}}</td>
                                <td id="{{'UpdateFC1-tr-2-3-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                                <td id="{{'UpdateFC1-tr-2-3-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->course_title}}</td>
                                <td id="{{'UpdateFC1-tr-2-3-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->semester_used.'/ '.$performance->ay_used}}</td>
                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>
                                <td style="text-align:right" id="{{'UpdateFC1-tr-2-3-'.$performance->id}}"
                                    data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>

            <h5>4. Revision/updating of UP textbook/laboratory/studio manuals</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published/Publisher</th>
                        <th>Course Title</th>
                        <th>Semester/AY used</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==4 )

                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-4-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>  {{$performance->title}}</a></td>

                                <td id="{{'UpdateFC1-tr-2-4-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->author}}</td>
                                <td id="{{'UpdateFC1-tr-2-4-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                                <td id="{{'UpdateFC1-tr-2-4-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->course_title}}</td>
                                <td id="{{'UpdateFC1-tr-2-4-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->semester_used.'/ '.$performance->ay_used}}</td>

                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>
                                <td id="{{'UpdateFC1-tr-2-4-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>5. Authorship of book review</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==5 )

                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-5-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->title}}</a></td>
                                <td id="{{'UpdateFC1-tr-2-5-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->author}}</td>
                                <td id="{{'UpdateFC1-tr-2-5-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{date('F d, Y', strtotime( $performance->date_published ))}}</td>

                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>
                                <td id="{{'UpdateFC1-tr-2-5-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach

                    </tbody>
                </table>
            </div>
            <br>


            <h5>6. Authorship of case studies </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published/Publisher</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==6 )

                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-6-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->title}}</a></td>
                                <td id="{{'UpdateFC1-tr-2-6-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->author}}</td>
                                <td id="{{'UpdateFC1-tr-2-6-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{date('F d, Y', strtotime( $performance->date_published )).'/ '.$performance->publisher}}</td>
                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>
                                <td id="{{'UpdateFC1-tr-2-6-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>7. Authorship of annotated bibliography</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published</th>
                        <th>Title, Vol. No. of Journal/Publication
                            <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other information"
                               data-animation="pop" data-target="webuiPopover"
                               data-content="
                                        <small>
                                            <p> Title, Vol. No. of Journal/Publication where annotated bibliography appears
                                            </p>
                                            
                                        </small>">
                                <i class="icon wb-info-circle" aria-hidden="true"></i>
                            </a>
                        </th>
                        <th>Nature of Publication</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==7 )


                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-7-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->title}}</a></td>
                                <td id="{{'UpdateFC1-tr-2-7-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">{{$performance->author}}</td>
                                <td id="{{'UpdateFC1-tr-2-7-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{date('F d, Y', strtotime( $performance->date_published ))}}</td>
                                <td id="{{'UpdateFC1-tr-2-7-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{ $performance->bibliography }}</td>
                                <td id="{{'UpdateFC1-tr-2-7-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{ $performance->nature_ofpublication }}</td>

                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>
                                <td id="{{'UpdateFC1-tr-2-7-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>8. Department endorsed audio-visual materials/course readers/laboratory/studio/manual/learning
                objects/computer software for instruction </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title Material</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==8)


                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-8-'.$performance->id}} " data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->title}}</a></td>
                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>
                                <td id="{{'UpdateFC1-tr-2-8-'.$performance->id}} " data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>9. For each dissertation advisee graduated</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>As</th>
                        <th>Name Of Graduate</th>
                        <th>Year Graduated</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==9)


                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-9-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->as_what}}</a></td>
                                <td id="{{'UpdateFC1-tr-2-9-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{$performance->name_ofgraduate}} </td>
                                <td id="{{'UpdateFC1-tr-2-9-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{$performance->year_graduated}} </td>


                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>

                                <td id="{{'UpdateFC1-tr-2-9-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>10. For each M.S. thesis advisee graduated</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>As</th>
                        <th>Name Of Graduate</th>
                        <th>Year Graduated</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==10)


                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-10-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->as_what}}</a></td>
                                <td id="{{'UpdateFC1-tr-2-10-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{$performance->name_ofgraduate}} </td>
                                <td id="{{'UpdateFC1-tr-2-10-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{$performance->year_graduated}} </td>


                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>

                                <td id="{{'UpdateFC1-tr-2-10-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>11. Undergraduate Thesis/Special Problem Adviser: for every undergraduate thesis advisee who graduated
                (max of 6 students per year)</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>As</th>
                        <th>Name Of Graduate</th>
                        <th>Year Graduated</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==11)


                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-11-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->as_what}}</a></td>
                                <td id="{{'UpdateFC1-tr-2-11-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{$performance->name_ofgraduate}} </td>
                                <td id="{{'UpdateFC1-tr-2-11-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal"> {{$performance->year_graduated}} </td>


                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>

                                <td id="{{'UpdateFC1-tr-2-11-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>12. Number of preparations per year; (SATE not lower than 2.0 in the combination of courses)</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th># of Preparations</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==12)


                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-12-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->number_ofpreparations}}</a></td>

                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach


                                </td>

                                <td id="{{'UpdateFC1-tr-2-12-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>


            <h5>13. Other contributions to teaching </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Other Contributions</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($performances as $performance)

                        @if($performance->category==2 && $performance->subcategory==13)


                            <tr role="button" data-isaccepteddeptchair="{{$performance->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$performance->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$performance->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC1-tr-2-13-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$performance->id}} data-isaccepteddeptchair =
                                    "{{$performance->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$performance->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$performance->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden=
                                    "true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$performance->other_contributions}}</a></td>


                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $performance->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC1-tr-2-13-'.$performance->id}}" data-target="#UpdateFC1-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-success">{{$performance->points}}</div>
                                </td>
                            </tr>

                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>
        </div>
    </div>
</div>


<script type="text/javascript">
    function shrink1() {
        document.getElementById("shrink").style.display = "none";
        document.getElementById("maximize").setAttribute("class", "col-lg-12 col-sm-12");
        document.getElementById("maxmin1").setAttribute("class", "icon wb-fullscreen-exit");
        document.getElementById("maxmin1").setAttribute("onclick", "expand1()");
    }

    function expand1() {
        document.getElementById("shrink").style.display = "block";
        document.getElementById("maximize").setAttribute("class", "col-lg-8 col-sm-8");
        document.getElementById("maxmin1").setAttribute("class", "icon wb-fullscreen");
        document.getElementById("maxmin1").setAttribute("onclick", "shrink1()");
    }
</script>


