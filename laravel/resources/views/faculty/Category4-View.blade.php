<div class="tab-pane" id="category-4" role="tabpanel">
    <div class="btn-group pull-right">
        <i class="icon wb-fullscreen" aria-hidden="true" role="button" onclick="shrink4()" style="font-size: 18px;"
           id="maxmin4" data-toggle="tooltip" title="Expand/Shrink"></i>
    </div>

    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">
        <h3><b>IV. PROFESSIONAL GROWTH</b></h3>

        <div class="example">
            <h5>1. Workshop/Training/Conference Organizer or Chair (maximum of four (4) activities/year) </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Workshop/Training/Conference</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>
                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==1)
                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-1-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-1-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->name_ofevent}}</td>
                                <td id="{{'UpdateFC4-tr-1-1-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>
                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>
                                <td id="{{'UpdateFC4-tr-1-1-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>

            <h5>2. Authorship of a reference book in one's field of discipline </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Title of the Reference Book</th>
                        <th>Author(s)</th>
                        <th>Date Published</th>
                        <th>Title, Vol. No. of Journal/Publication</th>
                        <th> Nature of Publication</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>
                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==2)
                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-2-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-2-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->author}}</td>
                                <td id="{{'UpdateFC4-tr-1-2-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_published ))}}</td>
                                <td id="{{'UpdateFC4-tr-1-2-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->bibliography}}</td>
                                <td id="{{'UpdateFC4-tr-1-2-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->nature_ofpublication}}</td>
                                <td>{{$pgrowth->other_details}}</td>
                                <td>
                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                        @endif
                                    @endforeach
                                </td>
                                <td id="{{'UpdateFC4-tr-1-2-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <br>

            <h5>3. Authorship of a college-level textbook/laboratory/course manual </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published</th>
                        <th>Title, Vol. No. of Journal/Publication</th>
                        <th> Nature of Publication</th>


                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==3)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-3-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-3-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-3-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->author}}</td>
                                <td id="{{'UpdateFC4-tr-1-3-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_published))}}</td>
                                <td id="{{'UpdateFC4-tr-1-3-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->bibliography}}</td>
                                <td id="{{'UpdateFC4-tr-1-3-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->nature_ofpublication}}</td>

                                <td id="{{'UpdateFC4-tr-1-3-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-3-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>4. Authorship of High School and Elementary Manual/textbook</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published</th>
                        <th>Title, Vol. No. of Journal/Publication</th>
                        <th>Nature</th>


                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==4)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-4-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-4-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-4-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->author}}</td>
                                <td id="{{'UpdateFC4-tr-1-4-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_published))}}</td>
                                <td id="{{'UpdateFC4-tr-1-4-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->bibliography}}</td>
                                <td id="{{'UpdateFC4-tr-1-4-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->nature_ofpublication}}</td>

                                <td id="{{'UpdateFC4-tr-1-4-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-4-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>5. Popular presentations and popularized lectures on topics within the discipline (Maximum of 12
                presentations / articles per year) </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Author(s)</th>
                        <th>Date Published/Lectured</th>
                        <th>Name of Event</th>
                        <th>Nature</th>


                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==5)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-5-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-5-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-5-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->author}}</td>
                                <td id="{{'UpdateFC4-tr-1-5-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_published))}}</td>
                                <td id="{{'UpdateFC4-tr-1-5-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->bibliography}}</td>
                                <td id="{{'UpdateFC4-tr-1-5-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->nature_ofpublication}}</td>

                                <td id="{{'UpdateFC4-tr-1-5-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-5-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>6. Resource person in print or broadcast media presentations </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Date and Place Presentation</th>
                        <th> Nature of Presentation</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==6)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-6-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-6-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-6-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                                <td id="{{'UpdateFC4-tr-1-6-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->nature_ofpublication}}</td>

                                <td id="{{'UpdateFC4-tr-1-6-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-6-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>7. Trainor/sports official within area of specialization (international/national) </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Training</th>
                        <th>Date and Place of Training</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==7)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-7-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-7-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-7-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                                <td id="{{'UpdateFC4-tr-1-7-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-7-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>8. Membership in technical committee </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Committee</th>
                        <th>Date of Appointment</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==8)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-8-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-8-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-8-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation))}}</td>
                                <td id="{{'UpdateFC4-tr-1-8-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-8-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>9. Advisorship to Professional Organization </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Organization</th>
                        <th>Date and Place</th>
                        <th>Nature of Organization</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==9)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-9-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-9-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-9-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                                <td id="{{'UpdateFC4-tr-1-9-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->nature_ofpublication}}</td>
                                <td id="{{'UpdateFC4-tr-1-9-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-9-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>10. Active Participation in outreach program related to own specialization (e.g. Pahinungod) </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Program</th>
                        <th>Participation</th>
                        <th>Date and Place</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==10)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-10-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-10-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-10-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->participation}}</td>
                                <td id="{{'UpdateFC4-tr-1-10-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                                <td id="{{'UpdateFC4-tr-1-10-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-10-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>11. Resource person in meetings/symposiums/extension activities related to issues within your
                discipline </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Activity</th>
                        <th>Participation</th>
                        <th>Date and Place</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==11)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-11-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-11-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-11-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->participation}}</td>
                                <td id="{{'UpdateFC4-tr-1-11-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>

                                <td>{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-11-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>12. Attendance in International/Regional meeting/conference/training course/workshop </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Title of Conference</th>
                        <th>Date and Place</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==12)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-12-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->title}}</td>

                                <td id="{{'UpdateFC4-tr-1-12-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>

                                <td id="{{'UpdateFC4-tr-1-12-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-12-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>13. Attendance in National/University meetings/conference/training course workshop</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Title of Conference</th>
                        <th>Date and Place</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==13)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-13-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->title}}</td>

                                <td id="{{'UpdateFC4-tr-1-13-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>

                                <td id="{{'UpdateFC4-tr-1-13-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-13-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>14. Highly specialized training related to discipline with contribution to the college (duration of at
                least 2 weeks)</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Title of Training</th>
                        <th>Date and Place</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==14)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-14-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->title}}</td>

                                <td id="{{'UpdateFC4-tr-1-14-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>

                                <td id="{{'UpdateFC4-tr-1-14-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-14-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>15. Patents </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Patent</th>
                        <th>Nature of Patent</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==15)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC4-tr-1-15-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-15-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-15-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->nature_ofpublication}}</td>
                                <td id="{{'UpdateFC4-tr-1-15-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-15-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>16. Citations (indicate which journal)</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Name of Citation</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==16)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-16-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->title}}</td>


                                <td id="{{'UpdateFC4-tr-1-16-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>
                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-16-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>17. Awards</h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Category</th>
                        <th>Name of Award</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==17)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-17-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-17-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-17-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-17-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>18. Membership in Editorial Board</h5>
            <h6>Refereed Publication (journal, proceedings)/other print media</h6>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Category</th>
                        <th>Name of Publication</th>
                        <th>Nature of Publication</th>
                        <th>Period Covered</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==18)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-18-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-18-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-18-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->nature_ofpublication}}</td>
                                <td id="{{'UpdateFC4-tr-1-18-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->period_covered}}</td>
                                <td id="{{'UpdateFC4-tr-1-18-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-18-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>19. Referee to a publication </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>

                        <th>Category</th>
                        <th>Title of Book/Artice</th>
                        <th>Publisher/Journal Vol./No.</th>
                        <th>Date of Publication</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==19)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-19-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->subcategory}}</td>
                                <td id="{{'UpdateFC4-tr-1-19-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-19-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->bibliography}}</td>
                                <td id="{{'UpdateFC4-tr-1-19-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{date('F d, Y', strtotime($pgrowth->date_published))}}</td>
                                <td id="{{'UpdateFC4-tr-1-19-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-19-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>20. Research Fellowship/Visiting Professor Appointment in a reputable university (with MOA) </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>


                        <th>Name of University</th>
                        <th>Duration of Appointment</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==20)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-20-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-20-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->duration_ofappointment}}</td>
                                <td id="{{'UpdateFC4-tr-1-20-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-20-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>21. Other related services to the community (e.g. reviewer of research/creative work development of
                discipline or tertiary education) </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Other Services</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($pgrowths as $pgrowth)
                        <?php $counterOuter++; ?>
                        @if($pgrowth->category==21)


                            <tr role="button" data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">

                                <td id="{{'UpdateFC4-tr-1-21-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$pgrowth->id}} data-isaccepteddeptchair =
                                    "{{$pgrowth->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$pgrowth->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$pgrowth->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$pgrowth->title}}</td>
                                <td id="{{'UpdateFC4-tr-1-21-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal">{{$pgrowth->other_details}}</td>

                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $pgrowth->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC4-tr-1-21-'.$pgrowth->id}}" data-target="#UpdateFC4-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-danger">{{$pgrowth->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>


        </div>
    </div>
</div>


{{-- SHRINKING AND EXPANDING TABLE PANELS --}}
<script type="text/javascript">
    function shrink4() {
        document.getElementById("shrink").style.display = "none";
        document.getElementById("maximize").setAttribute("class", "col-lg-12 col-sm-12");
        document.getElementById("maxmin4").setAttribute("class", "icon wb-fullscreen-exit");
        document.getElementById("maxmin4").setAttribute("onclick", "expand4()");
        // col-lg-8 col-sm-8
    }
    function expand4() {
        document.getElementById("shrink").style.display = "block";
        document.getElementById("maximize").setAttribute("class", "col-lg-8 col-sm-8");
        document.getElementById("maxmin4").setAttribute("class", "icon wb-fullscreen");
        document.getElementById("maxmin4").setAttribute("onclick", "shrink4()");
    }
</script>