{{-- FORM CATEGORY 4 MODAL --}}
<div class="modal fade modal-danger  example-modal-lg" id="exampleModalWarning7" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">A. PROFESSIONAL GROWTH </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="FormCat4Cat1" onclick="FormCat4Cat1ShowDiv()"
                            onkeydown="FormCat4Cat1ShowDiv()"
                            onkeydown="FormCat4Cat1ShowDiv()" onkeypress="FormCat4Cat1ShowDiv()">
                        <option value=1>01. Workshop/Training/Conference Organizer or Chair
                        </option>
                        <option value=2>02. Authorship of a reference book in one's field of discipline</option>
                        <option value=3>03. Authorship of a college-level textbook/laboratory/course manual</option>
                        <option value=4>04. Authorship of High School and Elementary Manual/textbook</option>

                        <option value=5>05. Popular presentations and popularized lectures on topics within the
                            discipline
                        </option>
                        <option value=6>06. Resource person in print or broadcast media presentations</option>
                        <option value=7>07. Trainor/sports official within area of specialization</option>
                        <option value=8>08. Membership in technical committee</option>
                        <option value=9>09. Advisorship to Professional Organization</option>


                        <option value=10>10. Active Participation in outreach program related to own specialization
                        </option>
                        <option value=11>11. Resource person in meetings/symposiums/extension activities related to
                            issues within your discipline
                        </option>
                        <option value=12>12. Attendance in International/Regional meeting/conference/training
                            course/workshop
                        </option>
                        <option value=13>13. Attendance in National/University meetings/conference/training course
                            workshop
                        </option>
                        <option value=14>14. Highly specialized training related to discipline with contribution to the
                            college (duration of at least 2 weeks)
                        </option>
                        <option value=15>15. Patents</option>

                        <option value=16>16. Citations (indicate which journal)</option>
                        <option value=17>17. Awards</option>
                        <option value=18>18. Membership in Editorial Board</option>
                        <option value=19>19. Referee to a publication</option>
                        <option value=20>20. Research Fellowship/Visiting Professor Appointment in a reputable
                            university (with MOA)
                        </option>
                        <option value=21>21. Other related services to the community</option>
                    </select>
                </div>

                {{-- CATEGORY 1--}}
                <div id="1FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 1) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="Chair (International)">1.1. Chair (International)
                                </option>
                                <option value="Member (International)">1.2. Member (International)
                                </option>
                                <option value="Chair (National)">1.3. Chair (National)
                                </option>
                                <option value="Member (National)">1.4. Member (National)
                                </option>
                            </select>
                        </div>

                        {{-- Name of Workshop/Training/Conference --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Workshop/Training/Conference</label>
                            <input type="text" class="form-control" name="name_ofevent"
                                   placeholder="Name of Workshop/Training/Conference" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> Chair (International)</td>
                                            <td>4 each</td>

                                        </tr>
                                        <tr>
                                            <td>Member (International)</td>
                                            <td>2 each</td>
                                        </tr>

                                        <tr>
                                            <td>Chair (National)</td>
                                            <td>2.5 each</td>
                                        </tr>

                                        <tr>
                                            <td>Member (National)</td>
                                            <td>1 each</td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>


                {{-- CATEGORY 2--}}
                <div id="2FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}

                        {{-- Title of the Reference Book  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title of the Reference Book </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title of the Reference Book" required>
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required>
                        </div>

                        {{-- Date Published  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published/Lectured </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{--Title, Vol. No. of Journal/Publication   --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication" required>
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of a reference book in one's field of discipline</td>
                                            <td>4 each</td>

                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 3--}}
                <div id="3FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 3) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="Textbook">3.1. Textbook
                                </option>
                                <option value="Manual">3.2. Manual
                                </option>

                            </select>
                        </div>

                        {{-- Title  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required>
                        </div>

                        {{-- Date Published  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{--Title, Vol. No. of Journal/Publication   --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication" required>
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Textbook  </td>
                                            <td>5 each</td>
                                        </tr>

                                        <tr>
                                            <td>Manual   </td>
                                            <td>4 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 4--}}
                <div id="4FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 4) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="Textbook">4.1. Textbook
                                </option>
                                <option value="Manual">4.2. Manual
                                </option>
                            </select>
                        </div>

                        {{-- Title  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required>
                        </div>

                        {{-- Date Published  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{--Title, Vol. No. of Journal/Publication   --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of
                                Journal/Publication </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication" required>
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Textbook  </td>
                                            <td>5 each</td>
                                        </tr>

                                        <tr>
                                            <td>Manual   </td>
                                            <td>4 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 5 --}}
                <div id="5FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 5) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="Authorship of articles">5.1. Authorship of articles
                                </option>
                                <option value="Popular lectures within the discipline">5.2. Popular lectures within the
                                    discipline
                                </option>

                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required>
                        </div>

                        {{-- Date Published  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published/Lectured </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{--Name of Publication/Conference/Seminar   --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of
                                Publication/Conference/Seminar </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Name of Publication/Conference/Seminar" required>
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of articles  </td>
                                            <td>1 each</td>
                                        </tr>

                                        <tr>
                                            <td>Popular lectures within the discipline   </td>
                                            <td>1 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 6 --}}
                <div id="6FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 6) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="Column writer for national publication">6.1. Column writer for national
                                    publication
                                </option>
                                <option value="Column Writer for local publication">6.2. Column Writer for local
                                    publication
                                </option>

                            </select>
                        </div>

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>


                        {{-- Author (s)  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s) </label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author(s)" required>
                        </div>

                        {{-- Date of Presentation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Presentation </label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Presentation" required>
                        </div>

                        {{-- Place of Presentation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Presentation</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Presentation" required>
                        </div>

                        {{--Nature of Publication    --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Presentation </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication " required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Column writer for national publication</td>
                                            <td>4 max/yr</td>
                                        </tr>

                                        <tr>
                                            <td>Column writer for local publication</td>
                                            <td>2 max/yr</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 7 --}}
                <div id="7FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 7) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="International">7.1. International
                                </option>
                                <option value="National/Local">7.2. National/Local
                                </option>

                            </select>
                        </div>

                        {{-- Name of Training  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Training </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Training" required>
                        </div>

                        {{-- Date of Training  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Training </label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Training" required>
                        </div>

                        {{-- Place of Training  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Training</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Training" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>International</td>
                                            <td>4 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/Local</td>
                                            <td>2 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 8 --}}
                <div id="8FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 8) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="International/Regional">8.1. International/Regional
                                </option>
                                <option value="National/University (Local)">8.2. National/University (Local)
                                </option>

                            </select>
                        </div>

                        {{-- Name of Committee  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Committee </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Committee" required>
                        </div>

                        {{-- Date of Date of Appointment  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Appointment </label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Date of Appointment" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> International/Regional</td>
                                            <td>2.5 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/University (Local)</td>
                                            <td>2 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2.5" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 9 --}}
                <div id="9FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 9) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="International/Regional">9.1. International/Regional
                                </option>
                                <option value="National/University (Local)">9.2. National/University (Local)
                                </option>

                            </select>
                        </div>

                        {{-- Name of Organization  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Organization </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Organization" required>
                        </div>

                        {{-- Date --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date" required>
                        </div>

                        {{-- Place --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place </label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place" required>
                        </div>

                        {{-- Nature of Organization --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Organization </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Organization" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> International/Regional</td>
                                            <td>2 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/University (Local)</td>
                                            <td>1 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 10 --}}
                <div id="10FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 10) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="International/Regional">10.1. International/Regional
                                </option>
                                <option value="National/University (Local)">10.2. National/University (Local)
                                </option>

                            </select>
                        </div>

                        {{-- Name of Program  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Program </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Program" required>
                        </div>

                        {{-- Participation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Participation
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. member etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="participation"
                                   placeholder="Participation" required>
                        </div>

                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required>
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> International/Regional</td>
                                            <td>2 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/University (Local)</td>
                                            <td>1 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 11 --}}
                <div id="11FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 11) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="International/Regional">11.1. International/Regional
                                </option>
                                <option value="National/University (Local)">11.2. National/University (Local)
                                </option>

                            </select>
                        </div>

                        {{-- Name of Activity  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Activity </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Activity" required>
                        </div>

                        {{-- Participation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Participation
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. member etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="participation"
                                   placeholder="Participation" required>
                        </div>

                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required>
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> International/Regional</td>
                                            <td>2 each</td>
                                        </tr>

                                        <tr>
                                            <td>National/University (Local)</td>
                                            <td>1 each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="2" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 12 --}}
                <div id="12FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 12) !!}

                        {{-- Name of Conference  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Conference </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Conference" required>
                        </div>


                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required>
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Attendance in International/Regional meetings/conference/workshop </td>
                                            <td>1 each</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 13 --}}
                <div id="13FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 13) !!}

                        {{-- Name of Conference  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Conference </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Conference" required>
                        </div>


                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required>
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Attendance in International/Regional meetings/conference/workshop </td>
                                            <td>1 each</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 14 --}}
                <div id="14FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 14) !!}

                        {{-- Name of Conference  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Conference </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Conference" required>
                        </div>


                        {{-- Date of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Activity</label>
                            <input type="date" class="form-control" name="date_ofpresentation"
                                   placeholder="Date of Activity" required>
                        </div>

                        {{-- Place of Activity --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Place of Activity</label>
                            <input type="text" class="form-control" name="place_ofpresentation"
                                   placeholder="Place of Activity" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Highly specialized training related to discipline with contribution to the college (duration of at least 2 weeks)</td>
                                            <td>1 each</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="1" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 15 --}}
                <div id="15FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 15) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="Patents approved">15.1. Patents approved
                                </option>
                                <option value="Patents filed and stamped at the IPO (National) or equivalent office in foreign countries">
                                    15.2. Patents filed and stamped at the IPO (National) or equivalent office in
                                    foreign countries
                                </option>


                            </select>
                        </div>

                        {{-- Name of Pantent  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Pantent </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Pantent" required>
                        </div>


                        {{-- Nature of Patent --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Patent</label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Patent" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Patents approved </td>
                                            <td>90 each</td>
                                        </tr>
                                        <tr>
                                            <td>Patents filed and stamped at the IPO (National) or equivalent office in foreign countries </td>
                                            <td>45 each</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="90" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 16 --}}
                <div id="16FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 16) !!}

                        {{-- Name of Citation  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Citation </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Citation" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Citations (indicate which article, journal/book)</td>
                                            <td>5 per citation</td>
                                        </tr>
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 17 --}}
                <div id="17FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 17) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="Honorific Awards: International Recognition">17.1.1 Honorific Awards:
                                    International Recognition
                                </option>
                                <option value="Honorific Awards: National Recognition">17.1.2 Honorific Awards: National
                                    Recognition
                                </option>
                                <option value="Honorific Awards: Local/Regional Recognition">17.1.3 Honorific Awards:
                                    Local/Regional Recognition
                                </option>
                                <option value="Competitive Awards: International Recognition">17.2.1 Competitive Awards:
                                    International Recognition
                                </option>
                                <option value="Competitive Awards: National Recognition">17.2.2 Competitive Awards:
                                    National Recognition
                                </option>
                                <option value="Competitive Awards: Local/Regional Recognition">17.2.3 Competitive
                                    Awards: Local/Regional Recognition
                                </option>


                            </select>
                        </div>

                        {{-- Name of Award  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Name of Award </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Award" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><b>HONORIFIC AWARDS</b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>International recognition(i.e., Caldicott Award, Ramon Magsaysay, SEA Write Awards) </td>
                                            <td>90pts</td>
                                        </tr>

                                        <tr>
                                            <td>National Recognition (i.e.,National Artist, National Scientist,/TOYM, Gawad Palanca, NAST Academician, etc.) </td>
                                            <td>60pts</td>
                                        </tr>

                                        <tr>
                                            <td>Local/Regional (i.e., Datu Bago Award, etc.) </td>
                                            <td>30pts</td>
                                        </tr>


                                        <tr class='success'>
                                            <td><b>COMPETITIVE AWARDS</b></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td>International award </td>
                                            <td>60pts</td>
                                        </tr>

                                        <tr>
                                            <td>National (i.e., Gawad Urian, Philippine Free Press Literary Contest, Metrobank Outstanding Teacher Award, Gawad Saka, CHED REPUBLICA, Best Research Papers in National Conferences) </td>
                                            <td>30pts</td>
                                        </tr>


                                        <tr>
                                            <td>Local/Regional (i.e., Gawad Chancellor, Best Oral/Poster e.g., SMARRDEC, etc.) </td>
                                            <td>15pts</td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="90" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 18 --}}
                <div id="18FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 18) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Refereed Publication (journal,
                                proceedings)/other print media</label>
                            <select class="form-control" name="subcategory">
                                <option value="Editor-in-Chief of an ISI-accredited scientific publication">18.1.1.
                                    Editor-in-Chief of an ISI-accredited scientific publication
                                </option>
                                <option value="Editor-in-Chief of creative print media (international)">18.1.2.
                                    Editor-in-Chief of creative print media (international)
                                </option>
                                <option value="Editor-in-Chief of non-ISI publication">18.1.3. Editor-in-Chief of
                                    non-ISI
                                    publication
                                </option>
                                <option value="Editor-in-chief of Creative Print Media (National)">18.1.4.
                                    Editor-in-chief of Creative Print Media (National)
                                </option>
                                <option value="Editorial Board Member">18.1.5. Editorial Board Member
                                </option>


                            </select>
                        </div>

                        {{-- Name of Publication  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Name of Publication </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of Publication" required>
                        </div>

                        {{-- Nature of Publication  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Nature of Publication
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> (e.g. Internation etc.) </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature of Publication" required>
                        </div>

                        {{-- Period Covered --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Period Covered </label>
                            <input type="text" class="form-control" name="period_covered"
                                   placeholder="Period Covered" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                       
                                        <tr>
                                            <td>Editor-in-Chief of an ISI-accredited publication</td>
                                            <td>5pts per year</td>
                                        </tr>

                                        <tr>
                                            <td>Editor-in-Chief of Creative Print Media (international) </td>
                                            <td>5pts per year</td>
                                        </tr>

                                         <tr>
                                            <td>Editor-in-Chief of non-ISI accredited publication  </td>
                                            <td>4pts per year</td>
                                        </tr>

                                        <tr>
                                            <td>Editor-in-Chief of Creative Print Media (national)   </td>
                                            <td>2pts per year</td>
                                        </tr>

                                        <tr>
                                            <td>Editorial Board Member    </td>
                                            <td>1pt per year</td>
                                        </tr>                                         

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 19 --}}
                <div id="19FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 19) !!}

                        <div class="col-lg-12 form-group">
                            <label class="control-label" for="selectMulti">Category</label>
                            <select class="form-control" name="subcategory">
                                <option value="International: Book">19.1.1 International: Book
                                </option>
                                <option value="International: Article">19.1.2 International: Article
                                </option>
                                <option value="National: Book">19.2.1 National: Book
                                </option>
                                <option value="National: Article">19.2.2 National: Article
                                </option>
                            </select>
                        </div>

                        {{-- Title of Book/Artice  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Title of Book/Artice </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title of Book/Artice" required>
                        </div>

                        {{-- Publisher  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher/Journal Vol./No. </label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Publisher/Journal Vol./No.">
                        </div>

                        {{-- Date of Publication  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date of Publication </label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date of Publication" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr  class='success'>
                                            <td><b>INTERNATIONAL </b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Book   </td>
                                            <td>4pts per book  </td>
                                        </tr>
                                        <tr>
                                            <td>Article   </td>
                                            <td>3pts per article  </td>
                                        </tr>

                                        

                                        <tr class='success'>
                                            <td><b>NATIONAL </b></td>
                                            <td></td>
                                        </tr>

                                       <tr>
                                            <td>Book   </td>
                                            <td>3pts per book  </td>
                                        </tr>
                                        <tr>
                                            <td>Article   </td>
                                            <td>2pts per article  </td>
                                        </tr>

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 20 --}}
                <div id="20FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 20) !!}



                        {{-- Name of University  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Name of University </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Name of University" required>
                        </div>

                        {{-- Duration of Appointment  --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Duration of Appointment </label>
                            <input type="text" class="form-control" name="duration_ofappointment"
                                   placeholder="Duration of Appointment" required>
                        </div>


                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <tr>
                                            <td>Research Fellowship /Visiting Professor Appointment in a reputable university (with MOA)    </td>
                                            <td>5pts per appt  </td>
                                        </tr>
                                       

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>


                        {{-- Other Details --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder="Other Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

                {{-- CATEGORY 21 --}}
                <div id="21FormCat4Cat1" class="FormCat4Cat1ShowDivTargetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat4' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 21) !!}

                        {{-- Other Services  --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Other Services
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Details"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> 
                                            (e.g. reviewer of research/creative work development of discipline or tertiary education) 
                                        </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Other Services" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <tr>
                                            <td>Other related services to the community (e.g., reviewer of research/creative work development of discipline or tertiary education/software developed for other agencies, etc.)   </td>
                                            <td>1pt per review</td>
                                        </tr>
                                       

                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="100" required>
                        </div>


                        {{--  Details --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti"> Details </label>
                            <input type="text" class="form-control" name="other_details"
                                   placeholder=" Details">
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat4" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>
                        </div>

                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Submit</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>


<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=inputFileAddCat4]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }

            }
        });
    });
</script>

{{-- FORMCAT2 CAT1 --}}
<script type="text/javascript">
    function FormCat4Cat1ShowDiv() {

        // make all the divs hidden
        var a = document.getElementsByClassName("FormCat4Cat1ShowDivTargetDiv");
        var b;
        for (b = 0; b < a.length; b++) {
            a[b].style.display = "none";
        }

        // make the selected value appear
        var selectFormCat4Cat1 = document.getElementById("FormCat4Cat1");

        var valueFormCat4Cat1 = selectFormCat4Cat1.options[selectFormCat4Cat1.selectedIndex].value;
        var divID = valueFormCat4Cat1 + "FormCat4Cat1";
        // alert(divID);
        document.getElementById(divID).style.display = "block";

    }

    // elem.onchange = function(){
    //     var showdiv = document.getElementById(val);
    //     showdiv.style.display = (this.value == val) ? "block":"none";
    // };

</script>

