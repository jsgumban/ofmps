<div class="modal fade modal-info" id="addNewCollege" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <form action={!! $preLink.'university/SaveNewCollege' !!} method="POST">
            {!! csrf_field() !!}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">CREATE NEW COLLEGE</h4>
                </div>

                <div class="modal-body">
                    {{-- COLLEGE NAME --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">College Name</label>
                        <input type="text" class="form-control" name="college_name" placeholder="College Name" required>
                    </div>


                    {{-- COLLEGE INITIAL NAME --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="inputText">Initials</label>
                        <input type="text" class="form-control" name="college_initial" placeholder="Initials">
                    </div>
                </div>

                {{-- buttons --}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>