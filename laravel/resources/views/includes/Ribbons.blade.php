@if (Request::is('*/EvaluateForm') && Auth::user()->position!=4)
    <div class="ribbon">
        @if($MpromotionForm->form_status==1)
            <?php $form_status = "DAPC LEVEL" ?>
        @elseif($MpromotionForm->form_status==1.5)
            <?php $form_status = "DAPC LEVEL - REJECTED" ?>
        @elseif($MpromotionForm->form_status==2)
            <?php $form_status = "CAPC LEVEL" ?>
        @elseif($MpromotionForm->form_status==2.5)
            <?php $form_status = "CAPC LEVEL - REJECTED" ?>
        @elseif($MpromotionForm->form_status==3)
            <?php $form_status = "UAPFC LEVEL" ?>
        @elseif($MpromotionForm->form_status==3.5)
            <?php $form_status = "UAPFC LEVEL - REJECTED" ?>
        @elseif($MpromotionForm->form_status>=4)
            <?php $form_status = "CHANCELLOR'S LEVEL" ?>
        @elseif($MpromotionForm->form_status==6)
            <?php $form_status = "APPROVED" . " (" . $MpromotionForm->initial_rank . " " . $MpromotionForm->initial_step . ")"  ?>
        @elseif($MpromotionForm->form_status==7)
            <?php $form_status = "CHANCELLOR'S LEVEL - REJECTED" ?>
        @endif
        <span class="ribbon-inner">EVALUATE FORM - {{$form_status}}</span>
    </div><br>
@elseif (Request::is('*/EvaluateForm') && Auth::user()->position==4)
    <div class="ribbon">
        <span class="ribbon-inner">APPROVE FORM - CHANCELLOR'S LEVEL</span>
    </div><br>


@elseif (Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') ||Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
    <div class="ribbon">
        @if($MpromotionForm->form_status==1)
            <?php $form_status = "DAPC LEVEL" ?>
        @elseif($MpromotionForm->form_status==1.5)
            <?php $form_status = "DAPC LEVEL - REJECTED" ?>
        @elseif($MpromotionForm->form_status==2)
            <?php $form_status = "CAPC LEVEL" ?>
        @elseif($MpromotionForm->form_status==2.5)
            <?php $form_status = "CAPC LEVEL - REJECTED" ?>
        @elseif($MpromotionForm->form_status==3)
            <?php $form_status = "UAPFC LEVEL" ?>
        @elseif($MpromotionForm->form_status==3.5)
            <?php $form_status = "UAPFC LEVEL - REJECTED" ?>
        @elseif($MpromotionForm->form_status==4)
            <?php $form_status = "CHANCELLOR'S LEVEL" ?>
        @elseif($MpromotionForm->form_status==6)
            <?php $form_status = "APPROVED" . " (" . $MpromotionForm->initial_rank . " " . $MpromotionForm->initial_step . ")"  ?>
        @elseif($MpromotionForm->form_status==7)
            <?php $form_status = "CHANCELLOR'S LEVEL - REJECTED" ?>
        @endif
        <span class="ribbon-inner"> MONITOR FORM - {{$form_status}}</span>
    </div><br>
@elseif (Request::is('*/UpdateForm'))
    <div class="ribbon">
        <span class="ribbon-inner"> UDPATE FORM</span>
    </div><br>
@elseif (Request::is('*/FillOutMeritPromotion'))
    <div class="ribbon">
        <span class="ribbon-inner"> FILL OUT FORM</span>
    </div><br>
@elseif (Request::is('*/ReviseForm'))
    <div class="ribbon">
        <span class="ribbon-inner"> DAPC LEVEL - REVISE FORM</span>
    </div><br>
@endif