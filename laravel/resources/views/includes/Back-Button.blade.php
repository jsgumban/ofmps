<h4 class="example-title">
    @if (Request::is('*/MonitorForm'))
        <a href="{{$preLink.'meritpromotion'}}" style="color:black">
            <i class="icon fa-arrow-circle-left" aria-hidden="true"
               style="font-size: 18px;" role="button" data-toggle="tooltip"
               title="Back"></i>
        </a>
    @elseif (Request::is('*/ReviseForm'))
        <a href="{{$preLink.'meritpromotion'}}" style="color:black">
            <i class="icon fa-arrow-circle-left" aria-hidden="true"
               style="font-size: 18px;" role="button" data-toggle="tooltip"
               title="Back"></i>
        </a>
    @elseif (Request::is('*/EvaluateForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
        @if((Auth::user()->position)==1)
            <a href="{{$preLink.'dapc/'.$MpromotionForm->callForPromotion_ID.'/ListOfSubmittedForms'}}"
               style="color:black">
                @elseif((Auth::user()->position)==2)
                    <a href="{{$preLink.'capc/'.$MpromotionForm->callForPromotion_ID.'/ListOfSubmittedForms'}}"
                       style="color:black">
                        @elseif((Auth::user()->position)==3)
                            <a href="{{$preLink.'uapfc/'.$MpromotionForm->callForPromotion_ID.'/ListOfSubmittedForms'}}"
                               style="color:black">
                                @elseif((Auth::user()->position)==4)
                                    <a href="{{$preLink.'chancellor/'.$MpromotionForm->callForPromotion_ID.'/ListOfSubmittedForms'}}"
                                       style="color:black">
                                        @endif
                                        <i class="icon fa-arrow-circle-left" aria-hidden="true"
                                           style="font-size: 18px;" role="button" data-toggle="tooltip"
                                           title="Back"></i>
                                    </a>
                                @else
                                    <a href="{{$preLink.'meritpromotion'}}"
                                       style="color:black">
                                        <i class="icon fa-arrow-circle-left" aria-hidden="true"
                                           style="font-size: 18px;" role="button" data-toggle="tooltip"
                                           title="Back"></i>
                                    </a>
                                @endif

                                <b>MERIT PROMOTION CATEGORIES</b>

                                <div class="btn-group pull-right">
                                    <a href="{!! $preLink.'meritpromotion/'.$MpromotionForm->id.'/generatePDF' !!}"
                                       target="_blank" style="color:maroon" title="Form(pdf)">
                                        <i class="icon fa-file-pdf-o" aria-hidden="true" style="font-size: 15px;"></i>
                                    </a>
                                    <a id="{{'zip-'.$MpromotionForm->id}}" role="button" target="_blank"
                                       style="color:red" title="Attachments(zip)">
                                        <i class="icon fa-file-zip-o "
                                           aria-hidden="true"
                                           style="font-size: 15px;"></i>
                                    </a>
                                    <a href="javascript:void(0)" data-plugin="webuiPopover"
                                       data-placement="right" data-animation="pop" data-target="webuiPopover"
                                       data-title="Form Information"
                                       data-target="webuiPopover9"
                                       data-content="
				<small> <table class='table'>
				<tbody>
					<tr>
						<td>Name </td>
						<td>{{ $user->last_name.', '.$user->first_name.' '.$user->middle_name[0]  }}</td>
					</tr>
                    <tr>
                        <td>Rank and Step </td>
                        <td> @if(!empty($user->rank)){{ $user->rank.' '.$user->step }}
                                       @else {{"none"}}
                                       @endif
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>Department </td>
                                               <td>  {{$user->department_belongs}}</td>
					</tr>

					<tr>
						<td>Filled out Date </td>
						<td>  {{date('F d, Y', strtotime($MpromotionForm->fillOut_date))}}</td>
					</tr>

                    <tr>
                        <td>Last promotion </td>
                        <td>  {{$user->last_promotion}}</td>
                    </tr>
                    
				</tbody>
				</table> </small>">
                                        <i class="icon wb-info-circle" aria-hidden="true"
                                           style="font-size: 16px;"></i>
                                    </a>

                                </div>
</h4>

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=zip]").click(function (e) {
            var id = (this.id).split('-');
            var formID = id[1];
            var generateZipURL = "{{ $preLink.'meritpromotion/'}}" + formID + '/generateZip';
            var downloadZipURL = "{{ $preLink.'meritpromotion/'}}" + formID + '/downloadZip';
            $.get(generateZipURL, function (data) {
                window.location = "{{ ZIPLOCATION }}" + "/" + data;
            });
        });
    });
</script>