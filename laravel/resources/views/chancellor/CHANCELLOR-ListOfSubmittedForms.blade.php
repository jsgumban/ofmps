@extends('app')
@section('title', 'OFMPS | SUBMITTED FORMS')
@section('content')

    @if(Auth::user())
        @if((Auth::user()->position)==5)
            <?php $preLink = FOLDERNAME . '/admin/'; ?>
            <?php $currentUser = 'admin'; ?>
        @elseif((Auth::user()->position)==4)
            <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
            <?php $currentUser = 'chancellor'; ?>
        @elseif((Auth::user()->position)==3)
            <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
            <?php $currentUser = 'vchancellor'; ?>
        @elseif((Auth::user()->position)==2)
            <?php $preLink = FOLDERNAME . '/dean/'; ?>
            <?php $currentUser = 'dean'; ?>
        @elseif((Auth::user()->position)==1)
            <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
            <?php $currentUser = 'deptchair'; ?>
        @elseif((Auth::user()->position)==0)
            <?php $preLink = FOLDERNAME . '/faculty/'; ?>
            <?php $currentUser = 'faculty'; ?>
        @endif
    @endif



    <div class="page animsition"><br>

        <div class="page-content">
            <div class="panel">
                <header class="panel-heading">
                    <div class="panel-actions"></div>
                    <h3 class="panel-title">

                        <b>SUBMITTED FORMS </b><br>
                        <small>UNIVERSITY OF THE PHILIPPINES MINDANAO</small>
                        <br>
                        <small>{{ 'CALL FOR PROMOTION: '.date('F d, Y', strtotime($callforpromotion[0]->mpromotion_date_start)).' - '.date('F d, Y', strtotime($callforpromotion[0]->mpromotion_date_end)) }}</small>
                    </h3>
                </header>
                <div class="panel-body">


                    {{-- NOTIFICATION --}}
                    @if(!empty(session('notification')))
                        <?php $notif = session('notification');?>
                        @if(strpos($notif,'SUCCESS')===false)
                            <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @else
                            <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @endif
                    @endif


                    <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Department</th>
                            <th>Date Submitted</th>

                            <th>Call for Promotion Date</th>
                            <th>Form Status</th>
                            <th>Points</th>
                            <th>
                                <center>
                                    Action
                                </center>
                            </th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Department</th>
                            <th>Date Submitted</th>
                            <th>Call for Promotion Date</th>
                            <th>Form Status</th>
                            <th>Points</th>
                            <th>
                                <center>
                                    Action
                                </center>
                            </th>
                        </tr>
                        </tfoot>


                        <tbody>
                        <?php $count = 1; ?>
                        @foreach ($submittedForms as $submittedForm)
                            <tr>
                                <td> <?php echo $count++; ?>  </td>
                                <td>{{ $submittedForm->last_name.', '.$submittedForm->first_name.' '.$submittedForm->middle_name[0].'.' }}</td>
                                <td>{{ $submittedForm->department_belongs }}</td>
                                <td> {{ date('F d, Y', strtotime($submittedForm->submit_date_deptchair)) }}  </td>
                                <td> {{ date('F d, Y', strtotime($submittedForm->callforpromotion_date_start)).' - '.date('F d, Y', strtotime($submittedForm->callforpromotion_date_end)) }} </td>
                                <td>
                                    @if($submittedForm->form_status==1)
                                        <span class="label  label-info">DAPC Level</span>
                                    @elseif($submittedForm->form_status==2)
                                        <span class="label  label-primary">CAPC Level</span>
                                    @elseif($submittedForm->form_status==3)
                                        <span class="label  label-success">UAPFC Level</span>
                                    @elseif($submittedForm->form_status==4)
                                        <span class="label  label-danger">Chancellor Level</span>
                                    @elseif($submittedForm->form_status==5)
                                        <span class="label  label-danger">Needs Revisions</span>
                                    @elseif($submittedForm->form_status==6)
                                        <span class="label  label-danger"> APPROVED 
                                        @if(empty($submittedForm->initial_rank))
                                            (0)
                                        @else
                                            {{ "(".$submittedForm->initial_rank." ".$submittedForm->initial_step.")" }}
                                        @endif
                                        

                                        </span>
                                    @elseif($submittedForm->form_status==7)
                                        <span class="label  label-danger">Chancellor Level - Rejected</span>
                                    @endif

                                </td>
                                <td><span class="label label-dark">  {{$submittedForm->faculty_total}}</span></td>
                                <td align="center">

                                    {{-- EVALUATE FORM --}}
                                    @if($submittedForm->form_status==4)
                                        <a href="{!! $preLink.'meritpromotion/'.$submittedForm->id.'/EvaluateForm' !!}"
                                           data-toggle="tooltip" title="Approve">
                                            <button type="button"
                                                    class="btn btn-icon btn-danger  btn-sm btn-sm  btn-sm"
                                                    >
                                                    <i class="icon fa-thumbs-o-up" aria-hidden="true" style="font-size: 15px;"></i>
                                                    </i></button>
                                        </a>
                                    @else
                                        <a href="{!! $preLink.'meritpromotion/'.$submittedForm->id.'/CHANCELLORMonitorForm' !!}"
                                           data-toggle="tooltip" title="Monitor Form">
                                            <button type="button"
                                                    class="btn btn-icon social-instagram  btn-sm btn-sm  btn-sm"
                                                    title=""><i class="icon wb-eye" aria-hidden="true"
                                                                style="font-size: 15px;"></i></button>
                                        </a>
                                    @endif

                                    {{-- GENERATE PDF --}}
                                    <a href="{!! $preLink.'meritpromotion/'.$submittedForm->id.'/generatePDF' !!}"
                                       data-toggle="tooltip" title="Form(pdf)" target="_blank">
                                        <button type="button"
                                                class="btn btn-icon btn-warning  btn-sm btn-sm  btn-sm"
                                                ><i class="icon fa-file-pdf-o"
                                                    aria-hidden="true"
                                                    style="font-size: 15px;"></i></button>
                                    </a>

                                    {{-- DOWNLOAD ATTACHMENTS --}}
                                    <a
                                            data-toggle="tooltip" title="Attachments(zip)"
                                            id="{{'zip'.$count.'-'.$submittedForm->id}}">
                                        <button type="button"
                                                class="btn btn-icon btn-info btn-active  btn-sm btn-sm  btn-sm"
                                                ><i class="icon fa-file-zip-o "
                                                    aria-hidden="true"
                                                    style="font-size: 15px;"></i></button>
                                    </a>


                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
            $("[id^=zip]").click(function (e) {
                var id = (this.id).split('-');
                var formID = id[1];
                var generateZipURL = "{{ $preLink.'meritpromotion/'}}" + formID + '/generateZip';
                var downloadZipURL = "{{ $preLink.'meritpromotion/'}}" + formID + '/downloadZip';
                $.get(generateZipURL, function (data) {
                    window.location = "{{ ZIPLOCATION }}" + "/" + data;

                });
            });
        });
    </script>
@endsection