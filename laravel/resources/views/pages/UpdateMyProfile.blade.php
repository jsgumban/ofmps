@extends('app')
@section('title', 'OFMPS | UPDATE MY PROFILE')
@section('content')

    @if(Auth::user())
        @if((Auth::user()->position)==5)
            <?php $preLink = FOLDERNAME . '/admin/'; ?>
            <?php $currentUser = 'admin'; ?>
        @elseif((Auth::user()->position)==4)
            <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
            <?php $currentUser = 'chancellor'; ?>
        @elseif((Auth::user()->position)==3)
            <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
            <?php $currentUser = 'vchancellor'; ?>
        @elseif((Auth::user()->position)==2)
            <?php $preLink = FOLDERNAME . '/dean/'; ?>
            <?php $currentUser = 'dean'; ?>
        @elseif((Auth::user()->position)==1)
            <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
            <?php $currentUser = 'deptchair'; ?>
        @elseif((Auth::user()->position)==0)
            <?php $preLink = FOLDERNAME . '/faculty/'; ?>
            <?php $currentUser = 'faculty'; ?>
        @endif
    @endif

    <div class="page animsition"><br>

        <div class="page-content">
            <div class="panel panel-bordered">


                {{-- HEADER --}}
                <header class="panel-heading">
                    <h3 class="panel-title">
                        <a href="{{$preLink.'profile/MyProfile'}}" style="color:black">
                            <i class="icon fa-arrow-circle-left" aria-hidden="true"
                               style="font-size: 18px;" role="button" data-toggle="tooltip"
                               title="Back"></i>
                        </a>
                        <b> UPDATE MY PROFILE </b>
                    </h3>
                </header>
                <div class="panel-body">


                    {{-- NOTIFICATION --}}
                    @if(!empty(session('notification')))
                        <?php $notif = session('notification');?>
                        @if(strpos($notif,'SUCCESS')===false)
                            <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @else
                            <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a class="alert-link">
                                    <center>{{ session('notification') }}</center>
                                </a>
                            </div>
                        @endif
                    @endif

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="editableUser">
                            <tbody>


                            {{-- FORM --}}
                            <form action={!! $preLink.'profile/SaveUpdateMyProfile' !!}
                                    method="POST">
                                {!! csrf_field() !!}



                                {{-- FIRST NAME --}}
                                <tr>
                                    <td>First Name *</td>
                                    <td>
                                        {!! Form::text('first_name', $user->first_name, ['class' => 'form-control', 'required']) !!}
                                    </td>
                                </tr>


                                {{-- MIDDLE NAME --}}
                                <tr>
                                    <td>Middle Name *</td>
                                    <td>
                                        {!! Form::text('middle_name', $user->middle_name, ['class' => 'form-control', 'required']) !!}
                                    </td>
                                </tr>


                                {{-- LAST NAME --}}
                                <tr>
                                    <td>Last Name *</td>
                                    <td>
                                        {!! Form::text('last_name', $user->last_name, ['class' => 'form-control', 'required']) !!}
                                    </td>
                                </tr>

                                <tr>
                                    <td>Gender *</td>
                                    <td>
                                        <select id="gender" class="form-control" name="gender" required>
                                            <option value="Male"> Male</option>
                                            <option value="Female"> Female</option>
                                        </select>
                                    </td>

                                    <script>
                                        document.getElementById("gender").value = "{!! $user->gender !!}";
                                    </script>
                                </tr>

                                {{-- EMPLOYEE CODE --}}
                                {{-- NON ADMIN --}}
                                @if($user->position<5)
                                    <tr>
                                        <td style="width:35%">Employee Code</td>
                                        <td style="width:65%">
                                            {!! Form::text('', $user->employee_code, ['class' => 'form-control', 'disabled']) !!}
                                            {!! Form::hidden('employee_code', $user->employee_code) !!}
                                        </td>
                                    </tr>
                                    {{-- ADMIN --}}
                                @else
                                    <tr>
                                        <td style="width:35%">Employee Code</td>
                                        <td style="width:65%">
                                            {!! Form::text('employee_code', $user->employee_code, ['pattern'=>'\d*', 'title' => 'Must be all numbers', 'class' => 'form-control', 'maxlength'=> 9,'minlength'=> 9, 'required']) !!}
                                        </td>
                                    </tr>
                                @endif



                                {{-- POSITION --}}
                                {{-- NON ADMIN --}}
                                @if($user->position<5)
                                    <tr>
                                        <td>Position</td>
                                        <td>
                                            @if(($user->position)==5)
                                                {!! Form::text('', 'System Administrator', ['class' => 'form-control', 'disabled']) !!}

                                            @elseif(($user->position)==4)
                                                {!! Form::text('', 'Chancellor ', ['class' => 'form-control', 'disabled']) !!}
                                            @elseif(($user->position)==3)
                                                {!! Form::text('', ' Vice Chancellor ', ['class' => 'form-control', 'disabled']) !!}
                                            @elseif(($user->position)==2)
                                                {!! Form::text('', ' Dean ', ['class' => 'form-control', 'disabled']) !!}
                                            @elseif(($user->position)==1)
                                                {!! Form::text('', ' Department Chairperson ', ['class' => 'form-control', 'disabled']) !!}
                                            @elseif(($user->position)==0)
                                                {!! Form::text('', ' Faculty Member ', ['class' => 'form-control', 'disabled']) !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endif



                                {{-- USER TYPE --}}
                                <tr>
                                    <td>User Type</td>
                                    <td>
                                        @if(($user->position)==5)
                                            {!! Form::text('', ' System Administrator ', ['class' => 'form-control', 'disabled']) !!}

                                        @elseif(($user->position)<5)
                                            {!! Form::text('', ' Faculty User', ['class' => 'form-control', 'disabled']) !!}
                                        @endif
                                    </td>
                                </tr>


                                {{-- RANK AND STEP --}}
                                {{-- NON ADMIN --}}
                                @if($user->position<5)
                                    <tr>
                                        <td>Rank and Step</td>
                                        <td>
                                            {!! Form::text('',  $user->rank . ' '.$user->step , ['class' => 'form-control', 'disabled']) !!}

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>College</td>
                                        <td>
                                            {!! Form::text('',  $collegeBelongs[0]->college_name . ' '.$user->step , ['class' => 'form-control', 'disabled']) !!}

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Department</td>
                                        <td>
                                            {!! Form::text('',  $user->department_belongs . ' '.$user->step , ['class' => 'form-control', 'disabled']) !!}
                                        </td>
                                    </tr>
                                @endif



                                {{-- BIRTHDAY --}}
                                <tr>
                                    <td>Birthday *</td>
                                    <td>
                                        {!! Form::input('date', 'birthday', $user->birthday, ['class' => 'form-control', 'placeholder' => 'Date', 'required', 'max' => '2000-01-01']); !!}
                                    </td>
                                </tr>


                                {{-- ADDRESS --}}
                                <tr>
                                    <td>Address</td>
                                    <td>
                                        {!! Form::text('address', $user->address, ['class' => 'form-control']) !!}
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="media">
                        <div align="right">


                            {{-- SUBMIT BUTTON --}}
                            <button type="submit" class="btn btn-success ladda-button" data-style="expand-right"
                                    data-title="Changes have been saved!" data-text="Click the OK button to proceed!"
                                    data-type="success">
                                <span class="ladda-label"><i class="icon wb-check margin-right-10"
                                                             aria-hidden="true"></i>Save Changes </span>
                                <span class="ladda-spinner"></span>
                            </button>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection