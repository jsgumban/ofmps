<div class="tab-pane" id="category-2" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">

        <h4 id="ASDASD">
            <div class="btn-group pull-right">
                <span data-toggle="tooltip" data-target="#id"
                      title="New College">
                  <button type="button" data-target="#addNewCollege"
                          data-toggle="modal"
                          class="btn btn-animate social-facebook dropdown-toggle"
                          id="exampleHoverDropdown3" data-toggle="dropdown">
                      <span><i class="icon wb-plus-circle" aria-hidden="true"></i> Create</span>
                  </button>
                </span>

            </div>
            <center><b>LIST OF ALL COLLEGES</b>&nbsp;
                <a href="javascript:void(0)" data-plugin="webuiPopover"
                   data-placement="right" data-animation="pop" data-target="webuiPopover" data-title="More Information"
                   data-target="webuiPopover9"
                   data-content="
                                <small> <table class='table'>
                                <tbody>
                                <tr>
                                    <td>Total no. of Colleges</td>
                                    <td>{!! count($colleges) !!}</td>
                                </tr>

                                <tr>
                                    <td>Total no. of Departments</td>
                                    <td>{!! count($departments) !!}</td>
                                </tr>

                                <tr>
                                    <td>Total no. of Faculty Members</td>
                                    <td>{!! count($allFaculty) !!} </td>
                                </tr>
    
                                </tbody>
                                </table> </small>">
                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                </a>
            </center>
        </h4>

        @if(session('activeTab')==2)

            @if(!empty(session('notification')))
                <br>
                <?php $notif = session('notification');?>
                @if(strpos($notif,'SUCCESS')===false)
                    <div class="alert alert-alt alert-danger alert-dismissible"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @else
                    <div class="alert alert-alt alert-success alert-dismissible"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @endif
            @endif
        @endif
        <hr>
        @if(empty($colleges))
            <br><br><br><br>
        @endif

        <?php $count = 1; ?>
        @foreach ($colleges as $college)
            <?php $count++ ?>
            <div class="panel">
                <br>
                <dl class="dl-horizontal">
                    <h4>{{ $college->college_name }}</h4>
                    <dt>Initials</dt>
                    <dd>{{ $college->college_initial }}</dd>


                    <dt>College Dean</dt>
                    @if(!empty($deans[$count-2]->last_name))
                        <dd>
                            <em>{{$deans[$count-2]->last_name.', '.$deans[$count-2]->first_name.' '.$deans[$count-2]->middle_name[0].'.' }}</em>
                        </dd>
                    @else
                        <dd><em>None</em></dd>
                    @endif

                    <dt>No. of Departments</dt>
                    <dd>{{ count($countDepratmentPerCollege[$count-2]) }}</dd>

                    <dt>No. of Faculty Members</dt>
                    <dd>{{ count($countFacultyPerCollege[$count-2]) }}</dd>
                    <br>

                    <dt>Update College</dt>
                    <dd>
                        <a id="{{'UpdateCollege-'.$college->id}}" data-target="#updateCollege"
                           data-toggle="modal" style="color:green" role="button">Click
                            Here</a>
                    </dd>

                    <dt>More Information</dt>
                    <dd>
                        <a href='{!! FOLDERNAME.'/admin/university/'.$college->id.'/ViewMoreInfoCollege' !!}'
                           role="button">Click
                            Here</a>
                    </dd>


                    <dt>Remove College</dt>
                    <dd>
                        <a href='{!! $preLink.'university/'.$college->id.'/RemoveCollege' !!}'
                           onclick="if (! confirm('Are you sure do you want remove this College?')) { return false; }
                            " style="color:#e71e1e">Click Here
                        </a>
                    </dd>
                </dl>
                <br>
            </div>
        @endforeach
        <hr>

        {{-- create new department button--}}
        <div class="btn-group pull-right">

        </div>
    </div>
</div>
@include('admin.UniversitySettings-College-Update')
@include('admin.UniversitySettings-College-UpdateScript')