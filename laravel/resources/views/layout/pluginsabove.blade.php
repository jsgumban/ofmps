<link rel="stylesheet" href="{{ asset('/vendor/toastr/toastr.css') }}">
<link rel="stylesheet" href="{{ asset('/vendor/datatables-bootstrap/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('/vendor/nestable/nestable.css') }}">
<link rel="stylesheet" href="{{ asset('/vendor/webui-popover/webui-popover.css') }}">
<link rel="stylesheet" href="{{ asset('/vendor/jquery-labelauty/jquery-labelauty.css') }}">