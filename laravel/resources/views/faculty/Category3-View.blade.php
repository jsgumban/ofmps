{{-- CATEGORY 3 --}}
<div class="tab-pane" id="category-3" role="tabpanel">


    <div class="btn-group pull-right">
        <i class="icon wb-fullscreen" aria-hidden="true" role="button" onclick="shrink3()" style="font-size: 18px;"
           id="maxmin3" data-toggle="tooltip" title="Expand/Shrink"></i>
    </div>


    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">

        <h3><b>III. SERVICE</b></h3>

        <div class="example">
            <h4><b>A. SERVICE TO DEPARTMENT/COLLEGE/UNIVERSITY </b></h4>

            <h5>1. Regular administrative positions in the College/Institute/Department </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Designation</th>
                        <th>Period Covered</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($services as $service)
                        <?php $counterOuter++; ?>
                        @if($service->category==1 && $service->subcategory==1)


                            <tr role="button" data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$service->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC3-tr-1-1-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$service->id}} data-isaccepteddeptchair =
                                    "{{$service->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$service->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$service->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$service->designation}}</td>
                                <td id="{{'UpdateFC3-tr-1-1-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->period_covered}}</td>
                                <td id="{{'UpdateFC3-tr-1-1-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->other_details}}</td>
                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $service->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC3-tr-1-1-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-warning">{{$service->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>


            <h5>2. Membership in a standing committee of the Institute/Department/College/University. If membership in
                the committee is by virtue of the faculty’s administrative post then no point is given. </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Name of Committee</th>
                        <th>Nature of Committee</th>
                        <th>Period Covered</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($services as $service)
                        <?php $counterOuter++; ?>
                        @if($service->category==1 && $service->subcategory==2)
                            <tr role="button" data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$service->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC3-tr-1-2-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$service->id}} data-isaccepteddeptchair =
                                    "{{$service->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$service->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$service->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$service->name_ofcommittee}}</td>
                                <td id="{{'UpdateFC3-tr-1-2-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->nature_ofcommittee}}</td>
                                <td id="{{'UpdateFC3-tr-1-2-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->period_covered}}</td>
                                <td id="{{'UpdateFC3-tr-1-2-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->other_details}}</td>
                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $service->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC3-tr-1-2-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-warning">{{$service->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>3. Resource Generation </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Funding Source</th>
                        <th>Purpose of Fund</th>
                        <th>Amount Donated</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($services as $service)
                        <?php $counterOuter++; ?>
                        @if($service->category==1 && $service->subcategory==3)


                            <tr role="button" data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$service->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC3-tr-1-3-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$service->id}} data-isaccepteddeptchair =
                                    "{{$service->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$service->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$service->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$service->funding_source}}</td>
                                <td id="{{'UpdateFC3-tr-1-3-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->purpose_offund}}</td>
                                <td id="{{'UpdateFC3-tr-1-3-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->ammount_donated}}</td>
                                <td id="{{'UpdateFC3-tr-1-3-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->other_details}}</td>
                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $service->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC3-tr-1-3-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-warning">{{$service->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>4. Officer-in-Charge to the Chancellor/Vice-Chancellor/Dean/Director/College Secretary/Department
                Head </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Designation of OIC</th>
                        <th>Level</th>
                        <th>Period Covered</th>
                        <th>Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($services as $service)
                        <?php $counterOuter++; ?>
                        @if($service->category==1 && $service->subcategory==4)


                            <tr role="button" data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$service->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC3-tr-1-4-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$service->id}} data-isaccepteddeptchair =
                                    "{{$service->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$service->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$service->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$service->designation_ofoic}}</td>
                                <td id="{{'UpdateFC3-tr-1-4-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->level}}</td>
                                <td id="{{'UpdateFC3-tr-1-4-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->period_covered}}</td>
                                <td id="{{'UpdateFC3-tr-1-4-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal">{{$service->other_details}}</td>
                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $service->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC3-tr-1-4-'.$service->id}}" data-target="#UpdateFC3-modal-1"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-warning">{{$service->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h4><b>B. SERVICE TO INTERNATIONAL/NATIONAL/REGIONAL COMMUNITY </b></h4>
            <h5>1. Presidency/Chairmanship of Professional association/organization </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Organization</th>
                        <th>Designation</th>
                        <th> Period Covered</th>
                        <th> Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($services as $service)
                        <?php $counterOuter++; ?>
                        @if($service->category==2 && $service->subcategory==1)


                            <tr role="button" data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$service->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC3-tr-2-1-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$service->id}} data-isaccepteddeptchair =
                                    "{{$service->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$service->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$service->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$service->subsubcategory}}</td>
                                <td id="{{'UpdateFC3-tr-2-1-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">{{$service->name_oforganization}}</td>
                                <td id="{{'UpdateFC3-tr-2-1-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">{{$service->designation}}</td>
                                <td id="{{'UpdateFC3-tr-2-1-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">{{$service->period_covered}}</td>
                                <td id="{{'UpdateFC3-tr-2-1-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">{{$service->other_details}}</td>
                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $service->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC3-tr-2-1-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-warning">{{$service->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>

            <h5>2. Position of responsibility in Professional association/organization </h5>

            <div class="table-responsive">
                <table class="table table-hover table-striped width-full">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name of Organization</th>
                        <th>Designation</th>
                        <th> Period Covered</th>
                        <th> Other Details</th>
                        <th>Attachment(s)</th>
                        <th style="text-align:right">Points</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $counterOuter = 0; ?>

                    @foreach ($services as $service)
                        <?php $counterOuter++; ?>
                        @if($service->category==2 && $service->subcategory==2)


                            <tr role="button" data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                                data-isaccepteddean="{{$service->isAccepted_dean}}"
                                data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                                <td id="{{'UpdateFC3-tr-2-2-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">
                                    <i id={{ 'redcheck-'.$service->id}} data-isaccepteddeptchair =
                                    "{{$service->isAccepted_deptchair}}" data-isaccepteddean =
                                    "{{$service->isAccepted_dean}}" data-isacceptedvchancellor =
                                    "{{$service->isAccepted_vchancellor}}" class="icon wb-check-circle" aria-hidden="
                                    true" style="font-size: 18px
                                    ;
                                        color: #800000
                                    ;
                                        display: none
                                    ;">&nbsp;</i>
                                    {{$service->subsubcategory}}</td>
                                <td id="{{'UpdateFC3-tr-2-2-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">{{$service->name_oforganization}}</td>
                                <td id="{{'UpdateFC3-tr-2-2-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">{{$service->designation}}</td>
                                <td id="{{'UpdateFC3-tr-2-2-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">{{$service->period_covered}}</td>
                                <td id="{{'UpdateFC3-tr-2-2-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal">{{$service->other_details}}</td>
                                <td>

                                    <?php   $attachmentNamesSpit = explode("+++", $service->attachment_filenames) ?>
                                    @foreach ($attachmentNamesSpit as $file)
                                        @if($file != null)
                                            <?php $fileType = substr(strrchr($file, '.'), 1); ?>

                                            <a href="{{ asset('/uploads/'.$file) }}"
                                               data-toggle="tooltip" title="{{$fileType}}" target="_blank">
                                                <i class="icon wb-tag" aria-hidden="true"
                                                   style="font-size: 20px;"></i>
                                            </a>
                                            {{-- // echo '<img src="/uploads/' . $file . '"/>'; --}}
                                        @endif
                                    @endforeach
                                </td>

                                <td id="{{'UpdateFC3-tr-2-2-'.$service->id}}" data-target="#UpdateFC3-modal-2"
                                    data-toggle="modal" style="text-align:right">
                                    <div class="label label-table label-warning">{{$service->points}}</div>
                                </td>
                            </tr>

                        @endif

                    @endforeach


                    </tbody>
                </table>
            </div>
            <br>
        </div>
    </div>
</div>


{{-- SHRINKING AND EXPANDING TABLE PANELS --}}
<script type="text/javascript">
    function shrink3() {
        document.getElementById("shrink").style.display = "none";
        document.getElementById("maximize").setAttribute("class", "col-lg-12 col-sm-12");
        document.getElementById("maxmin3").setAttribute("class", "icon wb-fullscreen-exit");
        document.getElementById("maxmin3").setAttribute("onclick", "expand3()");
        // col-lg-8 col-sm-8
    }
    function expand3() {
        document.getElementById("shrink").style.display = "block";
        document.getElementById("maximize").setAttribute("class", "col-lg-8 col-sm-8");
        document.getElementById("maxmin3").setAttribute("class", "icon wb-fullscreen");
        document.getElementById("maxmin3").setAttribute("onclick", "shrink3()");
    }
</script>