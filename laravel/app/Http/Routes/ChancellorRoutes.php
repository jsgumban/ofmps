<?php
Route::group(['middleware' => 'chancellor'], function () {
    Route::get('chancellor/home', 'ChancellorController@index');

    /////////////////
    // OWN PROFILE //
    /////////////////
    Route::get('chancellor/profile/MyProfile', ['uses' => 'AdminController@MyProfile']);
    Route::get('chancellor/profile/UpdateMyProfile', ['uses' => 'AdminController@UpdateMyProfile']);
    Route::post('chancellor/profile/SaveUpdateMyProfile', ['uses' => 'AdminController@SaveUpdateMyProfile']);
    Route::get('chancellor/profile/UpdateMyPassword', ['uses' => 'AdminController@UpdateMyPassword']);
    Route::post('chancellor/profile/SaveUpdateMyPassword', ['uses' => 'AdminController@SaveUpdateMyPassword']);

    /////////////////////
    // FACULTY MEMBERS //
    /////////////////////
    Route::get('chancellor/dapc/{id}/RetrieveUserData', ['uses' => 'DeptChairController@RetrieveUserData']);
    Route::get('chancellor/dapc/{id}/RetrieveSubmittedForms', ['uses' => 'DeptChairController@RetrieveSubmittedForms']);
    Route::get('chancellor/meritpromotion/{id}/RetrieveUserInfo', ['uses' => 'DeptChairController@RetrieveUserInfo']);

    /////////////////////
    // MERIT PROMOTION //
    /////////////////////
    Route::get('chancellor/chancellor', ['uses' => 'ChancellorController@CHANCELLOR']);
    Route::get('chancellor/chancellor/{id}/ListOfSubmittedForms', ['uses' => 'ChancellorController@ListOfSubmittedForms']);

    Route::get('chancellor/meritpromotion', ['uses' => 'FacultyController@MeritPromotion']);
    Route::post('chancellor/meritpromotion/AddNewForm', ['uses' => 'FacultyController@AddNewForm']);
    Route::get('chancellor/meritpromotion/{id}/FillOutMeritPromotion', ['uses' => 'FacultyController@FillOutMeritPromotion']);
    Route::get('chancellor/meritpromotion/SaveAsDraft', ['uses' => 'FacultyController@SaveAsDraft']);
    Route::get('chancellor/meritpromotion/{id}/SubmitDeptChair', ['uses' => 'FacultyController@SubmitDeptChair']);
    Route::get('chancellor/meritpromotion/{id}/MonitorForm', ['uses' => 'FacultyController@MonitorForm']);

    Route::get('chancellor/meritpromotion/{id}/EvaluateForm', ['uses' => 'ChancellorController@EvaluateForm']);
    Route::post('chancellor/meritpromotion/{id}/ApproveForm', ['uses' => 'ChancellorController@ApproveForm']);
    Route::get('chancellor/meritpromotion/{id}/RetrieveFormInfo', ['uses' => 'DeptChairController@RetrieveFormInfo']);
    Route::get('chancellor/meritpromotion/{id}/CHANCELLORMonitorForm', ['uses' => 'ChancellorController@CHANCELLORMonitorForm']);
    
    Route::get('chancellor/meritpromotion/{id}/generateZip', ['uses' => 'FacultyController@generateZip']);
    Route::get('chancellor/meritpromotion/{id}/generatePDF', ['uses' => 'FacultyController@generatePDF']);
    Route::get('chancellor/meritpromotion/{id}/ReviseForm', ['uses' => 'FacultyController@ReviseForm']);
    Route::get('chancellor/meritpromotion/{id}/UpdateForm', ['uses' => 'FacultyController@UpdateForm']);
    Route::get('chancellor/meritpromotion/{id}/DeleteDraftForm', ['uses' => 'FacultyController@DeleteDraftForm']);

    Route::post('chancellor/meritpromotion/{id}/SubmitFormCat1', ['uses' => 'FacultyController@SubmitFormCat1']);
    Route::post('chancellor/meritpromotion/{id}/SubmitFormCat2', ['uses' => 'FacultyController@SubmitFormCat2']);
    Route::post('chancellor/meritpromotion/{id}/SubmitFormCat3', ['uses' => 'FacultyController@SubmitFormCat3']);
    Route::post('chancellor/meritpromotion/{id}/SubmitFormCat4', ['uses' => 'FacultyController@SubmitFormCat4']);

    Route::get('chancellor/meritpromotion/{id}/RetrieveCat1', ['uses' => 'FacultyController@RetrieveCat1']);
    Route::get('chancellor/meritpromotion/{id}/RetrieveCat2', ['uses' => 'FacultyController@RetrieveCat2']);
    Route::get('chancellor/meritpromotion/{id}/RetrieveCat3', ['uses' => 'FacultyController@RetrieveCat3']);
    Route::get('chancellor/meritpromotion/{id}/RetrieveCat4', ['uses' => 'FacultyController@RetrieveCat4']);

    Route::post('chancellor/meritpromotion/{id}/UpdateFormCat1', ['uses' => 'FacultyController@UpdateFormCat1']);
    Route::post('chancellor/meritpromotion/{id}/UpdateFormCat2', ['uses' => 'FacultyController@UpdateFormCat2']);
    Route::post('chancellor/meritpromotion/{id}/UpdateFormCat3', ['uses' => 'FacultyController@UpdateFormCat3']);
    Route::post('chancellor/meritpromotion/{id}/UpdateFormCat4', ['uses' => 'FacultyController@UpdateFormCat4']);

    Route::get('chancellor/meritpromotion/{id}/DeleteEntryCat1', ['uses' => 'FacultyController@DeleteEntryCat1']);
    Route::get('chancellor/meritpromotion/{id}/DeleteEntryCat2', ['uses' => 'FacultyController@DeleteEntryCat2']);
    Route::get('chancellor/meritpromotion/{id}/DeleteEntryCat3', ['uses' => 'FacultyController@DeleteEntryCat3']);
    Route::get('chancellor/meritpromotion/{id}/DeleteEntryCat4', ['uses' => 'FacultyController@DeleteEntryCat4']);
});
?>