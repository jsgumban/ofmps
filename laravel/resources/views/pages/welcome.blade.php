@extends('app')
@section('title', 'OFMPS |  HOME')
@section('content')
    @if(Auth::user())
        @if((Auth::user()->position)==5)
            <?php $preLink = '/admin/'; ?>
            <?php $currentUser = 'admin'; ?>
        @elseif((Auth::user()->position)==4)
            <?php $preLink = '/chancellor/'; ?>
            <?php $currentUser = 'chancellor'; ?>
        @elseif((Auth::user()->position)==3)
            <?php $preLink = '/vchancellor/'; ?>
            <?php $currentUser = 'vchancellor'; ?>
        @elseif((Auth::user()->position)==2)
            <?php $preLink = '/dean/'; ?>
            <?php $currentUser = 'dean'; ?>
        @elseif((Auth::user()->position)==1)
            <?php $preLink = '/deptchair/'; ?>
            <?php $currentUser = 'deptchair'; ?>
        @elseif((Auth::user()->position)==0)
            <?php $preLink = '/faculty/'; ?>
            <?php $currentUser = 'faculty'; ?>
        @endif
    @endif
    <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
         data-animsition-out="fade-out">

        <div class="page-content vertical-align-middle">

            <div class="brand">
                <br>
                <img class="brand-img" src="{{asset('/images/upmindanao_logo.png')}}" style="width: 50%; height: 50%"
                     alt="...">
                      <p style="color:black">THE UNIVERSITY OF THE PHILIPPLINES MINDANAO ONLINE FACULTY MERIT PROMOTION SYSTEM</p>
            </div>
            <hr style="color:black">

           
            @if((!empty($isThereCallForPromotion)) && ($isThereCallForPromotion[0]->mpromotion_status!=1))
                <div class="toast-example" id="exampleToastrErrorShadow" aria-live="polite"
                     data-icon-class="toast-just-text toast-danger toast-shadow" role="alert">
                    <div class="toast toast-just-text toast-shadow toast-danger" style="background-color: #800000">
                        <button type="button" class="toast-close-button" aria-label="Close">
                        </button>
                        <div class="toast-message">
                            <strong>ATTENTION!</strong>
                            <br>
                            CALL FOR
                            PROMOTION: {{ date('F d, Y', strtotime($isThereCallForPromotion[0]->mpromotion_date_start )) }}
                            - {{ date('F d, Y', strtotime($isThereCallForPromotion[0]->mpromotion_date_end ))}} &nbsp;
                            <a href={!! $preLink.'meritpromotion/MeritPromotionForms' !!}>

                            </a>
                            <br>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection