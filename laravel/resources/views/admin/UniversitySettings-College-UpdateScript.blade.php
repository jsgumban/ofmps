<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=UpdateCollege-]").click(function (e) {
            e.preventDefault();
            var id = (this.id).split('-');
            var entryID = id[1];

            // alert(this.id);
            var retrieveDataUrl = "{{ $preLink.'university/' }}" + entryID + '/RetrieveCollege';
            var retrieveDataUrl2 = "{{ $preLink.'university/' }}" + entryID + '/RetrieveCollegeDean';
            var submitButtonUrl = "{{ $preLink.'university/'}}" + entryID + '/SaveUpdatedCollege';
            // var deleteButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/DeleteEntryCat4';
            $('#UpdateCollegeForm').attr('action', submitButtonUrl);
            $.get(retrieveDataUrl, function (data) {
                $('#UpdateCollegeName').val(data.college_name);
                $('#UpdateCollegeInitials').val(data.college_initial);
                $('[id^=UpdateCollegeDean]').attr("id", "UpdateCollegeDean-" + data.id);
            });

            $.get(retrieveDataUrl2, function (data) {
                $('[id^=UpdateCollegeDean]').empty();
                $('[id^=UpdateCollegeDean]').append("<option value=''>" + "None" + "</option>");

                for (x = 0; x < data.length; x++) {
                    $('[id^=UpdateCollegeDean]').append("<option value='" + data[x].id + "'>" + data[x].last_name + ", " + data[x].first_name + " " + data[x].middle_name[0] + "." + "</option>");
                }
                $('[id^=UpdateCollegeDean]').val(data[0].dean);
            });


        });
    });
</script>