{{-- CATEGORY1 MODAL --}}
<div class="modal fade modal-success" id="exampleModalWarning1" aria-hidden="true" aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">

        {{-- FORM --}}
        <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST">
            {!! csrf_field() !!}

            {{-- HIDDEN ATTRIBUTES --}}
            {!! Form::hidden('category', 1) !!}
            {!! Form::hidden('subcategory', 1) !!}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">A. PERFORMANCE RATING (STATE)</h4>
                </div>

                <div class="modal-body">
                    {{-- WEIGHTED AVERAGE RATING --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">Weighted Average Rating</label>
                        <input type="number" step="0.01" class="form-control" name="WARating"
                               placeholder="Weighted Average Rating" required>
                    </div>

                    {{-- POINTS --}}
                    <div class="col-lg-12 form-group">
                        <label class="control-label" for="selectMulti">Points
                            <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                               data-animation="pop" data-target="webuiPopover"
                               data-content="
                                        <small><small> <table class='table'><thead>
                                        <tr>
                                            <th>WEIGHTED AVERAGE RATING</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1.0</td>
                                            <td>20</td>
                                        </tr>

                                        <tr>
                                            <td>1.1</td>
                                            <td>19</td>
                                        </tr>

                                        <tr>
                                            <td>1.2</td>
                                            <td>18</td>
                                        </tr>

                                        <tr>
                                            <td>1.3</td>
                                            <td>17</td>
                                        </tr>

                                        <tr>
                                            <td>1.4</td>
                                            <td>16</td>
                                        </tr>

                                        <tr>
                                            <td>1.5</td>
                                            <td>15</td>
                                        </tr>

                                        <tr>
                                            <td>1.6</td>
                                            <td>14</td>
                                        </tr>

                                        <tr>
                                            <td>1.7</td>
                                            <td>13</td>
                                        </tr>

                                        <tr>
                                            <td>1.8</td>
                                            <td>12</td>
                                        </tr>

                                        <tr>
                                            <td>1.9</td>
                                            <td>11</td>
                                        </tr>

                                        <tr>
                                            <td>2.0</td>
                                            <td>10</td>
                                        </tr>

                                        <tr>
                                            <td>Less than  2.0</td>
                                            <td>0</td>
                                        </tr>
                                        </tbody>
                                        </table> </small></small>">
                                <i class="icon wb-info-circle" aria-hidden="true"></i>
                            </a>
                        </label>
                        <input type="number" step="0.01" class="form-control" name="points" placeholder="Points" min="0" max="20"
                               required>
                    </div>


                </div>
                <div class="modal-footer">


                    {{-- SUBMIT BUTTON --}}
                    <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                    {{-- CANCEL BUTTON --}}
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>


{{-- CATEGORY2 MODAL --}}
<div class="modal fade modal-success  example-modal-lg" id="exampleModalWarning2" aria-hidden="true"
     aria-labelledby="exampleModalWarning"
     role="dialog" tabindex="-5" style="display: none;" onclick="changeInputs()">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- HEADER --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">B. TEACHING PORTFOLIO / OTHER ACTIVITIES RELATED TO TEACHING </h4>
            </div>

            {{-- CONTENT --}}
            <div class="modal-body">
                {{-- DROPDOWN OPTIONS --}}
                <div class="col-lg-12 form-group">
                    <select class="form-control" id="Cat2SubCategories" onclick="showDiv()" onkeydown="showDiv()"
                            onkeydown="showDiv()" onkeypress="showDiv()">
                        <option value=1>01. Authorship of a UP level textbook/ audio-visual materials for one- semester
                            course
                        </option>
                        <option value=2>02. Authorship of a UP laboratory/studies manual</option>
                        <option value=3>03. Authorship of Modules (including wrap-around text)</option>
                        <option value=4>04. Revision/updating of UP textbook/laboratory/studio manuals</option>
                        <option value=5>05. Authorship of book review</option>
                        <option value=6>06. Authorship of case studies(Case studies are classroom-tested and with signed
                            company release)
                        </option>
                        <option value=7>07. Authorship of annotated bibliography</option>
                        <option value=8>08. Department-endorsed materials for instructions</option>
                        <option value=9>09. For each dissertation advisee graduated</option>
                        <option value=10>10. For each Master’s thesis advisee graduated</option>
                        <option value=11>11. Undergraduate Thesis/Special Problem/Business Plan: for every advisee who
                            graduated (Max of 6 students/year)
                        </option>
                        <option value=12>12. Number of preparations per year</option>
                        <option value=13>13. Other contributions to teaching</option>
                    </select>
                </div>


                {{-- SUBCATEGORY 1--}}
                <div id="1" class="targetDiv">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 1) !!}

                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required>
                        </div>

                        {{-- Course Title--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Course Title</label>
                            <input type="text" class="form-control" name="course_title"
                                   placeholder="Course Title" required>
                        </div>

                        {{-- Semester Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Semester Used</label>
                            <select class="form-control" name="semester_used"
                                    placeholder="Semester Used">
                                <option value="First Semester"> First Semester
                                </option>
                                <option value="Second Semester">Second Semester</option>
                                <option value="Summer">Summer</option>
                            </select>
                        </div>

                        {{-- AY Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">AY Used</label>
                            <input class="form-control" name="ay_used"
                                   placeholder="AY Used" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of a UP (any UP unit) level textbook/ audio-visual materials for one- semester course (recommended by unit for use in a course)</td>
                                            <td>20each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="20" required>
                        </div>


                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>


                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>

                    </form>
                </div>


                {{-- SUBCATEGORY 2 --}}
                <div id="2" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 2) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required>
                        </div>

                        {{-- Course Title--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Course Title</label>
                            <input type="text" class="form-control" name="course_title"
                                   placeholder="Course Title" required>
                        </div>

                        {{-- Semester Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Semester Used</label>
                            <select class="form-control" name="semester_used"
                                    placeholder="Semester Used">
                                <option value="First Semester"> First Semester
                                </option>
                                <option value="Second Semester">Second Semester</option>
                                <option value="Summer">Summer</option>
                            </select>
                        </div>

                        {{-- AY Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">AY Used</label>
                            <input class="form-control" name="ay_used"
                                   placeholder="AY Used" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of a UP laboratory/studies manual</td>
                                            <td>20each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="20" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>

                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>

                {{-- SUBCATEGORY 3 --}}
                <div id="3" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 3) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required>
                        </div>

                        {{-- Course Title--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Course Title</label>
                            <input type="text" class="form-control" name="course_title"
                                   placeholder="Course Title" required>
                        </div>

                        {{-- Semester Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Semester Used</label>
                            <select class="form-control" name="semester_used"
                                    placeholder="Semester Used">
                                <option value="First Semester"> First Semester
                                </option>
                                <option value="Second Semester">Second Semester</option>
                                <option value="Summer">Summer</option>
                            </select>
                        </div>

                        {{-- AY Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">AY Used</label>
                            <input class="form-control" name="ay_used"
                                   placeholder="AY Used" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of Modules (including wrap-around text)</td>
                                            <td>8each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="8" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>

                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>

                {{-- SUBCATEGORY 4 --}}
                <div id="4" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 4) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required>
                        </div>

                        {{-- Course Title--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Course Title</label>
                            <input type="text" class="form-control" name="course_title"
                                   placeholder="Course Title">
                        </div>

                        {{-- Semester Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Semester Used</label>
                            <select class="form-control" name="semester_used"
                                    placeholder="Semester Used">
                                <option value="First Semester"> First Semester
                                </option>
                                <option value="Second Semester">Second Semester</option>
                                <option value="Summer">Summer</option>
                            </select>
                        </div>

                        {{-- AY Used--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">AY Used</label>
                            <input class="form-control" name="ay_used"
                                   placeholder="AY Used" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Revision/updating of UP textbook/laboratory/studio manuals </td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>


                {{-- SUBCATEGORY 5 --}}
                <div id="5" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 5) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of book review </td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>

                {{-- SUBCATEGORY 6 --}}
                <div id="6" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 6) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{-- Publisher--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Publisher</label>
                            <input type="text" class="form-control" name="publisher"
                                   placeholder="Publisher" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of case studies (Case studies are classroom-tested and with signed company release)</td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>

                {{-- SUBCATEGORY 7 --}}
                <div id="7" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 7) !!}
                        {{-- Title --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Title</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title" required>
                        </div>

                        {{-- Author --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Author(s)</label>
                            <input type="text" class="form-control" name="author"
                                   placeholder="Author" required>
                        </div>

                        {{-- Date Published --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Date Published</label>
                            <input type="date" class="form-control" name="date_published"
                                   placeholder="Date Published" required>
                        </div>

                        {{-- Title, vol. No. of Journal/Publication where annotated bibliography appears --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Title, Vol. No. of Journal/Publication Where
                                Annotated Bibliography Appears</label>
                            <input type="text" class="form-control" name="bibliography"
                                   placeholder="Title, Vol. No. of Journal/Publication"
                                   required>
                        </div>

                        {{-- Nature Of Publication --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Nature Of Publication (e.g
                                international) </label>
                            <input type="text" class="form-control" name="nature_ofpublication"
                                   placeholder="Nature Of Publication" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Authorship of annotated bibliography</td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>
                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>

                {{-- SUBCATEGORY 8 --}}
                <div id="8" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 8) !!}
                        {{-- Title Of Material --}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Title Of Material</label>
                            <input type="text" class="form-control" name="title"
                                   placeholder="Title Of Material" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Department-endorsed audio-visual materials/course readers/laboratory/studio/manuals/learning object/computer software for instruction</td>
                                            <td>4each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="4" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>


                {{-- SUBCATEGORY 9 --}}
                <div id="9" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 9) !!}
                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="as_what">
                                <option value="As Adviser/Co-adviser">9.1 As Adviser/Co-adviser</option>
                                <option value="As Reader/Panelist/Examiner ">9.2 As Reader/Panelist/Examiner</option>

                            </select>
                        </div>


                        {{-- Name of Graduate --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Graduate</label>
                            <input type="text" class="form-control" name="name_ofgraduate"
                                   placeholder="Name of Graduate" required>
                        </div>

                        {{-- Year Graduated--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Year Graduated</label>
                            <input type="number" class="form-control" name="year_graduated"
                                   placeholder="Year Graduated" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>As Adviser/Co-adviser</td>
                                            <td>10each</td>
                                        </tr>

                                        <tr>
                                            <td>As Reader/Panelist/Examiner  </td>
                                            <td>5each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="10" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>

                {{-- SUBCATEGORY 10 --}}
                <div id="10" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 10) !!}
                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="as_what">
                                <option value="As Thesis Adviser/Co-Adviser ">10.1 As Thesis Adviser/Co-Adviser</option>
                                <option value="As Thesis Adviser/Co-Adviser ">10.2 As Thesis Adviser/Co-Adviser</option>
                                <option value="As Thesis Examiner">10.3 As Thesis Examiner</option>
                                <option value="As Strategic Plan Panelist for Master of Management (max of 6 students per year) ">
                                    10.4 As Strategic Plan Panelist for Master of Management (max of 6 students per
                                    year)
                                </option>
                            </select>
                        </div>

                        {{-- Name of Graduate --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Graduate</label>
                            <input type="text" class="form-control" name="name_ofgraduate"
                                   placeholder="Name of Graduate" required>
                        </div>

                        {{-- Year Graduated--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Year Graduated</label>
                            <input type="number" class="form-control" name="year_graduated"
                                   placeholder="Year Graduated" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>As Thesis Adviser/Co-Adviser</td>
                                            <td>8each</td>
                                        </tr>

                                        <tr>
                                            <td>As Thesis  Reader/Panelist/Critic</td>
                                            <td>4each</td>
                                        </tr>

                                        <tr>
                                            <td>As Thesis Examiner </td>
                                            <td>4each</td>
                                        </tr>

                                        <tr>
                                            <td>As Strategic Plan Panelist for Master of Management (max of 6 students per year)  </td>
                                            <td>4each</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="8" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>


                {{-- SUBCATEGORY 11 --}}
                <div id="11" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 11) !!}
                        <div class="col-lg-12 form-group">
                            <select class="form-control" name="as_what">
                                <option value="As Adviser/Co-Adviser  (6 units)">11.1 As Adviser/Co-Adviser (6 units)
                                </option>

                                <option value="As Adviser/Co-Adviser  (3 units)">11.3 As Adviser/Co-Adviser (3 units)
                                </option>

                                <option value="As Reader/Panelist/Critic ">11.5 As Reader/Panelist/Critic</option>

                            </select>
                        </div>

                        {{-- Name of Graduate --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Name of Graduate</label>
                            <input type="text" class="form-control" name="name_ofgraduate"
                                   placeholder="Name of Graduate" required>
                        </div>

                        {{-- Year Graduated--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Year Graduated</label>
                            <input type="number" class="form-control" name="year_graduated"
                                   placeholder="Year Graduated" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>As Adviser /Co-Adviser (6 units</td>
                                            <td>6each</td>
                                        </tr>

                                        <tr>
                                            <td>As Adviser/Co-Adviser (3 units) </td>
                                            <td>3each</td>
                                        </tr>

                                        <tr>
                                            <td>As Reader/Panelist/Critic  </td>
                                            <td>2ach</td>
                                        </tr>

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="6" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>


                {{-- SUBCATEGORY 12 --}}
                <div id="12" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 12) !!}

                        {{-- Number of Preparations--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Number of Preparations
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other Information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small>
                                            <p>(SATE not lower than 2.0 in the combination of courses) </p>
                                            <p>Lecture and Laboratory class shall be counted separately even for the same course </p>
                                        </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="1" class="form-control" name="number_ofpreparations"
                                   placeholder="Number of Preparations" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1 preparation</td>
                                            <td>2pts</td>
                                        </tr>

                                        <tr>
                                            <td>2 preparations</td>
                                            <td>3pts</td>
                                        </tr>

                                        <tr>
                                            <td>3 preparations</td>
                                            <td>4pts</td>
                                        </tr>

                                        <tr>
                                            <td>4 preparations</td>
                                            <td>5pts</td>
                                        </tr>

                                        <tr>
                                            <td>5 or more preparations</td>
                                            <td>6pts</td>
                                        </tr>
                                        

                                        
                                        
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="6" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>

                {{-- SUBCATEGORY 13 --}}
                <div id="13" class="targetDiv" style="display:none;">
                    <form action="{!! $preLink.'meritpromotion/'.$formID.'/SubmitFormCat1' !!}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        {{-- HIDDEN ATTRIBUTES --}}
                        {!! Form::hidden('category', 2) !!}
                        {!! Form::hidden('subcategory', 13) !!}
                        {{-- Other Contributions --}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Other Contributions
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="Other information"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small>
                                            <p>(e.g., participation in preliminary, candidacy, comprehensive, qualifying examinations; placement exams); academic contributions (participation in curricular review and implementation; program advising, mentoring until completion of the degree, participation in RGE review and proposal preparation ) – to be determined by the Institute/Department
                                            </p>
                                            
                                        </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" name="other_contributions"
                                   placeholder="Other Contributions" required>
                        </div>

                        {{-- Points--}}
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="selectMulti">Points
                                <a href="javascript:void(0)" data-plugin="webuiPopover" data-title="POINTS SYSTEM"
                                   data-animation="pop" data-target="webuiPopover"
                                   data-content="
                                        <small> <table class='table'><thead>
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>POINTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Other contributions to teaching to be determined by the Institute/Department</td>
                                            <td>5max/year</td>
                                        </tr>

            
                                        </tbody>
                                        </table> </small>">
                                    <i class="icon wb-info-circle" aria-hidden="true"></i>
                                </a>
                            </label>
                            <input type="number" step="0.01" class="form-control" name="points"
                                   placeholder="Points" min="0" max="5" required>
                        </div>

                        {{-- Proofs--}}
                        <div class="col-md-12 form-group">
                            <label class="control-label" for="selectMulti">Attachment(s)</label>

                            <div class="form-group form-material form-material-file ">

                                <input type="text" class="form-control" placeholder="Browse.." readonly="">
                                <input type="file" id="inputFileAddCat1" name="file[]" multiple=""
                                       class="" onchange="checkFile()">
                            </div>


                        </div>
                        {{-- BUTTONS --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-normal" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $("[id^=inputFileAddCat1]").change(function () {
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'bmp':
                case 'png':
                case 'tif':
                case 'jpeg':
                case 'pdf':
                case 'doc':
                case 'docx':
                    break
                default:
                {
                    alert('Invalid file format! Please try again.');
                    this.value = '';
                }
            }
        });
    });
</script>

{{-- SHOW FIELDS FOR EACH SUBCATEGORY --}}
<script type="text/javascript">
    function showDiv() {

        // make all the divs hidden
        var x = document.getElementsByClassName("targetDiv");
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }

        // make the selected value appear
        var select = document.getElementById("Cat2SubCategories");
        var value = select.options[select.selectedIndex].value;
        document.getElementById(value).style.display = "block";

    }
</script>