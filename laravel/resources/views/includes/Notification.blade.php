<small>
    @if(!empty(session('notification')))
        <?php $notif = session('notification');?>
        @if(strpos($notif,'SUCCESS')===false)
            <div class="alert alert-alt alert-danger alert-dismissible"
                 role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <a class="alert-link">
                    <center>{{ session('notification') }}</center>
                </a>
            </div>
        @else
            <div class="alert alert-alt alert-success alert-dismissible"
                 role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <a class="alert-link">
                    <center>{{ session('notification') }}</center>
                </a>
            </div>
        @endif
    @endif
</small>