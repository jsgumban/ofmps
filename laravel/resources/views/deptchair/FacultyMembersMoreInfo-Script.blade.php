@if(Auth::user())
    @if((Auth::user()->position)==5)
        <?php $preLink = FOLDERNAME . '/admin/'; ?>
        <?php $currentUser = 'admin'; ?>
    @elseif((Auth::user()->position)==4)
        <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
        <?php $currentUser = 'chancellor'; ?>
    @elseif((Auth::user()->position)==3)
        <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
        <?php $currentUser = 'vchancellor'; ?>
    @elseif((Auth::user()->position)==2)
        <?php $preLink = FOLDERNAME . '/dean/'; ?>
        <?php $currentUser = 'dean'; ?>
    @elseif((Auth::user()->position)==1)
        <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
        <?php $currentUser = 'deptchair'; ?>
    @elseif((Auth::user()->position)==0)
        <?php $preLink = FOLDERNAME . '/faculty/'; ?>
        <?php $currentUser = 'faculty'; ?>
    @endif
@endif

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("[id^=MoreInfo-]").click(function (e) {
            e.preventDefault();
            var id = (this.id).split('-');
            var entryID = id[1];
            var operation = id[2];
            var retrieveDataUrl = "{{ $preLink.'dapc/' }}" + entryID + '/RetrieveUserData';
            var retrieveSubmittedForms = "{{ $preLink.'dapc/' }}" + entryID + '/RetrieveSubmittedForms';


            // function : update and view user profiles
            if (operation != "addnew") {
                $.get(retrieveDataUrl, function (data) {
                    // function : filters fields for the main system administrator and faculty members
                    if ((data[0].position) == "5") {
                        document.getElementById("rankDiv").style.display = "none";
                        document.getElementById("stepDiv").style.display = "none";
                        document.getElementById("departmentDiv").style.display = "none";
                        document.getElementById("positionDiv").style.display = "none";
                    } else {
                        document.getElementById("rankDiv").style.display = "block";
                        document.getElementById("stepDiv").style.display = "block";
                        document.getElementById("departmentDiv").style.display = "block";
                        document.getElementById("positionDiv").style.display = "block";
                    }

                    document.getElementById("positionSelectDiv").style.display = "none";
                    $('#ModalMInfo-modal-1').attr('class', "modal fade modal-success in");

                    $('#first_name').val(data[0].first_name);
                    $('#middle_name').val(data[0].middle_name);
                    $('#last_name').val(data[0].last_name);
                    $('#gender').val(data[0].gender);
                    $('#employee_code').val(data[0].employee_code);
                    $('#department').val(data[0].department_belongs);
                    $('#address').val(data[0].address);
                    $("#lastPromotion").empty();
                    $("#lastPromotion").append(data[0].last_promotion);


                    // function : displays position
                    switch (data[0].position) {
                        case 0:
                            $('#position').val("Faculty");
                            break;
                        case 1:
                            $('#position').val("Department Chairperson");
                            break;
                        case 2:
                            $('#position').val("College Dean");
                            break;
                        case 3:
                            $('#position').val("Vice Chancellor");
                            break;
                        case 4:
                            $('#position').val("Chancellor");
                            break;
                    }

                    //  functions : displays rank and step
                    if (data[0].rank == null || data[0].step == null) {
                        $('#rank').val("None");
                        $('#step').val("");
                    } else {
                        $('#rank').val(data[0].rank);
                        $('#step').val(data[0].step);
                    }

                    if ((data[0].birthday) != '0000-00-00') {
                        $('#birthday').val(data[0].birthday);
                    } else {
                        $('#birthday').val("");
                    }

                    if (operation != 'update') {
                        // function : view information
                        document.getElementById("displayAction").innerHTML = "MORE INFORMATION";
                        document.getElementById("submitButton").style.display = "none";
                        document.getElementById("addNewButton").style.display = "none";

                        $('#ModalMInfo-modal-1').attr('class', "modal fade modal-danger in");
                        $('#first_name').attr("disabled", true);
                        $('#middle_name').attr("disabled", true);
                        $('#last_name').attr("disabled", true);
                        $('#gender').attr("disabled", true);
                        $('#employee_code').attr("disabled", true);
                        $('#position').attr("disabled", true);
                        $('#department').attr("disabled", true);
                        $('#rank').attr("disabled", true);
                        $('#step').attr("disabled", true);
                        $('#birthday').attr("disabled", true);
                        $('#address').attr("disabled", true);
                    } else {
                        // function : update information
                        document.getElementById("displayAction").innerHTML = "UPDATE INFORMATION";
                        document.getElementById("addNewButton").style.display = "none";
                        document.getElementById("submitButton").style.display = "block";


                        var submitUpdatedProfileButtonURL = "{{ $preLink.'userprofiles/'}}" + data[0].employee_code + '/SaveProfile';
                        $('#submitUpdatedProfileButton').attr('action', submitUpdatedProfileButtonURL);
                    }
                });

                $.get(retrieveSubmittedForms, function (data) {
                    $("#submittedForms").empty();

                    @if((Auth::user()->position)==1)
                        <?php $action = "DAPCMonitorForm"; ?>
                    @elseif((Auth::user()->position)==2)
                        <?php  $action = "CAPCMonitorForm"; ?>
                    @elseif((Auth::user()->position)==3)
                        <?php $action = "UAPFCMonitorForm"; ?>
                    @elseif((Auth::user()->position)==4)
                        <?php $action = "CHANCELLORMonitorForm"; ?>
                    @elseif((Auth::user()->position)==5)
                        <?php $action = "ADMINMonitorForm"; ?>
                    @endif
                    if (data.length != 0) {
                        for (var x = 0; x < data.length; x++) {

                            $("#submittedForms").append("<a href=" + "{{ $preLink }}" + "meritpromotion/" + data[x].id + "/" + "{{ $action }}" + " target=\"_blank\"+><i class=\"icon wb-file\" style=\"font-size: 18px\"></i></a>");
                        }
                    } else {
                        $("#submittedForms").append("None")
                    }
                });


            }


            // function : add new user
            else if (operation == "addnew") {
                $('#ModalMInfo-modal-1').attr('class', "modal fade modal-info in");
                document.getElementById("displayAction").innerHTML = "ADD NEW USER";
                document.getElementById("positionSelectDiv").style.display = "block";
                document.getElementById("addNewButton").style.display = "block";
                document.getElementById("submitButton").style.display = "none";
                document.getElementById("positionDiv").style.display = "none";
                document.getElementById("rankDiv").style.display = "block";
                document.getElementById("stepDiv").style.display = "block";
                document.getElementById("departmentDiv").style.display = "block";
                var submitUpdatedProfileButtonURL = "{{ $preLink.'userprofiles/SaveNewUser'}}";
                $('#submitUpdatedProfileButton').attr('action', submitUpdatedProfileButtonURL);

                $('#first_name').val("");
                $('#middle_name').val("");
                $('#last_name').val("");
                $('#gender').val("");
                $('#employee_code').val("");
                $('#department').val("");
                $('#rank').val("");
                $('#step').val("");
                $('#birthday').val("");
                $('#address').val("");
            }
        });
    });
</script>