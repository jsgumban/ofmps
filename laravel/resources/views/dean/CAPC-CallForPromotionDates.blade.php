@if(Auth::user())
    @if((Auth::user()->position)==5)
        <?php $preLink = FOLDERNAME . '/admin/'; ?>
        <?php $currentUser = 'admin'; ?>
    @elseif((Auth::user()->position)==4)
        <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
        <?php $currentUser = 'chancellor'; ?>
    @elseif((Auth::user()->position)==3)
        <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
        <?php $currentUser = 'vchancellor'; ?>
    @elseif((Auth::user()->position)==2)
        <?php $preLink = FOLDERNAME . '/dean/'; ?>
        <?php $currentUser = 'dean'; ?>
    @elseif((Auth::user()->position)==1)
        <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
        <?php $currentUser = 'deptchair'; ?>
    @elseif((Auth::user()->position)==0)
        <?php $preLink = FOLDERNAME . '/faculty/'; ?>
        <?php $currentUser = 'faculty'; ?>
    @endif
@endif

<div class="tab-pane active" id="category-1" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">
        <h4 id="ASDASD">
            <center><b>CALL FOR PROMOTION {{ ' ('.$college->college_initial.')' }}</b><br></center>
        </h4>
        @if(session('activeTab')==1)
            @if(!empty(session('notification')))
                <br>
                <?php $notif = session('notification');?>
                @if(strpos($notif,'SUCCESS')===false)
                    <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @else
                    <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @endif
            @endif
        @endif
        <hr>

        <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Start</th>
                <th>End</th>
                <th>Status</th>
                <th>Form count</th>
                <th>
                    <center> Action</center>
                </th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th>#</th>
                <th>Start</th>
                <th>End</th>
                <th>Status</th>
                <th>Form count</th>
                <th>
                    <center> Action</center>
                </th>
            </tr>
            </tfoot>

            <tbody>
            <?php $count = 1; ?>
            <?php $counter = 0; ?>
            @foreach ($mpromotionDates as $mpromotionDate)
                <tr>
                    <td> <?php echo $count++; ?>  </td>
                    <td> {{ date('F d, Y', strtotime($mpromotionDate->mpromotion_date_start )) }}</td>
                    <td> {{ date('F d, Y', strtotime($mpromotionDate->mpromotion_date_end )) }}</td>
                    <td>
                        @if($mpromotionDate->mpromotion_status == 0)
                            <span class="label label-success">ON-GOING</span>
                        @elseif($mpromotionDate->mpromotion_status == 1)
                            <span class="label label-danger ">FINISHED</span>
                        @elseif($mpromotionDate->mpromotion_status == 2)
                            <span class="label label-warning ">NOT YET STARTED</span>
                        @endif
                    </td>
                    <td><span class="label label-dark">{{ count($forms[$counter])}}</span></td>
                    <td>
                        <center>

                            <a href='{!! $preLink.'capc/'.$mpromotionDate->id.'/ListOfSubmittedForms' !!}'
                               data-toggle="tooltip"
                               title="Submmited Forms">
                                <button type="button" class="btn btn-icon social-instagram btn-sm"
                                        data-target="#exampleModalWarning1" data-toggle="modal"><i
                                            class="icon fa-file-text-o" aria-hidden="true"
                                            style="font-size: 15px;"></i>
                                </button>
                            </a>

                        </center>

                    </td>
                </tr>
                <?php $counter++; ?>
            @endforeach

            </tbody>
        </table>
    </div>
</div>



