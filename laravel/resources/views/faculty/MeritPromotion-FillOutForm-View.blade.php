@if(Auth::user())
    @if((Auth::user()->position)==5)
        <?php $preLink = FOLDERNAME . '/admin/'; ?>
        <?php $currentUser = 'admin'; ?>
    @elseif((Auth::user()->position)==4)
        <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
        <?php $currentUser = 'chancellor'; ?>
    @elseif((Auth::user()->position)==3)
        <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
        <?php $currentUser = 'vchancellor'; ?>
    @elseif((Auth::user()->position)==2)
        <?php $preLink = FOLDERNAME . '/dean/'; ?>
        <?php $currentUser = 'dean'; ?>
    @elseif((Auth::user()->position)==1)
        <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
        <?php $currentUser = 'deptchair'; ?>
    @elseif((Auth::user()->position)==0)
        <?php $preLink = FOLDERNAME . '/faculty/'; ?>
        <?php $currentUser = 'faculty'; ?>
    @endif
@endif

<div class="tab-pane" id="category-2" role="tabpanel">
    <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
         aria-multiselectable="true" role="tablist">
        <h4 id="ASDASD">
            <div class="btn-group pull-right">
                {{-- CREATE NEW COLLEGE --}}
                <span data-toggle="tooltip" data-target="#id"
                      title="Create New Form">
                      <button type="button" data-target="#addNewForm"
                              data-toggle="modal"
                              class="btn btn-animate social-facebook dropdown-toggle"
                              id="exampleHoverDropdown3" data-toggle="dropdown">
                          <span><i class="icon wb-plus-circle" aria-hidden="true"></i> New Form</span>
                      </button>
                </span>
            </div>
            <center><b>MERIT PROMOTION FORMS</b><br></center>
        </h4>

        @if(session('activeTab')==2)
            @if(!empty(session('notification')))
                <br>
                <?php $notif = session('notification');?>
                @if(strpos($notif,'SUCCESS')===false)
                    <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @else
                    <div class="alert alert-alt alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a class="alert-link">
                            <center>{{ session('notification') }}</center>
                        </a>
                    </div>
                @endif
            @endif
        @endif
        <hr>

        <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Form Name</th>
                <th>Filled Out Date</th>
                <th>Status</th>

                <th style="text-align:right">Action</th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th>#</th>
                <th>Form Name</th>
                <th>Filled Out Date</th>
                <th>Status</th>

                <th style="text-align:right">Action</th>
            </tr>
            </tfoot>


            <tbody>
            <?php $count = 1; ?>
            @foreach ($OwnDraftForms as $OwnDraftForm)
                <tr>
                    <td> <?php echo $count++; ?>  </td>
                    <td> {{ $OwnDraftForm->form_name }}  </td>
                    <td>
                        {{ date('F d, Y', strtotime($OwnDraftForm->fillOut_date)) }}

                    </td>
                    <td> Draft</td>
                    <td style="text-align:right">

                        {{-- UPDATE FORM --}}
                        <a href="{!! $preLink.'meritpromotion/'.$OwnDraftForm->id.'/UpdateForm' !!}"
                           data-toggle="tooltip" title="Update Form">
                            <button type="button"
                                    class="btn btn-icon btn-success  btn-sm btn-sm  btn-sm"
                                    ><i class="icon fa-pencil-square-o"
                                        aria-hidden="true"
                                        style="font-size: 15px;"></i></button>
                        </a>

                        {{-- GENERATE PDF --}}
                        <a href="{!! $preLink.'meritpromotion/'.$OwnDraftForm->id.'/generatePDF' !!}"
                           data-toggle="tooltip" title="Form(pdf)" target="_blank">
                            <button type="button"
                                    class="btn btn-icon btn-warning  btn-sm btn-sm  btn-sm"
                                    ><i class="icon fa-file-pdf-o"
                                        aria-hidden="true"
                                        style="font-size: 15px;"></i></button>
                        </a>

                        {{-- DOWNLOAD ATTACHMENTS --}}
                        <a
                                data-toggle="tooltip" title="Attachments(zip)"
                                id="{{'zip'.$count.'-'.$OwnDraftForm->id}}">
                            <button type="button"
                                    class="btn btn-icon btn-info btn-active  btn-sm btn-sm  btn-sm"
                                    ><i class="icon fa-file-zip-o "
                                        aria-hidden="true"
                                        style="font-size: 15px;"></i></button>
                        </a>

                        {{-- DELETE DRAFT FORM --}}
                        <a href="{!! $preLink.'meritpromotion/'.$OwnDraftForm->id.'/DeleteDraftForm' !!}"
                           onclick="
                                        if (! confirm('Are you sure do you want remove this form?')) { return false; }
                                        " data-toggle="tooltip" title="Delete Form">
                            <button type="button" class="btn btn-icon btn-danger  btn-sm btn-sm  btn-sm"
                                    ><i class="icon wb-trash" aria-hidden="true"
                                        style="font-size: 15px;"></i></button>
                        </a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>

<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // UPDATEFC1-TR-CATEGORY-SUBCAT-ENTRYID
        $("[id^=zip]").click(function (e) {
            var id = (this.id).split('-');
            var formID = id[1];
            var generateZipURL = "{{ $preLink.'meritpromotion/'}}" + formID + '/generateZip';
            var downloadZipURL = "{{ $preLink.'meritpromotion/'}}" + formID + '/downloadZip';
            $.get(generateZipURL, function (data) {
                window.location = "{{ ZIPLOCATION }}" + "/" + data;
            });
        });
    });
</script>
@include('faculty.MeritPromotion-FillOutForm-Add')