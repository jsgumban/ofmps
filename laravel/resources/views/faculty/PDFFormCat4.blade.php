<h3>IV. PROFESSIONAL GROWTH</h3>
<div class="page-break">
    <div>
        <h5>1. Workshop/Training/Conference Organizer or Chair (maximum of four (4) activities/year)
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Workshop/Training/Conference</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==1)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->name_ofevent}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>2. Authorship of a reference book in one's field of discipline
        </h5>
        <table>
            <thead>
            <tr>
                <th>Title of the Reference Book</th>
                <th>Author(s)</th>
                <th>Date Published</th>
                <th>Title, Vol. No. of Journal/Publication</th>
                <th> Nature of Publication</th>


                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==2)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->author}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_published ))}}</td>
                        <td>{{$pgrowth->bibliography}}</td>
                        <td>{{$pgrowth->nature_ofpublication}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>3. Authorship of a college-level textbook/laboratory/course manual
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published</th>
                <th>Title, Vol. No. of Journal/Publication</th>
                <th> Nature of Publication</th>


                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==3)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->author}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_published))}}</td>
                        <td>{{$pgrowth->bibliography}}</td>
                        <td>{{$pgrowth->nature_ofpublication}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>4. Authorship of High School and Elementary Manual/textbook
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published</th>
                <th>Title, Vol. No. of Journal/Publication</th>
                <th>Nature</th>


                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==4)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->author}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_published))}}</td>
                        <td>{{$pgrowth->bibliography}}</td>
                        <td>{{$pgrowth->nature_ofpublication}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>5. Popular presentations and popularized lectures on topics within the discipline (Maximum of 12
            presentations / articles per year)
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Author(s)</th>
                <th>Date Published/Lectured</th>
                <th>Name of Event</th>
                <th>Nature</th>


                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==5)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->author}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_published))}}</td>
                        <td>{{$pgrowth->bibliography}}</td>
                        <td>{{$pgrowth->nature_ofpublication}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>6. Resource person in print or broadcast media presentations
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title</th>
                <th>Date and Place Presentation</th>
                <th> Nature of Presentation</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==6)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                        <td>{{$pgrowth->nature_ofpublication}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>7. Trainor/sports official within area of specialization (international/national)
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Training</th>
                <th>Date and Place of Training</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==7)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>


    <div>
        <h5>8. Membership in technical committee
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Committee</th>
                <th>Date of Appointment</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==8)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation))}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>9. Advisorship to Professional Organization
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Organization</th>
                <th>Date and Place</th>
                <th>Nature of Organization</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==9)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                        <td>{{$pgrowth->nature_ofpublication}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>


    <div>
        <h5>10. Active Participation in outreach program related to own specialization (e.g. Pahinungod)
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Program</th>
                <th>Participation</th>
                <th>Date and Place</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==10)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->participation}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>11. Resource person in meetings/symposiums/extension activities related to issues within your
            discipline
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Activity</th>
                <th>Participation</th>
                <th>Date and Place</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==11)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->participation}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>12. Attendance in International/Regional meeting/conference/training course/workshop
        </h5>
        <table>
            <thead>
            <tr>
                <th>Title of Conference</th>
                <th>Date and Place</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==12)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->title}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>13. Attendance in National/University meetings/conference/training course workshop
        </h5>
        <table>
            <thead>
            <tr>
                <th>Title of Conference</th>
                <th>Date and Place</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==13)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->title}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>14. Highly specialized training related to discipline with contribution to the college (duration of at
            least 2 weeks)
        </h5>
        <table>
            <thead>
            <tr>
                <th>Title of Training</th>
                <th>Date and Place</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==14)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->title}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_ofpresentation)).'/'.$pgrowth->place_ofpresentation}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>15. Patents
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Patent</th>
                <th>Nature of Patent</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==15)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->nature_ofpublication}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>16. Citations (indicate which journal)
        </h5>
        <table>
            <thead>
            <tr>
                <th>Name of Citation</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==16)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>17. Awards
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Award</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==17)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>18. Membership in Editorial Board (Refereed Publication (journal, proceedings)/other print media)</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Publication</th>
                <th>Nature of Publication</th>
                <th>Period Covered</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==18)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->nature_ofpublication}}</td>
                        <td>{{$pgrowth->period_covered}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>19. Referee to a publication</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Title of Book/Artice</th>
                <th>Publisher/Journal Vol./No.</th>
                <th>Date of Publication</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==19)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->subcategory}}</td>
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->bibliography}}</td>
                        <td>{{date('F d, Y', strtotime($pgrowth->date_published))}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>20. Research Fellowship/Visiting Professor Appointment in a reputable university (with MOA)</h5>
        <table>
            <thead>
            <tr>
                <th>Name of University</th>
                <th>Duration of Appointment</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==20)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->duration_ofappointment}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>21. Other related services to the community (e.g. reviewer of research/creative work development of
            discipline or tertiary education)</h5>
        <table>
            <thead>
            <tr>
                <th>Other Services</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($pgrowths as $pgrowth)
                @if($pgrowth->category==21)
                    <tr data-isaccepteddeptchair="{{$pgrowth->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$pgrowth->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$pgrowth->isAccepted_vchancellor}}">
                        <td>{{$pgrowth->title}}</td>
                        <td>{{$pgrowth->other_details}}</td>
                        <td align="right"> {{$pgrowth->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

</div>

