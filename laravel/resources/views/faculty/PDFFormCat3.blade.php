<h3>III. SERVICE</h3>
<div class="page-break">
    <h4>A. SERVICE TO DEPARTMENT/COLLEGE/UNIVERSITY </h4>

    <div>
        <h5>1. Regular administrative positions in the College/Institute/Department
        </h5>
        <table>
            <thead>
            <tr>
                <th>Designation</th>
                <th>Period Covered</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($services as $service)
                @if($service->category==1 && $service->subcategory==1)
                    <tr data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$service->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                        <td>{{$service->designation}}</td>
                        <td>{{$service->period_covered}}</td>
                        <td>{{$service->other_details}}</td>
                        <td align="right"> {{$service->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5>2. Membership in a standing committee of the Institute/Department/College/University. If membership in
            the committee is by virtue of the faculty’s administrative post then no point is given. </h5>
        <table>
            <thead>
            <tr>
                <th>Name of Committee</th>
                <th>Nature of Committee</th>
                <th>Period Covered</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($services as $service)
                @if($service->category==1 && $service->subcategory==2)
                    <tr data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$service->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                        <td>{{$service->name_ofcommittee}}</td>
                        <td>{{$service->nature_ofcommittee}}</td>
                        <td>{{$service->period_covered}}</td>
                        <td>{{$service->other_details}}</td>
                        <td align="right">{{$service->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 3. Resource Generation</h5>
        <table>
            <thead>
            <tr>
                <th>Funding Source</th>
                <th>Purpose of Fund</th>
                <th>Amount Donated</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($services as $service)
                @if($service->category==1 && $service->subcategory==3)
                    <tr data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$service->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                        <td>{{$service->funding_source}}</td>
                        <td>{{$service->purpose_offund}}</td>
                        <td>{{$service->ammount_donated}}</td>
                        <td>{{$service->other_details}}</td>
                        <td align="right">{{$service->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 4. Officer-in-Charge to the Chancellor/Vice-Chancellor/Dean/Director/College Secretary/Department
            Head </h5>
        <table>
            <thead>
            <tr>
                <th>Designation of OIC</th>
                <th>Level</th>
                <th>Period Covered</th>
                <th>Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($services as $service)
                @if($service->category==1 && $service->subcategory==4)
                    <tr data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$service->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                        <td>{{$service->designation_ofoic}}</td>
                        <td>{{$service->level}}</td>
                        <td>{{$service->period_covered}}</td>
                        <td>{{$service->other_details}}</td>
                        <td align="right">{{$service->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>


    <h4>B. SERVICE TO INTERNATIONAL/NATIONAL/REGIONAL COMMUNITY </h4>

    <div>
        <h5>1. Presidency/Chairmanship of Professional association/organization
        </h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Organization</th>
                <th>Designation</th>
                <th> Period Covered</th>
                <th> Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($services as $service)
                @if($service->category==2 && $service->subcategory==1)
                    <tr data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$service->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                        <td>{{$service->subsubcategory}}</td>
                        <td>{{$service->name_oforganization}}</td>
                        <td>{{$service->designation}}</td>
                        <td>{{$service->period_covered}}</td>
                        <td>{{$service->other_details}}</td>
                        <td align="right"> {{$service->points}} </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>

    <div>
        <h5> 2. Position of responsibility in Professional association/organization</h5>
        <table>
            <thead>
            <tr>
                <th>Category</th>
                <th>Name of Organization</th>
                <th>Designation</th>
                <th> Period Covered</th>
                <th> Other Details</th>
                <th style="text-align:right">Points</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($services as $service)
                @if($service->category==2 && $service->subcategory==2)
                    <tr data-isaccepteddeptchair="{{$service->isAccepted_deptchair}}"
                        data-isaccepteddean="{{$service->isAccepted_dean}}"
                        data-isacceptedvchancellor="{{$service->isAccepted_vchancellor}}">
                        <td>{{$service->subsubcategory}}</td>
                        <td>{{$service->name_oforganization}}</td>
                        <td>{{$service->designation}}</td>
                        <td>{{$service->period_covered}}</td>
                        <td>{{$service->other_details}}</td>
                        <td align="right">{{$service->points}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <br><br>
</div>

