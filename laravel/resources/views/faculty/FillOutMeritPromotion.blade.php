@extends('app')
@section('content')

    @if(Auth::user())
        @if((Auth::user()->position)==5)
            <?php $preLink = FOLDERNAME . '/admin/'; ?>
            <?php $currentUser = 'admin'; ?>
        @elseif((Auth::user()->position)==4)
            <?php $preLink = FOLDERNAME . '/chancellor/'; ?>
            <?php $currentUser = 'chancellor'; ?>
        @elseif((Auth::user()->position)==3)
            <?php $preLink = FOLDERNAME . '/vchancellor/'; ?>
            <?php $currentUser = 'vchancellor'; ?>
        @elseif((Auth::user()->position)==2)
            <?php $preLink = FOLDERNAME . '/dean/'; ?>
            <?php $currentUser = 'dean'; ?>
        @elseif((Auth::user()->position)==1)
            <?php $preLink = FOLDERNAME . '/deptchair/'; ?>
            <?php $currentUser = 'deptchair'; ?>
        @elseif((Auth::user()->position)==0)
            <?php $preLink = FOLDERNAME . '/faculty/'; ?>
            <?php $currentUser = 'faculty'; ?>
        @endif
    @endif

    @include('includes.page-title')
    @include('includes.Styles')

    <div class="page animsition"><br>

        <div class="page-content container-fluid">
            <div class="row">
                {{-- left side --}}
                {{-- includes navigation panels --}}
                <div class="col-lg-4 col-sm-4" id="shrink">
                    <div class="panel">
                        @include('includes.Ribbons')
                        <div class="panel-body">
                            @include('includes.Navigation-Panel')
                            @include('includes.Submit-Buttons')
                        </div>
                    </div>
                    @include('includes.Remarks-Legend')
                </div>

                {{-- right side --}}
                {{-- includes main content --}}
                <div class="col-lg-8 col-sm-8" id="maximize">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                @if(Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                                    @include('faculty.Category1-View')
                                    @include('faculty.Category2-View')
                                    @include('faculty.Category3-View')
                                    @include('faculty.Category4-View')

                                    @include('faculty.Category1-Update')
                                    @include('faculty.Category1-Script')
                                    @include('faculty.Category2-Update')
                                    @include('faculty.Category2-Script')
                                    @include('faculty.Category3-Update')
                                    @include('faculty.Category3-Script')
                                    @include('faculty.Category4-Update')
                                    @include('faculty.Category4-Script')
                                @endif

                                @if (Request::is('*/FillOutMeritPromotion') || Request::is('*/UpdateForm') ||Request::is('*/EvaluateForm') || Request::is('*/ReviseForm'))
                                    @include('faculty.Category1-View')
                                    @include('faculty.Category1-Add')
                                    @include('faculty.Category1-Update')
                                    @include('faculty.Category1-Script')

                                    @include('faculty.Category2-View')
                                    @include('faculty.Category2-Add')
                                    @include('faculty.Category2-Update')
                                    @include('faculty.Category2-Script')


                                    @include('faculty.Category3-View')
                                    @include('faculty.Category3-Add')
                                    @include('faculty.Category3-Update')
                                    @include('faculty.Category3-Script')

                                    @include('faculty.Category4-View')
                                    @include('faculty.Category4-Add')
                                    @include('faculty.Category4-Update')
                                    @include('faculty.Category4-Script')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('includes.Submit-Modals')
    @include('includes.Remarks-Modals')
    @include('includes.Scripts')
@endsection