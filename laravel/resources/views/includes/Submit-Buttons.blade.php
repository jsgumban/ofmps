{{-- buttons --}}
@if (Request::is('*/FillOutMeritPromotion') || Request::is('*/UpdateForm') || Request::is('*/ReviseForm'))
    <p class=" pull-right">
        @if(Request::is('*/FillOutMeritPromotion') || Request::is('*/UpdateForm'))
            <a class="btn btn-sm btn-info" href="{!! $preLink.'meritpromotion/SaveAsDraft' !!} ">Save
                as Draft</a>
        @endif

        @if(((!empty($isThereCallForPromotion)) && (empty($isSubmittedToDeptChairOnThatCFP))))
            <a class="btn btn-sm btn-success" onclick="
                                        if (! confirm('Are you sure do you want submit this form to the Department Chairperson?')) { return false; }
                                        "
               href="{!! $preLink.'meritpromotion/'.$formID.'/SubmitDeptChair' !!}">Submit to
                Department Chair</a>
        @else
            <a class="btn btn-sm btn-success"
               href="#" disabled data-toggle="tooltip"
               title="Not Allowed">Submit to
                Department Chair</a>
        @endif
    </p>
@endif

@if(Request::is('*/EvaluateForm'))
    @if(Auth::user()->position==1)
        <p class=" pull-right">
            @if((!empty($isThereCallForPromotion)))
                {{-- not allowed to send dean --}}
                <a class="btn btn-sm btn-danger" data-toggle="modal" id="{{'needsRevisions-'.$formID}}"
                   data-target="#ModalRejectDAPC">
                    <i class="icon wb-close" aria-hidden="true"></i>
                </a>
            @else
               <a class="btn btn-sm btn-danger" disabled>
                    <i class="icon wb-close" aria-hidden="true"></i>
                </a>
            @endif


            @if($isEnableReviseButton==1)
                <a class="btn btn-sm btn-warning" data-toggle="modal" data-target="#ModalneedsRevisions"
                   id="{{'needsRevisions-'.$formID}}">
                    <i class="icon wb-warning" aria-hidden="true"></i> Needs Revisions
                </a>

                <a class="btn btn-sm btn-success" href="#" disabled><i class="icon wb-check" aria-hidden="true"></i></a>

            @elseif($isEnableReviseButton==0)
                <a class="btn btn-sm btn-warning" disabled>
                    <i class="icon wb-refresh" aria-hidden="true"></i>
                </a>
                @if(($isEnableSubmitDean==1) && (!empty($isThereCallForPromotion)))
                    <a class="btn btn-sm btn-success" data-toggle="modal" id="{{'submitDean-'.$formID}}"
                       data-target="#ModalsubmitDean">Submit to Dean</a>
                @else
                    <a class="btn btn-sm btn-success" href="#" disabled>Submit to Dean</a>
                @endif
            @endif
        </p>


    @elseif(Auth::user()->position==2)
        <p class=" pull-right">
         @if((!empty($isThereCallForPromotion)))
            <a class="btn btn-sm btn-danger" data-toggle="modal" id="{{'needsRevisions-'.$formID}}"
               data-target="#ModalRejectCAPC">
                <i class="icon wb-close" aria-hidden="true"></i>
            </a>
        @else
            <a class="btn btn-sm btn-danger" disabled>
                <i class="icon wb-close" aria-hidden="true"></i>
            </a>
        @endif


            @if($isEnableSubmitVChancellor==1 && (!empty($isThereCallForPromotion)))
                <a class="btn btn-sm btn-success" href="#" data-toggle="modal" data-target="#ModalsubmitVchancellor"
                   id="{{'submitVchancellor-'.$formID}}">Submit to Vice Chancellor</a>
            @else
                <a class="btn btn-sm btn-success" href="#" disabled>Submit to Vice Chancellor</a>
            @endif
        </p>


    @elseif(Auth::user()->position==3)
        <p class=" pull-right">
            @if((!empty($isThereCallForPromotion)))
                <a class="btn btn-sm btn-danger" data-toggle="modal" id="{{'needsRevisions-'.$formID}}"
                   data-target="#ModalRejectUAPFC">
                    <i class="icon wb-close" aria-hidden="true"></i>
                </a>
            @else
                <a class="btn btn-sm btn-danger" disabled>
                    <i class="icon wb-close" aria-hidden="true"></i>
                </a>
            @endif   

            @if($isEnableSubmitChancellor==1 && (!empty($isThereCallForPromotion)))
                <a class="btn btn-sm btn-success" id="{{'submitChancellorButton-'.$formID}}" href="#"
                   data-toggle="modal" data-target="#ModalsubmitChancellor">Submit to Chancellor</a>
            @else
                <a class="btn btn-sm btn-success" href="#" disabled>Submit to Chancellor</a>
            @endif
        </p>

    @elseif(Auth::user()->position==4)
        <p class=" pull-right">
            @if((!empty($isThereCallForPromotion)))
                <a class="btn btn-sm btn-success"
                   href="#" id="{{'chancellorEvaluation-'.$formID}}" data-toggle="modal"
                   data-target="#ModalapproveChancellor">Final Evaluation</a>
            @else
                <a class="btn btn-sm btn-success"
                   href="#" disabled>Final Evaluation</a>
            @endif
        </p>
    @endif
@endif
 
