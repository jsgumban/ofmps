<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("[id^=UpdateFC4-tr]").click(function (e) {
            e.preventDefault();

            // REVISE FUNCTION INITIALIZATION
            @if (Request::is('*/ReviseForm'))
                $("[id^=ribbon-revise]").css('display', "none");
            $("[id^=ribbon-rejected]").css('display', "none");
            $("[id^=UpdateFC4-]").attr('disabled', false);
            @endif

            // UPLOAD ATTACHMENT INITIALIZATION
            $('input:radio[name="proofAction"][value="1"]').prop('checked', true);
            $("[id^=c4BrowseDiv]").css('display', 'block');


            var id = (this.id).split('-');
            var category = id[2];
            var subCategory = id[3];
            var entryID = id[4];

            var retrieveDataUrl = "{{ $preLink.'meritpromotion/' }}" + entryID + '/RetrieveCat4';
            var submitButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/UpdateFormCat4';
            var deleteButtonUrl = "{{ $preLink.'meritpromotion/'}}" + entryID + '/DeleteEntryCat4';

            if (category == 1) {
                var x = document.getElementsByClassName("UpdateFC4-divClass-1");
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById("UpdateFC4-divID-" + category + "-" + subCategory).style.display = "block";
            } else if (category == 2) {
                var x = document.getElementsByClassName("UpdateFC4-divClass-2");
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById("UpdateFC4-divID-" + category + "-" + subCategory).style.display = "block";
            }

            $.get(retrieveDataUrl, function (data) {
                // REVISE FORM 
                @if (Request::is('*/ReviseForm'))
                    // initialize
                var entryStatus = data.isAccepted_deptchair;
                // submit buttons - textfields - upload section
                $('[id^=UpdateFC4-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC4-]").attr('disabled', true);
                $("[id^=UpdateFC4-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                // needs revision
                if (entryStatus == 3) {
                    $('[id^=UpdateFC4-reviseDiv-]').css('display', 'block');
                    $("[id^=UpdateFC4-]").attr('disabled', false);
                    $("[id^=UpdateFC4-proofs-]").css('display', "block");
                    $("[id^=ribbon-revise]").css('display', "block");
                    $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                } else if (entryStatus == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                }
                @endif

                @if (Request::is('*/MonitorForm') || Request::is('*/DAPCMonitorForm') || Request::is('*/CAPCMonitorForm') || Request::is('*/UAPFCMonitorForm') || Request::is('*/CHANCELLORMonitorForm'))
                    // submit buttons - textfields - upload section
                $('[id^=UpdateFC4-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC4-]").attr('disabled', true);
                $("[id^=UpdateFC4-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                if (data.isAccepted_deptchair == 3) {
                    $("[id^=ribbon-revise]").css('display', "block");
                    $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                } else if (data.isAccepted_deptchair == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                } else if (data.isAccepted_dean == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                } else if (data.isAccepted_vchancellor == 2) {
                    $("[id^=ribbon-rejected]").css('display', "block");
                    $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (UAPFC)");
                }
                @endif

                @if (Request::is('*/EvaluateForm'))
                    // checkbox
                $('[id^=labelauty-][value="1"]').prop("checked", false);
                $('[id^=labelauty-][value="2"]').prop("checked", false);
                $('[id^=labelauty-][value="3"]').prop("checked", false);

                // submit buttons - textfields - upload section
                $('[id^=UpdateFC4-reviseDiv-]').css('display', 'none');
                $("[id^=UpdateFC4-]").attr('disabled', true);
                $("[id^=UpdateFC4-proofs-]").css('display', "none");

                // ribons
                $("[id^=ribbon-revise]").css('display', "none");
                $("[id^=ribbon-rejected]").css('display', "none");
                $("[id^=ribbon-notyet]").css('display', "none");
                $("[id^=ribbon-accept]").css('display', "none");

                // points
                // data-points - check box purposes
                $("[id^=UpdateFC4-points-]").attr('disabled', true);
                $("[id^=UpdateFC4-points-]").css('background-color', '');
                $("[id^=UpdateFC4-points-]").css('color', '');
                $("[id^=labelauty]").attr("data-points", data.points);

                var formStatus = ({{ $MpromotionForm->form_status }});

                // FORM IS CURRENTLY EVALUATED BY THE DEPTCHAIR
                if (formStatus == 1) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    // accepted
                    if (entryStatusDC == 1) {
                        $("[id^=UpdateFC4-points-]").css('background-color', '#FFEFD5');
                        $("[id^=UpdateFC4-points-]").css('color', 'black');
                        $("[id^=UpdateFC4-points-]").attr('disabled', false);
                        $('[id^=labelauty-][value="1"]').prop("checked", true);

                        $("[id^=ribbon-accept]").css('display', "block");
                        $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (DAPC)");
                    }

                    // rejected
                    else if (entryStatusDC == 2) {
                        $("[id^=UpdateFC4-points-]").css('background-color', '');
                        $("[id^=UpdateFC4-points-]").css('color', '');
                        $("[id^=UpdateFC4-points-]").attr('disabled', true);
                        $('[id^=labelauty-][value="2"]').prop("checked", true);

                        $("[id^=ribbon-rejected]").css('display', "block");
                        $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                    }

                    // needs revision
                    else if (entryStatusDC == 3) {
                        $("[id^=UpdateFC4-points-]").css('background-color', '');
                        $("[id^=UpdateFC4-points-]").css('color', '');
                        $("[id^=UpdateFC4-points-]").attr('disabled', true);
                        $('[id^=labelauty-][value="3"]').prop("checked", true);


                        $("[id^=ribbon-revise]").css('display', "block");
                        $("[id^=ribbon-revise] .ribbon-inner").html("NEEDS REVISION (DAPC)");
                    }

                    // not yet evaluated
                    else if (entryStatusDC == null) {
                        $("[id^=UpdateFC4-points-]").css('background-color', '');
                        $("[id^=UpdateFC4-points-]").css('color', '');
                        $("[id^=UpdateFC4-points-]").attr('disabled', true);

                        $("[id^=ribbon-notyet]").css('display', "block");
                        $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (DAPC)");
                    }
                }

                // FORM IS CURRENTLY EVALUATED BY THE DEAN
                else if (formStatus == 2) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    var entryStatusDean = data.isAccepted_dean;
                    $("[id^=evaluationButtons]").css('display', "block");

                    if (entryStatusDC == 2) {
                        $("[id^=UpdateFC4-points-]").css('background-color', '');
                        $("[id^=UpdateFC4-points-]").css('color', '');
                        $("[id^=UpdateFC4-points-]").attr('disabled', true);
                        $("[id^=evaluationButtons]").css('display', "none");

                        $("[id^=ribbon-rejected]").css('display', "block");
                        $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                    } else {
                        if (entryStatusDean == 1) {
                            $("[id^=UpdateFC4-points-]").css('background-color', '#FFEFD5');
                            $("[id^=UpdateFC4-points-]").css('color', 'black');
                            $("[id^=UpdateFC4-points-]").attr('disabled', false);
                            $('[id^=labelauty-][value="1"]').prop("checked", true);


                            $("[id^=ribbon-accept]").css('display', "block");
                            $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (CAPC)");
                        }

                        else if (entryStatusDean == 2) {
                            $("[id^=UpdateFC4-points-]").css('background-color', '');
                            $("[id^=UpdateFC4-points-]").css('color', '');
                            $("[id^=UpdateFC4-points-]").attr('disabled', true);
                            $('[id^=labelauty-][value="2"]').prop("checked", true);

                            $("[id^=ribbon-rejected]").css('display', "block");
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                        }

                        else if (entryStatusDean == null) {
                            $("[id^=UpdateFC4-points-]").css('background-color', '');
                            $("[id^=UpdateFC4-points-]").css('color', '');
                            $("[id^=UpdateFC4-points-]").attr('disabled', true);

                            $("[id^=ribbon-notyet]").css('display', "block");
                            $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (CAPC)");
                        }
                    }
                }

                // FORM IS CURRENTLY EVALUATED BY THE VCHANCELLOR
                else if (formStatus == 3 || formStatus == 4) {
                    var entryStatusDC = data.isAccepted_deptchair;
                    var entryStatusDean = data.isAccepted_dean;
                    var entryStatusVchancellor = data.isAccepted_vchancellor;

                    @if(Auth::user()->position==4)
                        $("[id^=evaluationButtons]").css('display', "none");
                    @else
                        $("[id^=evaluationButtons]").css('display', "block");
                    @endif


                    if (entryStatusDC == 2 || entryStatusDean == 2) {
                        $("[id^=UpdateFC4-points-]").css('background-color', '');
                        $("[id^=UpdateFC4-points-]").css('color', '');
                        $("[id^=UpdateFC4-points-]").attr('disabled', true);
                        $("[id^=evaluationButtons]").css('display', "none");

                        $("[id^=ribbon-rejected]").css('display', "block");

                        if (entryStatusDC == 2) {
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (DAPC)");
                        }

                        if (entryStatusDean == 2) {
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (CAPC)");
                        }
                    } else {
                        if (entryStatusVchancellor == 1) {

                            $('[id^=labelauty-][value="1"]').prop("checked", true);


                            $("[id^=ribbon-accept]").css('display', "block");

                            @if(Auth::user()->position==4)
                                $("[id^=ribbon-accept] .ribbon-inner").html("ENTRY ACCEPTED");
                            $("[id^=UpdateFC4-points-]").attr('disabled', true);
                            $("[id^=UpdateFC4-points-]").css('background-color', '');
                            $("[id^=UpdateFC4-points-]").css('color', '');
                            @else
                                $("[id^=ribbon-accept] .ribbon-inner").html("ACCEPTED (UAPFC)");
                            $("[id^=UpdateFC4-points-]").attr('disabled', false);
                            $("[id^=UpdateFC4-points-]").css('background-color', '#FFEFD5');
                            $("[id^=UpdateFC4-points-]").css('color', 'black');
                            @endif


                        }

                        else if (entryStatusVchancellor == 2) {
                            $("[id^=UpdateFC4-points-]").css('background-color', '');
                            $("[id^=UpdateFC4-points-]").css('color', '');
                            $("[id^=UpdateFC4-points-]").attr('disabled', true);
                            $('[id^=labelauty-][value="2"]').prop("checked", true);

                            $("[id^=ribbon-rejected]").css('display', "block");
                            $("[id^=ribbon-rejected] .ribbon-inner").html("REJECTED (UAPFC)");
                        }

                        else if (entryStatusVchancellor == null) {
                            $("[id^=UpdateFC4-points-]").css('background-color', '');
                            $("[id^=UpdateFC4-points-]").css('color', '');
                            $("[id^=UpdateFC4-points-]").attr('disabled', true);

                            $("[id^=ribbon-notyet]").css('display', "block");
                            $("[id^=ribbon-notyet] .ribbon-inner").html("NOT YET EVALUATED (UAPFC)");
                        }
                    }
                }
                @endif
                

                if (category == 1) {
                    $('#UpdateFC4-SubSubCat-1').val(subCategory);
                    if (subCategory == 1) {
                        $('#UpdateFC4-subcategory-1-1').val(data.subcategory);
                        $('#UpdateFC4-name_ofevent-1-1').val(data.name_ofevent);
                        $('#UpdateFC4-points-1-1').val(data.points);
                        $('#UpdateFC4-other_details-1-1').val(data.other_details);

                        $('#UpdateFC4-form-1-1').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-1').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-1').attr('disabled', true);
                            $('#c4radio3-1-1').attr('disabled', true);
                        }
                    } else if (subCategory == 2) {
                        $('#UpdateFC4-title-1-2').val(data.title);
                        $('#UpdateFC4-author-1-2').val(data.author);
                        $('#UpdateFC4-date_published-1-2').val(data.date_published);
                        $('#UpdateFC4-bibliography-1-2').val(data.bibliography);
                        $('#UpdateFC4-nature_ofpublication-1-2').val(data.nature_ofpublication);
                        $('#UpdateFC4-points-1-2').val(data.points);
                        $('#UpdateFC4-other_details-1-2').val(data.other_details);


                        $('#UpdateFC4-form-1-2').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-2').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-2').attr('disabled', true);
                            $('#c4radio3-1-2').attr('disabled', true);
                        }
                    } else if (subCategory == 3) {
                        $('#UpdateFC4-subcategory-1-3').val(data.subcategory);
                        $('#UpdateFC4-title-1-3').val(data.title);
                        $('#UpdateFC4-author-1-3').val(data.author);
                        $('#UpdateFC4-date_published-1-3').val(data.date_published);
                        $('#UpdateFC4-bibliography-1-3').val(data.bibliography);
                        $('#UpdateFC4-nature_ofpublication-1-3').val(data.nature_ofpublication);
                        $('#UpdateFC4-points-1-3').val(data.points);
                        $('#UpdateFC4-other_details-1-3').val(data.other_details);

                        $('#UpdateFC4-form-1-3').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-3').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-3').attr('disabled', true);
                            $('#c4radio3-1-3').attr('disabled', true);
                        }
                    } else if (subCategory == 4) {
                        $('#UpdateFC4-subcategory-1-4').val(data.subcategory);
                        $('#UpdateFC4-title-1-4').val(data.title);
                        $('#UpdateFC4-author-1-4').val(data.author);
                        $('#UpdateFC4-date_published-1-4').val(data.date_published);
                        $('#UpdateFC4-bibliography-1-4').val(data.bibliography);
                        $('#UpdateFC4-nature_ofpublication-1-4').val(data.nature_ofpublication);
                        $('#UpdateFC4-points-1-4').val(data.points);
                        $('#UpdateFC4-other_details-1-4').val(data.other_details);

                        $('#UpdateFC4-form-1-4').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-4').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-4').attr('disabled', true);
                            $('#c4radio3-1-4').attr('disabled', true);
                        }
                    } else if (subCategory == 5) {
                        $('#UpdateFC4-subcategory-1-5').val(data.subcategory);
                        $('#UpdateFC4-title-1-5').val(data.title);
                        $('#UpdateFC4-author-1-5').val(data.author);
                        $('#UpdateFC4-date_published-1-5').val(data.date_published);
                        $('#UpdateFC4-bibliography-1-5').val(data.bibliography);
                        $('#UpdateFC4-nature_ofpublication-1-5').val(data.nature_ofpublication);
                        $('#UpdateFC4-points-1-5').val(data.points);
                        $('#UpdateFC4-other_details-1-5').val(data.other_details);

                        $('#UpdateFC4-form-1-5').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-5').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-5').attr('disabled', true);
                            $('#c4radio3-1-5').attr('disabled', true);
                        }
                    } else if (subCategory == 6) {
                        $('#UpdateFC4-subcategory-1-6').val(data.subcategory);
                        $('#UpdateFC4-title-1-6').val(data.title);
                        $('#UpdateFC4-author-1-6').val(data.author);

                        $('#UpdateFC4-date_ofpresentation-1-6').val(data.date_ofpresentation);
                        $('#UpdateFC4-place_ofpresentation-1-6').val(data.place_ofpresentation);
                        $('#UpdateFC4-nature_ofpublication-1-6').val(data.nature_ofpublication);
                        $('#UpdateFC4-points-1-6').val(data.points);
                        $('#UpdateFC4-other_details-1-6').val(data.other_details);

                        $('#UpdateFC4-form-1-6').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-6').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-6').attr('disabled', true);
                            $('#c4radio3-1-6').attr('disabled', true);
                        }
                    } else if (subCategory == 7) {

                        $('#UpdateFC4-subcategory-1-7').val(data.subcategory);
                        $('#UpdateFC4-title-1-7').val(data.title);

                        $('#UpdateFC4-date_ofpresentation-1-7').val(data.date_ofpresentation);
                        $('#UpdateFC4-place_ofpresentation-1-7').val(data.place_ofpresentation);
                        $('#UpdateFC4-points-1-7').val(data.points);
                        $('#UpdateFC4-other_details-1-7').val(data.other_details);

                        $('#UpdateFC4-form-1-7').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-7').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-7').attr('disabled', true);
                            $('#c4radio3-1-7').attr('disabled', true);
                        }
                    } else if (subCategory == 8) {
                        $('#UpdateFC4-subcategory-1-8').val(data.subcategory);
                        $('#UpdateFC4-title-1-8').val(data.title);

                        $('#UpdateFC4-date_ofpresentation-1-8').val(data.date_ofpresentation);
                        $('#UpdateFC4-points-1-8').val(data.points);
                        $('#UpdateFC4-other_details-1-8').val(data.other_details);

                        $('#UpdateFC4-form-1-8').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-8').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-8').attr('disabled', true);
                            $('#c4radio3-1-8').attr('disabled', true);
                        }
                    } else if (subCategory == 9) {
                        $('#UpdateFC4-subcategory-1-9').val(data.subcategory);
                        $('#UpdateFC4-title-1-9').val(data.title);


                        $('#UpdateFC4-date_ofpresentation-1-9').val(data.date_ofpresentation);
                        $('#UpdateFC4-place_ofpresentation-1-9').val(data.place_ofpresentation);
                        $('#UpdateFC4-nature_ofpublication-1-9').val(data.nature_ofpublication);
                        $('#UpdateFC4-points-1-9').val(data.points);
                        $('#UpdateFC4-other_details-1-9').val(data.other_details);

                        $('#UpdateFC4-form-1-9').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-9').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-9').attr('disabled', true);
                            $('#c4radio3-1-9').attr('disabled', true);
                        }
                    } else if (subCategory == 10) {
                        $('#UpdateFC4-subcategory-1-10').val(data.subcategory);
                        $('#UpdateFC4-title-1-10').val(data.title);
                        $('#UpdateFC4-participation-1-10').val(data.participation);

                        $('#UpdateFC4-date_ofpresentation-1-10').val(data.date_ofpresentation);
                        $('#UpdateFC4-place_ofpresentation-1-10').val(data.place_ofpresentation);
                        $('#UpdateFC4-points-1-10').val(data.points);
                        $('#UpdateFC4-other_details-1-10').val(data.other_details);

                        $('#UpdateFC4-form-1-10').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-10').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-10').attr('disabled', true);
                            $('#c4radio3-1-10').attr('disabled', true);
                        }
                    } else if (subCategory == 11) {
                        $('#UpdateFC4-subcategory-1-11').val(data.subcategory);
                        $('#UpdateFC4-title-1-11').val(data.title);
                        $('#UpdateFC4-participation-1-11').val(data.participation);

                        $('#UpdateFC4-date_ofpresentation-1-11').val(data.date_ofpresentation);
                        $('#UpdateFC4-place_ofpresentation-1-11').val(data.place_ofpresentation);
                        $('#UpdateFC4-points-1-11').val(data.points);
                        $('#UpdateFC4-other_details-1-11').val(data.other_details);

                        $('#UpdateFC4-form-1-11').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-11').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-11').attr('disabled', true);
                            $('#c4radio3-1-11').attr('disabled', true);
                        }
                    } else if (subCategory == 12) {
                        $('#UpdateFC4-title-1-12').val(data.title);
                        $('#UpdateFC4-date_ofpresentation-1-12').val(data.date_ofpresentation);
                        $('#UpdateFC4-place_ofpresentation-1-12').val(data.place_ofpresentation);
                        $('#UpdateFC4-points-1-12').val(data.points);
                        $('#UpdateFC4-other_details-1-12').val(data.other_details);

                        $('#UpdateFC4-form-1-12').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-12').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-12').attr('disabled', true);
                            $('#c4radio3-1-12').attr('disabled', true);
                        }
                    } else if (subCategory == 13) {
                        $('#UpdateFC4-title-1-13').val(data.title);
                        $('#UpdateFC4-date_ofpresentation-1-13').val(data.date_ofpresentation);
                        $('#UpdateFC4-place_ofpresentation-1-13').val(data.place_ofpresentation);
                        $('#UpdateFC4-points-1-13').val(data.points);
                        $('#UpdateFC4-other_details-1-13').val(data.other_details);

                        $('#UpdateFC4-form-1-13').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-13').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-13').attr('disabled', true);
                            $('#c4radio3-1-13').attr('disabled', true);
                        }
                    } else if (subCategory == 14) {
                        $('#UpdateFC4-title-1-14').val(data.title);
                        $('#UpdateFC4-date_ofpresentation-1-14').val(data.date_ofpresentation);
                        $('#UpdateFC4-place_ofpresentation-1-14').val(data.place_ofpresentation);
                        $('#UpdateFC4-points-1-14').val(data.points);
                        $('#UpdateFC4-other_details-1-14').val(data.other_details);

                        $('#UpdateFC4-form-1-14').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-14').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-14').attr('disabled', true);
                            $('#c4radio3-1-14').attr('disabled', true);
                        }
                    } else if (subCategory == 15) {
                        $('#UpdateFC4-subcategory-1-15').val(data.subcategory);
                        $('#UpdateFC4-title-1-15').val(data.title);
                        $('#UpdateFC4-nature_ofpublication-1-15').val(data.nature_ofpublication);

                        $('#UpdateFC4-points-1-15').val(data.points);
                        $('#UpdateFC4-other_details-1-15').val(data.other_details);

                        $('#UpdateFC4-form-1-15').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-15').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-15').attr('disabled', true);
                            $('#c4radio3-1-15').attr('disabled', true);
                        }
                    } else if (subCategory == 16) {
                        $('#UpdateFC4-title-1-16').val(data.title);
                        $('#UpdateFC4-points-1-16').val(data.points);
                        $('#UpdateFC4-other_details-1-16').val(data.other_details);

                        $('#UpdateFC4-form-1-16').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-16').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-16').attr('disabled', true);
                            $('#c4radio3-1-16').attr('disabled', true);
                        }
                    } else if (subCategory == 17) {
                        $('#UpdateFC4-subcategory-1-17').val(data.subcategory);
                        $('#UpdateFC4-title-1-17').val(data.title);
                        $('#UpdateFC4-points-1-17').val(data.points);
                        $('#UpdateFC4-other_details-1-17').val(data.other_details);

                        $('#UpdateFC4-form-1-17').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-17').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-17').attr('disabled', true);
                            $('#c4radio3-1-17').attr('disabled', true);
                        }
                    } else if (subCategory == 18) {
                        $('#UpdateFC4-subcategory-1-18').val(data.subcategory);
                        $('#UpdateFC4-title-1-18').val(data.title);
                        $('#UpdateFC4-nature_ofpublication-1-18').val(data.nature_ofpublication);
                        $('#UpdateFC4-period_covered-1-18').val(data.period_covered);
                        $('#UpdateFC4-points-1-18').val(data.points);
                        $('#UpdateFC4-other_details-1-18').val(data.other_details);

                        $('#UpdateFC4-form-1-18').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-18').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-18').attr('disabled', true);
                            $('#c4radio3-1-18').attr('disabled', true);
                        }
                    } else if (subCategory == 19) {
                        $('#UpdateFC4-subcategory-1-19').val(data.subcategory);
                        $('#UpdateFC4-title-1-19').val(data.title);
                        $('#UpdateFC4-bibliography-1-19').val(data.bibliography);
                        $('#UpdateFC4-date_published-1-19').val(data.date_published);
                        $('#UpdateFC4-points-1-19').val(data.points);
                        $('#UpdateFC4-other_details-1-19').val(data.other_details);

                        $('#UpdateFC4-form-1-19').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-19').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-19').attr('disabled', true);
                            $('#c4radio3-1-19').attr('disabled', true);
                        }
                    } else if (subCategory == 20) {
                        $('#UpdateFC4-title-1-20').val(data.title);
                        $('#UpdateFC4-duration_ofappointment-1-20').val(data.duration_ofappointment);

                        $('#UpdateFC4-points-1-20').val(data.points);
                        $('#UpdateFC4-other_details-1-20').val(data.other_details);

                        $('#UpdateFC4-form-1-20').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-20').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-20').attr('disabled', true);
                            $('#c4radio3-1-20').attr('disabled', true);
                        }
                    } else if (subCategory == 21) {
                        $('#UpdateFC4-title-1-21').val(data.title);
                        $('#UpdateFC4-points-1-21').val(data.points);
                        $('#UpdateFC4-other_details-1-21').val(data.other_details);

                        $('#UpdateFC4-form-1-21').attr('action', submitButtonUrl);
                        $('#UpdateFC4-delete-1-21').attr('href', deleteButtonUrl);

                        if (data.attachment_filenames == null) {
                            $('#c4radio2-1-21').attr('disabled', true);
                            $('#c4radio3-1-21').attr('disabled', true);
                        }
                    }
                }
            });

        });
    });
</script>