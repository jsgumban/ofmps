<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Mpromotion;
use App\MpromotionForm;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;
use Validator;

class VChancellorController extends Controller
{
    public function CallForPromtion()
    {
        try {
            $id = DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
            if (!empty($id)) {
                $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.id=' . $id[0]->id);
                if (!empty($isThereCallForPromotion)) {
                    $startDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_start);
                    $endDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_end);
                    $currentDate = strtotime(date("Y-m-d"));
                    $mpromotion = Mpromotion::find($isThereCallForPromotion[0]->id);

                    if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                        // call for promotion status: on-going
                        $mpromotion->mpromotion_status = 0;
                        $mpromotion->save();
                    } else if ($currentDate > $endDate) {
                        // call for promotion status: finished
                        $mpromotion->mpromotion_status = 1;
                        $mpromotion->save();
                    } else if ($startDate > $currentDate) {
                        // call for promotion status: not yet started
                        $mpromotion->mpromotion_status = 2;
                        $mpromotion->save();
                    }
                }
            }
        } catch (Exception $e) {
            $isThereCallForPromotion = "";
        }
    }

    public function index()
    {
        $this->CallForPromtion();
        return view('pages.welcome');
    }

    public function ListOfSubmittedForms($id)
    {
        $this->CallForPromtion();
        $submittedForms = DB::select('SELECT
                    mpromotion_forms.id,
                    mpromotion_forms.user_id,
                    mpromotion_forms.form_status,
                    mpromotion_forms.lastPromotion_date,
                    mpromotion_forms.fillOut_date,
                    mpromotion_forms.form_name,
                    mpromotion_forms.submit_date_deptchair,
                    mpromotion_forms.callForPromotion_ID,
                    mpromotion_forms.callforpromotion_date_start,
                    mpromotion_forms.callforpromotion_date_end,
                    mpromotion_forms.faculty_total,
                    mpromotion_forms.initial_rank,
                    mpromotion_forms.initial_step,
                    users.first_name,
                    users.middle_name,
                    users.last_name,
                    users.department_belongs
                    FROM
                    mpromotion_forms
                    INNER JOIN users ON mpromotion_forms.user_id = users.id
                    INNER JOIN departments ON users.department_belongs = departments.department_name
                    WHERE
                    (mpromotion_forms.user_id = users.id AND
                    (mpromotion_forms.form_status >= 3 and mpromotion_forms.form_status != 5) AND
                    mpromotion_forms.callForPromotion_ID = \'' . $id . '\') 
                    ORDER BY mpromotion_forms.form_status ASC           
                         
        ');
        $callforpromotion = DB::select('SELECT
                            mpromotion.id,
                            mpromotion.mpromotion_status,
                            mpromotion.mpromotion_date_end,
                            mpromotion.mpromotion_date_start,
                            mpromotion.details
                            FROM
                            mpromotion
                            WHERE
                            mpromotion.id = \'' . $id . '\'
                            ');
        return view('vchancellor.UAPFC-ListOfSubmittedForms', compact('submittedForms', 'callforpromotion'));
    }

    public function UAPFCMonitorForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if (!empty($MpromotionForm)) {
            $user = User::find($MpromotionForm->user_id);
            $univerity = DB::select('SELECT * FROM universities WHERE universities.vchancellor = \'' . Auth::user()->id . '\'');

            if (($MpromotionForm->form_status > 3 && $MpromotionForm->form_status != 5)) {
                $this->facultyController = new FacultyController();
                return $this->facultyController->FillOutMeritPromotion($id);
            } else {
                return view('pages.error404');
            }
        } else {
            return view('pages.error404');
        }
    }

    public function EvaluateForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if (!empty($MpromotionForm)) {
            $user = User::find($MpromotionForm->user_id);
            $univerity = DB::select('SELECT * FROM universities WHERE universities.vchancellor = \'' . Auth::user()->id . '\'');

            if (($MpromotionForm->form_status == 3 && $MpromotionForm->form_status != 5)) {
                $this->facultyController = new FacultyController();
                return $this->facultyController->FillOutMeritPromotion($id);
            } else {
                return view('pages.error404');
            }
        } else {
            return view('pages.error404');
        }
    }

    public function SubmitChancellor($id, Request $request)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        $MpromotionForm->form_status = 4;
        $MpromotionForm->vchancellor_points = $MpromotionForm->faculty_total;
        $MpromotionForm->vchancellor_remarks = Input::get('vchancellor_remarks');

        $files = $request->file('file');
        $attachmentFileNames = "";
        $attachmentNamesSpit[] = "";
        if (!(empty($files[0]))) {
            foreach ($files as $file):
                $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                $file->move('uploads', $fileName);
                $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
            endforeach;
        }
        $MpromotionForm->vchancellor_remarks_attachments = $attachmentFileNames;

        if (Input::get('initial_rank') == "None" || empty(Input::get('initial_rank'))) {
            $MpromotionForm->initial_rank = null;
            $MpromotionForm->initial_step = null;
        } else {
            $MpromotionForm->initial_rank = Input::get('initial_rank');
            $MpromotionForm->initial_step = Input::get('initial_step');
        }


        $MpromotionForm->save();

        $notification = "SUCCESS : Form has been successfull submitted to the chancellor.";
        return redirect('/vchancellor/uapfc/' . Input::get('callForPromotion_ID') . '/ListOfSubmittedForms')->with('notification', $notification);
    }

    public function RejectUAPFC($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        $MpromotionForm->form_status = 3.5;
        $MpromotionForm->vchancellor_remarks = Input::get('vchancellor_remarks');
        $MpromotionForm->save();
        $notification = "SUCCESS : Form has been successfull rejected.";
        return redirect('/vchancellor/uapfc/' . Input::get('callForPromotion_ID') . '/ListOfSubmittedForms')->with('notification', $notification);
    }

    public function UAPFC()
    {
        $this->CallForPromtion();
        $this->CallForPromtion();
        $users = DB::select('SELECT * FROM users WHERE users.position!=5');
        $allFaculty = DB::select('SELECT *FROM users WHERE users.position <5');
        $departments = DB::select('SELECT * FROM departments order by department_name');
        $colleges = DB::select('SELECT * FROM colleges order by college_name');
        $chancellor = DB::select('SELECT users.id, users.first_name, users.middle_name, users.last_name FROM users , universities WHERE users.id = universities.chancellor');
        $vchancellor = DB::select('SELECT users.id, users.first_name, users.middle_name, users.last_name FROM users , universities WHERE users.id = universities.vchancellor');


        $mpromotionDates = DB::select('SELECT * FROM mpromotion ORDER BY mpromotion.mpromotion_status ASC');
        $count = 0;

        // number of forms submitted per mpromotion
        foreach ($mpromotionDates as $mpromotionDate) {
            $temp = DB::select('SELECT
                                *
                                FROM
                                mpromotion_forms
                                INNER JOIN users ON mpromotion_forms.user_id = users.id
                                INNER JOIN departments ON users.department_belongs = departments.department_name
                                WHERE
                                mpromotion_forms.callForPromotion_ID = \'' . $mpromotionDate->id . '\' AND 
                                (mpromotion_forms.form_status >= \'3\' AND mpromotion_forms.form_status < \'5\')
                                ');
            $forms[$count] = $temp;
            $count++;
        }

        return view('vchancellor.UAPFC', compact('users', 'departments', 'colleges', 'allFaculty', 'chancellor', 'vchancellor', 'mpromotionDates', 'forms'));
    }
}
