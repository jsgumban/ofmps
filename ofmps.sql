-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2016 at 06:10 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ofmps`
--

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE `colleges` (
  `id` int(11) NOT NULL,
  `college_name` varchar(255) DEFAULT NULL,
  `college_initial` varchar(255) DEFAULT NULL,
  `dean` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `creativework_attrib`
--

CREATE TABLE `creativework_attrib` (
  `id` int(11) NOT NULL,
  `creativework_tblID` int(11) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `subcategory` varchar(255) DEFAULT NULL,
  `subsubcategory` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `date_published` date DEFAULT NULL,
  `bibliography` varchar(255) DEFAULT NULL,
  `nature_ofpublication` varchar(255) DEFAULT NULL,
  `points` float(255,2) DEFAULT NULL,
  `attachment_filenames` varchar(1000) DEFAULT NULL,
  `other_details` varchar(1000) DEFAULT NULL,
  `date_ofpresentation` date DEFAULT NULL,
  `place_ofpresentation` varchar(255) DEFAULT NULL,
  `title_ofconference` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `isAccepted_deptchair` int(255) DEFAULT NULL,
  `isAccepted_dean` int(255) DEFAULT NULL,
  `isAccepted_vchancellor` int(255) DEFAULT NULL,
  `isAccepted_chancellor` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `creativework_tbl`
--

CREATE TABLE `creativework_tbl` (
  `id` int(11) NOT NULL,
  `mpromotion_formID` int(11) NOT NULL,
  `total` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(255) DEFAULT NULL,
  `department_initial` varchar(255) DEFAULT NULL,
  `deptchair` varchar(255) DEFAULT NULL,
  `college_belongs` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mpromotion`
--

CREATE TABLE `mpromotion` (
  `id` int(11) NOT NULL,
  `mpromotion_status` varchar(255) NOT NULL DEFAULT '0',
  `mpromotion_date_end` varchar(250) DEFAULT NULL,
  `mpromotion_date_start` varchar(250) DEFAULT NULL,
  `details` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpromotion_forms`
--

CREATE TABLE `mpromotion_forms` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `form_status` varchar(255) NOT NULL,
  `lastPromotion_date` date DEFAULT NULL,
  `fillOut_date` date NOT NULL,
  `form_name` varchar(255) DEFAULT '',
  `submit_date_deptchair` date DEFAULT NULL,
  `callForPromotion_ID` int(11) DEFAULT NULL,
  `callforpromotion_date_start` varchar(250) DEFAULT NULL,
  `callforpromotion_date_end` varchar(250) DEFAULT NULL,
  `faculty_total` varchar(255) DEFAULT NULL,
  `deptchair_remarks_attachments` varchar(500) DEFAULT NULL,
  `dean_remarks_attachments` varchar(500) DEFAULT NULL,
  `vchancellor_remarks_attachments` varchar(500) DEFAULT NULL,
  `chancellor_remarks_attachments` varchar(500) DEFAULT NULL,
  `deptchair_remarks` varchar(500) DEFAULT NULL,
  `dean_remarks` varchar(500) DEFAULT NULL,
  `vchancellor_remarks` varchar(500) DEFAULT NULL,
  `chancellor_remarks` varchar(500) DEFAULT NULL,
  `deptchair_points` varchar(255) DEFAULT NULL,
  `dean_points` varchar(255) DEFAULT NULL,
  `vchancellor_points` varchar(255) DEFAULT NULL,
  `chancellor_points` varchar(255) DEFAULT NULL,
  `initial_rank` varchar(255) DEFAULT NULL,
  `initial_step` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `performance_attrib`
--

CREATE TABLE `performance_attrib` (
  `id` int(11) NOT NULL,
  `performance_tblID` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `subcategory` varchar(255) DEFAULT NULL,
  `WARating` float(255,2) DEFAULT NULL,
  `points` float(255,2) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `date_published` date DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `course_title` varchar(255) DEFAULT NULL,
  `semester_used` varchar(255) DEFAULT NULL,
  `ay_used` varchar(255) DEFAULT NULL,
  `bibliography` varchar(255) DEFAULT NULL,
  `nature_ofpublication` varchar(255) DEFAULT NULL,
  `as_what` varchar(255) DEFAULT NULL,
  `year_graduated` int(255) DEFAULT NULL,
  `name_ofgraduate` varchar(255) DEFAULT NULL,
  `number_ofpreparations` varchar(255) DEFAULT NULL,
  `other_contributions` varchar(255) DEFAULT NULL,
  `attachment_filenames` varchar(500) DEFAULT NULL,
  `isAccepted_deptchair` int(255) DEFAULT NULL,
  `isAccepted_dean` int(255) DEFAULT NULL,
  `isAccepted_vchancellor` int(255) DEFAULT NULL,
  `isAccepted_chancellor` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `performance_tbl`
--

CREATE TABLE `performance_tbl` (
  `id` int(11) NOT NULL,
  `mpromotion_formID` int(11) NOT NULL,
  `total` float(255,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pgrowth_attrib`
--

CREATE TABLE `pgrowth_attrib` (
  `id` int(11) NOT NULL,
  `pgrowth_tblID` int(11) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `subcategory` varchar(255) DEFAULT NULL,
  `name_ofevent` varchar(255) DEFAULT NULL,
  `subsubcategory` varchar(255) DEFAULT NULL,
  `points` float(255,2) DEFAULT NULL,
  `other_details` varchar(255) DEFAULT NULL,
  `attachment_filenames` varchar(500) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `nature_ofpublication` varchar(255) DEFAULT NULL,
  `date_published` date DEFAULT NULL,
  `bibliography` varchar(255) DEFAULT NULL,
  `date_ofpresentation` date DEFAULT NULL,
  `place_ofpresentation` varchar(255) DEFAULT NULL,
  `participation` varchar(255) DEFAULT NULL,
  `period_covered` varchar(255) DEFAULT NULL,
  `duration_ofappointment` varchar(255) DEFAULT NULL,
  `isAccepted_deptchair` int(255) DEFAULT NULL,
  `isAccepted_dean` int(255) DEFAULT NULL,
  `isAccepted_vchancellor` int(255) DEFAULT NULL,
  `isAccepted_chancellor` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pgrowth_tbl`
--

CREATE TABLE `pgrowth_tbl` (
  `id` int(11) NOT NULL,
  `mpromotion_formID` int(11) NOT NULL,
  `total` float(255,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_attrib`
--

CREATE TABLE `service_attrib` (
  `id` int(11) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `subcategory` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `period_covered` varchar(255) DEFAULT NULL,
  `points` float(255,2) DEFAULT NULL,
  `service_tblID` int(11) DEFAULT NULL,
  `other_details` varchar(500) DEFAULT NULL,
  `attachment_filenames` varchar(500) DEFAULT NULL,
  `name_ofcommittee` varchar(255) DEFAULT NULL,
  `nature_ofcommittee` varchar(255) DEFAULT NULL,
  `funding_source` varchar(255) DEFAULT NULL,
  `purpose_offund` varchar(255) DEFAULT NULL,
  `ammount_donated` varchar(255) DEFAULT NULL,
  `designation_ofoic` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `subsubcategory` varchar(255) DEFAULT NULL,
  `name_oforganization` varchar(255) DEFAULT NULL,
  `isAccepted_deptchair` int(255) DEFAULT NULL,
  `isAccepted_dean` int(255) DEFAULT NULL,
  `isAccepted_vchancellor` int(255) DEFAULT NULL,
  `isAccepted_chancellor` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_tbl`
--

CREATE TABLE `service_tbl` (
  `id` int(11) NOT NULL,
  `mpromotion_formID` int(11) NOT NULL,
  `total` float(255,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` int(11) NOT NULL,
  `university_name` varchar(255) NOT NULL,
  `chancellor` int(10) UNSIGNED DEFAULT NULL,
  `vchancellor` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `university_name`, `chancellor`, `vchancellor`) VALUES
(1, 'University of the Philippines', 7, 6);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_belongs` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `employee_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `rank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `step` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(255) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `delete` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_promotion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `department_belongs`, `employee_code`, `password`, `first_name`, `middle_name`, `last_name`, `birthday`, `rank`, `step`, `position`, `address`, `remember_token`, `created_at`, `updated_at`, `delete`, `gender`, `last_promotion`) VALUES
(1, NULL, '999999999', '$2y$10$aM9hV1LAEEWg0c8pQkrCs.HjSRRD8XwPyiV/fIgJHGeTw1C6/PIHy', 'John Hel', 'Sorolla', 'Gumban', '1995-10-13', 'None', NULL, 5, 'Davao City', 'KttyM431NoIEUnkHGEUrqre3fYLenwK3HWJCILIcDMSc5YaKR5e1qQD95EDF', '2015-12-20 00:41:14', '2016-04-29 07:41:35', '0', 'Male', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `college_name` (`college_name`);

--
-- Indexes for table `creativework_attrib`
--
ALTER TABLE `creativework_attrib`
  ADD PRIMARY KEY (`id`,`creativework_tblID`),
  ADD KEY `creativework_tblID` (`creativework_tblID`);

--
-- Indexes for table `creativework_tbl`
--
ALTER TABLE `creativework_tbl`
  ADD PRIMARY KEY (`id`,`mpromotion_formID`),
  ADD KEY `mpromotion_formID2` (`mpromotion_formID`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_name` (`department_name`),
  ADD KEY `college_belongs` (`college_belongs`);

--
-- Indexes for table `mpromotion`
--
ALTER TABLE `mpromotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mpromotion_date_end` (`mpromotion_date_end`),
  ADD KEY `mpromotion_date_start` (`mpromotion_date_start`);

--
-- Indexes for table `mpromotion_forms`
--
ALTER TABLE `mpromotion_forms`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `callForPromotion_ID` (`callForPromotion_ID`),
  ADD KEY `callforpromotion_date_start` (`callforpromotion_date_start`),
  ADD KEY `callforpromotion_date_end` (`callforpromotion_date_end`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `performance_attrib`
--
ALTER TABLE `performance_attrib`
  ADD PRIMARY KEY (`id`),
  ADD KEY `performance_tblID` (`performance_tblID`);

--
-- Indexes for table `performance_tbl`
--
ALTER TABLE `performance_tbl`
  ADD PRIMARY KEY (`id`,`mpromotion_formID`),
  ADD KEY `mpromotion_formID` (`mpromotion_formID`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `pgrowth_attrib`
--
ALTER TABLE `pgrowth_attrib`
  ADD PRIMARY KEY (`id`,`pgrowth_tblID`),
  ADD KEY `pgrowth_tblID` (`pgrowth_tblID`);

--
-- Indexes for table `pgrowth_tbl`
--
ALTER TABLE `pgrowth_tbl`
  ADD PRIMARY KEY (`id`,`mpromotion_formID`),
  ADD KEY `mpromotion_formID4` (`mpromotion_formID`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `service_attrib`
--
ALTER TABLE `service_attrib`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_tblID` (`service_tblID`);

--
-- Indexes for table `service_tbl`
--
ALTER TABLE `service_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mpromotion_formID3` (`mpromotion_formID`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`employee_code`),
  ADD KEY `department_belongs` (`department_belongs`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `creativework_attrib`
--
ALTER TABLE `creativework_attrib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `creativework_tbl`
--
ALTER TABLE `creativework_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mpromotion`
--
ALTER TABLE `mpromotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mpromotion_forms`
--
ALTER TABLE `mpromotion_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `performance_attrib`
--
ALTER TABLE `performance_attrib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `performance_tbl`
--
ALTER TABLE `performance_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pgrowth_attrib`
--
ALTER TABLE `pgrowth_attrib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pgrowth_tbl`
--
ALTER TABLE `pgrowth_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `service_attrib`
--
ALTER TABLE `service_attrib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `service_tbl`
--
ALTER TABLE `service_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `creativework_attrib`
--
ALTER TABLE `creativework_attrib`
  ADD CONSTRAINT `creativework_tblID` FOREIGN KEY (`creativework_tblID`) REFERENCES `creativework_tbl` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `creativework_tbl`
--
ALTER TABLE `creativework_tbl`
  ADD CONSTRAINT `mpromotion_formID2` FOREIGN KEY (`mpromotion_formID`) REFERENCES `mpromotion_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `college_belongs` FOREIGN KEY (`college_belongs`) REFERENCES `colleges` (`college_name`) ON UPDATE CASCADE;

--
-- Constraints for table `mpromotion_forms`
--
ALTER TABLE `mpromotion_forms`
  ADD CONSTRAINT `callForPromotion_ID` FOREIGN KEY (`callForPromotion_ID`) REFERENCES `mpromotion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `callforpromotion_date_end` FOREIGN KEY (`callforpromotion_date_end`) REFERENCES `mpromotion` (`mpromotion_date_end`) ON UPDATE CASCADE,
  ADD CONSTRAINT `callforpromotion_date_start` FOREIGN KEY (`callforpromotion_date_start`) REFERENCES `mpromotion` (`mpromotion_date_start`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `performance_attrib`
--
ALTER TABLE `performance_attrib`
  ADD CONSTRAINT `performance_tblID` FOREIGN KEY (`performance_tblID`) REFERENCES `performance_tbl` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `performance_tbl`
--
ALTER TABLE `performance_tbl`
  ADD CONSTRAINT `mpromotion_formID` FOREIGN KEY (`mpromotion_formID`) REFERENCES `mpromotion_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pgrowth_attrib`
--
ALTER TABLE `pgrowth_attrib`
  ADD CONSTRAINT `pgrowth_tblID` FOREIGN KEY (`pgrowth_tblID`) REFERENCES `pgrowth_tbl` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pgrowth_tbl`
--
ALTER TABLE `pgrowth_tbl`
  ADD CONSTRAINT `mpromotion_formID4` FOREIGN KEY (`mpromotion_formID`) REFERENCES `mpromotion_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_attrib`
--
ALTER TABLE `service_attrib`
  ADD CONSTRAINT `service_tblID` FOREIGN KEY (`service_tblID`) REFERENCES `service_tbl` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_tbl`
--
ALTER TABLE `service_tbl`
  ADD CONSTRAINT `mpromotion_formID3` FOREIGN KEY (`mpromotion_formID`) REFERENCES `mpromotion_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `department_belongs` FOREIGN KEY (`department_belongs`) REFERENCES `departments` (`department_name`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
