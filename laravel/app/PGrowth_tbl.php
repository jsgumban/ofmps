<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PGrowth_tbl extends Model
{
    protected $table = 'pgrowth_tbl';
    public $timestamps = false;

}
