<!-- resources/views/auth/register.blade.php -->

<form method="POST" action="/auth/register">
    {!! csrf_field() !!}
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <span class="label label-danger">{!! $error !!}</span>
        @endforeach
    @endif

    <div>
        Name
        <input type="text" name="name" value="{{ old('name') }}">
    </div>

    <div>
        Employee Code
        <input type="text" name="employee_code" value="{{ old('employee_code') }}">
    </div>

    <div>
        User Type
        <input type="text" name="position" value="{{ old('position') }}">
    </div>

    <div>
        Password
        <input type="password" name="password">
    </div>

    <div>
        Confirm Password
        <input type="password" name="password_confirmation">
    </div>

    <div>
        <button type="submit">Register</button>
    </div>
</form>