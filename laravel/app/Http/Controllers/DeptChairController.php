<?php

namespace App\Http\Controllers;

use App\Department;
use App\deptchair;
use App\Http\Requests;
use App\Mpromotion;
use App\MpromotionForm;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;
use Validator;

class DeptChairController extends Controller
{
    public function CallForPromtion()
    {
        try {
            $id = DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
            if (!empty($id)) {
                $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.id=' . $id[0]->id);
                if (!empty($isThereCallForPromotion)) {
                    $startDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_start);
                    $endDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_end);
                    $currentDate = strtotime(date("Y-m-d"));
                    $mpromotion = Mpromotion::find($isThereCallForPromotion[0]->id);

                    if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                        // call for promotion status: on-going
                        $mpromotion->mpromotion_status = 0;
                        $mpromotion->save();
                    } else if ($currentDate > $endDate) {
                        // call for promotion status: finished
                        $mpromotion->mpromotion_status = 1;
                        $mpromotion->save();
                    } else if ($startDate > $currentDate) {
                        // call for promotion status: not yet started
                        $mpromotion->mpromotion_status = 2;
                        $mpromotion->save();
                    }
                }
            }
        } catch (Exception $e) {
            $isThereCallForPromotion = "";
        }
    }

    public function index()
    {
        $this->CallForPromtion();
        return view('pages.welcome');
    }

    public function RetrieveUserData($id, Request $request)
    {
        $this->CallForPromtion();
        $user = DB::select('SELECT * FROM users WHERE users.id = \'' . $id . '\'');
        if ($request->ajax()) {
            return $user;
        }
    }

    public function ListOfSubmittedForms($id)
    {
        $this->CallForPromtion();
        $department = DB::select('SELECT
                                    departments.id, departments.department_name, departments.deptchair
                                    FROM
                                    departments
                                    INNER JOIN users ON users.department_belongs = departments.department_name
                                    WHERE
                                    departments.deptchair = \'' . Auth::user()->id . '\'');
        $departmentInfo = Department::find($department[0]->id);
        $submittedForms = DB::select('SELECT
                    mpromotion_forms.id,
                    mpromotion_forms.user_id,
                    mpromotion_forms.form_status,
                    mpromotion_forms.lastPromotion_date,
                    mpromotion_forms.fillOut_date,
                    mpromotion_forms.form_name,
                    mpromotion_forms.submit_date_deptchair,
                    mpromotion_forms.callForPromotion_ID,
                    mpromotion_forms.callforpromotion_date_start,
                    mpromotion_forms.callforpromotion_date_end,
                    mpromotion_forms.faculty_total,
                    mpromotion_forms.initial_rank,
                    mpromotion_forms.initial_step,
                    users.first_name,
                    users.middle_name,
                    users.last_name
                    FROM
                    mpromotion_forms
                    INNER JOIN users ON mpromotion_forms.user_id = users.id
                    WHERE
                    mpromotion_forms.user_id = users.id AND
                    (mpromotion_forms.form_status >= 1 and mpromotion_forms.form_status != 5) AND
                    mpromotion_forms.callForPromotion_ID = \'' . $id . '\' AND
                    users.department_belongs = \'' . $departmentInfo->department_name . '\'    
                    ORDER BY mpromotion_forms.form_status ASC                   
        ');
        $callforpromotion = DB::select('SELECT
                            mpromotion.id,
                            mpromotion.mpromotion_status,
                            mpromotion.mpromotion_date_end,
                            mpromotion.mpromotion_date_start,
                            mpromotion.details
                            FROM
                            mpromotion
                            WHERE
                            mpromotion.id = \'' . $id . '\'
                            ');
        return view('deptchair.DAPC-ListOfSubmittedForms', compact('submittedForms', 'department', 'callforpromotion'));
    }

    public function EvaluateForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if (!empty($MpromotionForm)) {
            $user = User::find($MpromotionForm->user_id);
            if ($user->department_belongs == Auth::user()->department_belongs && $MpromotionForm->form_status == 1) {
                $this->facultyController = new FacultyController();
                return $this->facultyController->FillOutMeritPromotion($id);
            } else {
                return view('pages.error404');
            }
        } else {
            return view('pages.error404');
        }
    }

    public function RejectDAPC($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        $MpromotionForm->form_status = 1.5;
        $MpromotionForm->deptchair_remarks = Input::get('deptchair_remarks');
        $MpromotionForm->save();
        $notification = "SUCCESS : Form has been successfull rejected.";
        return redirect('/deptchair/dapc/' . Input::get('callForPromotion_ID') . '/ListOfSubmittedForms')->with('notification', $notification);
    }

    public function NeedsRevision($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        $MpromotionForm->form_status = 5;
        $MpromotionForm->deptchair_remarks = Input::get('deptchair_remarks');
        $MpromotionForm->save();
        $notification = "SUCCESS : Form has been successfull submitted back to the faculty member for revisions.";
        return redirect('/deptchair/dapc/' . Input::get('callForPromotion_ID') . '/ListOfSubmittedForms')->with('notification', $notification);
    }

    public function SubmitDean($id, Request $request)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        $MpromotionForm->form_status = 2;
        $MpromotionForm->deptchair_points = $MpromotionForm->faculty_total;
        $MpromotionForm->deptchair_remarks = Input::get('deptchair_remarks');

        $files = $request->file('file');
        $attachmentFileNames = "";
        $attachmentNamesSpit[] = "";
        if (!(empty($files[0]))) {
            foreach ($files as $file):
                $fileExtension = '.' . (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
                $fileName = (md5($file->getClientOriginalName() . microtime())) . $fileExtension;
                $file->move('uploads', $fileName);
                $attachmentFileNames = $fileName . "+++" . $attachmentFileNames;
            endforeach;
        }

        $MpromotionForm->deptchair_remarks_attachments = $attachmentFileNames;

        $MpromotionForm->save();
        $notification = "SUCCESS : Form has been successfull submitted to the college dean.";
        return redirect('/deptchair/dapc/' . Input::get('callForPromotion_ID') . '/ListOfSubmittedForms')->with('notification', $notification);
    }

    public function DAPCMonitorForm($id)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if (!empty($MpromotionForm)) {
            $user = User::find($MpromotionForm->user_id);
            if ($user->department_belongs == Auth::user()->department_belongs && ($MpromotionForm->form_status > 1 && $MpromotionForm->form_status != 5)) {
                $this->facultyController = new FacultyController();
                return $this->facultyController->FillOutMeritPromotion($id);
            } else {
                return view('pages.error404');
            }
        } else {
            return view('pages.error404');
        }
    }

    public function RetrieveFormInfo($id, Request $request)
    {
        $this->CallForPromtion();
        $MpromotionForm = MpromotionForm::find($id);
        if ($request->ajax()) {
            return $MpromotionForm;
        }
    }

    public function RetrieveUserInfo($id, Request $request)
    {
        $this->CallForPromtion();
        $MpromotionForm = DB::select('SELECT
            mpromotion_forms.id,
            mpromotion_forms.user_id,
            mpromotion_forms.form_status,
            mpromotion_forms.lastPromotion_date,
            mpromotion_forms.fillOut_date,
            mpromotion_forms.form_name,
            mpromotion_forms.submit_date_deptchair,
            mpromotion_forms.callForPromotion_ID,
            mpromotion_forms.callforpromotion_date_start,
            mpromotion_forms.callforpromotion_date_end,
            mpromotion_forms.faculty_total,
            mpromotion_forms.deptchair_remarks_attachments,
            mpromotion_forms.dean_remarks_attachments,
            mpromotion_forms.vchancellor_remarks_attachments,
            mpromotion_forms.deptchair_remarks,
            mpromotion_forms.dean_remarks,
            mpromotion_forms.vchancellor_remarks,
            mpromotion_forms.chancellor_remarks,
            mpromotion_forms.deptchair_points,
            mpromotion_forms.dean_points,
            mpromotion_forms.vchancellor_points,
            users.step,
            users.rank,
            users.position,
            users.first_name,
            users.middle_name,
            users.last_name,
            mpromotion_forms.initial_rank,
            mpromotion_forms.initial_step,
            users.last_promotion
            FROM
            mpromotion_forms
            INNER JOIN users ON mpromotion_forms.user_id = users.id
            WHERE
            mpromotion_forms.id = \'' . $id . '\'
        ');

        if ($request->ajax()) {
            return $MpromotionForm;
        }
    }

    public function RetrieveSubmittedForms($id, Request $request)
    {
        $this->CallForPromtion();
        $SubmittedForms = DB::select('SELECT mpromotion_forms.id FROM mpromotion_forms INNER JOIN users ON mpromotion_forms.user_id = users.id WHERE mpromotion_forms.user_id = users.id AND users.id = \'' . $id . '\'
            AND mpromotion_forms.form_status > \'' . Auth::user()->position . '\' AND mpromotion_forms.form_status!=\'5\'
            ');
        if ($request->ajax()) {
            return $SubmittedForms;
        }
    }

    public function DAPC()
    {
        $this->CallForPromtion();
        $departmentID = DB::select('SELECT
                                    departments.id, departments.department_name, departments.deptchair
                                    FROM
                                    departments
                                    INNER JOIN users ON users.department_belongs = departments.department_name
                                    WHERE
                                    departments.deptchair = \'' . Auth::user()->id . '\'
        ');
        $departmentInfo = Department::find($departmentID[0]->id);
        $users = DB::select('SELECT users.id,
                                    users.department_belongs,
                                    users.employee_code,
                                    users.first_name,
                                    users.middle_name,
                                    users.last_name,
                                    users.birthday,
                                    users.rank,
                                    users.step,
                                    users.position,
                                    users.address,
                                    users.remember_token,
                                    users.created_at,
                                    users.updated_at
                                    FROM users as users INNER JOIN departments ON users.department_belongs = departments.department_name WHERE 
                                    users.department_belongs = \'' . $departmentID[0]->department_name . '\'');
        $dc = DB::select('SELECT * FROM users INNER JOIN departments ON users.department_belongs = departments.department_name
            WHERE users.id = \'' . $departmentID[0]->deptchair . '\'');
        $departments = DB::select('SELECT * FROM departments order by department_name');

        $department = DB::select('SELECT
                                    departments.id, departments.department_name, departments.deptchair
                                    FROM
                                    departments
                                    INNER JOIN users ON users.department_belongs = departments.department_name
                                    WHERE
                                    departments.deptchair = \'' . Auth::user()->id . '\'
                                    ');
        $mpromotionDates = DB::select('SELECT * FROM mpromotion ORDER BY mpromotion.mpromotion_status ASC');
        $forms = array();
        $count = 0;
        foreach ($mpromotionDates as $mpromotionDate) {
            $temp = DB::select('SELECT
                                *
                                FROM
                                mpromotion_forms
                                INNER JOIN users ON mpromotion_forms.user_id = users.id
                                WHERE
                                mpromotion_forms.callForPromotion_ID = \'' . $mpromotionDate->id . '\' AND 
                                (mpromotion_forms.form_status >= \'1\' AND mpromotion_forms.form_status != \'5\') AND
                                users.department_belongs = \'' . $department[0]->department_name . '\'
                                ');
            $forms[$count] = $temp;
            $count++;
        }
        return view('deptchair.DAPC', compact('departmentInfo', 'users', 'dc', 'departments', 'mpromotionDates', 'forms', 'department'));
    }
}


