<?php
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mpromotion;

include 'folder.php';
include 'Routes/AdminRoutes.php';
include 'Routes/FacultyRoutes.php';
include 'Routes/DeptChairRoutes.php';
include 'Routes/DeanRoutes.php';
include 'Routes/VChancellorRoutes.php';
include 'Routes/ChancellorRoutes.php';

// VIEW HOMEPAGE
Route::get('/', ['middleware' => 'auth', function () {
    try{
        $id=DB::select('SELECT mpromotion.id FROM mpromotion WHERE mpromotion.mpromotion_status = 0 OR mpromotion.mpromotion_status = 2');
        if(!empty($id)){
            $isThereCallForPromotion = DB::select('SELECT * FROM mpromotion WHERE mpromotion.id='.$id[0]->id);
            if(!empty($isThereCallForPromotion)){
                $startDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_start);
                $endDate = strtotime($isThereCallForPromotion[0]->mpromotion_date_end);
                $currentDate = strtotime(date("Y-m-d"));
                $mpromotion = Mpromotion::find($isThereCallForPromotion[0]->id);
            
                if (($currentDate >= $startDate) && ($currentDate <= $endDate)){
                    // call for promotion status: on-going
                    $mpromotion->mpromotion_status=0;
                    $mpromotion->save();
                }else if($currentDate>$endDate){
                    // call for promotion status: finished
                    $mpromotion->mpromotion_status=1;
                    $mpromotion->save();
                }else if($startDate>$currentDate){
                    // call for promotion status: not yet started
                    $mpromotion->mpromotion_status=2;
                    $mpromotion->save();
                }
            }
        }
        return view('pages.welcome', compact('isThereCallForPromotion'));
    }catch (Exception $e) {
        $isThereCallForPromotion = "";
        return view('pages.welcome', compact('isThereCallForPromotion'));
    }
    
    
    
}]);

Route::get('home', ['middleware' => 'auth', function () {
    return redirect('/');
}]);

Route::get('sample', function () {
    return view('sample');
});


// AUTHENITCATION ROUTES
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// REGISTRATION ROUTES
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');































// Route::get('/', function () {
//     return view('pages.welcome');
// });

// Route::get('login', function () {
//     return view('pages.login');
// });

// Route::get('blank', function () {
//     return view('pages.blank');
// });

// Route::get('home', function(){
// 	if(Auth::guest()){
// 		return Redirect::to('auth/login');
// 	}else{
// 		echo 'Welcome Home!';

// 	}
// });

// Route::get('home', ['middleware' => 'auth', function(){
// 	echo "Welcome Home!" . Auth::user()->name;
// }]);
// 
// 
// // Route::get('home', function(){
// 	if(Auth::user()->user_type==0){
// 		echo "Welcome to your faculty page!";
// 	}
// });

// Route::get('admin/profile/{id}', ['uses' =>'FacultyController@viewAdminProfile']);
//Route::resource('adminprofile', 'FacultyController');
// Route::resource('admin/profile', 'FacultyController');

//  General Users Routes
// Route::get('password',  function(){
//     return view('index');
// });

// //  SAVE CHANGE PASSWORD
// Route::post('admin/password/savepassword', function(){
//    		// return Input::get('password');
//    		// echo "string";
// $newpassword = Input::get('currentpass');
// $oldpassword = User::find(Auth::user()->id)->password;

// // var_dump($oldpassword);
// if(Hash::check($newpassword, $oldpassword)){
// 	// var_dump('paswword matches');
// 	$user = User::find(Auth::user()->id);
// 	$user->password = Hash::make(Input::get('newpass'));
// 	$user->save();
// 	return "update password";
// }else{
// 	echo "No";
// }
// 	}
// );